﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay
{
    public class TimerBasedGamePlay : BaseGamePlay
    {
        public Text timerText;
        private Coroutine coroutine;

        public override void OnEnable()
        {
            GameManager.Instance.OnGameOver += StopCO;
            base.OnEnable();
        }
       
        public override void OnDisable()
        {
            GameManager.Instance.OnGameOver += StopCO;
            base.OnDisable();
            StopCO();
        }

        public override void Init()
        {
            base.Init();
            StartCorutine();
        }

        public override void ResetScore()
        {
            base.ResetScore();
            timerText.text = "00 : 00";
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
            StartCorutine();
        }

        public IEnumerator TimerCo()
        {
            int time = GameManager.Instance.timerDataSO.time;
            while (time >= 0)
            {
                int min = TimeSpan.FromSeconds(time).Minutes;
                int sec = TimeSpan.FromSeconds(time).Seconds;

                timerText.text = min + " : " + sec;

                yield return new WaitForSeconds(1);
                time--;
            }
            OnTimerOver();
        }

        private void OnTimerOver()
        {
            coroutine = null;
            GameManager.Instance.isGameOver = true;
            StopCO();
            UiManager.Instance.GetGameOverViewByType(gamePlayType).gameObject.SetActive(true);
            gameObject.SetActive(false);
            PolyominoHolder.Instance.SetLastPolyominoInSlot();
        }

        private void StartCorutine()
        {
            coroutine = StartCoroutine(TimerCo());
        }
        public void StopCO()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }
    }
}
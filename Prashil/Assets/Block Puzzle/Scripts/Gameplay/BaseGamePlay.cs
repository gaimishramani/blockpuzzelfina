﻿using GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseGamePlay : MonoBehaviour
{
    #region PUBLIC_VARS
    public GamePlayType gamePlayType;

    public Text currentScoreText;
    public Text highScoreText;
    public bool isSettingClicked = false;
    public GameObject settingButtonPanel;
    public Animator settingButtonPanelAnim;
    public GameObject SoundOnObject;
    public GameObject SoundOffObject;

    #endregion

    #region PRIVATE_VARS
    public int currentScore;
    public int highScore;
    #endregion

    #region UNITY_CALLBACKS
    public virtual void OnEnable()
    {
        Init();
    }
    public virtual void OnDisable()
    {
        SaveScore();
    }
    #endregion

    #region PUBLIC_FUNCTIONS
    public virtual void Init()
    {
        currentScore = PlayerPrefs.GetInt(Constants.CURRENT_SCORE, 0);
        highScore = PlayerPrefs.GetInt(Constants.HIGH_SCORE, 0);
        DisplayScore();
        SetSoundImage();
    }
    public void UpdateScore(int score)
    {
        int tempScore = currentScore;
        currentScore += score;

        if (currentScore >= highScore)
            highScore = currentScore;
        //DisplayScore();

        StartCoroutine(ScoreAddAnimationCo(score, tempScore));
    }
    public virtual void ResetScore()
    {
        currentScore = 0;
        DisplayScore();
    }
    public void SaveScore()
    {
        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, currentScore);
        PlayerPrefs.SetInt(Constants.HIGH_SCORE, highScore);
    }
    #endregion

    #region PRIVATE_FUNCTIONS
    private void DisplayScore()
    {
        currentScoreText.text =  currentScore.ToString();
        highScoreText.text =   highScore.ToString();
    }
    private void SetSoundImage()
    {
        if (SoundManager.Instance.Sound)
        {
            SoundOnObject.SetActive(true);
            SoundOffObject.SetActive(false);
        }
        else
        {
            SoundOffObject.SetActive(true);
            SoundOnObject.SetActive(false);
        }
    }
    #endregion

    #region CO-ROUTINES

    IEnumerator ScoreAddAnimationCo(int score, int tempScore)
    {
        for (int i = 1; i <= score; i++)
        {
            currentScoreText.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            tempScore = tempScore + 1;
            currentScoreText.text = tempScore.ToString();

            yield return new WaitForSeconds(0.02f);
            currentScoreText.gameObject.transform.localScale = Vector3.one;

        }

        highScoreText.text =  highScore.ToString();

        yield return 0;
    }
    #endregion

    #region EVENT_HANDLERS
    #endregion

    #region UI_CALLBACKS
    public void OnSettingClick()
    {
        if (!isSettingClicked)
        {
            isSettingClicked = true;
            settingButtonPanel.SetActive(true);
            settingButtonPanelAnim.Play("InAnimation");
            SoundManager.Instance.PlayButtonClickSFX();
        }
        else
        {
            Debug.LogError("panel close");
            isSettingClicked = false;
            settingButtonPanelAnim.Play("OutAnimation");
            StartCoroutine(SettingPannelSetActive());
        }
    }

    public void OnDummyButtonClick()
    {
        settingButtonPanelAnim.Play("OutAnimation");
        isSettingClicked = false;
        StartCoroutine(SettingPannelSetActive());

    }

    public IEnumerator SettingPannelSetActive()
    {
        yield return new WaitForSeconds(0.35f);
        settingButtonPanel.SetActive(false);
    }
    public void OnToggleSoundClick()
    {
        SoundManager.Instance.ToggleSound();
        SetSoundImage();
    }
    public void HomeClick()
    {
        isSettingClicked = false;
        settingButtonPanel.SetActive(false);
        SoundManager.Instance.PlayButtonClickSFX();
        gameObject.SetActive(false);
        GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
        GameManager.Instance.grid.gameObject.SetActive(false);
        GameManager.Instance.grid.ResetAllCells();
        PolyominoHolder.Instance.ClearAllPolyomino();
        UiManager.Instance.gameMainHomeView.ShowView();

        // GridAndHolderData.Instance.SetGameData();
    }
    public virtual void OnRestartClick()
    {
        GameManager.Instance.RestartGame();
        SoundManager.Instance.PlayButtonClickSFX();

        OnDummyButtonClick();
    }

    public void OnBackBttonClick()
    {
        SoundManager.Instance.PlayButtonClickSFX();
        gameObject.SetActive(false);
        GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
        GameManager.Instance.grid.gameObject.SetActive(false);
        GameManager.Instance.grid.ResetAllCells();
        PolyominoHolder.Instance.ClearAllPolyomino();
        Debug.LogError(GameManager.Instance.currentGamePlay.gamePlayType);
        UiManager.Instance.GetHomeViewByType(GameManager.Instance.currentGamePlay.gamePlayType).gameObject.SetActive(true);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class Block : BaseBlock
    {
        #region PUBLIC_VARS
        public SpriteTypes colorType;
        public SpriteRenderer temporarySpriteRenderer;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
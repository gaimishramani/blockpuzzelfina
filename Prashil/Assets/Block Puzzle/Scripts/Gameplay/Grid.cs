﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

namespace GamePlay
{
    public class Grid : MonoBehaviour
    {
        #region PUBLIC_VARS
        public List<Cell> cells;
        public Cell[,] cells2dArray = new Cell[10, 10];
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Start()
        {

        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void SetPolyominoInCell(SimplePolyomino polyomino)
        {
            int rowIndex = polyomino.dropCell.rowIndex, colIndex = polyomino.dropCell.colIndex;
            foreach (PolyominoBlock polyominoBlocks in polyomino.polyominoBlocks)
                cells2dArray[rowIndex + polyominoBlocks.rowIndex, colIndex + polyominoBlocks.colIndex].SetBlock(polyomino.colorType);
            RuleManager.Instance.CheckDropRule(polyomino);
        }

        public void PlayHintPS(SimplePolyomino polyomino)
        {
            if (polyomino.placeableDropCell == null)
            {
                return;
            }
            int rowIndex = polyomino.placeableDropCell.rowIndex, colIndex = polyomino.placeableDropCell.colIndex;
            Grid grid = GameManager.Instance.grid;
            foreach (PolyominoBlock polyominoBlocks in polyomino.polyominoBlocks)
            {
                grid.cells2dArray[rowIndex + polyominoBlocks.rowIndex, colIndex + polyominoBlocks.colIndex].PlayHintFX();
            }
        }
        public void StopHintPS(SimplePolyomino polyomino)
        {
            if (polyomino.placeableDropCell == null)
            {
                return;
            }
            int rowIndex = polyomino.placeableDropCell.rowIndex, colIndex = polyomino.placeableDropCell.colIndex;
            Grid grid = GameManager.Instance.grid;
            foreach (PolyominoBlock polyominoBlocks in polyomino.polyominoBlocks)
            {
                grid.cells2dArray[rowIndex + polyominoBlocks.rowIndex, colIndex + polyominoBlocks.colIndex].StopHintFX();
            }
        }
        public bool IsItRowFull(int rowIndex)
        {
            for (int i = 0; i < Constants.RAW_SIZE; i++)
            {
                //cells.Add(cells2dArray[rowIndex, i]);
                if (cells2dArray[rowIndex, i].IsEmpty())
                {
                    return false;
                }
            }
            return true;
        }
        public bool IsItColFull(int colIndex)
        {
            for (int i = 0; i < Constants.COL_SIZE; i++)
            {
                //cells.Add(cells2dArray[i, colIndex]);
                if (cells2dArray[i, colIndex].IsEmpty())
                {
                    return false;
                }
            }
            return true;
        }
        public void DestroyBlocks(List<DestroyCellData> destroyCellDatas, SpriteTypes colorType)
        {
            for (int i = 0; i < destroyCellDatas.Count; i++)
            {
                StartCoroutine(DestroyBlockCO(destroyCellDatas[i], colorType));
            }
            DOVirtual.DelayedCall(0.5f, () => PerformActionAfterDestroyBlock());
        }
        public void SetCell()
        {
            cells.ForEach(x => cells2dArray[x.rowIndex, x.colIndex] = x);
        }
        public void SetBlockFromData(SpriteTypes[,] colorType2dArray)
        {
            for (int i = 0; i < Constants.RAW_SIZE; i++)
            {
                for (int j = 0; j < Constants.COL_SIZE; j++)
                {
                    if (colorType2dArray[i, j] != SpriteTypes.NONE)
                        cells2dArray[i, j].SetBlock(colorType2dArray[i, j]);
                }
            }
        }
        public void OnGameOver()
        {
            StartCoroutine(WaitAndSetSprite());
        }
        public void StartActiveCellEffect()
        {
            cells.ForEach(x => x.ActivateCellEffect());
        }
        public void StopActiveCellEffect()
        {
            cells.ForEach(x => x.DeactivateCellEffect());
        }
        public void SetCellEffectsByRow(int row)
        {
            for (int i = 0; i < Constants.RAW_SIZE; i++)
            {
                cells2dArray[row, i].ChangeCellEffectLayer(2);
            }
        }
        public void SetCellEffectsByCol(int col)
        {
            for (int i = 0; i < Constants.COL_SIZE; i++)
            {
                cells2dArray[i, col].ChangeCellEffectLayer(2);
            }
        }

        public void DestroyRandomBlocks()
        {
            List<int> numbers = new List<int>();
            while (numbers.Count != 3)
            {
                int temp = Random.Range(0, 10);
                if (!numbers.Contains(temp))
                    numbers.Add(temp);
            }
            RuleManager.Instance.ResetTemporarySprite();
            if (Random.Range(0, 2) == 0)
            {
                for (int i = 0; i < numbers.Count; i++)
                {
                    for (int j = 0; j < Constants.RAW_SIZE; j++)
                    {
                        cells2dArray[numbers[i], j].DestroyBlock();
                    }
                }
            }
            else
            {
                for (int i = 0; i < numbers.Count; i++)
                {
                    for (int j = 0; j < Constants.COL_SIZE; j++)
                    {
                        cells2dArray[j, numbers[i]].DestroyBlock();
                    }
                }
            }
            PolyominoHolder.Instance.ClearAllPolyomino();
        }

        public void ResetAllCells()
        {
            cells.ForEach(x => x.ResetData());
        }
        public bool IsGridEmpty()
        {
            for (int i = 0; i < cells.Count; i++)
            {
                if (!cells[i].IsEmpty())
                    return false;
            }
            return true;
        }
        public void PerformActionAfterDestroyBlock()
        {
            PolyominoHolder.Instance.CheckForPolyominoGenerate();
            InputManager.Instance.inputState = true;
        }


        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        private IEnumerator WaitAndSetSprite()
        {
            yield return null;
            List<Cell> shuffledList = cells.OrderBy(x => Random.value).ToList();
            for (int i = 0; i < cells.Count; i++)
            {
                shuffledList[i].SetTemporarySprite(SpriteTypes.UN_PLACED, 1, false);
                yield return new WaitForSeconds(0.002f);
            }
            //KK Change display View 

            Debug.LogError(GameManager.Instance.currentGamePlay.gamePlayType);
            UiManager.Instance.GetGameOverViewByType(GameManager.Instance.currentGamePlay.gamePlayType).gameObject.SetActive(true);
            SoundManager.Instance.PlayGameLooseSFX();
        }
        private IEnumerator DestroyBlockCO(DestroyCellData destroyCellData, SpriteTypes colorType)
        {
            foreach (Cell item in destroyCellData.cells)
            {
                item.DestroyBlock(colorType);
                yield return new WaitForSeconds(0.06f);
            }
        }
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
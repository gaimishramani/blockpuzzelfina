﻿using UnityEngine;
using UnityEngine.UI;

namespace GamePlay
{
    public class ThemeSelectionView : MonoBehaviour
    {
        [SerializeField] private Sprite haloweenBGSprite;
        [SerializeField] private Sprite christmasBGSprite;
        [SerializeField] private Sprite esterBGSprite;

        [SerializeField] private Sprite haloweenBlockBGSprite;
        [SerializeField] private Sprite christmasBlockBGSprite;
        [SerializeField] private Sprite esterBlockBGSprite;


        [SerializeField] private Image bGImage;
        [SerializeField] private Image bgimageforBlock;
        [SerializeField] private GameObject hellowinBlockBG;
        [SerializeField] private GameObject chrismasBlockBG;
        [SerializeField] private GameObject easterBlockBG;


        public void OnEnable()
        {
            SoundManager.Instance.PlayBackgroundMusic();
            SoundManager.Instance.StopHaloweenBGSound();
            SoundManager.Instance.StopEsterBGSound();
            SoundManager.Instance.StopChristmasBGSound();

        }
        public void ShowView()
        {
            gameObject.SetActive(true);
        }

        public void HideView()
        {
            gameObject.SetActive(false);
        }

        public void OnHalloweenPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;
            uiManager.halloweenHomeView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Helloween);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Helloween);
            bGImage.sprite = haloweenBGSprite;
            bgimageforBlock.sprite = haloweenBlockBGSprite;
            SoundManager.Instance.PlayBackgroundMusicHalloween();
            SoundManager.Instance.StopBGSound();
            //ShowHellowinBlockBg();
        }

        public void OnChristmasPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;

            uiManager.christmasHomeView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Christmas);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Christmas);
            bGImage.sprite = christmasBGSprite;
            bgimageforBlock.sprite = christmasBlockBGSprite;
            SoundManager.Instance.PlayBackgroundMusicChristmas();
            SoundManager.Instance.StopBGSound();
            //ShowChismasBlockBg();
        }

        public void OnEsterPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;

            uiManager.esterHomePlayView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Ester);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Ester);
            bGImage.sprite = esterBGSprite;
            bgimageforBlock.sprite = esterBlockBGSprite;
            SoundManager.Instance.PlayBackgroundMusicEster();
            SoundManager.Instance.StopBGSound();
            //ShowEasterBlockBg();
        }

        public void OnBackBUttonClick()
        {
            gameObject.SetActive(false);
            UiManager.Instance.gameMainHomeView.gameObject.SetActive(true);
        }


        private void ShowHellowinBlockBg()
        {
            hellowinBlockBG.SetActive(true);
            chrismasBlockBG.SetActive(false);
            easterBlockBG.SetActive(false);
        }

        private void ShowChismasBlockBg()
        {
            hellowinBlockBG.SetActive(false);
            chrismasBlockBG.SetActive(true);
            easterBlockBG.SetActive(false);
        }

        private void ShowEasterBlockBg()
        {
            hellowinBlockBG.SetActive(false);
            chrismasBlockBG.SetActive(false);
            easterBlockBG.SetActive(true);
        }
    }

}
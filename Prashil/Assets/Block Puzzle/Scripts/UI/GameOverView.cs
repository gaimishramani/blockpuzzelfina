﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay
{
    public class GameOverView : MonoBehaviour
    {
        #region PUBLIC_VARS

        public GamePlayType playType;
        public GameObject gameOverObject;
        public GameObject timeOverObject;
        public Text currentScoreText;
        //public Text highScoreText;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void OnEnable()
        {
            DisplayScores();
            SetHeading(UiManager.Instance.isAnyTimerGameplyOn());
        }

        private void OnDisable()
        {
            gameOverObject.SetActive(true);
            timeOverObject.SetActive(false);
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        #endregion

        #region PRIVATE_FUNCTIONS
        private void DisplayScores()
        {
            currentScoreText.text = GameManager.Instance.currentGamePlay.currentScore.ToString();
            //highScoreText.text = GameManager.Instance.currentGamePlay.highScore.ToString();
            DataAnalyster.Instance.SetDataOnGameComplite(GameManager.Instance.currentGamePlay.currentScore);
        }

        public void SetHeading(bool istimer)
        {
            if (istimer)
            {
                timeOverObject.SetActive(true);
                gameOverObject.SetActive(false);
            }
            else
            {
                UiManager.Instance.gameLossView.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnHomeClick()
        {
            gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
            GameManager.Instance.grid.ResetAllCells();
            PolyominoHolder.Instance.ClearAllPolyomino();
            GameManager.Instance.isGameOver = false;
            GameManager.Instance.grid.gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.ResetScore();
            PlayerPrefs.DeleteKey(Constants.CURRENT_SCORE);
            SoundManager.Instance.PlayButtonClickSFX();
            UiManager.Instance.gameMainHomeView.ShowView();
        }
        public void OnRestart()
        {
            PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
            DataManager.Instance.ResetPolyominoGeneratorCount();
            gameObject.SetActive(false);
            SoundManager.Instance.PlayButtonClickSFX();
            GameManager.Instance.RestartGame();
        }

        public void OnQuitClick()
        {
            gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
            GameManager.Instance.grid.ResetAllCells();
            PolyominoHolder.Instance.ClearAllPolyomino();
            GameManager.Instance.isGameOver = false;
            GameManager.Instance.grid.gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.ResetScore();
            PlayerPrefs.DeleteKey(Constants.CURRENT_SCORE);
            SoundManager.Instance.PlayButtonClickSFX();
            UiManager.Instance.gameMainHomeView.ShowView();

        }
        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class GameLossView : MonoBehaviour
    {
        #region PUBLIC_VARS
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS

        public void SetContinueGameReward()
        {
            SoundManager.Instance.PlayButtonClickSFX();
            gameObject.SetActive(false);
            Admanager.Instance.ShowAdRewardedAd();

            GameManager.Instance.OnResumeGame();
            PolyominoHolder.Instance.SetPolyominoInSlot();
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        public void OnCloseClick()
        {
            gameObject.SetActive(false);
            SoundManager.Instance.PlayButtonClickSFX();
        }

        #endregion
    }
}
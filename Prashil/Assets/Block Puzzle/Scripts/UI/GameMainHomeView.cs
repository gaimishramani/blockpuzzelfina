﻿using GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMainHomeView : MonoBehaviour
{

    public GameObject SoundOnObject;
    public GameObject SoundOffObject;

    public void OnEnable()
    {
        SoundManager.Instance.PlayBackgroundMusic();
        SoundManager.Instance.StopHaloweenBGSound();
        SoundManager.Instance.StopEsterBGSound();
        SoundManager.Instance.StopChristmasBGSound();
    }

    public void ShowView()
    {
        gameObject.SetActive(true);
    }

    public void HideView()
    {
        gameObject.SetActive(false);
    }
    public void OnPlayButtonClick()
    {
        HideView();
        SoundManager.Instance.StopBGSound();
        UiManager.Instance.themeSelectionView.gameObject.SetActive(true);
    }

    private void SetSoundImage()
    {
        if (SoundManager.Instance.Sound)
        {
            SoundOnObject.SetActive(true);
            SoundOffObject.SetActive(false);
        }
        else
        {
            SoundOffObject.SetActive(true);
            SoundOnObject.SetActive(false);
        }
    }

    public void OnSoundButtonClick()
    {
        SoundManager.Instance.ToggleSound();
        SetSoundImage();
    }

}

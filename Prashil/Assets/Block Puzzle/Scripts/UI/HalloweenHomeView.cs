﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace GamePlay
{
    public class HalloweenHomeView : BaseHomeView
    {
        #region PUBLIC_VARS

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        private IEnumerator ShareAndroidText()
        {
            yield return new WaitForEndOfFrame();
#if UNITY_ANDROID
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "Block Puzzle");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "*Enjoy from Best Puzzle Game for you*\n" + Constants.GAME_URL);
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("startActivity", intentObject);
#endif

        }
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
       
        //public void OnSettingClick()
        //{
        //    UiManager.Instance.settingView.gameObject.SetActive(true);
        //    SoundManager.Instance.PlayButtonClickSFX();
        //}

        //private void SetSoundImage()
        //{
        //    if (SoundManager.Instance.Sound)
        //    {
        //        SoundOnObject.SetActive(true);
        //        SoundOffObject.SetActive(false);
        //    }
        //    else
        //    {
        //        SoundOffObject.SetActive(true);
        //        SoundOnObject.SetActive(false);
        //    }
        //}

        //public void OnSoundButtonClick()
        //{
        //    SoundManager.Instance.ToggleSound();
        //    //SetSoundImage();
        //}

        //public void OnShareButtonClick()
        //{
        //    StartCoroutine(ShareAndroidText());
        //}


        #endregion
    }
}
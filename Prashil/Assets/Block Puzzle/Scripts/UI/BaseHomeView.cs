﻿using DG.Tweening;
using GamePlay;
using UnityEngine;

public class BaseHomeView : MonoBehaviour
{
    public GamePlayType gamePlayType;


    private void OnEnable()
    {
        //SetSoundImage();
        Admanager.Instance.LoadAdInterStracialAd();
    }
    public void OnPlayClick()
    {
        Admanager.Instance.ShowAdInterStracialAd();
        GameManager.Instance.currentGamePlay = UiManager.Instance.GetGamePlayViewByType(gamePlayType);
        GameManager.Instance.currentGamePlay.gamePlayType = gamePlayType;

        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
        DataManager.Instance.ResetPolyominoGeneratorCount();
        DOVirtual.DelayedCall(0.5f, () => { });
        gameObject.SetActive(false);
        GameManager.Instance.Init();
        UiManager.Instance.GetGamePlayViewByType(gamePlayType).gameObject.SetActive(true);
        SoundManager.Instance.PlayButtonClickSFX();
        PolyominoHolder.Instance.IsTimeBaseGame = false;

    }
    public void OnTimerBasedPlayModeClick()
    {
        Admanager.Instance.ShowAdInterStracialAd();
        Debug.LogError(gamePlayType);
        GameManager.Instance.currentGamePlay = UiManager.Instance.GetTimerBasedGamePlayViewByType(gamePlayType);
        GameManager.Instance.currentGamePlay.gamePlayType = gamePlayType;
        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
        DataManager.Instance.ResetPolyominoGeneratorCount();
        DOVirtual.DelayedCall(0.5f, () => { });
        gameObject.SetActive(false);
        GameManager.Instance.Init();
        UiManager.Instance.GetTimerBasedGamePlayViewByType(gamePlayType).gameObject.SetActive(true);
        SoundManager.Instance.PlayButtonClickSFX();
        PolyominoHolder.Instance.IsTimeBaseGame = true;
    }

    public void OnBackButtonClick()
    {
        gameObject.SetActive(false);
        UiManager.Instance.themeSelectionView.ShowView();
    }
}

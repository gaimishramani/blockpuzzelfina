﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class GridAndHolderData : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static GridAndHolderData Instance;
        public SpriteTypes[,] ColorType2dArray = new SpriteTypes[10, 10];
        public static readonly string SAVEDATA_KEY = "gamedata";
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }

        private void OnApplicationQuit()
        {
           // SetGameData();
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public SpriteTypes[,] GetGameData()
        {
            return To2DArray(JsonHelper.FromJson<SpriteTypes>(PlayerPrefs.GetString(SAVEDATA_KEY)));
        }
        public void SetGameData()
        {
            PlayerPrefs.DeleteKey(SAVEDATA_KEY);
            if (GameManager.Instance.isGameOver == true)
                return;

            for (int i = 0; i < Constants.COL_SIZE; i++)
            {
                for (int j = 0; j < Constants.RAW_SIZE; j++)
                {
                    ColorType2dArray[i, j] = GameManager.Instance.grid.cells2dArray[i, j].block.colorType;
                }
            }
            PlayerPrefs.SetString(SAVEDATA_KEY, JsonHelper.ToJson(To1DArray(ColorType2dArray)));
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        private SpriteTypes[] To1DArray(SpriteTypes[,] input)
        {
            // Step 1: get total size of 2D array, and allocate 1D array.
            int size = input.Length;
            SpriteTypes[] result = new SpriteTypes[size];

            // Step 2: copy 2D array elements into a 1D array.
            int write = 0;
            for (int i = 0; i <= input.GetUpperBound(0); i++)
            {
                for (int z = 0; z <= input.GetUpperBound(1); z++)
                {
                    result[write++] = input[i, z];
                }
            }
            // Step 3: return the new array.
            return result;
        }
        private SpriteTypes[,] To2DArray(SpriteTypes[] input)
        {
            SpriteTypes[,] result = new SpriteTypes[10, 10];

            int read = 0;
            for (int i = 0; i < Constants.COL_SIZE; i++)
            {
                for (int j = 0; j < Constants.RAW_SIZE; j++)
                {
                    result[i, j] = input[read++];
                }
            }
            return result;
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
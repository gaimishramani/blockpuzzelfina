using UnityEngine;

namespace CoreGame
{

    public class ShopManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static ShopManager Instance;
        public bool ISAnyPurchaseNOAd;
        #endregion

        #region PRIVATE_VARS
        private IAPProductType _iAPProductType;
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        private void OnEnable()
        {
            IAPManager.OnPurchaseSuccess += Event_OnPurchaseSuccess;
            IAPManager.OnPurchaseCancel += Event_OnPurchaseCancle;
        }

        private void OnDisable()
        {
            IAPManager.OnPurchaseSuccess -= Event_OnPurchaseSuccess;
            IAPManager.OnPurchaseCancel -= Event_OnPurchaseCancle;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void OnIapButtonClick()
        {
            _iAPProductType = IAPProductType.REMOVE_ADS;
            //    UiManager.Instance.loadingView.ShowView();
            PurchaseProductIfInternetConnected(true);
            GoogleMobileAdsDemoScript.Instance.bannerView.Hide();
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void PurchaseProductIfInternetConnected(bool isInternetConnected)
        {
            if (!isInternetConnected)
            {
                //   UiManager.Instance.loadingView.HideView();
                // UiManager.Instance.noInternetView.ShowView();
                return;
            }
            IAPManager.Instance.PurchaseProduct(IAPManager.Instance.GetProductData(_iAPProductType).productId);

        }
        private void AddIapToData(IAPProductType iAPProductType)
        {
            switch (iAPProductType)
            {
                case IAPProductType.REMOVE_ADS:
                    ISAnyPurchaseNOAd = true;
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        private void Event_OnPurchaseSuccess(IAPProductData iAPProductData)
        {
            //   UiManager.Instance.loadingView.HideView();
            AddIapToData(iAPProductData.iapProductType);
        }

        private void Event_OnPurchaseCancle(IAPProductData iAPProductData)
        {
            //  UiManager.Instance.loadingView.HideView();
        }
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using System;
using Unity.Services.Core;
using Unity.Services.Core.Environments;

namespace CoreGame
{


    public class IAPManager : MonoBehaviour, IStoreListener
    {
        [SerializeField] private List<IAPProductData> products;
        [SerializeField] private List<string> adsProductId;
        public static IAPManager Instance;
        private static IStoreController m_StoreController;

        // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider;

        // The store-specific Purchasing subsystems.
        [SerializeField] private bool _isRemoteInited = false;

        public bool isAnyNonConsumablePurchased;

        // Product identifiers for all products capable of being purchased:
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        //public static string kProductIDConsumable = "consumable";


        public delegate void PurchaseSuccess(IAPProductData productData);

        public static event PurchaseSuccess OnPurchaseSuccess;

        public void RaisePurchaseSuccess(IAPProductData productData)
        {
            if (OnPurchaseSuccess != null)
                OnPurchaseSuccess(productData);
        }

        public delegate void PurchaseCancel(IAPProductData productData);

        public static event PurchaseCancel OnPurchaseCancel;

        public void RaisePurchaseCancel(IAPProductData productData)
        {
            if (OnPurchaseCancel != null)
                OnPurchaseCancel(productData);
        }

        public void Start()
        {
            Instance = this;
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }
        }
        public string environment = "production";

        async void Awake()
        {
            try
            {
                var options = new InitializationOptions()
                    .SetEnvironmentName(environment);

                await UnityServices.InitializeAsync(options);
            }
            catch (Exception exception)
            {
                Debug.LogError(exception.Message);
                // An error occurred during initialization.
            }
        }
        public Coroutine OnInitManager()
        {
            return StartCoroutine(InitRoutine());
        }

        IEnumerator InitRoutine()
        {
            /*#if UNITY_ANDROID
                    while (!_isRemoteInited)
                        yield return new WaitForSeconds(1f);
            #endif*/

            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }

            yield return null;
        }

        public void InitializePurchasing()
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            //builder.AddProduct(kProductIDConsumable, ProductType.Consumable);

            Debug.Log("product count " + products.Count);
            for (int i = 0; i < products.Count; i++)
            {
                Debug.Log("product id " + products[i].productId);
                builder.AddProduct(products[i].productId, products[i].productType);
            }

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);
            //StartCoroutine(WaitForInternetCheckBoughtProducts());
        }

        public bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public IAPProductData GetProductData(IAPProductType iapProductType)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].iapProductType == iapProductType)
                    return products[i];
            }

            return null;
        }

        public IAPProductData GetProductData(string productId)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].productId == productId)
                    return products[i];
            }

            return null;
        }

        // public void PurchaseProduct(IAPProductType iapProductType)
        // {
        //     BuyProductID(GetProductData(iapProductType).productId);
        // }

        public void PurchaseProduct(string productId)
        {
            BuyProductID(productId);
        }
        public void PurchaseProduct(IAPProductType iAPProductType)
        {
            BuyProductID(GetProductData(iAPProductType).productId);
        }

        // public string GetIAPPrice(IAPProductType iapProductType)
        // {
        //     if (m_StoreController != null)
        //     {
        //         return m_StoreController.products.WithID(GetProductData(iapProductType).productId).metadata.localizedPriceString;
        //     }
        //     else
        //         return "";
        //
        // }
        //
        public string GetIAPPrice(string productId)
        {
            if (m_StoreController != null)
            {
                return m_StoreController.products.WithID(productId).metadata.localizedPriceString;
            }
            else
                return "";
        }


        public double GetIAPPriceInDouble(IAPProductType iapProductType)
        {
            if (m_StoreController != null)
            {
                return (double)m_StoreController.products.WithID(GetProductData(iapProductType).productId).metadata.localizedPrice;
            }
            else
                return 0;
        }

        public string GetIAPCurrency(IAPProductType iapProductType)
        {
            if (m_StoreController != null)
            {
                return m_StoreController.products.WithID(GetProductData(iapProductType).productId).metadata.isoCurrencyCode;
            }
            else
                return "";
        }


        // Sunny : Check if Product is Already Bought By its  Purchase Receipt
        public bool IsProductAlreadyBought(string iapProductString)
        {
            if (m_StoreController != null)
            {
                Product product = m_StoreController.products.WithID(iapProductString);

                if (product != null && product.hasReceipt)
                {
                    return true;
                }
            }

            return false;
        }


        public bool IsAnyAdProductPurchased()
        {
            Debug.Log(m_StoreController + " m_storeController");
            if (m_StoreController != null)
            {
                for (int i = 0; i < adsProductId.Count; i++)
                {
                    Product product = m_StoreController.products.WithID(adsProductId[i]);

                    if (product != null && product.hasReceipt)
                    {
                        try
                        {

                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.Message);
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        private void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                //Show Loading Popup

                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    //Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    // Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                // Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google.
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                // Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                //Show Loading Popup

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });

                //HidePopup
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //
        // --- IStoreListener
        //

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            //Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;

            //foreach (Product product in controller.products.all)
            //{
            //    Debug.LogError("IAP _Init_Prod_Reciept : " + product.receipt);
            //}

            ShopManager.Instance.ISAnyPurchaseNOAd = IsAnyAdProductPurchased();
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            // Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            IAPProductData iAPProductData = GetProductData(args.purchasedProduct.definition.id);
            RaisePurchaseSuccess(iAPProductData);

            //HideLoading Popup
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            //   Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

            //HideLoading Popup
            string productId = GetProductData(product.definition.id).productId;
            RaisePurchaseCancel(GetProductData(product.definition.id));
        }

        private IEnumerator WaitForInternetCheckBoughtProducts()
        {
            while (!IsInitialized())
            {
                yield return new WaitForSeconds(1.0f);
            }

            for (int i = 0; i < products.Count; i++)
            {
                Product product = m_StoreController.products.WithID(products[i].productId);
                if (product != null && product.hasReceipt)
                {

                    break;
                }
            }
        }

        public string GetProductPurchasedReceipt(IAPProductType iapProductType)
        {
            if (m_StoreController != null)
            {
                return "";
            }
            Product product = m_StoreController.products.WithID(GetProductData(iapProductType).productId);
            if (product != null && product.hasReceipt)
            {
                return product.receipt;
            }
            return "";
        }
    }

    [System.Serializable]
    public class IAPProductData
    {
        public IAPProductType iapProductType;
        public string productId;
        public ProductType productType;

        public bool Equals(IAPProductData obj)
        {
            return productId == obj.productId;
        }

        public IAPProductData()
        {
            productId = productType.ToString();
        }
    }

    public enum IAPProductType
    {
        REMOVE_ADS,
    }
}
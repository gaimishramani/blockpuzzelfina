﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class Constants
    {
        #region PUBLIC_VARS
        public static int RAW_SIZE = 10;
        public static int COL_SIZE = 10;
        public static string SOUND = "Sound";
        public static string HIGH_SCORE = "High Score";
        public static string CURRENT_SCORE = "Current Score";
        public static string LAST_TIME_TO_SPIN = "Last Time To Spin";
        public static string IS_SPIN_AVAILABLE = "Is Free Spin Available";
        public static string IS_IT_STARTING_POLYOMINO_PHASE="Is It Starting Polyomino Phase";
        public static int TIME_TO_SPIN_HOURS = 4;
        public static int MAX_BOOSTER_COUNT = 25;
        public static int DEFAULT_BOOSTER_COUNT = 3;


        public static string TOTAL_NO_OF_GAME_PLAYED = "TotalNoOfGamePlayed";
        public static string AVERAGE_SCORE = "AverageScore";
        public static string SPIN_USE_COUNT = "SpinCount";
        public static string CONTINUE_GAME_COUNT = "CountinueGameCount";
        public static string ONE_BLOCK_BOOSTER_USE_COUNT = "OneBlockBoosterUseCount";
        public static string BIG_BOMB_BLOCK_BOOSTER_USE_COUNT = "BigBombBlockBoosterUseCount";
        public static string RAINBOW_BLOCK_BOOSTER_USE_COUNT = "RainbowBlockBoosterUseCount";
        public static string GAME_DATA_VERSION = "GameDataVersion";
        public static string INTERSTITIAL_ADS_SHOW_COUNT = "InterstitialAdsShowCount";
        public static string CAN_BANNER_AD_SHOW = "CanBannerAdShow";

        public const string GAME_URL = "Google.com";


        public static class TimeConstants
        {
            public const int MINUTES_TO_SECONDS_MULTIPLIER = 60;
            public const int HOURS_TO_SECONDS_MULTIPLIER = 60 * 60;
            public const int DAYS_TO_SECONDS_MULTIPLIER = 60 * 60 * 24;
        }

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
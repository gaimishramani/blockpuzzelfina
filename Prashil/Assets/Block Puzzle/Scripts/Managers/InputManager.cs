﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class InputManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static InputManager Instance;

        public Cell[] dropPoints;
        public float dropSensitivity;
        public Vector3 offset;
        public bool inputState = true;
        #endregion

        #region PRIVATE_VARS
        public BasePolyomino polyomino;
        private bool isAnimationOver = false;
        #endregion

        #region UNITY_CALLBACKS

        private void Awake()
        {
            Instance = this;
        }
        private void Start()
        {
            Input.multiTouchEnabled = false;
        }
        private void Update()
        {
            if (inputState)
            {
                PlayerInput();
            }
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        private void PlayerInput()
        {
            if (Input.GetMouseButtonDown(0) && polyomino == null)
            {
                CheckHitObject();
            }
            if (Input.GetMouseButton(0) && polyomino != null && isAnimationOver)
            {
                DragObject();
            }
            if (Input.GetMouseButtonUp(0) && polyomino != null)
            {
                inputState = false;
                DropObject();
            }
        }
        private void CheckHitObject()
        {
            RaycastHit2D raycastHit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));

            if (raycastHit.collider != null)
            {
                GameObject objSelected = raycastHit.transform.gameObject;
                polyomino = objSelected.transform.GetComponentInChildren<BasePolyomino>();
                if (polyomino != null)
                {
                    if (!polyomino.isployominoPlaceable)
                    {
                        polyomino = null;
                        return;
                    }
                    isAnimationOver = false;

#if UNITY_EDITOR || UNITY_STANDALONE
                    Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f)) + offset;
#else
                    Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 10.0f)) + offset;
#endif
                    polyomino.StartAnimation(polyomino.transform, new CustomTransform(endPos, Vector3.one), delegate { isAnimationOver = true; });
                    polyomino.OnDrag();
                }
            }
        }
        private void DragObject()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            polyomino.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f)) + offset;
#else
            polyomino.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 10.0f)) + offset;
#endif
        }
        private void DropObject()
        {
            polyomino.isOnDrag = false;
            isAnimationOver = false;
            if (IsValidMove(polyomino))
            {
                polyomino.OnDrop(true);
            }
            else
            {
                polyomino.OnDrop(false);
            }
            polyomino = null;
        }
        private bool IsValidMove(BasePolyomino polyomino)
        {
            return RuleManager.Instance.IsPlaceAvailable(polyomino);
        }


        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
    public class CustomTransform
    {
        public Vector3 position;
        public Vector3 scale;
        public CustomTransform() { }
        public CustomTransform(Vector3 pos, Vector3 scale)
        {
            this.position = pos;
            this.scale = scale;
        }
    }
}
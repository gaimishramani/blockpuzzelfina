﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{
    public class PolyominoGenerator : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static PolyominoGenerator Instance;
        public PolyominoData polyominoData;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public BasePolyomino GetPolyomino(Transform parent)
        {
            return Instantiate(polyominoData.GetRandomPolyomino(), parent);
        }
        public BasePolyomino GetStartingPolyomino(Transform parent)
        {
            return Instantiate(polyominoData.GetStartingPolyomino(), parent);
        }
       
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
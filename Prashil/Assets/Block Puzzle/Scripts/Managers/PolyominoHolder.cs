﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class PolyominoHolder : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static PolyominoHolder Instance;
        public List<PolyominoSlot> polyominoSlots;
        public bool IsTimeBaseGame;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void SetPolyominoInSlot()
        {
            if (DataManager.Instance.IsItStaringPolyominoPhase())
            {
                for (int i = 0; i < 3; i++)
                {
                    polyominoSlots[i].SetPolyomino(PolyominoGenerator.Instance.GetStartingPolyomino(polyominoSlots[i].transform));
                }
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    polyominoSlots[i].SetPolyomino(PolyominoGenerator.Instance.GetPolyomino(polyominoSlots[i].transform));
                }
            }
            CheckForUnplacedPolyomino();
        }

        public void CheckForPolyominoGenerate()
        {
            CheckForUnplacedPolyomino();
            if (IsTimeBaseGame)
            {
                for (int i = 0; i < polyominoSlots.Count; i++)
                {
                    if (polyominoSlots[i].IsEmpty())
                    {
                        polyominoSlots[i].SetPolyomino(PolyominoGenerator.Instance.GetPolyomino(polyominoSlots[i].transform));
                        return;
                    }
                }
            }
            else
            {
                for (int i = 0; i < polyominoSlots.Count; i++)
                {
                    if (!polyominoSlots[i].IsEmpty())
                    {
                        return;
                    }
                }
                SetPolyominoInSlot();
            }
        }
        public void SaveLastPolyomino()
        {
            polyominoSlots.ForEach(x => x.SaveLastPolyomino());
        }
        public void SetLastPolyominoInSlot()
        {
            polyominoSlots.ForEach(x => x.SetLastPolyomino());
            CheckForUnplacedPolyomino();
        }
        public void ResetLastPolyomino()
        {
            polyominoSlots.ForEach(x => x.ResetLastPolyomino());
        }
        public void ClearAllPolyomino()
        {
            for (int i = 0; i < polyominoSlots.Count; i++)
            {
                polyominoSlots[i].ResetPolyomino();
            }
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void CheckForUnplacedPolyomino()
        {
            polyominoSlots.ForEach(x => x.CheckPlaceAvailable());
            CheckForGameOver();
        }
        private bool IsAllSlotEmpty()
        {
            for (int i = 0; i < polyominoSlots.Count; i++)
            {
                if (!polyominoSlots[i].IsEmpty())
                    return false;
            }
            return true;
        }
        private void CheckForGameOver()
        {
            if (IsAllSlotEmpty())
                return;
            for (int i = 0; i < polyominoSlots.Count; i++)
            {
                if (!polyominoSlots[i].IsEmpty() && polyominoSlots[i].polyomino.isployominoPlaceable)
                    return;
            }
            Debug.Log("gameOver");
            GameManager.Instance.isGameOver = true;
            GameManager.Instance.grid.OnGameOver();
            GameManager.Instance.OnGameOver?.Invoke();
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
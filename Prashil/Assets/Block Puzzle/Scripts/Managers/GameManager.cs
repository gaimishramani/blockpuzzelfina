﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{
    public class GameManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static GameManager Instance;
        public Grid grid;
        public ColorData colorData;
        public MaterialDataSO materialDataSO;
        public bool isGameOver = true;
        public BaseGamePlay currentGamePlay;
        public TimerDataSO timerDataSO; 
        public ColorData[] colorDatas;

        public Action OnGameOver;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        private void Start()
        {
            isGameOver = true;
            SoundManager.Instance.PlayBackgroundMusic();
            //Init();
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void Init()
        {
            SoundManager.Instance.PlayGameStart();
            isGameOver = false;
            grid.gameObject.SetActive(true);
            grid.SetCell();
            if (PlayerPrefs.HasKey(GridAndHolderData.SAVEDATA_KEY))
            {
                grid.SetBlockFromData(GridAndHolderData.Instance.GetGameData());
            }
            PolyominoHolder.Instance.SetPolyominoInSlot();
        }
        public void RestartGame()
        {
            grid.ResetAllCells();
            PolyominoHolder.Instance.ClearAllPolyomino();
            PolyominoHolder.Instance.SetPolyominoInSlot();
            isGameOver = false;
            GameManager.Instance.currentGamePlay.ResetScore();
            PlayerPrefs.DeleteKey(Constants.CURRENT_SCORE);
            currentGamePlay.gameObject.SetActive(true);
        }
        public void OnResumeGame()
        {
            grid.DestroyRandomBlocks();
        }

        public void SetColorDataByGamePlay(GamePlayType gamePlayType)
        {
            for (int i = 0; i < colorDatas.Length; i++)
            {
                if (gamePlayType == colorDatas[i].gamePlayType)
                {
                    colorData = colorDatas[i];
                    return;
                }
            }
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class UiManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static UiManager Instance;
        public GameMainHomeView gameMainHomeView;
        public HalloweenHomeView halloweenHomeView;
        public ChristmasHomeView christmasHomeView;
        public EsterHomePlayView esterHomePlayView;
        public GamePlayView[] gamePlayViews;
        public TimerBasedGamePlay[] timerBasedGamePlayViews;
        public SettingView settingView;
        public GameLossView gameLossView;
        public GameQuitView gameQuitView;
        public RateUsView rateUsView;
        public FeedBackView feedBackView;
       // public LeaderBoardView leaderBoardView;
        public ThemeSelectionView themeSelectionView;
        public GridUIManager gridUIManager;

        public BaseHomeView[] baseHomeViews;
        public GameOverView[] gameOverViews;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        public GamePlayView GetGamePlayViewByType(GamePlayType gamePlayType)
        {
            for (int i = 0; i < gamePlayViews.Length; i++)
            {
                if (gamePlayViews[i].gamePlayType == gamePlayType)
                {
                    return gamePlayViews[i];
                }
            }
            return null;
        }

        public TimerBasedGamePlay GetTimerBasedGamePlayViewByType(GamePlayType gamePlayType)
        {
            for (int i = 0; i < timerBasedGamePlayViews.Length; i++)
            {
                if (timerBasedGamePlayViews[i].gamePlayType == gamePlayType)
                {
                    return timerBasedGamePlayViews[i];
                }
            }
            return null;
        }

        public BaseHomeView GetHomeViewByType(GamePlayType gamePlayType)
        {
            for (int i = 0; i < baseHomeViews.Length; i++)
            {
                if (baseHomeViews[i].gamePlayType == gamePlayType)
                {
                    return baseHomeViews[i];
                }
            }
            return null;
        } 
        
        public GameOverView GetGameOverViewByType(GamePlayType gamePlayType)
        {
            Debug.LogError(gamePlayType);
            for (int i = 0; i < gameOverViews.Length; i++)
            {
                if (gameOverViews[i].playType == gamePlayType)
                {
                    return gameOverViews[i];
                }
            }
            return null;
        }

        public bool isAnyTimerGameplyOn()
        {
            for (int i = 0; i < timerBasedGamePlayViews.Length; i++)
            {
                if (timerBasedGamePlayViews[i].gameObject.activeSelf)
                {
                    return true;
                }
            }
            return false;
        }

        public void ButtonSound()
        {
            SoundManager.Instance.PlayButtonClickSFX();
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
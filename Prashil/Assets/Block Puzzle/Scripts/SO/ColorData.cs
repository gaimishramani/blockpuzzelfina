﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    [CreateAssetMenu(fileName = "ColorData", menuName = "ScriptableObject/ColorData", order = 1)]
    public class ColorData : ScriptableObject
    {
        #region PUBLIC_VARS
        public GamePlayType gamePlayType;
        public List<SquareSpriteData> squareSpriteDatas;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public Sprite GetSprite(SpriteTypes colorType)
        {
            return squareSpriteDatas.Find(x => x.colorType == colorType).sprite;
        }
        public SpriteTypes GetRandomColorType()
        {
            return squareSpriteDatas[Random.Range(0, squareSpriteDatas.Count-1)].colorType;
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
    [System.Serializable]
    public class SquareSpriteData
    {
        public SpriteTypes colorType;
        public Sprite sprite;
    }
    [System.Serializable]
    public enum SpriteTypes
    {
        NONE,
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        p,
        q,
        r,
        s,
        t,
        u,
        v,
        w,
        x,
        y,
        z,
        UN_PLACED
    }
}
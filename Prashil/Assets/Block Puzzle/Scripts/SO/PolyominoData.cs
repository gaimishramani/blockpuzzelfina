﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    [CreateAssetMenu(fileName = "PolyominoData", menuName = "ScriptableObject/PolyominoData", order = 2)]
    public class PolyominoData : ScriptableObject
    {
        #region PUBLIC_VARS
        public List<SimplePolyomino> startingPolyominos;
        public List<SimplePolyomino> polyominos;
        public SimplePolyomino onePolyomino;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public SimplePolyomino GetRandomPolyomino()
        {
            return polyominos[Random.Range(0, polyominos.Count)];
        }
        public SimplePolyomino GetStartingPolyomino()
        {
            return startingPolyominos[Random.Range(0, startingPolyominos.Count)];
        }
      
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
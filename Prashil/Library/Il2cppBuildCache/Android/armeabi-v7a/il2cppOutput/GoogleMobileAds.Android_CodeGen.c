﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GoogleMobileAds.Android.AdErrorClient::.ctor(UnityEngine.AndroidJavaObject)
extern void AdErrorClient__ctor_m4EF9FB479F6C1050FD8E8BEADC2A413F997FCDC0 (void);
// 0x00000002 System.Int32 GoogleMobileAds.Android.AdErrorClient::GetCode()
extern void AdErrorClient_GetCode_m8C0822D34DBB88B8E6F97AF1466C2E7E752B91A4 (void);
// 0x00000003 System.String GoogleMobileAds.Android.AdErrorClient::GetDomain()
extern void AdErrorClient_GetDomain_m0D706AC7FB0CDDC00A6111C28973B110D479EA37 (void);
// 0x00000004 System.String GoogleMobileAds.Android.AdErrorClient::GetMessage()
extern void AdErrorClient_GetMessage_m73C88FC0D9EA1E70694A62608C04457C3D87BD3F (void);
// 0x00000005 GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Android.AdErrorClient::GetCause()
extern void AdErrorClient_GetCause_m41B1B7BAB0839A0DD5ACABACE9388CD8D4C9E7AE (void);
// 0x00000006 System.String GoogleMobileAds.Android.AdErrorClient::ToString()
extern void AdErrorClient_ToString_mE2CE5A173A96EB8A014B2C510EDE3B97A27C1B40 (void);
// 0x00000007 System.Void GoogleMobileAds.Android.AdInspectorErrorClient::.ctor(UnityEngine.AndroidJavaObject)
extern void AdInspectorErrorClient__ctor_m3F6A0C2A4866F9106182284B8AC526D9ABAD16D1 (void);
// 0x00000008 System.Void GoogleMobileAds.Android.AdInspectorListener::.ctor(System.Action`1<GoogleMobileAds.Common.AdInspectorErrorClientEventArgs>)
extern void AdInspectorListener__ctor_m2FE3287832C565FC00F5E461F1077230D45D8B30 (void);
// 0x00000009 System.Void GoogleMobileAds.Android.AdInspectorListener::onAdInspectorClosed(UnityEngine.AndroidJavaObject)
extern void AdInspectorListener_onAdInspectorClosed_m38942C83C28B187CD40BD58FBFA305958E1DCB0E (void);
// 0x0000000A System.Void GoogleMobileAds.Android.AppOpenAdClient::.ctor()
extern void AppOpenAdClient__ctor_mA2DA64F6E3C9905565931B25CDEF35273ADB8AF3 (void);
// 0x0000000B System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_add_OnAdLoaded_m82256490C82C19B0A74234B311DF1BEE1394EA9C (void);
// 0x0000000C System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_remove_OnAdLoaded_m273BCA24FC4D7B70276688D0C818ED65ACC94621 (void);
// 0x0000000D System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void AppOpenAdClient_add_OnAdFailedToLoad_mD23C392026278127FB4CFB009D643B558DC46FAB (void);
// 0x0000000E System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void AppOpenAdClient_remove_OnAdFailedToLoad_m2B9413888EEB7178AC2DBC656D05E86E0EC9D103 (void);
// 0x0000000F System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void AppOpenAdClient_add_OnPaidEvent_m276F308202C1B3D89BEAEB4FA812E727075CD2F2 (void);
// 0x00000010 System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void AppOpenAdClient_remove_OnPaidEvent_m23D60FF9E09861F77D58EDAFB9977AF146B6D0E8 (void);
// 0x00000011 System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void AppOpenAdClient_add_OnAdFailedToPresentFullScreenContent_mC1CFD20C3B15A02CDE576A0B0AD59B607D9E8ACA (void);
// 0x00000012 System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void AppOpenAdClient_remove_OnAdFailedToPresentFullScreenContent_mCA21CCF96BF5286AFC3CDB26519CA9ED2CAA77C4 (void);
// 0x00000013 System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_add_OnAdDidPresentFullScreenContent_m6BA345E0B51235971E9A5A2E2B96952ED751C404 (void);
// 0x00000014 System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_remove_OnAdDidPresentFullScreenContent_m7941EF64EE1DBBC632EB6959C66A57713A2B0327 (void);
// 0x00000015 System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_add_OnAdDidDismissFullScreenContent_mE1B56A956DE441DA045779A5214BE60FD829854F (void);
// 0x00000016 System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_remove_OnAdDidDismissFullScreenContent_mC6EA7292DBA761403FB14B10D6D5E779BB587FD9 (void);
// 0x00000017 System.Void GoogleMobileAds.Android.AppOpenAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_add_OnAdDidRecordImpression_mBCA5228D7DB1898B787CB2EA85AFCCB0FCC118FD (void);
// 0x00000018 System.Void GoogleMobileAds.Android.AppOpenAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdClient_remove_OnAdDidRecordImpression_m5B3F1F116BF9BF9335D9E3254A6ADC03C0DCA510 (void);
// 0x00000019 System.Void GoogleMobileAds.Android.AppOpenAdClient::CreateAppOpenAd()
extern void AppOpenAdClient_CreateAppOpenAd_mFA698FA5308AD2F346DEF7FA18F3EAB86C99195F (void);
// 0x0000001A System.Void GoogleMobileAds.Android.AppOpenAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest,UnityEngine.ScreenOrientation)
extern void AppOpenAdClient_LoadAd_m07D4FD238E55222A7C7837280A118D8042A8A955 (void);
// 0x0000001B System.Void GoogleMobileAds.Android.AppOpenAdClient::Show()
extern void AppOpenAdClient_Show_m5DC2CC016DFAC474D3EAF717AB7DDB7ED0A6FCCC (void);
// 0x0000001C GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.AppOpenAdClient::GetResponseInfoClient()
extern void AppOpenAdClient_GetResponseInfoClient_m680E2AD1FCC29DB732DE166BE9C99ADA5F08E586 (void);
// 0x0000001D System.Void GoogleMobileAds.Android.AppOpenAdClient::DestroyAppOpenAd()
extern void AppOpenAdClient_DestroyAppOpenAd_mF96C66D0F57102608830C5A61B52289F50343257 (void);
// 0x0000001E System.Void GoogleMobileAds.Android.AppOpenAdClient::onAppOpenAdLoaded()
extern void AppOpenAdClient_onAppOpenAdLoaded_m786B2A93EFA8E6456384CD04C37C798DBED05C30 (void);
// 0x0000001F System.Void GoogleMobileAds.Android.AppOpenAdClient::onAppOpenAdFailedToLoad(UnityEngine.AndroidJavaObject)
extern void AppOpenAdClient_onAppOpenAdFailedToLoad_m5D0649C83548D1B1644A51D974429D9FA9EF6016 (void);
// 0x00000020 System.Void GoogleMobileAds.Android.AppOpenAdClient::onAdFailedToShowFullScreenContent(UnityEngine.AndroidJavaObject)
extern void AppOpenAdClient_onAdFailedToShowFullScreenContent_mDB38ED96FE92348F6675B762FBDC6456CE8324F1 (void);
// 0x00000021 System.Void GoogleMobileAds.Android.AppOpenAdClient::onAdShowedFullScreenContent()
extern void AppOpenAdClient_onAdShowedFullScreenContent_m8FFE22C210FE01FB01D88309DB86EE05C2DF4A55 (void);
// 0x00000022 System.Void GoogleMobileAds.Android.AppOpenAdClient::onAdDismissedFullScreenContent()
extern void AppOpenAdClient_onAdDismissedFullScreenContent_m1D7BB655E614D85B75C7128AE882E9BDBEDC3D18 (void);
// 0x00000023 System.Void GoogleMobileAds.Android.AppOpenAdClient::onAdImpression()
extern void AppOpenAdClient_onAdImpression_mB7DCE1C0D2305C36A449258DA4EB72B4983C0FD5 (void);
// 0x00000024 System.Void GoogleMobileAds.Android.AppOpenAdClient::onPaidEvent(System.Int32,System.Int64,System.String)
extern void AppOpenAdClient_onPaidEvent_m5218C84AB3D283E210263E5D3A2AD5BD6C9D75D8 (void);
// 0x00000025 System.Void GoogleMobileAds.Android.BannerClient::.ctor()
extern void BannerClient__ctor_m0F993CD63ED3D7D2074F9F98E28C98501454262F (void);
// 0x00000026 System.Void GoogleMobileAds.Android.BannerClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdLoaded_m21CD2AA02500251CA92F21A895D58854A2900CC0 (void);
// 0x00000027 System.Void GoogleMobileAds.Android.BannerClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdLoaded_mB0F8AD8B66E91DA77015A12055FC175730FE4BB2 (void);
// 0x00000028 System.Void GoogleMobileAds.Android.BannerClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void BannerClient_add_OnAdFailedToLoad_mB28B7BF777DA4EC4062AC4950076312105C746ED (void);
// 0x00000029 System.Void GoogleMobileAds.Android.BannerClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void BannerClient_remove_OnAdFailedToLoad_m3EC71D294152D2D92178762F8D820CCAA6C13978 (void);
// 0x0000002A System.Void GoogleMobileAds.Android.BannerClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdOpening_mC2508BE4215D39611AF3CBE061F6B0EA87BB62F6 (void);
// 0x0000002B System.Void GoogleMobileAds.Android.BannerClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdOpening_mB5AB19DC9AEE84A8691BC772FAC0ADCA2452CB3B (void);
// 0x0000002C System.Void GoogleMobileAds.Android.BannerClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdClosed_m8E1D02C107A07863B26811BFBF6E31601FCA7D9E (void);
// 0x0000002D System.Void GoogleMobileAds.Android.BannerClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdClosed_m49E905B350E78DF2029C79E8592ABF87E2B40974 (void);
// 0x0000002E System.Void GoogleMobileAds.Android.BannerClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void BannerClient_add_OnPaidEvent_mFF45FFDBA05C11C2A5DF0A0B9E1668007645C591 (void);
// 0x0000002F System.Void GoogleMobileAds.Android.BannerClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void BannerClient_remove_OnPaidEvent_m9B89DF6F7DBEDB8044D83AACDD02FED37D6EBBF8 (void);
// 0x00000030 System.Void GoogleMobileAds.Android.BannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void BannerClient_CreateBannerView_mB0A3B5520B85ACEC73EF2438CB780DB3CBFDD453 (void);
// 0x00000031 System.Void GoogleMobileAds.Android.BannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void BannerClient_CreateBannerView_mEFEF8A1B01F76FAB28722619330055846D1D8DD0 (void);
// 0x00000032 System.Void GoogleMobileAds.Android.BannerClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void BannerClient_LoadAd_mE6DC2F2DF900A1A281593931CA86A6D09C977517 (void);
// 0x00000033 System.Void GoogleMobileAds.Android.BannerClient::ShowBannerView()
extern void BannerClient_ShowBannerView_m9D1019EFA04E129C48295D8106B8FCE2DA17FEA1 (void);
// 0x00000034 System.Void GoogleMobileAds.Android.BannerClient::HideBannerView()
extern void BannerClient_HideBannerView_m69CE3F3C3F03EFC9D1AC07DEC3BA1B10004F7E18 (void);
// 0x00000035 System.Void GoogleMobileAds.Android.BannerClient::DestroyBannerView()
extern void BannerClient_DestroyBannerView_mE4A26FDB76CBAC62785950AF9CD87ED9D8F959C3 (void);
// 0x00000036 System.Single GoogleMobileAds.Android.BannerClient::GetHeightInPixels()
extern void BannerClient_GetHeightInPixels_mB278686BDCADB9E55ECD0FEE7C3245481BCCF029 (void);
// 0x00000037 System.Single GoogleMobileAds.Android.BannerClient::GetWidthInPixels()
extern void BannerClient_GetWidthInPixels_m9236417131E8ACDFC2B85E3A9B2DAD1E65D03D9C (void);
// 0x00000038 System.Void GoogleMobileAds.Android.BannerClient::SetPosition(GoogleMobileAds.Api.AdPosition)
extern void BannerClient_SetPosition_m11E27297D38B278DC5F46B865CBD0D52D1478923 (void);
// 0x00000039 System.Void GoogleMobileAds.Android.BannerClient::SetPosition(System.Int32,System.Int32)
extern void BannerClient_SetPosition_m30307D51BD7B2B9B0CC80F058E052F25E6312CA3 (void);
// 0x0000003A GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.BannerClient::GetResponseInfoClient()
extern void BannerClient_GetResponseInfoClient_mC0C7134A6AC8EA3DC5B6BFD44D38B79ADF50CC49 (void);
// 0x0000003B System.Void GoogleMobileAds.Android.BannerClient::onAdLoaded()
extern void BannerClient_onAdLoaded_m18A765F93305F4C11B8620906F95DABF4709C061 (void);
// 0x0000003C System.Void GoogleMobileAds.Android.BannerClient::onAdFailedToLoad(UnityEngine.AndroidJavaObject)
extern void BannerClient_onAdFailedToLoad_mDFC91C01083D694720AA4AD1B1D1D81F17C6A43D (void);
// 0x0000003D System.Void GoogleMobileAds.Android.BannerClient::onAdOpened()
extern void BannerClient_onAdOpened_m030073FB4ABD3C83850431E88C5CD4A7A38E1911 (void);
// 0x0000003E System.Void GoogleMobileAds.Android.BannerClient::onAdClosed()
extern void BannerClient_onAdClosed_mBC75D3A885FC3B698AC7A2140193863AA06F2319 (void);
// 0x0000003F System.Void GoogleMobileAds.Android.BannerClient::onPaidEvent(System.Int32,System.Int64,System.String)
extern void BannerClient_onPaidEvent_m7C18A89B6E08264A888885ADFEF892665197FDAD (void);
// 0x00000040 System.Void GoogleMobileAds.Android.DisplayMetrics::.ctor()
extern void DisplayMetrics__ctor_mE347749A762E09B76B0EC5D494CFFE5F989BD75F (void);
// 0x00000041 System.Single GoogleMobileAds.Android.DisplayMetrics::get_Density()
extern void DisplayMetrics_get_Density_m2E9544ABAC12986FBD8D1D5B018EB45BF389F908 (void);
// 0x00000042 System.Void GoogleMobileAds.Android.DisplayMetrics::set_Density(System.Single)
extern void DisplayMetrics_set_Density_m598C0B34AE7C67829711C1C761592473A12F72F6 (void);
// 0x00000043 System.Int32 GoogleMobileAds.Android.DisplayMetrics::get_HeightPixels()
extern void DisplayMetrics_get_HeightPixels_m91950B7B18946935FFC7776BC461C91814FB7ABE (void);
// 0x00000044 System.Void GoogleMobileAds.Android.DisplayMetrics::set_HeightPixels(System.Int32)
extern void DisplayMetrics_set_HeightPixels_m57AABE2FB5899A1CBB07A6711D6E3353DAC5B65C (void);
// 0x00000045 System.Int32 GoogleMobileAds.Android.DisplayMetrics::get_WidthPixels()
extern void DisplayMetrics_get_WidthPixels_m26981CAAE146209B5BCD1A288887F4413EF3089F (void);
// 0x00000046 System.Void GoogleMobileAds.Android.DisplayMetrics::set_WidthPixels(System.Int32)
extern void DisplayMetrics_set_WidthPixels_m1B5ABA7A5AF14D1D4E1DA60FBE8C1741B6D2EF23 (void);
// 0x00000047 System.Void GoogleMobileAds.GoogleMobileAdsClientFactory::.ctor()
extern void GoogleMobileAdsClientFactory__ctor_mF944334218F2CC52EA570229BC02951CE97C290C (void);
// 0x00000048 GoogleMobileAds.Common.IAppOpenAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildAppOpenAdClient()
extern void GoogleMobileAdsClientFactory_BuildAppOpenAdClient_mDEA3C6CCA56EFDA7F226D0D45BDF542AA650BABA (void);
// 0x00000049 GoogleMobileAds.Common.IBannerClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildBannerClient()
extern void GoogleMobileAdsClientFactory_BuildBannerClient_m905DEAF223667D8B0BE04AA98744B0961FCAA63A (void);
// 0x0000004A GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildInterstitialClient()
extern void GoogleMobileAdsClientFactory_BuildInterstitialClient_mBF3CB3A2632A74C3D4FCF54F021CAA5965B7FCB1 (void);
// 0x0000004B GoogleMobileAds.Common.IRewardedAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildRewardedAdClient()
extern void GoogleMobileAdsClientFactory_BuildRewardedAdClient_m20922F427D86569456BE5BE94311BDAC04D9FA05 (void);
// 0x0000004C GoogleMobileAds.Common.IRewardedInterstitialAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildRewardedInterstitialAdClient()
extern void GoogleMobileAdsClientFactory_BuildRewardedInterstitialAdClient_m4BECC376EE944A0FE85A68483F2AACA16878CC0E (void);
// 0x0000004D GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.GoogleMobileAdsClientFactory::MobileAdsInstance()
extern void GoogleMobileAdsClientFactory_MobileAdsInstance_mDD968ED42241F0F732D69AAC1E2ABD53557B89BE (void);
// 0x0000004E System.Void GoogleMobileAds.Android.InitializationStatusClient::.ctor(UnityEngine.AndroidJavaObject)
extern void InitializationStatusClient__ctor_m0D4310B939BC057432AA9E59F327F8082FDBDD40 (void);
// 0x0000004F GoogleMobileAds.Api.AdapterStatus GoogleMobileAds.Android.InitializationStatusClient::getAdapterStatusForClassName(System.String)
extern void InitializationStatusClient_getAdapterStatusForClassName_m1E480F27FFB5159BBDF9E5739970948752F23B32 (void);
// 0x00000050 System.Collections.Generic.Dictionary`2<System.String,GoogleMobileAds.Api.AdapterStatus> GoogleMobileAds.Android.InitializationStatusClient::getAdapterStatusMap()
extern void InitializationStatusClient_getAdapterStatusMap_mA2AD91B2A9CC66E6C3D2AAC6092BB3F3047A8983 (void);
// 0x00000051 System.String[] GoogleMobileAds.Android.InitializationStatusClient::getKeys()
extern void InitializationStatusClient_getKeys_mAE6F419D5E02E0AC8D1D25F19CFE6D1F4F02C514 (void);
// 0x00000052 System.Void GoogleMobileAds.Android.InterstitialClient::.ctor()
extern void InterstitialClient__ctor_mEFDE4883CB7E9143996C952E8A24C82D496E7308 (void);
// 0x00000053 System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdLoaded_mDECECD3A4637629C5E52C14C67C028C5FD5416E8 (void);
// 0x00000054 System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdLoaded_mE808CB0007912C3DDCA2C5BB0C424959E05A81D2 (void);
// 0x00000055 System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void InterstitialClient_add_OnAdFailedToLoad_mCF7CCB9880A8F48DEFC88FB4718D64EE9E8D2363 (void);
// 0x00000056 System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void InterstitialClient_remove_OnAdFailedToLoad_m5B0177EC8718CC9AAB847889AE838A7AF41B398C (void);
// 0x00000057 System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void InterstitialClient_add_OnAdFailedToPresentFullScreenContent_mD4432750366B1EFC53E952B0765EB4E59EAB2DA1 (void);
// 0x00000058 System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void InterstitialClient_remove_OnAdFailedToPresentFullScreenContent_mA9B4F15524262422D06C929DD154F55F005BBEE7 (void);
// 0x00000059 System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdDidPresentFullScreenContent_m6C384B7B407927DB658E1042D433C03FCB87ACBE (void);
// 0x0000005A System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdDidPresentFullScreenContent_m7D55F7C9FC94009E0A5FB98491FFBEEF67C129AB (void);
// 0x0000005B System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdDidDismissFullScreenContent_mAF60318CA87AA19D60ED29D402E724ABF7CD08ED (void);
// 0x0000005C System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdDidDismissFullScreenContent_mF32C2F21CB391A833ABBFEAB64A942C771A0A6F3 (void);
// 0x0000005D System.Void GoogleMobileAds.Android.InterstitialClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdDidRecordImpression_mFD5C1459CB8DC73A58A76F4754BC546087EFB2FC (void);
// 0x0000005E System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdDidRecordImpression_m083E14E59B66BC70F5BBA38EC6ABD863E13F2876 (void);
// 0x0000005F System.Void GoogleMobileAds.Android.InterstitialClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void InterstitialClient_add_OnPaidEvent_m773AFECED841B7D341BA587A3402F8AA389724E3 (void);
// 0x00000060 System.Void GoogleMobileAds.Android.InterstitialClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void InterstitialClient_remove_OnPaidEvent_m80F3F7BE51E5F93EEC6C5414E5BED2DA6A2F8AA7 (void);
// 0x00000061 System.Void GoogleMobileAds.Android.InterstitialClient::CreateInterstitialAd()
extern void InterstitialClient_CreateInterstitialAd_m2AD6BB5B49C2894D0CE5A6FD2CE1D7DC73AF99CA (void);
// 0x00000062 System.Void GoogleMobileAds.Android.InterstitialClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
extern void InterstitialClient_LoadAd_m979C6F5D5F0BF3F2B3E1FBD6A2E7E440DA940095 (void);
// 0x00000063 System.Void GoogleMobileAds.Android.InterstitialClient::Show()
extern void InterstitialClient_Show_m2ECCF75FF2668A464E0CC323ECA9585B341AC6DF (void);
// 0x00000064 System.Void GoogleMobileAds.Android.InterstitialClient::DestroyInterstitial()
extern void InterstitialClient_DestroyInterstitial_m12A23D0E0569BC237A6480BAEB3F8EB57FB33115 (void);
// 0x00000065 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.InterstitialClient::GetResponseInfoClient()
extern void InterstitialClient_GetResponseInfoClient_mCD551BC65AAFE1D3D3996BFA1DBB62B76C7F600A (void);
// 0x00000066 System.Void GoogleMobileAds.Android.InterstitialClient::onInterstitialAdLoaded()
extern void InterstitialClient_onInterstitialAdLoaded_m74C38DC21CBC7425B0D491AE4C006D43CCED8445 (void);
// 0x00000067 System.Void GoogleMobileAds.Android.InterstitialClient::onInterstitialAdFailedToLoad(UnityEngine.AndroidJavaObject)
extern void InterstitialClient_onInterstitialAdFailedToLoad_mBBEEE24D7B7323BD73F35EF58C7A133375EE1FE9 (void);
// 0x00000068 System.Void GoogleMobileAds.Android.InterstitialClient::onAdFailedToShowFullScreenContent(UnityEngine.AndroidJavaObject)
extern void InterstitialClient_onAdFailedToShowFullScreenContent_m0541FC205A9BD6816DEF6523A75ECC718E6502ED (void);
// 0x00000069 System.Void GoogleMobileAds.Android.InterstitialClient::onAdShowedFullScreenContent()
extern void InterstitialClient_onAdShowedFullScreenContent_m46D5E251E55DB338076A3DAB0B5C8FC0688AAAC3 (void);
// 0x0000006A System.Void GoogleMobileAds.Android.InterstitialClient::onAdDismissedFullScreenContent()
extern void InterstitialClient_onAdDismissedFullScreenContent_mAE9B0F97F1DF97712A17BC61D7C3F99C69DB6E6D (void);
// 0x0000006B System.Void GoogleMobileAds.Android.InterstitialClient::onAdImpression()
extern void InterstitialClient_onAdImpression_mAAFEF759A14569B83A801926DB16CED40A3E98A4 (void);
// 0x0000006C System.Void GoogleMobileAds.Android.InterstitialClient::onPaidEvent(System.Int32,System.Int64,System.String)
extern void InterstitialClient_onPaidEvent_m9932AC4D4CA3E61748C307F71699EC59D9F18786 (void);
// 0x0000006D System.Void GoogleMobileAds.Android.LoadAdErrorClient::.ctor(UnityEngine.AndroidJavaObject)
extern void LoadAdErrorClient__ctor_m3EAD185D00FCD86677E905603D224D4B27F9B41B (void);
// 0x0000006E System.Int32 GoogleMobileAds.Android.LoadAdErrorClient::GetCode()
extern void LoadAdErrorClient_GetCode_mB00C9F2B5D41BAF59E342C6F12369FDB2D91EE8A (void);
// 0x0000006F System.String GoogleMobileAds.Android.LoadAdErrorClient::GetDomain()
extern void LoadAdErrorClient_GetDomain_mF46AF72E0A282587FC3AB8C756471C79BAB0B8A5 (void);
// 0x00000070 System.String GoogleMobileAds.Android.LoadAdErrorClient::GetMessage()
extern void LoadAdErrorClient_GetMessage_m858CA7416FA85F78738A93026EB340422B736977 (void);
// 0x00000071 GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Android.LoadAdErrorClient::GetCause()
extern void LoadAdErrorClient_GetCause_m9957EAB8BEDF671E419D497201455A4F1C0E13CB (void);
// 0x00000072 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.LoadAdErrorClient::GetResponseInfoClient()
extern void LoadAdErrorClient_GetResponseInfoClient_m0B5077AAB51D8CDAB705E899F87CF7C7FDD2FADC (void);
// 0x00000073 System.String GoogleMobileAds.Android.LoadAdErrorClient::ToString()
extern void LoadAdErrorClient_ToString_mF76414A8BB0C5F2083BE59B93BA9E823B4C8F658 (void);
// 0x00000074 System.Void GoogleMobileAds.Android.MobileAdsClient::.ctor()
extern void MobileAdsClient__ctor_m9D8D0F1F7D757059364BDDAEA448A927980D52DC (void);
// 0x00000075 GoogleMobileAds.Android.MobileAdsClient GoogleMobileAds.Android.MobileAdsClient::get_Instance()
extern void MobileAdsClient_get_Instance_m6788E1D9C347BFD2153C6BDF879A35552807C65B (void);
// 0x00000076 System.Void GoogleMobileAds.Android.MobileAdsClient::Initialize(System.Action`1<GoogleMobileAds.Common.IInitializationStatusClient>)
extern void MobileAdsClient_Initialize_m0A5C6788D0CB12A975ABF3D5C24A115E6F4EAE7A (void);
// 0x00000077 System.Void GoogleMobileAds.Android.MobileAdsClient::SetApplicationVolume(System.Single)
extern void MobileAdsClient_SetApplicationVolume_mC2C6E8EAA91218403774E1BF3B45FDE5030AC3BE (void);
// 0x00000078 System.Void GoogleMobileAds.Android.MobileAdsClient::DisableMediationInitialization()
extern void MobileAdsClient_DisableMediationInitialization_mCF844B96B98E1A8CD2E900E7F6EAB6A0C4D851EC (void);
// 0x00000079 System.Void GoogleMobileAds.Android.MobileAdsClient::SetApplicationMuted(System.Boolean)
extern void MobileAdsClient_SetApplicationMuted_mDAAC3B21F34BDB50E54146F3E68C93A5A100AB93 (void);
// 0x0000007A System.Void GoogleMobileAds.Android.MobileAdsClient::SetRequestConfiguration(GoogleMobileAds.Api.RequestConfiguration)
extern void MobileAdsClient_SetRequestConfiguration_m56FED6B21E92F1A77F2BA18A8B38D8BFEA464ECF (void);
// 0x0000007B GoogleMobileAds.Api.RequestConfiguration GoogleMobileAds.Android.MobileAdsClient::GetRequestConfiguration()
extern void MobileAdsClient_GetRequestConfiguration_m10FB20B7C0A22763DF5D878B8E61B811692EB1F5 (void);
// 0x0000007C System.Void GoogleMobileAds.Android.MobileAdsClient::SetiOSAppPauseOnBackground(System.Boolean)
extern void MobileAdsClient_SetiOSAppPauseOnBackground_mC1BCAF1C0A5370163FAA7349AFCA0EF8885F3461 (void);
// 0x0000007D System.Void GoogleMobileAds.Android.MobileAdsClient::OpenAdInspector(System.Action`1<GoogleMobileAds.Common.AdInspectorErrorClientEventArgs>)
extern void MobileAdsClient_OpenAdInspector_m3DD2B5B30FA62BB42B0ADA4119EE2B921F2DA60A (void);
// 0x0000007E System.Single GoogleMobileAds.Android.MobileAdsClient::GetDeviceScale()
extern void MobileAdsClient_GetDeviceScale_mE0225099AFC043323EA24D15036E7847693CF940 (void);
// 0x0000007F System.Int32 GoogleMobileAds.Android.MobileAdsClient::GetDeviceSafeWidth()
extern void MobileAdsClient_GetDeviceSafeWidth_m6578CA4F3E98192170D87237856B9108EDE87802 (void);
// 0x00000080 System.Void GoogleMobileAds.Android.MobileAdsClient::onInitializationComplete(UnityEngine.AndroidJavaObject)
extern void MobileAdsClient_onInitializationComplete_mFA0BB0B4BD03DBAC3AD0220571CFEFDC7CED8BD3 (void);
// 0x00000081 System.Void GoogleMobileAds.Android.MobileAdsClient::.cctor()
extern void MobileAdsClient__cctor_mFC4C7D6F7D7B4CE6244082669EDF18396D5575EE (void);
// 0x00000082 System.Void GoogleMobileAds.Android.RequestConfigurationClient::.ctor()
extern void RequestConfigurationClient__ctor_m2DD966394C922916E5D3A4C318B7F328F2F80704 (void);
// 0x00000083 UnityEngine.AndroidJavaObject GoogleMobileAds.Android.RequestConfigurationClient::BuildRequestConfiguration(GoogleMobileAds.Api.RequestConfiguration)
extern void RequestConfigurationClient_BuildRequestConfiguration_m55882B3109C6BAA490BCD9C5F09506819B6B7B4B (void);
// 0x00000084 GoogleMobileAds.Api.RequestConfiguration GoogleMobileAds.Android.RequestConfigurationClient::GetRequestConfiguration(UnityEngine.AndroidJavaObject)
extern void RequestConfigurationClient_GetRequestConfiguration_m684CD8958B6D500E8F6E413EC3B15A83B1F28B9C (void);
// 0x00000085 System.Void GoogleMobileAds.Android.ResponseInfoClient::.ctor(GoogleMobileAds.Common.ResponseInfoClientType,UnityEngine.AndroidJavaObject)
extern void ResponseInfoClient__ctor_mD8C9D92CF16C05609AFA667C8595337B6DA8D218 (void);
// 0x00000086 System.String GoogleMobileAds.Android.ResponseInfoClient::GetMediationAdapterClassName()
extern void ResponseInfoClient_GetMediationAdapterClassName_mDCB26DEAA02E5AE84D74BF864723B54DA2645F95 (void);
// 0x00000087 System.String GoogleMobileAds.Android.ResponseInfoClient::GetResponseId()
extern void ResponseInfoClient_GetResponseId_m5A808CF501A79813ADDAEF3569C47912D03704F3 (void);
// 0x00000088 System.String GoogleMobileAds.Android.ResponseInfoClient::ToString()
extern void ResponseInfoClient_ToString_m25F0020488848A2FA1DA6F633F21B0AFCA25B68D (void);
// 0x00000089 System.Void GoogleMobileAds.Android.RewardedAdClient::.ctor()
extern void RewardedAdClient__ctor_mC4ACC0A8824EE14FF197557C691D8C21F21E5BCF (void);
// 0x0000008A System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_add_OnAdLoaded_m1ACFD173C355E2F6EB4090D711847CC5255CB971 (void);
// 0x0000008B System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_remove_OnAdLoaded_m44F58246F920A2E477FC666A30FCE709160295AB (void);
// 0x0000008C System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedAdClient_add_OnAdFailedToLoad_mC8DA38B33A6755B30CF3DF6F29D2E77CA08FC20F (void);
// 0x0000008D System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedAdClient_remove_OnAdFailedToLoad_m5A2C4D9F453C271DC9D1C2C08276435BCF35BB78 (void);
// 0x0000008E System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAdClient_add_OnUserEarnedReward_m1DA138A415FE07DF39ACA5053111C3A2ACCD8268 (void);
// 0x0000008F System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAdClient_remove_OnUserEarnedReward_m29BDC518B0F40E5335420813AE55A9EEB398163D (void);
// 0x00000090 System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAdClient_add_OnPaidEvent_m7D6681D78940E98D8EBDED77CE4E1B1049FFE9B3 (void);
// 0x00000091 System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAdClient_remove_OnPaidEvent_m0D5974299A995F5B1229189CDB85E68F2190D2E3 (void);
// 0x00000092 System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedAdClient_add_OnAdFailedToPresentFullScreenContent_m6C110E08A38CB067BF8F90C087E4261EBE2F17C8 (void);
// 0x00000093 System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedAdClient_remove_OnAdFailedToPresentFullScreenContent_mE986CFC68128090F1E3D6EE13704BB00084FAED1 (void);
// 0x00000094 System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_add_OnAdDidPresentFullScreenContent_mEB5B7419DD10F4CAF041C55E0D37B39665406652 (void);
// 0x00000095 System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_remove_OnAdDidPresentFullScreenContent_m81E4445B6AE6D6C684890532B7A43D641CD88422 (void);
// 0x00000096 System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_add_OnAdDidDismissFullScreenContent_m3C788DE482D095DB11E97B44E0A3672A2387B16E (void);
// 0x00000097 System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_remove_OnAdDidDismissFullScreenContent_mBF976A09DE25B694C311CA2E08862EE031188C85 (void);
// 0x00000098 System.Void GoogleMobileAds.Android.RewardedAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_add_OnAdDidRecordImpression_m5D4B9F52C51BE30604FA68650BD5E2E521FE9D5A (void);
// 0x00000099 System.Void GoogleMobileAds.Android.RewardedAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdClient_remove_OnAdDidRecordImpression_m2D72A89C2B4FCB4CCB5A4220FAA303DF3526D538 (void);
// 0x0000009A System.Void GoogleMobileAds.Android.RewardedAdClient::CreateRewardedAd()
extern void RewardedAdClient_CreateRewardedAd_mD4A7B0FA3A2B4831B4950A542E251C4FA37838B0 (void);
// 0x0000009B System.Void GoogleMobileAds.Android.RewardedAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
extern void RewardedAdClient_LoadAd_m902E23054ACC0A250B3E847841A9D5AC6FFA3C25 (void);
// 0x0000009C System.Void GoogleMobileAds.Android.RewardedAdClient::Show()
extern void RewardedAdClient_Show_m2964AA99F1680A74023A56CC206DFD03B246053B (void);
// 0x0000009D System.Void GoogleMobileAds.Android.RewardedAdClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
extern void RewardedAdClient_SetServerSideVerificationOptions_mCEEE19008C4A0CF293AF4C9F606B64B13EF5CB0C (void);
// 0x0000009E GoogleMobileAds.Api.Reward GoogleMobileAds.Android.RewardedAdClient::GetRewardItem()
extern void RewardedAdClient_GetRewardItem_m4708F663E9050B5C3694471738819511EFA6464E (void);
// 0x0000009F GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.RewardedAdClient::GetResponseInfoClient()
extern void RewardedAdClient_GetResponseInfoClient_m6379FCC42D0055AF4021C95A848187E1CD613B32 (void);
// 0x000000A0 System.Void GoogleMobileAds.Android.RewardedAdClient::DestroyRewardedAd()
extern void RewardedAdClient_DestroyRewardedAd_mD6C49E7B474F492B5301A0FEFF941A5C133C437F (void);
// 0x000000A1 System.Void GoogleMobileAds.Android.RewardedAdClient::onRewardedAdLoaded()
extern void RewardedAdClient_onRewardedAdLoaded_mAAED7D5F3DF0C38876142A38DC9BEF6253B28CC5 (void);
// 0x000000A2 System.Void GoogleMobileAds.Android.RewardedAdClient::onRewardedAdFailedToLoad(UnityEngine.AndroidJavaObject)
extern void RewardedAdClient_onRewardedAdFailedToLoad_m72B4A3544B0859C02FAB0EFA44E6173F621E4151 (void);
// 0x000000A3 System.Void GoogleMobileAds.Android.RewardedAdClient::onAdFailedToShowFullScreenContent(UnityEngine.AndroidJavaObject)
extern void RewardedAdClient_onAdFailedToShowFullScreenContent_m6FBCAC0B293CB5C46B8313C82D8AFC6D7E47B122 (void);
// 0x000000A4 System.Void GoogleMobileAds.Android.RewardedAdClient::onAdShowedFullScreenContent()
extern void RewardedAdClient_onAdShowedFullScreenContent_m40141ABD14D489CD48544979F206D43BD68A0304 (void);
// 0x000000A5 System.Void GoogleMobileAds.Android.RewardedAdClient::onAdDismissedFullScreenContent()
extern void RewardedAdClient_onAdDismissedFullScreenContent_m0382791A745BBB32FBB6EEC70528CFB3D0A4A859 (void);
// 0x000000A6 System.Void GoogleMobileAds.Android.RewardedAdClient::onAdImpression()
extern void RewardedAdClient_onAdImpression_m87361B61CF97B889A0124D2F741FF6A8BD1E26F6 (void);
// 0x000000A7 System.Void GoogleMobileAds.Android.RewardedAdClient::onUserEarnedReward(System.String,System.Single)
extern void RewardedAdClient_onUserEarnedReward_m5AAE1BC8646931D4B03F5869318E891F4DBF0F2D (void);
// 0x000000A8 System.Void GoogleMobileAds.Android.RewardedAdClient::onPaidEvent(System.Int32,System.Int64,System.String)
extern void RewardedAdClient_onPaidEvent_m828ECC4AFF9B0798BCD6D8612099C3C5DB29510C (void);
// 0x000000A9 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::.ctor()
extern void RewardedInterstitialAdClient__ctor_m217C6154172A23DCA325BCAF84FA49187AC33082 (void);
// 0x000000AA System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdLoaded_m968959A2CC30EF53E29B44EC0838659BDDB70248 (void);
// 0x000000AB System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdLoaded_m4B67E2E8F17B50E06146535AF2062EB216A06FC9 (void);
// 0x000000AC System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdFailedToLoad_mBA3B1E94D189229B7BAF3563CEC9C5AC7B864193 (void);
// 0x000000AD System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdFailedToLoad_m668F1EF6B8DBD31464C67EA1C96B040A1ADA4B92 (void);
// 0x000000AE System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedInterstitialAdClient_add_OnUserEarnedReward_mF0140C9E0ABED9D3470963A81AB1A1C64EAF08B4 (void);
// 0x000000AF System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedInterstitialAdClient_remove_OnUserEarnedReward_mC25332BF4E680657F6172E44E02AE483A5CE8A96 (void);
// 0x000000B0 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedInterstitialAdClient_add_OnPaidEvent_m2553DF6D4E263A1596985BA9352BFA18752002DC (void);
// 0x000000B1 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedInterstitialAdClient_remove_OnPaidEvent_mB02A83A63A885F617D4699A90EB8F0CDF553A219 (void);
// 0x000000B2 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdFailedToPresentFullScreenContent_mFE5FDC6F80BFC552F00194EDA0BF013A091E65B5 (void);
// 0x000000B3 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdFailedToPresentFullScreenContent_m30E8A0D43F62B4FE1337326449C23C02A3911ABB (void);
// 0x000000B4 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdDidPresentFullScreenContent_m3711721FD59FF6C8BA501DE386026D68F2CDBE13 (void);
// 0x000000B5 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdDidPresentFullScreenContent_m891226C282A4F760E50F31F9ADA32D82DC912A4C (void);
// 0x000000B6 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdDidDismissFullScreenContent_mA8CCBB2710FBEBEEAC3B83B94317FEFBE928327F (void);
// 0x000000B7 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdDidDismissFullScreenContent_mD65CEC1AFA011A92FB56985D638E18CBD834AFAE (void);
// 0x000000B8 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_add_OnAdDidRecordImpression_m34D54ED09D21E780906A3CA3D4CE5D2EF4433C3B (void);
// 0x000000B9 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdClient_remove_OnAdDidRecordImpression_m4D14BC29FFC171331AD843C457B19A1EA15A5A7A (void);
// 0x000000BA System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::CreateRewardedInterstitialAd()
extern void RewardedInterstitialAdClient_CreateRewardedInterstitialAd_mE4D63DD94D3583EE0D729569817CECDA88D1C3F7 (void);
// 0x000000BB System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
extern void RewardedInterstitialAdClient_LoadAd_m73A34C9C46251F72EFD8C44F16F4E5EB95BF67CD (void);
// 0x000000BC System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::Show()
extern void RewardedInterstitialAdClient_Show_m6BF69BAA35CAED180464AD7383F2CDC0546BD0E2 (void);
// 0x000000BD GoogleMobileAds.Api.Reward GoogleMobileAds.Android.RewardedInterstitialAdClient::GetRewardItem()
extern void RewardedInterstitialAdClient_GetRewardItem_m0840847B8EFE45AA6F2DE49FE82F41FDA4DD187E (void);
// 0x000000BE System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
extern void RewardedInterstitialAdClient_SetServerSideVerificationOptions_m502BFA362530765077B3626DCF6D923AB5BB1253 (void);
// 0x000000BF GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Android.RewardedInterstitialAdClient::GetResponseInfoClient()
extern void RewardedInterstitialAdClient_GetResponseInfoClient_mCB9E9A9BF3769684E5E22137A1004387309D4717 (void);
// 0x000000C0 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::DestroyRewardedInterstitialAd()
extern void RewardedInterstitialAdClient_DestroyRewardedInterstitialAd_mBAD2FEE33F70B29787E4E2490DA50563670A6766 (void);
// 0x000000C1 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onRewardedInterstitialAdLoaded()
extern void RewardedInterstitialAdClient_onRewardedInterstitialAdLoaded_m18C8920BFD03A1E06AC6BFC1B92E30ADCBCDF892 (void);
// 0x000000C2 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onRewardedInterstitialAdFailedToLoad(UnityEngine.AndroidJavaObject)
extern void RewardedInterstitialAdClient_onRewardedInterstitialAdFailedToLoad_m42E484B59E53D8CCB4A8FABC03630432856A766C (void);
// 0x000000C3 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onAdFailedToShowFullScreenContent(UnityEngine.AndroidJavaObject)
extern void RewardedInterstitialAdClient_onAdFailedToShowFullScreenContent_mDB87DBAF1AD20870CD10B393D69C04F68C9B509E (void);
// 0x000000C4 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onAdShowedFullScreenContent()
extern void RewardedInterstitialAdClient_onAdShowedFullScreenContent_m2B1E59093B0AEE23B46B7DFE1AA374620788004A (void);
// 0x000000C5 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onAdDismissedFullScreenContent()
extern void RewardedInterstitialAdClient_onAdDismissedFullScreenContent_m58749B308F8120B6F7A53FF9E5F6587B0D6C7871 (void);
// 0x000000C6 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onAdImpression()
extern void RewardedInterstitialAdClient_onAdImpression_m8FCA5FC0BEBEB209B21254A10A798D1FD2EBE397 (void);
// 0x000000C7 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onUserEarnedReward(System.String,System.Single)
extern void RewardedInterstitialAdClient_onUserEarnedReward_m0A30F509548D8A374BD4EAE9C2DE03B11E6B600E (void);
// 0x000000C8 System.Void GoogleMobileAds.Android.RewardedInterstitialAdClient::onPaidEvent(System.Int32,System.Int64,System.String)
extern void RewardedInterstitialAdClient_onPaidEvent_mA60FC5BB6F6EBE6E8E0C801B466F996F1D76B8E8 (void);
// 0x000000C9 System.Void GoogleMobileAds.Android.Utils::.ctor()
extern void Utils__ctor_m23A1DDF17555E55A6233877FC4EA1671C478DE43 (void);
// 0x000000CA UnityEngine.AndroidJavaObject GoogleMobileAds.Android.Utils::GetAdSizeJavaObject(GoogleMobileAds.Api.AdSize)
extern void Utils_GetAdSizeJavaObject_mD15CDAE9794E13A3A74FA87E8FBC05F807DADAC0 (void);
// 0x000000CB System.Int32 GoogleMobileAds.Android.Utils::GetAppOpenAdOrientation(UnityEngine.ScreenOrientation)
extern void Utils_GetAppOpenAdOrientation_m3022EFF96AC0A985BF11811A3210D8996BD9DDC4 (void);
// 0x000000CC System.Int32 GoogleMobileAds.Android.Utils::GetScreenWidth()
extern void Utils_GetScreenWidth_m8AAEAC651D3ABEEEFEDCBD7E9288772FB69609EC (void);
// 0x000000CD UnityEngine.AndroidJavaObject GoogleMobileAds.Android.Utils::GetAdRequestJavaObject(GoogleMobileAds.Api.AdRequest,System.String)
extern void Utils_GetAdRequestJavaObject_mE20988FEE8A63BCD55DF91D7BA786D19D444800A (void);
// 0x000000CE UnityEngine.AndroidJavaObject GoogleMobileAds.Android.Utils::GetJavaListObject(System.Collections.Generic.List`1<System.String>)
extern void Utils_GetJavaListObject_mE6FD73C41C672A5D9213354B5AB71C56B5B5F24C (void);
// 0x000000CF System.Collections.Generic.List`1<System.String> GoogleMobileAds.Android.Utils::GetCsTypeList(UnityEngine.AndroidJavaObject)
extern void Utils_GetCsTypeList_m51894CF039AAE81DBE01D37ECEA4F39C1A7AD6F4 (void);
// 0x000000D0 UnityEngine.AndroidJavaObject GoogleMobileAds.Android.Utils::GetServerSideVerificationOptionsJavaObject(GoogleMobileAds.Api.ServerSideVerificationOptions)
extern void Utils_GetServerSideVerificationOptionsJavaObject_m36BD4FEE8AFB67A393807022530683C2E65ED500 (void);
static Il2CppMethodPointer s_methodPointers[208] = 
{
	AdErrorClient__ctor_m4EF9FB479F6C1050FD8E8BEADC2A413F997FCDC0,
	AdErrorClient_GetCode_m8C0822D34DBB88B8E6F97AF1466C2E7E752B91A4,
	AdErrorClient_GetDomain_m0D706AC7FB0CDDC00A6111C28973B110D479EA37,
	AdErrorClient_GetMessage_m73C88FC0D9EA1E70694A62608C04457C3D87BD3F,
	AdErrorClient_GetCause_m41B1B7BAB0839A0DD5ACABACE9388CD8D4C9E7AE,
	AdErrorClient_ToString_mE2CE5A173A96EB8A014B2C510EDE3B97A27C1B40,
	AdInspectorErrorClient__ctor_m3F6A0C2A4866F9106182284B8AC526D9ABAD16D1,
	AdInspectorListener__ctor_m2FE3287832C565FC00F5E461F1077230D45D8B30,
	AdInspectorListener_onAdInspectorClosed_m38942C83C28B187CD40BD58FBFA305958E1DCB0E,
	AppOpenAdClient__ctor_mA2DA64F6E3C9905565931B25CDEF35273ADB8AF3,
	AppOpenAdClient_add_OnAdLoaded_m82256490C82C19B0A74234B311DF1BEE1394EA9C,
	AppOpenAdClient_remove_OnAdLoaded_m273BCA24FC4D7B70276688D0C818ED65ACC94621,
	AppOpenAdClient_add_OnAdFailedToLoad_mD23C392026278127FB4CFB009D643B558DC46FAB,
	AppOpenAdClient_remove_OnAdFailedToLoad_m2B9413888EEB7178AC2DBC656D05E86E0EC9D103,
	AppOpenAdClient_add_OnPaidEvent_m276F308202C1B3D89BEAEB4FA812E727075CD2F2,
	AppOpenAdClient_remove_OnPaidEvent_m23D60FF9E09861F77D58EDAFB9977AF146B6D0E8,
	AppOpenAdClient_add_OnAdFailedToPresentFullScreenContent_mC1CFD20C3B15A02CDE576A0B0AD59B607D9E8ACA,
	AppOpenAdClient_remove_OnAdFailedToPresentFullScreenContent_mCA21CCF96BF5286AFC3CDB26519CA9ED2CAA77C4,
	AppOpenAdClient_add_OnAdDidPresentFullScreenContent_m6BA345E0B51235971E9A5A2E2B96952ED751C404,
	AppOpenAdClient_remove_OnAdDidPresentFullScreenContent_m7941EF64EE1DBBC632EB6959C66A57713A2B0327,
	AppOpenAdClient_add_OnAdDidDismissFullScreenContent_mE1B56A956DE441DA045779A5214BE60FD829854F,
	AppOpenAdClient_remove_OnAdDidDismissFullScreenContent_mC6EA7292DBA761403FB14B10D6D5E779BB587FD9,
	AppOpenAdClient_add_OnAdDidRecordImpression_mBCA5228D7DB1898B787CB2EA85AFCCB0FCC118FD,
	AppOpenAdClient_remove_OnAdDidRecordImpression_m5B3F1F116BF9BF9335D9E3254A6ADC03C0DCA510,
	AppOpenAdClient_CreateAppOpenAd_mFA698FA5308AD2F346DEF7FA18F3EAB86C99195F,
	AppOpenAdClient_LoadAd_m07D4FD238E55222A7C7837280A118D8042A8A955,
	AppOpenAdClient_Show_m5DC2CC016DFAC474D3EAF717AB7DDB7ED0A6FCCC,
	AppOpenAdClient_GetResponseInfoClient_m680E2AD1FCC29DB732DE166BE9C99ADA5F08E586,
	AppOpenAdClient_DestroyAppOpenAd_mF96C66D0F57102608830C5A61B52289F50343257,
	AppOpenAdClient_onAppOpenAdLoaded_m786B2A93EFA8E6456384CD04C37C798DBED05C30,
	AppOpenAdClient_onAppOpenAdFailedToLoad_m5D0649C83548D1B1644A51D974429D9FA9EF6016,
	AppOpenAdClient_onAdFailedToShowFullScreenContent_mDB38ED96FE92348F6675B762FBDC6456CE8324F1,
	AppOpenAdClient_onAdShowedFullScreenContent_m8FFE22C210FE01FB01D88309DB86EE05C2DF4A55,
	AppOpenAdClient_onAdDismissedFullScreenContent_m1D7BB655E614D85B75C7128AE882E9BDBEDC3D18,
	AppOpenAdClient_onAdImpression_mB7DCE1C0D2305C36A449258DA4EB72B4983C0FD5,
	AppOpenAdClient_onPaidEvent_m5218C84AB3D283E210263E5D3A2AD5BD6C9D75D8,
	BannerClient__ctor_m0F993CD63ED3D7D2074F9F98E28C98501454262F,
	BannerClient_add_OnAdLoaded_m21CD2AA02500251CA92F21A895D58854A2900CC0,
	BannerClient_remove_OnAdLoaded_mB0F8AD8B66E91DA77015A12055FC175730FE4BB2,
	BannerClient_add_OnAdFailedToLoad_mB28B7BF777DA4EC4062AC4950076312105C746ED,
	BannerClient_remove_OnAdFailedToLoad_m3EC71D294152D2D92178762F8D820CCAA6C13978,
	BannerClient_add_OnAdOpening_mC2508BE4215D39611AF3CBE061F6B0EA87BB62F6,
	BannerClient_remove_OnAdOpening_mB5AB19DC9AEE84A8691BC772FAC0ADCA2452CB3B,
	BannerClient_add_OnAdClosed_m8E1D02C107A07863B26811BFBF6E31601FCA7D9E,
	BannerClient_remove_OnAdClosed_m49E905B350E78DF2029C79E8592ABF87E2B40974,
	BannerClient_add_OnPaidEvent_mFF45FFDBA05C11C2A5DF0A0B9E1668007645C591,
	BannerClient_remove_OnPaidEvent_m9B89DF6F7DBEDB8044D83AACDD02FED37D6EBBF8,
	BannerClient_CreateBannerView_mB0A3B5520B85ACEC73EF2438CB780DB3CBFDD453,
	BannerClient_CreateBannerView_mEFEF8A1B01F76FAB28722619330055846D1D8DD0,
	BannerClient_LoadAd_mE6DC2F2DF900A1A281593931CA86A6D09C977517,
	BannerClient_ShowBannerView_m9D1019EFA04E129C48295D8106B8FCE2DA17FEA1,
	BannerClient_HideBannerView_m69CE3F3C3F03EFC9D1AC07DEC3BA1B10004F7E18,
	BannerClient_DestroyBannerView_mE4A26FDB76CBAC62785950AF9CD87ED9D8F959C3,
	BannerClient_GetHeightInPixels_mB278686BDCADB9E55ECD0FEE7C3245481BCCF029,
	BannerClient_GetWidthInPixels_m9236417131E8ACDFC2B85E3A9B2DAD1E65D03D9C,
	BannerClient_SetPosition_m11E27297D38B278DC5F46B865CBD0D52D1478923,
	BannerClient_SetPosition_m30307D51BD7B2B9B0CC80F058E052F25E6312CA3,
	BannerClient_GetResponseInfoClient_mC0C7134A6AC8EA3DC5B6BFD44D38B79ADF50CC49,
	BannerClient_onAdLoaded_m18A765F93305F4C11B8620906F95DABF4709C061,
	BannerClient_onAdFailedToLoad_mDFC91C01083D694720AA4AD1B1D1D81F17C6A43D,
	BannerClient_onAdOpened_m030073FB4ABD3C83850431E88C5CD4A7A38E1911,
	BannerClient_onAdClosed_mBC75D3A885FC3B698AC7A2140193863AA06F2319,
	BannerClient_onPaidEvent_m7C18A89B6E08264A888885ADFEF892665197FDAD,
	DisplayMetrics__ctor_mE347749A762E09B76B0EC5D494CFFE5F989BD75F,
	DisplayMetrics_get_Density_m2E9544ABAC12986FBD8D1D5B018EB45BF389F908,
	DisplayMetrics_set_Density_m598C0B34AE7C67829711C1C761592473A12F72F6,
	DisplayMetrics_get_HeightPixels_m91950B7B18946935FFC7776BC461C91814FB7ABE,
	DisplayMetrics_set_HeightPixels_m57AABE2FB5899A1CBB07A6711D6E3353DAC5B65C,
	DisplayMetrics_get_WidthPixels_m26981CAAE146209B5BCD1A288887F4413EF3089F,
	DisplayMetrics_set_WidthPixels_m1B5ABA7A5AF14D1D4E1DA60FBE8C1741B6D2EF23,
	GoogleMobileAdsClientFactory__ctor_mF944334218F2CC52EA570229BC02951CE97C290C,
	GoogleMobileAdsClientFactory_BuildAppOpenAdClient_mDEA3C6CCA56EFDA7F226D0D45BDF542AA650BABA,
	GoogleMobileAdsClientFactory_BuildBannerClient_m905DEAF223667D8B0BE04AA98744B0961FCAA63A,
	GoogleMobileAdsClientFactory_BuildInterstitialClient_mBF3CB3A2632A74C3D4FCF54F021CAA5965B7FCB1,
	GoogleMobileAdsClientFactory_BuildRewardedAdClient_m20922F427D86569456BE5BE94311BDAC04D9FA05,
	GoogleMobileAdsClientFactory_BuildRewardedInterstitialAdClient_m4BECC376EE944A0FE85A68483F2AACA16878CC0E,
	GoogleMobileAdsClientFactory_MobileAdsInstance_mDD968ED42241F0F732D69AAC1E2ABD53557B89BE,
	InitializationStatusClient__ctor_m0D4310B939BC057432AA9E59F327F8082FDBDD40,
	InitializationStatusClient_getAdapterStatusForClassName_m1E480F27FFB5159BBDF9E5739970948752F23B32,
	InitializationStatusClient_getAdapterStatusMap_mA2AD91B2A9CC66E6C3D2AAC6092BB3F3047A8983,
	InitializationStatusClient_getKeys_mAE6F419D5E02E0AC8D1D25F19CFE6D1F4F02C514,
	InterstitialClient__ctor_mEFDE4883CB7E9143996C952E8A24C82D496E7308,
	InterstitialClient_add_OnAdLoaded_mDECECD3A4637629C5E52C14C67C028C5FD5416E8,
	InterstitialClient_remove_OnAdLoaded_mE808CB0007912C3DDCA2C5BB0C424959E05A81D2,
	InterstitialClient_add_OnAdFailedToLoad_mCF7CCB9880A8F48DEFC88FB4718D64EE9E8D2363,
	InterstitialClient_remove_OnAdFailedToLoad_m5B0177EC8718CC9AAB847889AE838A7AF41B398C,
	InterstitialClient_add_OnAdFailedToPresentFullScreenContent_mD4432750366B1EFC53E952B0765EB4E59EAB2DA1,
	InterstitialClient_remove_OnAdFailedToPresentFullScreenContent_mA9B4F15524262422D06C929DD154F55F005BBEE7,
	InterstitialClient_add_OnAdDidPresentFullScreenContent_m6C384B7B407927DB658E1042D433C03FCB87ACBE,
	InterstitialClient_remove_OnAdDidPresentFullScreenContent_m7D55F7C9FC94009E0A5FB98491FFBEEF67C129AB,
	InterstitialClient_add_OnAdDidDismissFullScreenContent_mAF60318CA87AA19D60ED29D402E724ABF7CD08ED,
	InterstitialClient_remove_OnAdDidDismissFullScreenContent_mF32C2F21CB391A833ABBFEAB64A942C771A0A6F3,
	InterstitialClient_add_OnAdDidRecordImpression_mFD5C1459CB8DC73A58A76F4754BC546087EFB2FC,
	InterstitialClient_remove_OnAdDidRecordImpression_m083E14E59B66BC70F5BBA38EC6ABD863E13F2876,
	InterstitialClient_add_OnPaidEvent_m773AFECED841B7D341BA587A3402F8AA389724E3,
	InterstitialClient_remove_OnPaidEvent_m80F3F7BE51E5F93EEC6C5414E5BED2DA6A2F8AA7,
	InterstitialClient_CreateInterstitialAd_m2AD6BB5B49C2894D0CE5A6FD2CE1D7DC73AF99CA,
	InterstitialClient_LoadAd_m979C6F5D5F0BF3F2B3E1FBD6A2E7E440DA940095,
	InterstitialClient_Show_m2ECCF75FF2668A464E0CC323ECA9585B341AC6DF,
	InterstitialClient_DestroyInterstitial_m12A23D0E0569BC237A6480BAEB3F8EB57FB33115,
	InterstitialClient_GetResponseInfoClient_mCD551BC65AAFE1D3D3996BFA1DBB62B76C7F600A,
	InterstitialClient_onInterstitialAdLoaded_m74C38DC21CBC7425B0D491AE4C006D43CCED8445,
	InterstitialClient_onInterstitialAdFailedToLoad_mBBEEE24D7B7323BD73F35EF58C7A133375EE1FE9,
	InterstitialClient_onAdFailedToShowFullScreenContent_m0541FC205A9BD6816DEF6523A75ECC718E6502ED,
	InterstitialClient_onAdShowedFullScreenContent_m46D5E251E55DB338076A3DAB0B5C8FC0688AAAC3,
	InterstitialClient_onAdDismissedFullScreenContent_mAE9B0F97F1DF97712A17BC61D7C3F99C69DB6E6D,
	InterstitialClient_onAdImpression_mAAFEF759A14569B83A801926DB16CED40A3E98A4,
	InterstitialClient_onPaidEvent_m9932AC4D4CA3E61748C307F71699EC59D9F18786,
	LoadAdErrorClient__ctor_m3EAD185D00FCD86677E905603D224D4B27F9B41B,
	LoadAdErrorClient_GetCode_mB00C9F2B5D41BAF59E342C6F12369FDB2D91EE8A,
	LoadAdErrorClient_GetDomain_mF46AF72E0A282587FC3AB8C756471C79BAB0B8A5,
	LoadAdErrorClient_GetMessage_m858CA7416FA85F78738A93026EB340422B736977,
	LoadAdErrorClient_GetCause_m9957EAB8BEDF671E419D497201455A4F1C0E13CB,
	LoadAdErrorClient_GetResponseInfoClient_m0B5077AAB51D8CDAB705E899F87CF7C7FDD2FADC,
	LoadAdErrorClient_ToString_mF76414A8BB0C5F2083BE59B93BA9E823B4C8F658,
	MobileAdsClient__ctor_m9D8D0F1F7D757059364BDDAEA448A927980D52DC,
	MobileAdsClient_get_Instance_m6788E1D9C347BFD2153C6BDF879A35552807C65B,
	MobileAdsClient_Initialize_m0A5C6788D0CB12A975ABF3D5C24A115E6F4EAE7A,
	MobileAdsClient_SetApplicationVolume_mC2C6E8EAA91218403774E1BF3B45FDE5030AC3BE,
	MobileAdsClient_DisableMediationInitialization_mCF844B96B98E1A8CD2E900E7F6EAB6A0C4D851EC,
	MobileAdsClient_SetApplicationMuted_mDAAC3B21F34BDB50E54146F3E68C93A5A100AB93,
	MobileAdsClient_SetRequestConfiguration_m56FED6B21E92F1A77F2BA18A8B38D8BFEA464ECF,
	MobileAdsClient_GetRequestConfiguration_m10FB20B7C0A22763DF5D878B8E61B811692EB1F5,
	MobileAdsClient_SetiOSAppPauseOnBackground_mC1BCAF1C0A5370163FAA7349AFCA0EF8885F3461,
	MobileAdsClient_OpenAdInspector_m3DD2B5B30FA62BB42B0ADA4119EE2B921F2DA60A,
	MobileAdsClient_GetDeviceScale_mE0225099AFC043323EA24D15036E7847693CF940,
	MobileAdsClient_GetDeviceSafeWidth_m6578CA4F3E98192170D87237856B9108EDE87802,
	MobileAdsClient_onInitializationComplete_mFA0BB0B4BD03DBAC3AD0220571CFEFDC7CED8BD3,
	MobileAdsClient__cctor_mFC4C7D6F7D7B4CE6244082669EDF18396D5575EE,
	RequestConfigurationClient__ctor_m2DD966394C922916E5D3A4C318B7F328F2F80704,
	RequestConfigurationClient_BuildRequestConfiguration_m55882B3109C6BAA490BCD9C5F09506819B6B7B4B,
	RequestConfigurationClient_GetRequestConfiguration_m684CD8958B6D500E8F6E413EC3B15A83B1F28B9C,
	ResponseInfoClient__ctor_mD8C9D92CF16C05609AFA667C8595337B6DA8D218,
	ResponseInfoClient_GetMediationAdapterClassName_mDCB26DEAA02E5AE84D74BF864723B54DA2645F95,
	ResponseInfoClient_GetResponseId_m5A808CF501A79813ADDAEF3569C47912D03704F3,
	ResponseInfoClient_ToString_m25F0020488848A2FA1DA6F633F21B0AFCA25B68D,
	RewardedAdClient__ctor_mC4ACC0A8824EE14FF197557C691D8C21F21E5BCF,
	RewardedAdClient_add_OnAdLoaded_m1ACFD173C355E2F6EB4090D711847CC5255CB971,
	RewardedAdClient_remove_OnAdLoaded_m44F58246F920A2E477FC666A30FCE709160295AB,
	RewardedAdClient_add_OnAdFailedToLoad_mC8DA38B33A6755B30CF3DF6F29D2E77CA08FC20F,
	RewardedAdClient_remove_OnAdFailedToLoad_m5A2C4D9F453C271DC9D1C2C08276435BCF35BB78,
	RewardedAdClient_add_OnUserEarnedReward_m1DA138A415FE07DF39ACA5053111C3A2ACCD8268,
	RewardedAdClient_remove_OnUserEarnedReward_m29BDC518B0F40E5335420813AE55A9EEB398163D,
	RewardedAdClient_add_OnPaidEvent_m7D6681D78940E98D8EBDED77CE4E1B1049FFE9B3,
	RewardedAdClient_remove_OnPaidEvent_m0D5974299A995F5B1229189CDB85E68F2190D2E3,
	RewardedAdClient_add_OnAdFailedToPresentFullScreenContent_m6C110E08A38CB067BF8F90C087E4261EBE2F17C8,
	RewardedAdClient_remove_OnAdFailedToPresentFullScreenContent_mE986CFC68128090F1E3D6EE13704BB00084FAED1,
	RewardedAdClient_add_OnAdDidPresentFullScreenContent_mEB5B7419DD10F4CAF041C55E0D37B39665406652,
	RewardedAdClient_remove_OnAdDidPresentFullScreenContent_m81E4445B6AE6D6C684890532B7A43D641CD88422,
	RewardedAdClient_add_OnAdDidDismissFullScreenContent_m3C788DE482D095DB11E97B44E0A3672A2387B16E,
	RewardedAdClient_remove_OnAdDidDismissFullScreenContent_mBF976A09DE25B694C311CA2E08862EE031188C85,
	RewardedAdClient_add_OnAdDidRecordImpression_m5D4B9F52C51BE30604FA68650BD5E2E521FE9D5A,
	RewardedAdClient_remove_OnAdDidRecordImpression_m2D72A89C2B4FCB4CCB5A4220FAA303DF3526D538,
	RewardedAdClient_CreateRewardedAd_mD4A7B0FA3A2B4831B4950A542E251C4FA37838B0,
	RewardedAdClient_LoadAd_m902E23054ACC0A250B3E847841A9D5AC6FFA3C25,
	RewardedAdClient_Show_m2964AA99F1680A74023A56CC206DFD03B246053B,
	RewardedAdClient_SetServerSideVerificationOptions_mCEEE19008C4A0CF293AF4C9F606B64B13EF5CB0C,
	RewardedAdClient_GetRewardItem_m4708F663E9050B5C3694471738819511EFA6464E,
	RewardedAdClient_GetResponseInfoClient_m6379FCC42D0055AF4021C95A848187E1CD613B32,
	RewardedAdClient_DestroyRewardedAd_mD6C49E7B474F492B5301A0FEFF941A5C133C437F,
	RewardedAdClient_onRewardedAdLoaded_mAAED7D5F3DF0C38876142A38DC9BEF6253B28CC5,
	RewardedAdClient_onRewardedAdFailedToLoad_m72B4A3544B0859C02FAB0EFA44E6173F621E4151,
	RewardedAdClient_onAdFailedToShowFullScreenContent_m6FBCAC0B293CB5C46B8313C82D8AFC6D7E47B122,
	RewardedAdClient_onAdShowedFullScreenContent_m40141ABD14D489CD48544979F206D43BD68A0304,
	RewardedAdClient_onAdDismissedFullScreenContent_m0382791A745BBB32FBB6EEC70528CFB3D0A4A859,
	RewardedAdClient_onAdImpression_m87361B61CF97B889A0124D2F741FF6A8BD1E26F6,
	RewardedAdClient_onUserEarnedReward_m5AAE1BC8646931D4B03F5869318E891F4DBF0F2D,
	RewardedAdClient_onPaidEvent_m828ECC4AFF9B0798BCD6D8612099C3C5DB29510C,
	RewardedInterstitialAdClient__ctor_m217C6154172A23DCA325BCAF84FA49187AC33082,
	RewardedInterstitialAdClient_add_OnAdLoaded_m968959A2CC30EF53E29B44EC0838659BDDB70248,
	RewardedInterstitialAdClient_remove_OnAdLoaded_m4B67E2E8F17B50E06146535AF2062EB216A06FC9,
	RewardedInterstitialAdClient_add_OnAdFailedToLoad_mBA3B1E94D189229B7BAF3563CEC9C5AC7B864193,
	RewardedInterstitialAdClient_remove_OnAdFailedToLoad_m668F1EF6B8DBD31464C67EA1C96B040A1ADA4B92,
	RewardedInterstitialAdClient_add_OnUserEarnedReward_mF0140C9E0ABED9D3470963A81AB1A1C64EAF08B4,
	RewardedInterstitialAdClient_remove_OnUserEarnedReward_mC25332BF4E680657F6172E44E02AE483A5CE8A96,
	RewardedInterstitialAdClient_add_OnPaidEvent_m2553DF6D4E263A1596985BA9352BFA18752002DC,
	RewardedInterstitialAdClient_remove_OnPaidEvent_mB02A83A63A885F617D4699A90EB8F0CDF553A219,
	RewardedInterstitialAdClient_add_OnAdFailedToPresentFullScreenContent_mFE5FDC6F80BFC552F00194EDA0BF013A091E65B5,
	RewardedInterstitialAdClient_remove_OnAdFailedToPresentFullScreenContent_m30E8A0D43F62B4FE1337326449C23C02A3911ABB,
	RewardedInterstitialAdClient_add_OnAdDidPresentFullScreenContent_m3711721FD59FF6C8BA501DE386026D68F2CDBE13,
	RewardedInterstitialAdClient_remove_OnAdDidPresentFullScreenContent_m891226C282A4F760E50F31F9ADA32D82DC912A4C,
	RewardedInterstitialAdClient_add_OnAdDidDismissFullScreenContent_mA8CCBB2710FBEBEEAC3B83B94317FEFBE928327F,
	RewardedInterstitialAdClient_remove_OnAdDidDismissFullScreenContent_mD65CEC1AFA011A92FB56985D638E18CBD834AFAE,
	RewardedInterstitialAdClient_add_OnAdDidRecordImpression_m34D54ED09D21E780906A3CA3D4CE5D2EF4433C3B,
	RewardedInterstitialAdClient_remove_OnAdDidRecordImpression_m4D14BC29FFC171331AD843C457B19A1EA15A5A7A,
	RewardedInterstitialAdClient_CreateRewardedInterstitialAd_mE4D63DD94D3583EE0D729569817CECDA88D1C3F7,
	RewardedInterstitialAdClient_LoadAd_m73A34C9C46251F72EFD8C44F16F4E5EB95BF67CD,
	RewardedInterstitialAdClient_Show_m6BF69BAA35CAED180464AD7383F2CDC0546BD0E2,
	RewardedInterstitialAdClient_GetRewardItem_m0840847B8EFE45AA6F2DE49FE82F41FDA4DD187E,
	RewardedInterstitialAdClient_SetServerSideVerificationOptions_m502BFA362530765077B3626DCF6D923AB5BB1253,
	RewardedInterstitialAdClient_GetResponseInfoClient_mCB9E9A9BF3769684E5E22137A1004387309D4717,
	RewardedInterstitialAdClient_DestroyRewardedInterstitialAd_mBAD2FEE33F70B29787E4E2490DA50563670A6766,
	RewardedInterstitialAdClient_onRewardedInterstitialAdLoaded_m18C8920BFD03A1E06AC6BFC1B92E30ADCBCDF892,
	RewardedInterstitialAdClient_onRewardedInterstitialAdFailedToLoad_m42E484B59E53D8CCB4A8FABC03630432856A766C,
	RewardedInterstitialAdClient_onAdFailedToShowFullScreenContent_mDB87DBAF1AD20870CD10B393D69C04F68C9B509E,
	RewardedInterstitialAdClient_onAdShowedFullScreenContent_m2B1E59093B0AEE23B46B7DFE1AA374620788004A,
	RewardedInterstitialAdClient_onAdDismissedFullScreenContent_m58749B308F8120B6F7A53FF9E5F6587B0D6C7871,
	RewardedInterstitialAdClient_onAdImpression_m8FCA5FC0BEBEB209B21254A10A798D1FD2EBE397,
	RewardedInterstitialAdClient_onUserEarnedReward_m0A30F509548D8A374BD4EAE9C2DE03B11E6B600E,
	RewardedInterstitialAdClient_onPaidEvent_mA60FC5BB6F6EBE6E8E0C801B466F996F1D76B8E8,
	Utils__ctor_m23A1DDF17555E55A6233877FC4EA1671C478DE43,
	Utils_GetAdSizeJavaObject_mD15CDAE9794E13A3A74FA87E8FBC05F807DADAC0,
	Utils_GetAppOpenAdOrientation_m3022EFF96AC0A985BF11811A3210D8996BD9DDC4,
	Utils_GetScreenWidth_m8AAEAC651D3ABEEEFEDCBD7E9288772FB69609EC,
	Utils_GetAdRequestJavaObject_mE20988FEE8A63BCD55DF91D7BA786D19D444800A,
	Utils_GetJavaListObject_mE6FD73C41C672A5D9213354B5AB71C56B5B5F24C,
	Utils_GetCsTypeList_m51894CF039AAE81DBE01D37ECEA4F39C1A7AD6F4,
	Utils_GetServerSideVerificationOptionsJavaObject_m36BD4FEE8AFB67A393807022530683C2E65ED500,
};
static const int32_t s_InvokerIndices[208] = 
{
	2563,
	2985,
	3002,
	3002,
	3002,
	3002,
	2563,
	2563,
	2563,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1031,
	3068,
	3002,
	3068,
	3068,
	2563,
	2563,
	3068,
	3068,
	3068,
	994,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	1031,
	661,
	2563,
	3068,
	3068,
	3068,
	3029,
	3029,
	2548,
	1443,
	3002,
	3068,
	2563,
	3068,
	3068,
	994,
	3068,
	3029,
	2588,
	2985,
	2548,
	2985,
	2548,
	3068,
	3002,
	3002,
	3002,
	3002,
	3002,
	3002,
	2563,
	2029,
	3002,
	3002,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3068,
	3068,
	3002,
	3068,
	2563,
	2563,
	3068,
	3068,
	3068,
	994,
	2563,
	2985,
	3002,
	3002,
	3002,
	3002,
	3002,
	3068,
	4788,
	2563,
	2588,
	3068,
	2585,
	2563,
	3002,
	2585,
	2563,
	3029,
	2985,
	2563,
	4807,
	3068,
	4601,
	4601,
	1458,
	3002,
	3002,
	3002,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3068,
	2563,
	3002,
	3002,
	3068,
	3068,
	2563,
	2563,
	3068,
	3068,
	3068,
	1570,
	994,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3068,
	3002,
	2563,
	3002,
	3068,
	3068,
	2563,
	2563,
	3068,
	3068,
	3068,
	1570,
	994,
	3068,
	4601,
	4520,
	4783,
	4208,
	4601,
	4601,
	4601,
};
extern const CustomAttributesCacheGenerator g_GoogleMobileAds_Android_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_GoogleMobileAds_Android_CodeGenModule;
const Il2CppCodeGenModule g_GoogleMobileAds_Android_CodeGenModule = 
{
	"GoogleMobileAds.Android.dll",
	208,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_GoogleMobileAds_Android_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

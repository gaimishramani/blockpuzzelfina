﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Unity.Services.Core.InitializationOptions Unity.Services.Core.Environments.EnvironmentsOptionsExtensions::SetEnvironmentName(Unity.Services.Core.InitializationOptions,System.String)
extern void EnvironmentsOptionsExtensions_SetEnvironmentName_m6F6C51A40D173AC245E522292E22E857C51555CB (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	EnvironmentsOptionsExtensions_SetEnvironmentName_m6F6C51A40D173AC245E522292E22E857C51555CB,
};
static const int32_t s_InvokerIndices[1] = 
{
	4208,
};
extern const CustomAttributesCacheGenerator g_Unity_Services_Core_Environments_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_Services_Core_Environments_CodeGenModule;
const Il2CppCodeGenModule g_Unity_Services_Core_Environments_CodeGenModule = 
{
	"Unity.Services.Core.Environments.dll",
	1,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_Services_Core_Environments_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

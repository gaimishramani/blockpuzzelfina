﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BaseGamePlay_tD2B2BCC868E8810CC482F905CC5DBF704FFD148E_CustomAttributesCacheGenerator_BaseGamePlay_ScoreAddAnimationCo_m3C24F4BF626C5B7AC26219B79F212308C9B5FE81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_0_0_0_var), NULL);
	}
}
static void BaseGamePlay_tD2B2BCC868E8810CC482F905CC5DBF704FFD148E_CustomAttributesCacheGenerator_BaseGamePlay_SettingPannelSetActive_m0BFC6B7C772CFE426E248E7103D7F94EC3523203(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_0_0_0_var), NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18__ctor_m8EC82519287ECAAFBA39C5CEE63FD15688248F04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_IDisposable_Dispose_m666951E2C45AA724446E55B254071A4D1EED0B72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF83A5DBD4F110E0F7FFCA7334F7C7A14443B7A45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_Reset_m5504B8BF34108E89426ED5A1242AFFC96C8BE280(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_get_Current_m7686727706784EA45AC761E89C88713861A91708(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21__ctor_m670E0B3DDFD7550DDFF55DA58A34E83E355128F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_IDisposable_Dispose_m34D2D4427AAC05311069E68B47F303E3A62815DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B6FD2D99C4DEDED021B9555C09DE73BE432BFE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_Reset_mFA1700E5C0151AC8623508B6489EFC0ADE514F52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_get_Current_m211E5C8CA6F25079022FB27F7454CCAB6C34977C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t4AC729552355147E1903875E89EF315CC14CB45E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimerDataSO_tA997AD32C8C76299D942F78A1DD6C3E9CC1496A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x72\x44\x61\x74\x61\x53\x4F"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x2F\x54\x69\x6D\x65\x72\x44\x61\x74\x61\x53\x4F"), NULL);
	}
}
static void TimerDataSO_tA997AD32C8C76299D942F78A1DD6C3E9CC1496A1_CustomAttributesCacheGenerator_time(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x69\x6E\x20\x53\x65\x63"), NULL);
	}
}
static void ScoreAnimation_tEF6C58007312804427EDE40AB28C0BDA726B9D6E_CustomAttributesCacheGenerator_ScoreAnimation_WaitAndStopAnimation_mE9E35EA4A41BE2E5A27363BEA368687D29635B7F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_0_0_0_var), NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5__ctor_m56D36877C16C8DBEAFECDD08F7BBF531EBD56CA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_IDisposable_Dispose_mFEDE48E928634E276BAD35BD2DBB38B27E8783C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE3B1F8C83B81B6EA4CB171EF8596703449793D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_Reset_mF3496B9B1A7A6533F3E659B5CA2DCC025B1F012D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m545B77BD53146A74099EEF30F7C835C85F83A85E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t65FD208A4160A3C932191B13EF6B7D12A5F58A10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GridUIManager_tD5BF0DDD51192B2745B2144EC2E48B209E172C3E_CustomAttributesCacheGenerator_bgSpriteRender(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridUIManager_tD5BF0DDD51192B2745B2144EC2E48B209E172C3E_CustomAttributesCacheGenerator_poliminoBgSpriteRender(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MultiKeyDictionary_3_t2B1CC96845A3BD779954B5A61D8CBE4650294456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void Reporter_t5BB849345D261779B8942BF5898916A58DF7CDFF_CustomAttributesCacheGenerator_show(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Reporter_t5BB849345D261779B8942BF5898916A58DF7CDFF_CustomAttributesCacheGenerator_Reporter_readInfo_m96DE3344D84AEC06C476B18794B3A3C82E58E737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_0_0_0_var), NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188__ctor_m1074CD57ECB5282678BE9366813C0FD134F3C04A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m7FA47F089E3E44C215AA02F56A71F03F179AE492(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6964659507DB03DC202BE8B05A79F8E228D1807(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_m7950A1C07E9941FC6F83BD1E1DF4B14FA26A7C2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m5A3DFB2378273D241AE9C3EF98DF3FAC754AA2EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SafeArea_tB1F982A1F3E734A124A8B2BC71E94045C1AD4023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void BasePolyomino_t8FD18C69B728C579D94DCF5D2A91A271473C7D17_CustomAttributesCacheGenerator_BasePolyomino_LearpObject_m1E73D023FAFB6FBE91413439AE665FD2797A72F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_0_0_0_var), NULL);
	}
}
static void BasePolyomino_t8FD18C69B728C579D94DCF5D2A91A271473C7D17_CustomAttributesCacheGenerator_BasePolyomino_WaitAndDestroyCO_m21B75FEF605879F8BA9E42FE2CE1C72F8742C02F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_0_0_0_var), NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14__ctor_m9C780924431DA2807CB8A33945EC0E5FBA9E740B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_IDisposable_Dispose_m4657DFB93555CB9FB7B11CA58BD68707F96B9D7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA34BF89B49E1F65604B719E972471D5670115E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_Reset_m6C440EB8EC4B4D64DA392E5471EFE768DA7BB43D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_get_Current_m698F9838A2D8A51C7F991185F04BED54EB77AC11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15__ctor_m0D93DF3C044D855E7EB240C71A66DB98B8D67862(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_IDisposable_Dispose_m9E461FEC944FCCB2BA19375FD03BAD8463CD97B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B6C2664DA2E0F1C690B6C6DD52864E10B36058C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_Reset_m048D08D06E80BB24A37CCE345F2524C94B1F9E02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_get_Current_m14BF06A405A57EF85B5FDE966414B3C2751A031C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t4AB491D7ABE68EAB1D4095568D3406E72C749327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_WaitAndSetSprite_m316CE6E8DC6664F84EED514C63EE471CF8318EA6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_0_0_0_var), NULL);
	}
}
static void Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_DestroyBlockCO_m1D911C9BE4279B4450C6BC18D2D76E4807594B95(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_0_0_0_var), NULL);
	}
}
static void Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_U3CDestroyBlocksU3Eb__8_0_m5253B5681B09948DFC858E3B6D56BF568D3A1C2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_U3CSetCellU3Eb__9_0_m89A6DF9BC810C8151061BC242CC103CF3FF1AAE3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t21F4FE864F6400E906649FB180B9EE03A51F1AE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20__ctor_m8B991F753CEBCCE65339C24F6B4C3F4BE799FB1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_IDisposable_Dispose_m94B304D9922828FE66F1CBA3B1322BEE2CC514DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8E8B6C83FDCA0C021EF13E15DB317AF200BACC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_Reset_m2F3B06310784C6ADECB39F0CCBDD9C940B5F25CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_get_Current_m47C74139557B5ED8AD836B7B376879EC7F6C6422(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21__ctor_m2657C6103D4DA7EECA4CB3DABBAB7A9958226226(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_IDisposable_Dispose_m9404FFD07E53A22FD55D8E0CBDB23054491CC476(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2B4BF82C64017FBF76034F6FDEEB7F8654E0E70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_Reset_m3E4B139B7532EB2465B72E6396E6CDB180F27BD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_get_Current_m591535103E9317CB93B8745E6660AC4315822E36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_WaitAndHint_mFE2AB7C6FE5E037F2EEB0C1633897F45492F3068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_0_0_0_var), NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_SetUp_mD425ECAC66C97FEFFD381FF1EDA3C45E5158482F(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x55\x70"), NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_PolyominoBlockPosSetUp_m80E2F0978B150BF93858BD4B2772547F4AA29D7D(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x42\x6C\x6F\x63\x6B\x20\x50\x6F\x73"), NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_0_m393E9055C749DADD1826522B462FEB9D6BA4BF55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_2_mFE1FED896F0D6218C9A3064E5E4DD2AD647CED5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_1_m34A618CE2D1E31F800CAEDEF62DA355D0D13E0F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_tCD947B4DBBC97A6F7BCC5AAAA627AE79E079DACC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20__ctor_m8AE8440CB025905B9A795A658BCA376BE72D3EF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_IDisposable_Dispose_mFE3F40C5A75650729FDF2FA8D5122E448F216CB1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0680968ACA99E565FAA2AE7DF6B0332309C98C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_Reset_m7CB3DAD8F63D190EA692853C0D3CAA843040569A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_get_Current_mA9C88B63A4ED03143C2B09622F5930DA10263ADF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t0518C03335B25C140907224112D529EE7BED1EDC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_t1CEEAD078E7958A9BEF0FA3568515FCBEB124C13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_t63CC6C82833A647290B9C70B7FB474948CF764BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_t185ED1AF3A0595D65EE2DB115634FED47DBF4B83_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimerBasedGamePlay_tC1D97C3324F6ADC92E7096F1DD657A282332774B_CustomAttributesCacheGenerator_TimerBasedGamePlay_TimerCo_mB4235BDB64294813F17CA6C8468A6E7573350AC9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_0_0_0_var), NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5__ctor_m7C0CAF6DFDC89E73368768DCE0A7D0891C8909C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_IDisposable_Dispose_m56059E0A8A9CFB18236BDCB78DF67BCF116D57D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DA6D2BE5096A517728D1CE497E334E0555196F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_IEnumerator_Reset_mB3FA62DE93F2E10646F763FD243FF892CCB3CAF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_IEnumerator_get_Current_m8DD692E1E16E3D5191F940125B6CE7543963737B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CameraManager_t46C26ADE328E1EBFF5607A56C1A5B7892DA8ABD1_CustomAttributesCacheGenerator_requiredGameplayBounds(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x75\x6E\x64\x73\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void InputManager_t0DC0D23AD06A09CCA2359048FBE163CC38218714_CustomAttributesCacheGenerator_InputManager_U3CCheckHitObjectU3Eb__11_0_m3839724C17AF4AEB0F23BB4D8C2069F702567F01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF397EE71D9124F64D5F331579772525048DE1B38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB5E9A00B304E893EB15230BEA09F22B43358E271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorData_t0949602303447CEC08F4BF8C7802064EB71C2321_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x2F\x43\x6F\x6C\x6F\x72\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tDFB05BE4B7B06BDB16489ED31C7E7C1F350A7BEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MaterialDataSO_t7FAFC547669046918B21F036578F6A84076C9BC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x44\x61\x74\x61\x53\x4F"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x2F\x4D\x61\x74\x65\x72\x69\x61\x6C\x44\x61\x74\x61\x53\x4F"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t7F6A87EB8D5F7186EDD97CBFB8C40CACEFC0CEAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PolyominoData_tE29540B9C9ED2631ED99D652B9580E78EF362EA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x6C\x79\x6F\x6D\x69\x6E\x6F\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x2F\x50\x6F\x6C\x79\x6F\x6D\x69\x6E\x6F\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 2LL, NULL);
	}
}
static void HalloweenHomeView_tB7545B3FF465D3B070BE932BABE090C6354392A7_CustomAttributesCacheGenerator_HalloweenHomeView_ShareAndroidText_mA26D80EDCEE60FA4BB6174D054FE7C68FB8FCCB5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_0_0_0_var), NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0__ctor_m48577E7AEE8D5EF769B60039753B00923B4F4D0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_IDisposable_Dispose_m83E9BB33539619ABF1F7D0FD653F0A3C82873C79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7293609AAF42E7BEEA3D708D3870D5539196F74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_Reset_mCBEF07A86217C7241C3922903833A00169C5A61B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_get_Current_mE6506CB9E172A57BE3F6F7F673D200C2164B9D6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_haloweenBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_christmasBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_esterBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_haloweenBlockBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_christmasBlockBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_esterBlockBGSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_bGImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_bgimageforBlock(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_hellowinBlockBG(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_chrismasBlockBG(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_easterBlockBG(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AdManager_tEAEC8AF53829C449EB7B2ACE5DA811A1B649A6D9_CustomAttributesCacheGenerator_interstitialAd(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AdManager_tEAEC8AF53829C449EB7B2ACE5DA811A1B649A6D9_CustomAttributesCacheGenerator_rewardAd(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tB0DA5D629062EC459A00150CF1EA652EB5184622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_U3CIsInWaitingForLoadAdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_get_IsInWaitingForLoadAd_m5114D97AE42B31734F05691958CB51593107846A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_set_IsInWaitingForLoadAd_mF7FB0DCE6E53F38285C568767983FBE17A6DD40F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_HandleRewardedAd_m9F7FD689AFDA9120DB3361E106EBFC55492B5DE7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_0_0_0_var), NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13__ctor_m02BE4FDF06BBE5BD0CE25900C3140BD75B5023F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_IDisposable_Dispose_mB5B980BA7922C6E36C6A04D346E8C48ED7039468(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFF8EF072C53C6BB648D0A57E0CCE523D2CD075B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_Reset_m617931E8D8470D9FBA9537D3D4EBD5880B76B845(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_get_Current_m062DD8F084A5E3FAADCCC5E78DF6A77B0BEF2326(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_products(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_adsProductId(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator__isRemoteInited(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_OnPurchaseSuccess(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_OnPurchaseCancel(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_add_OnPurchaseSuccess_m5770136C682BCBD528FA184FA5CDB28E0271E657(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_remove_OnPurchaseSuccess_m976C6D79491D93B215CFEEE7C510A252D3C7282B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_add_OnPurchaseCancel_mF70A1C83B724A55EB6AA44148393A1E86AE26DFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_remove_OnPurchaseCancel_m80BA81676142BD8AAFBE953E060FF9DD24E05290(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_Awake_mAB98EA00323B1EA23BA2E40E9E3805895195CEC9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_0_0_0_var), NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_InitRoutine_m491B305A643C997913E216D26127A2A3A9CE8D7F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_0_0_0_var), NULL);
	}
}
static void IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_WaitForInternetCheckBoughtProducts_mE7920CF75BB949A0A8793B65579E6CD29E1A1E43(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_0_0_0_var), NULL);
	}
}
static void U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_CustomAttributesCacheGenerator_U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21__ctor_mAEFF8084A9F8B86448E061EF04B768F1437D4AC9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_IDisposable_Dispose_m711DF51248BAA1DED0B7C429C1FE4F3C0FD4952C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C0EB3766FB22F1052F0A8B652861373D6836697(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_Reset_m44D85717BECADB3DF040C3E30DB95EBBF5156493(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA6E8778BFB06FF1C798CB6A051649066FA1B369B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t47D64C165614A6711C16B03B7F034F27868C3DA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39__ctor_m71B9CEDF53BDC71546A62B488B91351CC449F4D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_IDisposable_Dispose_m57524CDD0E864C5A6581FCF33725B5A134D353D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m526879C01B18A1C91F35EF33F1D10246AA88BF62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_Reset_mA9D2ED022B84D573757B25E8D10C3BA19FC84C76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_get_Current_m703564928F8B8E4DC10D7B3F53259ABCFDF1F602(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[161] = 
{
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator,
	U3CU3Ec_t4AC729552355147E1903875E89EF315CC14CB45E_CustomAttributesCacheGenerator,
	TimerDataSO_tA997AD32C8C76299D942F78A1DD6C3E9CC1496A1_CustomAttributesCacheGenerator,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator,
	U3CU3Ec_t65FD208A4160A3C932191B13EF6B7D12A5F58A10_CustomAttributesCacheGenerator,
	MultiKeyDictionary_3_t2B1CC96845A3BD779954B5A61D8CBE4650294456_CustomAttributesCacheGenerator,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator,
	SafeArea_tB1F982A1F3E734A124A8B2BC71E94045C1AD4023_CustomAttributesCacheGenerator,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator,
	U3CU3Ec_t4AB491D7ABE68EAB1D4095568D3406E72C749327_CustomAttributesCacheGenerator,
	U3CU3Ec_t21F4FE864F6400E906649FB180B9EE03A51F1AE7_CustomAttributesCacheGenerator,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_tCD947B4DBBC97A6F7BCC5AAAA627AE79E079DACC_CustomAttributesCacheGenerator,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t0518C03335B25C140907224112D529EE7BED1EDC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_t1CEEAD078E7958A9BEF0FA3568515FCBEB124C13_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_t63CC6C82833A647290B9C70B7FB474948CF764BB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_t185ED1AF3A0595D65EE2DB115634FED47DBF4B83_CustomAttributesCacheGenerator,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator,
	U3CU3Ec_tF397EE71D9124F64D5F331579772525048DE1B38_CustomAttributesCacheGenerator,
	U3CU3Ec_tB5E9A00B304E893EB15230BEA09F22B43358E271_CustomAttributesCacheGenerator,
	ColorData_t0949602303447CEC08F4BF8C7802064EB71C2321_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tDFB05BE4B7B06BDB16489ED31C7E7C1F350A7BEA_CustomAttributesCacheGenerator,
	MaterialDataSO_t7FAFC547669046918B21F036578F6A84076C9BC3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t7F6A87EB8D5F7186EDD97CBFB8C40CACEFC0CEAA_CustomAttributesCacheGenerator,
	PolyominoData_tE29540B9C9ED2631ED99D652B9580E78EF362EA7_CustomAttributesCacheGenerator,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator,
	U3CU3Ec_tB0DA5D629062EC459A00150CF1EA652EB5184622_CustomAttributesCacheGenerator,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator,
	U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_CustomAttributesCacheGenerator,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator,
	U3CU3Ec_t47D64C165614A6711C16B03B7F034F27868C3DA2_CustomAttributesCacheGenerator,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator,
	TimerDataSO_tA997AD32C8C76299D942F78A1DD6C3E9CC1496A1_CustomAttributesCacheGenerator_time,
	GridUIManager_tD5BF0DDD51192B2745B2144EC2E48B209E172C3E_CustomAttributesCacheGenerator_bgSpriteRender,
	GridUIManager_tD5BF0DDD51192B2745B2144EC2E48B209E172C3E_CustomAttributesCacheGenerator_poliminoBgSpriteRender,
	Reporter_t5BB849345D261779B8942BF5898916A58DF7CDFF_CustomAttributesCacheGenerator_show,
	CameraManager_t46C26ADE328E1EBFF5607A56C1A5B7892DA8ABD1_CustomAttributesCacheGenerator_requiredGameplayBounds,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_haloweenBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_christmasBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_esterBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_haloweenBlockBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_christmasBlockBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_esterBlockBGSprite,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_bGImage,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_bgimageforBlock,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_hellowinBlockBG,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_chrismasBlockBG,
	ThemeSelectionView_t826715AD6B7BB9F76EE8D7C9D85CA2AF4BE8DB26_CustomAttributesCacheGenerator_easterBlockBG,
	AdManager_tEAEC8AF53829C449EB7B2ACE5DA811A1B649A6D9_CustomAttributesCacheGenerator_interstitialAd,
	AdManager_tEAEC8AF53829C449EB7B2ACE5DA811A1B649A6D9_CustomAttributesCacheGenerator_rewardAd,
	RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_U3CIsInWaitingForLoadAdU3Ek__BackingField,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_products,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_adsProductId,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator__isRemoteInited,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_OnPurchaseSuccess,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_OnPurchaseCancel,
	BaseGamePlay_tD2B2BCC868E8810CC482F905CC5DBF704FFD148E_CustomAttributesCacheGenerator_BaseGamePlay_ScoreAddAnimationCo_m3C24F4BF626C5B7AC26219B79F212308C9B5FE81,
	BaseGamePlay_tD2B2BCC868E8810CC482F905CC5DBF704FFD148E_CustomAttributesCacheGenerator_BaseGamePlay_SettingPannelSetActive_m0BFC6B7C772CFE426E248E7103D7F94EC3523203,
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18__ctor_m8EC82519287ECAAFBA39C5CEE63FD15688248F04,
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_IDisposable_Dispose_m666951E2C45AA724446E55B254071A4D1EED0B72,
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF83A5DBD4F110E0F7FFCA7334F7C7A14443B7A45,
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_Reset_m5504B8BF34108E89426ED5A1242AFFC96C8BE280,
	U3CScoreAddAnimationCoU3Ed__18_tD25C68111770694511ADD19A53B5B01E07A067D4_CustomAttributesCacheGenerator_U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_get_Current_m7686727706784EA45AC761E89C88713861A91708,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21__ctor_m670E0B3DDFD7550DDFF55DA58A34E83E355128F6,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_IDisposable_Dispose_m34D2D4427AAC05311069E68B47F303E3A62815DF,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B6FD2D99C4DEDED021B9555C09DE73BE432BFE9,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_Reset_mFA1700E5C0151AC8623508B6489EFC0ADE514F52,
	U3CSettingPannelSetActiveU3Ed__21_t69E777AA1B7158D7557F4981B52A064C27FD64D7_CustomAttributesCacheGenerator_U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_get_Current_m211E5C8CA6F25079022FB27F7454CCAB6C34977C,
	ScoreAnimation_tEF6C58007312804427EDE40AB28C0BDA726B9D6E_CustomAttributesCacheGenerator_ScoreAnimation_WaitAndStopAnimation_mE9E35EA4A41BE2E5A27363BEA368687D29635B7F,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5__ctor_m56D36877C16C8DBEAFECDD08F7BBF531EBD56CA7,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_IDisposable_Dispose_mFEDE48E928634E276BAD35BD2DBB38B27E8783C3,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE3B1F8C83B81B6EA4CB171EF8596703449793D,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_Reset_mF3496B9B1A7A6533F3E659B5CA2DCC025B1F012D,
	U3CWaitAndStopAnimationU3Ed__5_t5539CD83E34A95328548335BF8D042A09429D49C_CustomAttributesCacheGenerator_U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m545B77BD53146A74099EEF30F7C835C85F83A85E,
	Reporter_t5BB849345D261779B8942BF5898916A58DF7CDFF_CustomAttributesCacheGenerator_Reporter_readInfo_m96DE3344D84AEC06C476B18794B3A3C82E58E737,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188__ctor_m1074CD57ECB5282678BE9366813C0FD134F3C04A,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m7FA47F089E3E44C215AA02F56A71F03F179AE492,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6964659507DB03DC202BE8B05A79F8E228D1807,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_m7950A1C07E9941FC6F83BD1E1DF4B14FA26A7C2E,
	U3CreadInfoU3Ed__188_t16AFA6F05AE5D3BA84D48FCE0F624F31CF6E6F60_CustomAttributesCacheGenerator_U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m5A3DFB2378273D241AE9C3EF98DF3FAC754AA2EA,
	BasePolyomino_t8FD18C69B728C579D94DCF5D2A91A271473C7D17_CustomAttributesCacheGenerator_BasePolyomino_LearpObject_m1E73D023FAFB6FBE91413439AE665FD2797A72F0,
	BasePolyomino_t8FD18C69B728C579D94DCF5D2A91A271473C7D17_CustomAttributesCacheGenerator_BasePolyomino_WaitAndDestroyCO_m21B75FEF605879F8BA9E42FE2CE1C72F8742C02F,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14__ctor_m9C780924431DA2807CB8A33945EC0E5FBA9E740B,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_IDisposable_Dispose_m4657DFB93555CB9FB7B11CA58BD68707F96B9D7B,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA34BF89B49E1F65604B719E972471D5670115E0,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_Reset_m6C440EB8EC4B4D64DA392E5471EFE768DA7BB43D,
	U3CLearpObjectU3Ed__14_tEB79F08EC241A34A56B30DCC0A37CD655B24CF84_CustomAttributesCacheGenerator_U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_get_Current_m698F9838A2D8A51C7F991185F04BED54EB77AC11,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15__ctor_m0D93DF3C044D855E7EB240C71A66DB98B8D67862,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_IDisposable_Dispose_m9E461FEC944FCCB2BA19375FD03BAD8463CD97B1,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B6C2664DA2E0F1C690B6C6DD52864E10B36058C,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_Reset_m048D08D06E80BB24A37CCE345F2524C94B1F9E02,
	U3CWaitAndDestroyCOU3Ed__15_t26781C89C4ED21A319E407D3B4D8F35F8056FBFC_CustomAttributesCacheGenerator_U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_get_Current_m14BF06A405A57EF85B5FDE966414B3C2751A031C,
	Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_WaitAndSetSprite_m316CE6E8DC6664F84EED514C63EE471CF8318EA6,
	Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_DestroyBlockCO_m1D911C9BE4279B4450C6BC18D2D76E4807594B95,
	Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_U3CDestroyBlocksU3Eb__8_0_m5253B5681B09948DFC858E3B6D56BF568D3A1C2C,
	Grid_t0703B97CD540F57532841D7F61F3679568F1243B_CustomAttributesCacheGenerator_Grid_U3CSetCellU3Eb__9_0_m89A6DF9BC810C8151061BC242CC103CF3FF1AAE3,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20__ctor_m8B991F753CEBCCE65339C24F6B4C3F4BE799FB1B,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_IDisposable_Dispose_m94B304D9922828FE66F1CBA3B1322BEE2CC514DC,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8E8B6C83FDCA0C021EF13E15DB317AF200BACC,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_Reset_m2F3B06310784C6ADECB39F0CCBDD9C940B5F25CE,
	U3CWaitAndSetSpriteU3Ed__20_tA31334E23B3FE7CA6FE271717D6EC39F8EB430DF_CustomAttributesCacheGenerator_U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_get_Current_m47C74139557B5ED8AD836B7B376879EC7F6C6422,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21__ctor_m2657C6103D4DA7EECA4CB3DABBAB7A9958226226,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_IDisposable_Dispose_m9404FFD07E53A22FD55D8E0CBDB23054491CC476,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2B4BF82C64017FBF76034F6FDEEB7F8654E0E70,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_Reset_m3E4B139B7532EB2465B72E6396E6CDB180F27BD2,
	U3CDestroyBlockCOU3Ed__21_t60D03EFEDD20CD9FD2E60989A61C0C0AA948E1E6_CustomAttributesCacheGenerator_U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_get_Current_m591535103E9317CB93B8745E6660AC4315822E36,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_WaitAndHint_mFE2AB7C6FE5E037F2EEB0C1633897F45492F3068,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_SetUp_mD425ECAC66C97FEFFD381FF1EDA3C45E5158482F,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_PolyominoBlockPosSetUp_m80E2F0978B150BF93858BD4B2772547F4AA29D7D,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_0_m393E9055C749DADD1826522B462FEB9D6BA4BF55,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_2_mFE1FED896F0D6218C9A3064E5E4DD2AD647CED5C,
	SimplePolyomino_t051E930B96B224450610E1DF3843727640AFEE6C_CustomAttributesCacheGenerator_SimplePolyomino_U3COnDropU3Eb__13_1_m34A618CE2D1E31F800CAEDEF62DA355D0D13E0F0,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20__ctor_m8AE8440CB025905B9A795A658BCA376BE72D3EF3,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_IDisposable_Dispose_mFE3F40C5A75650729FDF2FA8D5122E448F216CB1,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0680968ACA99E565FAA2AE7DF6B0332309C98C,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_Reset_m7CB3DAD8F63D190EA692853C0D3CAA843040569A,
	U3CWaitAndHintU3Ed__20_t741F5B0293D23F450B44C79C44B2435DA0DFC291_CustomAttributesCacheGenerator_U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_get_Current_mA9C88B63A4ED03143C2B09622F5930DA10263ADF,
	TimerBasedGamePlay_tC1D97C3324F6ADC92E7096F1DD657A282332774B_CustomAttributesCacheGenerator_TimerBasedGamePlay_TimerCo_mB4235BDB64294813F17CA6C8468A6E7573350AC9,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5__ctor_m7C0CAF6DFDC89E73368768DCE0A7D0891C8909C4,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_IDisposable_Dispose_m56059E0A8A9CFB18236BDCB78DF67BCF116D57D6,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DA6D2BE5096A517728D1CE497E334E0555196F,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_IEnumerator_Reset_mB3FA62DE93F2E10646F763FD243FF892CCB3CAF2,
	U3CTimerCoU3Ed__5_tE1C41219B01D34663EE48A937C00AE2E287AA391_CustomAttributesCacheGenerator_U3CTimerCoU3Ed__5_System_Collections_IEnumerator_get_Current_m8DD692E1E16E3D5191F940125B6CE7543963737B,
	InputManager_t0DC0D23AD06A09CCA2359048FBE163CC38218714_CustomAttributesCacheGenerator_InputManager_U3CCheckHitObjectU3Eb__11_0_m3839724C17AF4AEB0F23BB4D8C2069F702567F01,
	HalloweenHomeView_tB7545B3FF465D3B070BE932BABE090C6354392A7_CustomAttributesCacheGenerator_HalloweenHomeView_ShareAndroidText_mA26D80EDCEE60FA4BB6174D054FE7C68FB8FCCB5,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0__ctor_m48577E7AEE8D5EF769B60039753B00923B4F4D0D,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_IDisposable_Dispose_m83E9BB33539619ABF1F7D0FD653F0A3C82873C79,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7293609AAF42E7BEEA3D708D3870D5539196F74,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_Reset_mCBEF07A86217C7241C3922903833A00169C5A61B,
	U3CShareAndroidTextU3Ed__0_t248C41850919C0986A116E1DD73CD1F9CBDC249F_CustomAttributesCacheGenerator_U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_get_Current_mE6506CB9E172A57BE3F6F7F673D200C2164B9D6E,
	RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_get_IsInWaitingForLoadAd_m5114D97AE42B31734F05691958CB51593107846A,
	RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_set_IsInWaitingForLoadAd_mF7FB0DCE6E53F38285C568767983FBE17A6DD40F,
	RewardAd_tFB67877787DB9EF6F741325FF268851688119C33_CustomAttributesCacheGenerator_RewardAd_HandleRewardedAd_m9F7FD689AFDA9120DB3361E106EBFC55492B5DE7,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13__ctor_m02BE4FDF06BBE5BD0CE25900C3140BD75B5023F5,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_IDisposable_Dispose_mB5B980BA7922C6E36C6A04D346E8C48ED7039468,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFF8EF072C53C6BB648D0A57E0CCE523D2CD075B,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_Reset_m617931E8D8470D9FBA9537D3D4EBD5880B76B845,
	U3CHandleRewardedAdU3Ed__13_t303F0B2B5B4B6534A514D9E6B7422B723C5A9F04_CustomAttributesCacheGenerator_U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_get_Current_m062DD8F084A5E3FAADCCC5E78DF6A77B0BEF2326,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_add_OnPurchaseSuccess_m5770136C682BCBD528FA184FA5CDB28E0271E657,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_remove_OnPurchaseSuccess_m976C6D79491D93B215CFEEE7C510A252D3C7282B,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_add_OnPurchaseCancel_mF70A1C83B724A55EB6AA44148393A1E86AE26DFC,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_remove_OnPurchaseCancel_m80BA81676142BD8AAFBE953E060FF9DD24E05290,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_Awake_mAB98EA00323B1EA23BA2E40E9E3805895195CEC9,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_InitRoutine_m491B305A643C997913E216D26127A2A3A9CE8D7F,
	IAPManager_tFACE38C8ACE62FC524C3B973339E7F96247FD530_CustomAttributesCacheGenerator_IAPManager_WaitForInternetCheckBoughtProducts_mE7920CF75BB949A0A8793B65579E6CD29E1A1E43,
	U3CAwakeU3Ed__19_tA3AD199149978168AA7680F4A22F2C19B2AC206F_CustomAttributesCacheGenerator_U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21__ctor_mAEFF8084A9F8B86448E061EF04B768F1437D4AC9,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_IDisposable_Dispose_m711DF51248BAA1DED0B7C429C1FE4F3C0FD4952C,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C0EB3766FB22F1052F0A8B652861373D6836697,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_Reset_m44D85717BECADB3DF040C3E30DB95EBBF5156493,
	U3CInitRoutineU3Ed__21_t48E619568C820D40DA7A94E1A235B29B909452D5_CustomAttributesCacheGenerator_U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA6E8778BFB06FF1C798CB6A051649066FA1B369B,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39__ctor_m71B9CEDF53BDC71546A62B488B91351CC449F4D9,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_IDisposable_Dispose_m57524CDD0E864C5A6581FCF33725B5A134D353D2,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m526879C01B18A1C91F35EF33F1D10246AA88BF62,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_Reset_mA9D2ED022B84D573757B25E8D10C3BA19FC84C76,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_t4E993F38B22A556391AE2D8C7FA265B77CF36BD8_CustomAttributesCacheGenerator_U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_get_Current_m703564928F8B8E4DC10D7B3F53259ABCFDF1F602,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}

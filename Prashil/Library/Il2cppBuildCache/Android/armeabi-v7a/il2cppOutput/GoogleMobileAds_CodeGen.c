﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GoogleMobileAds.Api.AdError::.ctor(GoogleMobileAds.Common.IAdErrorClient)
extern void AdError__ctor_mC38C346CF8A03E3DC984E12C538738E4FCB67BC3 (void);
// 0x00000002 System.String GoogleMobileAds.Api.AdError::GetMessage()
extern void AdError_GetMessage_m5311104192B08C55BD0E34735E45330FE3E07A54 (void);
// 0x00000003 System.String GoogleMobileAds.Api.AdError::ToString()
extern void AdError_ToString_m31CCDC54DECD29ECA162D855313F0705A2675C52 (void);
// 0x00000004 System.Void GoogleMobileAds.Api.AdErrorEventArgs::.ctor()
extern void AdErrorEventArgs__ctor_mDA9407103CD61DCD3656AA1DA2FC95756EDEFB77 (void);
// 0x00000005 System.Void GoogleMobileAds.Api.AdErrorEventArgs::set_AdError(GoogleMobileAds.Api.AdError)
extern void AdErrorEventArgs_set_AdError_m861B08F310E196385C79F1BC469E5B7D790E881C (void);
// 0x00000006 System.Void GoogleMobileAds.Api.AdFailedToLoadEventArgs::.ctor()
extern void AdFailedToLoadEventArgs__ctor_m4F60827DAE5FFF31835C1AA795ACEDFC859B2301 (void);
// 0x00000007 GoogleMobileAds.Api.LoadAdError GoogleMobileAds.Api.AdFailedToLoadEventArgs::get_LoadAdError()
extern void AdFailedToLoadEventArgs_get_LoadAdError_mE0117EBECCED9601511BD04D9831285267873D93 (void);
// 0x00000008 System.Void GoogleMobileAds.Api.AdFailedToLoadEventArgs::set_LoadAdError(GoogleMobileAds.Api.LoadAdError)
extern void AdFailedToLoadEventArgs_set_LoadAdError_m57575ECA76FBACD3FCF5800A20D23851D86E035C (void);
// 0x00000009 System.Void GoogleMobileAds.Api.BannerView::.ctor(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void BannerView__ctor_mFF864899CD7456880253F63684F1F55B7CFD6366 (void);
// 0x0000000A System.Void GoogleMobileAds.Api.BannerView::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdLoaded_m506B2F7AD83FF1076D85B9F5FFEAC15283016B35 (void);
// 0x0000000B System.Void GoogleMobileAds.Api.BannerView::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdLoaded_mC3F49BC24EFE1A619B3EC55C144E274F708880C1 (void);
// 0x0000000C System.Void GoogleMobileAds.Api.BannerView::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerView_add_OnAdFailedToLoad_m78D418E3049E8C74A9D835FE595D20BC25F1EA0A (void);
// 0x0000000D System.Void GoogleMobileAds.Api.BannerView::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerView_remove_OnAdFailedToLoad_mEAF313631300F9BEA45846990977F8B7199BB348 (void);
// 0x0000000E System.Void GoogleMobileAds.Api.BannerView::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdOpening_mD6C44C170D8C1F9CC0FF30CAA603D0EB691E7A75 (void);
// 0x0000000F System.Void GoogleMobileAds.Api.BannerView::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdOpening_m62D082706E37C149765E67D4D0ED9DCF78A79713 (void);
// 0x00000010 System.Void GoogleMobileAds.Api.BannerView::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdClosed_m772F8BC4B1255E78BED318592766ECA335BD1A9D (void);
// 0x00000011 System.Void GoogleMobileAds.Api.BannerView::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdClosed_m1BA6F60F5939D45580A2BF6297FC008648579259 (void);
// 0x00000012 System.Void GoogleMobileAds.Api.BannerView::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void BannerView_LoadAd_mB9CACDC88AB0BAA99E8C960D5965E58415B87159 (void);
// 0x00000013 System.Void GoogleMobileAds.Api.BannerView::ConfigureBannerEvents()
extern void BannerView_ConfigureBannerEvents_m22D1A80A3BB009314BBA75EEBBA19A9CE8B2A3FF (void);
// 0x00000014 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>m__0(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Em__0_m9211309D6C31C2A40A45EA2CF437E9DB8EF555B8 (void);
// 0x00000015 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>m__1(System.Object,GoogleMobileAds.Common.LoadAdErrorClientEventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Em__1_mE95A0FEB326EE53B1362DFDDEF8D8CCA940B6E06 (void);
// 0x00000016 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>m__2(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Em__2_mD9E55A16C5074FEA65D598928259C6CC0CB6DA15 (void);
// 0x00000017 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>m__3(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Em__3_m6508B7A4FE74974989D045F32224FD7E5E2F0A4E (void);
// 0x00000018 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>m__4(System.Object,GoogleMobileAds.Api.AdValueEventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Em__4_mE30966AA5238B99E6F365C0DAF8B43065E22507A (void);
// 0x00000019 System.Void GoogleMobileAds.Api.InitializationStatus::.ctor(GoogleMobileAds.Common.IInitializationStatusClient)
extern void InitializationStatus__ctor_m203B90A488612CD1257221879B41BB38215D48D2 (void);
// 0x0000001A System.Void GoogleMobileAds.Api.InterstitialAd::.ctor(System.String)
extern void InterstitialAd__ctor_m466F1ABACA88AFBE2E47014092E29981D8954B44 (void);
// 0x0000001B System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdLoaded_mCDBDFB7120A1BF227BC161DFF486D3405BBDA951 (void);
// 0x0000001C System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdLoaded_m50ECDEDBD0DD9A866AB9B3FDD0F52C67A43B8E0C (void);
// 0x0000001D System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialAd_add_OnAdFailedToLoad_mF07D125984B7F813661A1CCB841CECAED255AC45 (void);
// 0x0000001E System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialAd_remove_OnAdFailedToLoad_mC237AED821C39C03DB9F02036282DB6B7963C035 (void);
// 0x0000001F System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdOpening_m3B4C511F24E886ECA2304A1FE35E10A62CB9F939 (void);
// 0x00000020 System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdOpening_m5909D5FF7422C8F9D03F12976907C6E98CED8FBE (void);
// 0x00000021 System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdClosed_mAF5C18E6713BA8DAC7EFE2C134B1D2117AC42C77 (void);
// 0x00000022 System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdClosed_m16767D501D07A33D1C7DA590E8961160654F4BD4 (void);
// 0x00000023 System.Void GoogleMobileAds.Api.InterstitialAd::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void InterstitialAd_add_OnPaidEvent_mD2793E0D90C025A3C8008B17AC24CD6F3E6AE9EE (void);
// 0x00000024 System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void InterstitialAd_remove_OnPaidEvent_m6D1AC36D53E1DB4480A01EBE8027390A5F031DF6 (void);
// 0x00000025 System.Void GoogleMobileAds.Api.InterstitialAd::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void InterstitialAd_LoadAd_mD77748B1D4354B9CE35D3242CBDBEFD92E574BD0 (void);
// 0x00000026 System.Boolean GoogleMobileAds.Api.InterstitialAd::IsLoaded()
extern void InterstitialAd_IsLoaded_m3A41B1D590D97C9FB80C0681C3FFD2B486751ED1 (void);
// 0x00000027 System.Void GoogleMobileAds.Api.InterstitialAd::Show()
extern void InterstitialAd_Show_m4D6142D4E8EB052A48C6869472572AC636FFB00C (void);
// 0x00000028 ResponseInfo GoogleMobileAds.Api.InterstitialAd::GetResponseInfo()
extern void InterstitialAd_GetResponseInfo_m5F4315F3208F0700A36292DEB2F7880AC06CC995 (void);
// 0x00000029 System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__0(System.Object,System.EventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__0_m455A8A5BF6AB2EF58D61F22A8E647FBE43E0A627 (void);
// 0x0000002A System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__1(System.Object,GoogleMobileAds.Common.LoadAdErrorClientEventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__1_m4AB5D26E9730C969FE7DA41C1AF130050D09E865 (void);
// 0x0000002B System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__2(System.Object,System.EventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__2_m8E50A34566DA3E9484FC68007F44C8AB57E8EF23 (void);
// 0x0000002C System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__3(System.Object,System.EventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__3_m847FEEDC73896FC7E4C14EC94518CA12A7A9B8EC (void);
// 0x0000002D System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__4(System.Object,GoogleMobileAds.Common.AdErrorClientEventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__4_m07347F8C1DC5124F710FD328C8DC3DFF2025CBEB (void);
// 0x0000002E System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__5(System.Object,System.EventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__5_m5F4E1BDBF49C353A56F47933F48A17D6ADEAACD7 (void);
// 0x0000002F System.Void GoogleMobileAds.Api.InterstitialAd::<InterstitialAd>m__6(System.Object,GoogleMobileAds.Api.AdValueEventArgs)
extern void InterstitialAd_U3CInterstitialAdU3Em__6_m89B45428576EB8BF288A7EC57208D38F04A6687C (void);
// 0x00000030 System.Void GoogleMobileAds.Api.LoadAdError::.ctor(GoogleMobileAds.Common.ILoadAdErrorClient)
extern void LoadAdError__ctor_m162C74F49A83EDAAE287922598CF7309B65B2872 (void);
// 0x00000031 System.String GoogleMobileAds.Api.LoadAdError::ToString()
extern void LoadAdError_ToString_mCBED4EDB2CB490D8D8E12EB1C2A4FD900F84C601 (void);
// 0x00000032 System.Void GoogleMobileAds.Api.MobileAds::.ctor()
extern void MobileAds__ctor_m2A67D2A60A8C856ACB5E99B4EEFA04F469A00ACF (void);
// 0x00000033 GoogleMobileAds.Api.MobileAds GoogleMobileAds.Api.MobileAds::get_Instance()
extern void MobileAds_get_Instance_mDF4578178A58F587F7393797BF1AD3D9851B1589 (void);
// 0x00000034 System.Void GoogleMobileAds.Api.MobileAds::Initialize(System.Action`1<GoogleMobileAds.Api.InitializationStatus>)
extern void MobileAds_Initialize_m4BE910CE7907E2E236C39DDB4A3CF20CDC015FE9 (void);
// 0x00000035 System.Void GoogleMobileAds.Api.MobileAds::SetRequestConfiguration(GoogleMobileAds.Api.RequestConfiguration)
extern void MobileAds_SetRequestConfiguration_mB481979FE7BB5B89CAD2E3003DC0C1F6C3F002DF (void);
// 0x00000036 System.Void GoogleMobileAds.Api.MobileAds::SetiOSAppPauseOnBackground(System.Boolean)
extern void MobileAds_SetiOSAppPauseOnBackground_m05B6D1D6AA4A0A6583E5473455CD65F2A3B35044 (void);
// 0x00000037 GoogleMobileAds.IClientFactory GoogleMobileAds.Api.MobileAds::GetClientFactory()
extern void MobileAds_GetClientFactory_m4C38A16687444F18A251DB32693C8DD4435968FE (void);
// 0x00000038 GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.Api.MobileAds::GetMobileAdsClient()
extern void MobileAds_GetMobileAdsClient_m83286C3BDE16C4F047D80772C93E282BB68F7E52 (void);
// 0x00000039 System.Void GoogleMobileAds.Api.MobileAds/<Initialize>c__AnonStorey0::.ctor()
extern void U3CInitializeU3Ec__AnonStorey0__ctor_m2E5FBBCA45231800CDB5F7332A27B8747EB7F843 (void);
// 0x0000003A System.Void GoogleMobileAds.Api.MobileAds/<Initialize>c__AnonStorey0::<>m__0(GoogleMobileAds.Common.IInitializationStatusClient)
extern void U3CInitializeU3Ec__AnonStorey0_U3CU3Em__0_m8C8A10194EE35AD423797D6E09A52D65EC2EF163 (void);
// 0x0000003B System.Void ResponseInfo::.ctor(GoogleMobileAds.Common.IResponseInfoClient)
extern void ResponseInfo__ctor_mD74218E313340A4BAD3A5FF302E15D766B7DB8BE (void);
// 0x0000003C System.String ResponseInfo::GetMediationAdapterClassName()
extern void ResponseInfo_GetMediationAdapterClassName_mBCD5EDF7E126EC6228B743F042F881F2E105A7E1 (void);
// 0x0000003D System.String ResponseInfo::ToString()
extern void ResponseInfo_ToString_m3025C2660C30701FE627F35E96508FC58A3104D2 (void);
// 0x0000003E System.Void GoogleMobileAds.Api.RewardedAd::.ctor(System.String)
extern void RewardedAd__ctor_mE6E5D17F0160ABB57C10A5FA1526AEC1B7CEB369 (void);
// 0x0000003F System.Void GoogleMobileAds.Api.RewardedAd::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_add_OnAdLoaded_m33D1098F40BD3ECB8FB880A8D45DF8C013889C54 (void);
// 0x00000040 System.Void GoogleMobileAds.Api.RewardedAd::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_remove_OnAdLoaded_m8D731C34BD0942C0E847E4672919E1F74FE8BEA0 (void);
// 0x00000041 System.Void GoogleMobileAds.Api.RewardedAd::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardedAd_add_OnAdFailedToLoad_m642675F6423DA3C6D3D8AF16F085224C8A102499 (void);
// 0x00000042 System.Void GoogleMobileAds.Api.RewardedAd::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardedAd_remove_OnAdFailedToLoad_mDC10C1667F7433087E51BDF67335C4892CB82704 (void);
// 0x00000043 System.Void GoogleMobileAds.Api.RewardedAd::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_add_OnAdOpening_m925179D731E6149CED42B4A9A5C3FD75070C762B (void);
// 0x00000044 System.Void GoogleMobileAds.Api.RewardedAd::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_remove_OnAdOpening_mE442C11EFB5B767B7140A7BFE4CDBB6F4BAD1367 (void);
// 0x00000045 System.Void GoogleMobileAds.Api.RewardedAd::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_add_OnAdClosed_m5F6E93F7761D4254001B58310E2233712217542C (void);
// 0x00000046 System.Void GoogleMobileAds.Api.RewardedAd::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardedAd_remove_OnAdClosed_m6C4963725130A1A201DEF3981721CC9AA190E230 (void);
// 0x00000047 System.Void GoogleMobileAds.Api.RewardedAd::add_OnAdFailedToShow(System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs>)
extern void RewardedAd_add_OnAdFailedToShow_m44231E772AC4FF0315F68BE9AC2B20271EC72B29 (void);
// 0x00000048 System.Void GoogleMobileAds.Api.RewardedAd::remove_OnAdFailedToShow(System.EventHandler`1<GoogleMobileAds.Api.AdErrorEventArgs>)
extern void RewardedAd_remove_OnAdFailedToShow_m6838DA37C9F675788C27DB0E3229C60167B8BDBF (void);
// 0x00000049 System.Void GoogleMobileAds.Api.RewardedAd::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAd_add_OnUserEarnedReward_mFA8E920C7BB65419DE9ABF366C3353154FE25FCC (void);
// 0x0000004A System.Void GoogleMobileAds.Api.RewardedAd::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAd_remove_OnUserEarnedReward_m5A5C5F49DA51CAF1EE38B845B19ACD56D5A4BFAD (void);
// 0x0000004B System.Void GoogleMobileAds.Api.RewardedAd::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAd_add_OnPaidEvent_m9F80372A0DFBE351C04B7643C41708750E25B4A5 (void);
// 0x0000004C System.Void GoogleMobileAds.Api.RewardedAd::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAd_remove_OnPaidEvent_m9FBF9B4B96BD70777388B877D69AE8839FBA00E8 (void);
// 0x0000004D System.Void GoogleMobileAds.Api.RewardedAd::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void RewardedAd_LoadAd_m575D3CE3B400CED2EBFE99899F818A22CA1D7849 (void);
// 0x0000004E System.Boolean GoogleMobileAds.Api.RewardedAd::IsLoaded()
extern void RewardedAd_IsLoaded_mE9C7C233E3503F0BC404DECD02020B78DEE67D74 (void);
// 0x0000004F System.Void GoogleMobileAds.Api.RewardedAd::Show()
extern void RewardedAd_Show_mA23637193AD632D847AF72FA62BAEB3B35DD282D (void);
// 0x00000050 ResponseInfo GoogleMobileAds.Api.RewardedAd::GetResponseInfo()
extern void RewardedAd_GetResponseInfo_mFE4BB72E18093089671BDC49F683147C1B6505D9 (void);
// 0x00000051 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__0(System.Object,System.EventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__0_mA9498DB92581910BE5C28AC8898957B06829E1F0 (void);
// 0x00000052 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__1(System.Object,GoogleMobileAds.Common.LoadAdErrorClientEventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__1_m5DCC4AC97775E13441BC74B8DEB295C9B767D050 (void);
// 0x00000053 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__2(System.Object,GoogleMobileAds.Common.AdErrorClientEventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__2_mB88B334155386300458DC02669C5B4E31955ABDD (void);
// 0x00000054 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__3(System.Object,System.EventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__3_mC629136FDEBF02A0BB3D178D89AAE47BC545609B (void);
// 0x00000055 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__4(System.Object,System.EventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__4_mA601115EDBD79685E7F4F49E521DC4468091AA25 (void);
// 0x00000056 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__5(System.Object,GoogleMobileAds.Common.AdErrorClientEventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__5_m81CA107330BF025380E143C2F04AAAC763D7775D (void);
// 0x00000057 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__6(System.Object,System.EventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__6_m54E208E99E0CB58875300FFE2FA0E8C570E1BF90 (void);
// 0x00000058 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__7(System.Object,GoogleMobileAds.Api.Reward)
extern void RewardedAd_U3CRewardedAdU3Em__7_mBBE7384E4A397DB3F82C3DD30EF3FBA0056BE00B (void);
// 0x00000059 System.Void GoogleMobileAds.Api.RewardedAd::<RewardedAd>m__8(System.Object,GoogleMobileAds.Api.AdValueEventArgs)
extern void RewardedAd_U3CRewardedAdU3Em__8_m69DC03E147DA22EDE5D135F9D3C10937A7F2942A (void);
static Il2CppMethodPointer s_methodPointers[89] = 
{
	AdError__ctor_mC38C346CF8A03E3DC984E12C538738E4FCB67BC3,
	AdError_GetMessage_m5311104192B08C55BD0E34735E45330FE3E07A54,
	AdError_ToString_m31CCDC54DECD29ECA162D855313F0705A2675C52,
	AdErrorEventArgs__ctor_mDA9407103CD61DCD3656AA1DA2FC95756EDEFB77,
	AdErrorEventArgs_set_AdError_m861B08F310E196385C79F1BC469E5B7D790E881C,
	AdFailedToLoadEventArgs__ctor_m4F60827DAE5FFF31835C1AA795ACEDFC859B2301,
	AdFailedToLoadEventArgs_get_LoadAdError_mE0117EBECCED9601511BD04D9831285267873D93,
	AdFailedToLoadEventArgs_set_LoadAdError_m57575ECA76FBACD3FCF5800A20D23851D86E035C,
	BannerView__ctor_mFF864899CD7456880253F63684F1F55B7CFD6366,
	BannerView_add_OnAdLoaded_m506B2F7AD83FF1076D85B9F5FFEAC15283016B35,
	BannerView_remove_OnAdLoaded_mC3F49BC24EFE1A619B3EC55C144E274F708880C1,
	BannerView_add_OnAdFailedToLoad_m78D418E3049E8C74A9D835FE595D20BC25F1EA0A,
	BannerView_remove_OnAdFailedToLoad_mEAF313631300F9BEA45846990977F8B7199BB348,
	BannerView_add_OnAdOpening_mD6C44C170D8C1F9CC0FF30CAA603D0EB691E7A75,
	BannerView_remove_OnAdOpening_m62D082706E37C149765E67D4D0ED9DCF78A79713,
	BannerView_add_OnAdClosed_m772F8BC4B1255E78BED318592766ECA335BD1A9D,
	BannerView_remove_OnAdClosed_m1BA6F60F5939D45580A2BF6297FC008648579259,
	BannerView_LoadAd_mB9CACDC88AB0BAA99E8C960D5965E58415B87159,
	BannerView_ConfigureBannerEvents_m22D1A80A3BB009314BBA75EEBBA19A9CE8B2A3FF,
	BannerView_U3CConfigureBannerEventsU3Em__0_m9211309D6C31C2A40A45EA2CF437E9DB8EF555B8,
	BannerView_U3CConfigureBannerEventsU3Em__1_mE95A0FEB326EE53B1362DFDDEF8D8CCA940B6E06,
	BannerView_U3CConfigureBannerEventsU3Em__2_mD9E55A16C5074FEA65D598928259C6CC0CB6DA15,
	BannerView_U3CConfigureBannerEventsU3Em__3_m6508B7A4FE74974989D045F32224FD7E5E2F0A4E,
	BannerView_U3CConfigureBannerEventsU3Em__4_mE30966AA5238B99E6F365C0DAF8B43065E22507A,
	InitializationStatus__ctor_m203B90A488612CD1257221879B41BB38215D48D2,
	InterstitialAd__ctor_m466F1ABACA88AFBE2E47014092E29981D8954B44,
	InterstitialAd_add_OnAdLoaded_mCDBDFB7120A1BF227BC161DFF486D3405BBDA951,
	InterstitialAd_remove_OnAdLoaded_m50ECDEDBD0DD9A866AB9B3FDD0F52C67A43B8E0C,
	InterstitialAd_add_OnAdFailedToLoad_mF07D125984B7F813661A1CCB841CECAED255AC45,
	InterstitialAd_remove_OnAdFailedToLoad_mC237AED821C39C03DB9F02036282DB6B7963C035,
	InterstitialAd_add_OnAdOpening_m3B4C511F24E886ECA2304A1FE35E10A62CB9F939,
	InterstitialAd_remove_OnAdOpening_m5909D5FF7422C8F9D03F12976907C6E98CED8FBE,
	InterstitialAd_add_OnAdClosed_mAF5C18E6713BA8DAC7EFE2C134B1D2117AC42C77,
	InterstitialAd_remove_OnAdClosed_m16767D501D07A33D1C7DA590E8961160654F4BD4,
	InterstitialAd_add_OnPaidEvent_mD2793E0D90C025A3C8008B17AC24CD6F3E6AE9EE,
	InterstitialAd_remove_OnPaidEvent_m6D1AC36D53E1DB4480A01EBE8027390A5F031DF6,
	InterstitialAd_LoadAd_mD77748B1D4354B9CE35D3242CBDBEFD92E574BD0,
	InterstitialAd_IsLoaded_m3A41B1D590D97C9FB80C0681C3FFD2B486751ED1,
	InterstitialAd_Show_m4D6142D4E8EB052A48C6869472572AC636FFB00C,
	InterstitialAd_GetResponseInfo_m5F4315F3208F0700A36292DEB2F7880AC06CC995,
	InterstitialAd_U3CInterstitialAdU3Em__0_m455A8A5BF6AB2EF58D61F22A8E647FBE43E0A627,
	InterstitialAd_U3CInterstitialAdU3Em__1_m4AB5D26E9730C969FE7DA41C1AF130050D09E865,
	InterstitialAd_U3CInterstitialAdU3Em__2_m8E50A34566DA3E9484FC68007F44C8AB57E8EF23,
	InterstitialAd_U3CInterstitialAdU3Em__3_m847FEEDC73896FC7E4C14EC94518CA12A7A9B8EC,
	InterstitialAd_U3CInterstitialAdU3Em__4_m07347F8C1DC5124F710FD328C8DC3DFF2025CBEB,
	InterstitialAd_U3CInterstitialAdU3Em__5_m5F4E1BDBF49C353A56F47933F48A17D6ADEAACD7,
	InterstitialAd_U3CInterstitialAdU3Em__6_m89B45428576EB8BF288A7EC57208D38F04A6687C,
	LoadAdError__ctor_m162C74F49A83EDAAE287922598CF7309B65B2872,
	LoadAdError_ToString_mCBED4EDB2CB490D8D8E12EB1C2A4FD900F84C601,
	MobileAds__ctor_m2A67D2A60A8C856ACB5E99B4EEFA04F469A00ACF,
	MobileAds_get_Instance_mDF4578178A58F587F7393797BF1AD3D9851B1589,
	MobileAds_Initialize_m4BE910CE7907E2E236C39DDB4A3CF20CDC015FE9,
	MobileAds_SetRequestConfiguration_mB481979FE7BB5B89CAD2E3003DC0C1F6C3F002DF,
	MobileAds_SetiOSAppPauseOnBackground_m05B6D1D6AA4A0A6583E5473455CD65F2A3B35044,
	MobileAds_GetClientFactory_m4C38A16687444F18A251DB32693C8DD4435968FE,
	MobileAds_GetMobileAdsClient_m83286C3BDE16C4F047D80772C93E282BB68F7E52,
	U3CInitializeU3Ec__AnonStorey0__ctor_m2E5FBBCA45231800CDB5F7332A27B8747EB7F843,
	U3CInitializeU3Ec__AnonStorey0_U3CU3Em__0_m8C8A10194EE35AD423797D6E09A52D65EC2EF163,
	ResponseInfo__ctor_mD74218E313340A4BAD3A5FF302E15D766B7DB8BE,
	ResponseInfo_GetMediationAdapterClassName_mBCD5EDF7E126EC6228B743F042F881F2E105A7E1,
	ResponseInfo_ToString_m3025C2660C30701FE627F35E96508FC58A3104D2,
	RewardedAd__ctor_mE6E5D17F0160ABB57C10A5FA1526AEC1B7CEB369,
	RewardedAd_add_OnAdLoaded_m33D1098F40BD3ECB8FB880A8D45DF8C013889C54,
	RewardedAd_remove_OnAdLoaded_m8D731C34BD0942C0E847E4672919E1F74FE8BEA0,
	RewardedAd_add_OnAdFailedToLoad_m642675F6423DA3C6D3D8AF16F085224C8A102499,
	RewardedAd_remove_OnAdFailedToLoad_mDC10C1667F7433087E51BDF67335C4892CB82704,
	RewardedAd_add_OnAdOpening_m925179D731E6149CED42B4A9A5C3FD75070C762B,
	RewardedAd_remove_OnAdOpening_mE442C11EFB5B767B7140A7BFE4CDBB6F4BAD1367,
	RewardedAd_add_OnAdClosed_m5F6E93F7761D4254001B58310E2233712217542C,
	RewardedAd_remove_OnAdClosed_m6C4963725130A1A201DEF3981721CC9AA190E230,
	RewardedAd_add_OnAdFailedToShow_m44231E772AC4FF0315F68BE9AC2B20271EC72B29,
	RewardedAd_remove_OnAdFailedToShow_m6838DA37C9F675788C27DB0E3229C60167B8BDBF,
	RewardedAd_add_OnUserEarnedReward_mFA8E920C7BB65419DE9ABF366C3353154FE25FCC,
	RewardedAd_remove_OnUserEarnedReward_m5A5C5F49DA51CAF1EE38B845B19ACD56D5A4BFAD,
	RewardedAd_add_OnPaidEvent_m9F80372A0DFBE351C04B7643C41708750E25B4A5,
	RewardedAd_remove_OnPaidEvent_m9FBF9B4B96BD70777388B877D69AE8839FBA00E8,
	RewardedAd_LoadAd_m575D3CE3B400CED2EBFE99899F818A22CA1D7849,
	RewardedAd_IsLoaded_mE9C7C233E3503F0BC404DECD02020B78DEE67D74,
	RewardedAd_Show_mA23637193AD632D847AF72FA62BAEB3B35DD282D,
	RewardedAd_GetResponseInfo_mFE4BB72E18093089671BDC49F683147C1B6505D9,
	RewardedAd_U3CRewardedAdU3Em__0_mA9498DB92581910BE5C28AC8898957B06829E1F0,
	RewardedAd_U3CRewardedAdU3Em__1_m5DCC4AC97775E13441BC74B8DEB295C9B767D050,
	RewardedAd_U3CRewardedAdU3Em__2_mB88B334155386300458DC02669C5B4E31955ABDD,
	RewardedAd_U3CRewardedAdU3Em__3_mC629136FDEBF02A0BB3D178D89AAE47BC545609B,
	RewardedAd_U3CRewardedAdU3Em__4_mA601115EDBD79685E7F4F49E521DC4468091AA25,
	RewardedAd_U3CRewardedAdU3Em__5_m81CA107330BF025380E143C2F04AAAC763D7775D,
	RewardedAd_U3CRewardedAdU3Em__6_m54E208E99E0CB58875300FFE2FA0E8C570E1BF90,
	RewardedAd_U3CRewardedAdU3Em__7_mBBE7384E4A397DB3F82C3DD30EF3FBA0056BE00B,
	RewardedAd_U3CRewardedAdU3Em__8_m69DC03E147DA22EDE5D135F9D3C10937A7F2942A,
};
static const int32_t s_InvokerIndices[89] = 
{
	2563,
	3002,
	3002,
	3068,
	2563,
	3068,
	3002,
	2563,
	1031,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	1567,
	1567,
	1567,
	1567,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3026,
	3068,
	3002,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	2563,
	3002,
	3068,
	4788,
	4745,
	4745,
	4749,
	4788,
	4788,
	3068,
	2563,
	2563,
	3002,
	3002,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3026,
	3068,
	3002,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
};
extern const CustomAttributesCacheGenerator g_GoogleMobileAds_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_GoogleMobileAds_CodeGenModule;
const Il2CppCodeGenModule g_GoogleMobileAds_CodeGenModule = 
{
	"GoogleMobileAds.dll",
	89,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_GoogleMobileAds_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

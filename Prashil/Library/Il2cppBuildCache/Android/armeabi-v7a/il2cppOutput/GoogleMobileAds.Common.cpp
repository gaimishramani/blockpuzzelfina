﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>
struct EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB;
// System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>
struct EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722;
// System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>
struct EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7;
// System.EventHandler`1<System.Object>
struct EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229;
// System.Collections.Generic.IEnumerable`1<System.Action>
struct IEnumerable_1_t6CEED8910660964668522055C0010B575E62C702;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235;
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct List_1_tF5F68C7FC4DC93BB7E5BFF8AB0BE8C81CBDFA469;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Action[]
struct ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// GoogleMobileAds.Common.AdErrorClientEventArgs
struct AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549;
// GoogleMobileAds.Common.AdInspectorErrorClientEventArgs
struct AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE;
// GoogleMobileAds.Api.AdRequest
struct AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2;
// GoogleMobileAds.Api.AdValueEventArgs
struct AdValueEventArgs_tCBAEB27B4870A4B954AD1F9ABA126A58CC22B61A;
// GoogleMobileAds.Common.AppOpenAdAdDummyClient
struct AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// GoogleMobileAds.Common.IAdErrorClient
struct IAdErrorClient_t82D6DD86F6D9F64B2AB18580620DD0C6645EB77D;
// GoogleMobileAds.Common.IAdInspectorErrorClient
struct IAdInspectorErrorClient_tDBFCF6395B9C51CC05B93F412CF0478C9A3378B8;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// GoogleMobileAds.Common.ILoadAdErrorClient
struct ILoadAdErrorClient_tF60F056E72415FCCE67CE952761B1FF18021636B;
// GoogleMobileAds.Common.IResponseInfoClient
struct IResponseInfoClient_t7606205EEA3B64A67CFFE4313865262682C05032;
// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// GoogleMobileAds.Common.LoadAdErrorClientEventArgs
struct LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// GoogleMobileAds.Common.MobileAdsEventExecutor
struct MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// GoogleMobileAds.Api.Reward
struct Reward_tD7F06295563E1242E649B33C6DB13848322402B6;
// GoogleMobileAds.Common.RewardedAdDummyClient
struct RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6;
// GoogleMobileAds.Common.RewardedInterstitialAdDummyClient
struct RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// GoogleMobileAds.Api.ServerSideVerificationOptions
struct ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// GoogleMobileAds.Common.Utils
struct Utils_tE8647868519A26A49DB2EF9B1370B5947011B491;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0
struct U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24;

IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1A8FD152DF02205C2FBF8ACC2505613B83477A5C;
IL2CPP_EXTERN_C String_t* _stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8;
IL2CPP_EXTERN_C String_t* _stringLiteralB977C8ABD5DC51A9CAB375AB6B9292A8DE3E94BB;
IL2CPP_EXTERN_C String_t* _stringLiteralFE4B457907B315DC803DC1665C4DBE73F1BC72D4;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mFAB75CBB7CF407D418704DAC8F325645927793B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m3D2AE71694710C857CE1B64AA938846F1EDEB50A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m37474172B0E9F5DD721C7B801409358615A00441_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1_Invoke_mF69BA60CAF322C4A19F99D91FA94DC3E842035DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisMobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_m01D25401BAE61A73AD6E6A4A4C448947930228D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_mC32923569CAAE589250C6AD9DCEEBFEC2AE11335_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mAFD3E92217FFE1CC0A595FC5C1394D30FECC3BC4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mE95B13B5EC29D2F9B4766626876B29BE769A3AB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Utils_GetTexture2DFromByteArray_m8CDFB66C3562334923A1F7933602821DAC7F0A2B_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tEEE22AD418AD0C911DA3D3E7861BE8E7084165DB 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____items_1)); }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_StaticFields, ____emptyArray_5)); }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// GoogleMobileAds.Api.AdRequest
struct AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::<Keywords>k__BackingField
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___U3CKeywordsU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::<Extras>k__BackingField
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___U3CExtrasU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::<MediationExtras>k__BackingField
	List_1_tF5F68C7FC4DC93BB7E5BFF8AB0BE8C81CBDFA469 * ___U3CMediationExtrasU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2, ___U3CKeywordsU3Ek__BackingField_1)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_U3CKeywordsU3Ek__BackingField_1() const { return ___U3CKeywordsU3Ek__BackingField_1; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_U3CKeywordsU3Ek__BackingField_1() { return &___U3CKeywordsU3Ek__BackingField_1; }
	inline void set_U3CKeywordsU3Ek__BackingField_1(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___U3CKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CKeywordsU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2, ___U3CExtrasU3Ek__BackingField_2)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_U3CExtrasU3Ek__BackingField_2() const { return ___U3CExtrasU3Ek__BackingField_2; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_U3CExtrasU3Ek__BackingField_2() { return &___U3CExtrasU3Ek__BackingField_2; }
	inline void set_U3CExtrasU3Ek__BackingField_2(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___U3CExtrasU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtrasU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2, ___U3CMediationExtrasU3Ek__BackingField_3)); }
	inline List_1_tF5F68C7FC4DC93BB7E5BFF8AB0BE8C81CBDFA469 * get_U3CMediationExtrasU3Ek__BackingField_3() const { return ___U3CMediationExtrasU3Ek__BackingField_3; }
	inline List_1_tF5F68C7FC4DC93BB7E5BFF8AB0BE8C81CBDFA469 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_3() { return &___U3CMediationExtrasU3Ek__BackingField_3; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_3(List_1_tF5F68C7FC4DC93BB7E5BFF8AB0BE8C81CBDFA469 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMediationExtrasU3Ek__BackingField_3), (void*)value);
	}
};

struct AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2_StaticFields
{
public:
	// System.String GoogleMobileAds.Api.AdRequest::<Version>k__BackingField
	String_t* ___U3CVersionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2_StaticFields, ___U3CVersionU3Ek__BackingField_0)); }
	inline String_t* get_U3CVersionU3Ek__BackingField_0() const { return ___U3CVersionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CVersionU3Ek__BackingField_0() { return &___U3CVersionU3Ek__BackingField_0; }
	inline void set_U3CVersionU3Ek__BackingField_0(String_t* value)
	{
		___U3CVersionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CVersionU3Ek__BackingField_0), (void*)value);
	}
};


// GoogleMobileAds.Common.AppOpenAdAdDummyClient
struct AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdLoaded
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdFailedToLoad
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnPaidEvent
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___OnPaidEvent_2;
	// System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdFailedToPresentFullScreenContent
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___OnAdFailedToPresentFullScreenContent_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdDidPresentFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidPresentFullScreenContent_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdDidDismissFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidDismissFullScreenContent_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.AppOpenAdAdDummyClient::OnAdDidRecordImpression
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidRecordImpression_6;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdLoaded_0)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdLoaded_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToLoad_1), (void*)value);
	}

	inline static int32_t get_offset_of_OnPaidEvent_2() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnPaidEvent_2)); }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * get_OnPaidEvent_2() const { return ___OnPaidEvent_2; }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** get_address_of_OnPaidEvent_2() { return &___OnPaidEvent_2; }
	inline void set_OnPaidEvent_2(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * value)
	{
		___OnPaidEvent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPaidEvent_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToPresentFullScreenContent_3() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdFailedToPresentFullScreenContent_3)); }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * get_OnAdFailedToPresentFullScreenContent_3() const { return ___OnAdFailedToPresentFullScreenContent_3; }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** get_address_of_OnAdFailedToPresentFullScreenContent_3() { return &___OnAdFailedToPresentFullScreenContent_3; }
	inline void set_OnAdFailedToPresentFullScreenContent_3(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * value)
	{
		___OnAdFailedToPresentFullScreenContent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToPresentFullScreenContent_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidPresentFullScreenContent_4() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdDidPresentFullScreenContent_4)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidPresentFullScreenContent_4() const { return ___OnAdDidPresentFullScreenContent_4; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidPresentFullScreenContent_4() { return &___OnAdDidPresentFullScreenContent_4; }
	inline void set_OnAdDidPresentFullScreenContent_4(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidPresentFullScreenContent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidPresentFullScreenContent_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidDismissFullScreenContent_5() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdDidDismissFullScreenContent_5)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidDismissFullScreenContent_5() const { return ___OnAdDidDismissFullScreenContent_5; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidDismissFullScreenContent_5() { return &___OnAdDidDismissFullScreenContent_5; }
	inline void set_OnAdDidDismissFullScreenContent_5(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidDismissFullScreenContent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidDismissFullScreenContent_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidRecordImpression_6() { return static_cast<int32_t>(offsetof(AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89, ___OnAdDidRecordImpression_6)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidRecordImpression_6() const { return ___OnAdDidRecordImpression_6; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidRecordImpression_6() { return &___OnAdDidRecordImpression_6; }
	inline void set_OnAdDidRecordImpression_6(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidRecordImpression_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidRecordImpression_6), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields, ___Empty_0)); }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// GoogleMobileAds.Common.RewardedAdDummyClient
struct RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdLoaded
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdFailedToLoad
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Common.RewardedAdDummyClient::OnUserEarnedReward
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___OnUserEarnedReward_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnPaidEvent
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___OnPaidEvent_3;
	// System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdFailedToPresentFullScreenContent
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___OnAdFailedToPresentFullScreenContent_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdDidPresentFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidPresentFullScreenContent_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdDidDismissFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidDismissFullScreenContent_6;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedAdDummyClient::OnAdDidRecordImpression
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidRecordImpression_7;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdLoaded_0)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdLoaded_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToLoad_1), (void*)value);
	}

	inline static int32_t get_offset_of_OnUserEarnedReward_2() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnUserEarnedReward_2)); }
	inline EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * get_OnUserEarnedReward_2() const { return ___OnUserEarnedReward_2; }
	inline EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** get_address_of_OnUserEarnedReward_2() { return &___OnUserEarnedReward_2; }
	inline void set_OnUserEarnedReward_2(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * value)
	{
		___OnUserEarnedReward_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnUserEarnedReward_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnPaidEvent_3() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnPaidEvent_3)); }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * get_OnPaidEvent_3() const { return ___OnPaidEvent_3; }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** get_address_of_OnPaidEvent_3() { return &___OnPaidEvent_3; }
	inline void set_OnPaidEvent_3(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * value)
	{
		___OnPaidEvent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPaidEvent_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToPresentFullScreenContent_4() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdFailedToPresentFullScreenContent_4)); }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * get_OnAdFailedToPresentFullScreenContent_4() const { return ___OnAdFailedToPresentFullScreenContent_4; }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** get_address_of_OnAdFailedToPresentFullScreenContent_4() { return &___OnAdFailedToPresentFullScreenContent_4; }
	inline void set_OnAdFailedToPresentFullScreenContent_4(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * value)
	{
		___OnAdFailedToPresentFullScreenContent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToPresentFullScreenContent_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidPresentFullScreenContent_5() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdDidPresentFullScreenContent_5)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidPresentFullScreenContent_5() const { return ___OnAdDidPresentFullScreenContent_5; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidPresentFullScreenContent_5() { return &___OnAdDidPresentFullScreenContent_5; }
	inline void set_OnAdDidPresentFullScreenContent_5(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidPresentFullScreenContent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidPresentFullScreenContent_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidDismissFullScreenContent_6() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdDidDismissFullScreenContent_6)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidDismissFullScreenContent_6() const { return ___OnAdDidDismissFullScreenContent_6; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidDismissFullScreenContent_6() { return &___OnAdDidDismissFullScreenContent_6; }
	inline void set_OnAdDidDismissFullScreenContent_6(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidDismissFullScreenContent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidDismissFullScreenContent_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidRecordImpression_7() { return static_cast<int32_t>(offsetof(RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6, ___OnAdDidRecordImpression_7)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidRecordImpression_7() const { return ___OnAdDidRecordImpression_7; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidRecordImpression_7() { return &___OnAdDidRecordImpression_7; }
	inline void set_OnAdDidRecordImpression_7(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidRecordImpression_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidRecordImpression_7), (void*)value);
	}
};


// GoogleMobileAds.Common.RewardedInterstitialAdDummyClient
struct RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdLoaded
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdFailedToLoad
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnUserEarnedReward
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___OnUserEarnedReward_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnPaidEvent
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___OnPaidEvent_3;
	// System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdFailedToPresentFullScreenContent
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___OnAdFailedToPresentFullScreenContent_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdDidPresentFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidPresentFullScreenContent_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdDidDismissFullScreenContent
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidDismissFullScreenContent_6;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::OnAdDidRecordImpression
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___OnAdDidRecordImpression_7;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdLoaded_0)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdLoaded_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToLoad_1), (void*)value);
	}

	inline static int32_t get_offset_of_OnUserEarnedReward_2() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnUserEarnedReward_2)); }
	inline EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * get_OnUserEarnedReward_2() const { return ___OnUserEarnedReward_2; }
	inline EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** get_address_of_OnUserEarnedReward_2() { return &___OnUserEarnedReward_2; }
	inline void set_OnUserEarnedReward_2(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * value)
	{
		___OnUserEarnedReward_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnUserEarnedReward_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnPaidEvent_3() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnPaidEvent_3)); }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * get_OnPaidEvent_3() const { return ___OnPaidEvent_3; }
	inline EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** get_address_of_OnPaidEvent_3() { return &___OnPaidEvent_3; }
	inline void set_OnPaidEvent_3(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * value)
	{
		___OnPaidEvent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPaidEvent_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToPresentFullScreenContent_4() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdFailedToPresentFullScreenContent_4)); }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * get_OnAdFailedToPresentFullScreenContent_4() const { return ___OnAdFailedToPresentFullScreenContent_4; }
	inline EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** get_address_of_OnAdFailedToPresentFullScreenContent_4() { return &___OnAdFailedToPresentFullScreenContent_4; }
	inline void set_OnAdFailedToPresentFullScreenContent_4(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * value)
	{
		___OnAdFailedToPresentFullScreenContent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToPresentFullScreenContent_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidPresentFullScreenContent_5() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdDidPresentFullScreenContent_5)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidPresentFullScreenContent_5() const { return ___OnAdDidPresentFullScreenContent_5; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidPresentFullScreenContent_5() { return &___OnAdDidPresentFullScreenContent_5; }
	inline void set_OnAdDidPresentFullScreenContent_5(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidPresentFullScreenContent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidPresentFullScreenContent_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidDismissFullScreenContent_6() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdDidDismissFullScreenContent_6)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidDismissFullScreenContent_6() const { return ___OnAdDidDismissFullScreenContent_6; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidDismissFullScreenContent_6() { return &___OnAdDidDismissFullScreenContent_6; }
	inline void set_OnAdDidDismissFullScreenContent_6(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidDismissFullScreenContent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidDismissFullScreenContent_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdDidRecordImpression_7() { return static_cast<int32_t>(offsetof(RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5, ___OnAdDidRecordImpression_7)); }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * get_OnAdDidRecordImpression_7() const { return ___OnAdDidRecordImpression_7; }
	inline EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** get_address_of_OnAdDidRecordImpression_7() { return &___OnAdDidRecordImpression_7; }
	inline void set_OnAdDidRecordImpression_7(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * value)
	{
		___OnAdDidRecordImpression_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdDidRecordImpression_7), (void*)value);
	}
};


// GoogleMobileAds.Api.ServerSideVerificationOptions
struct ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148  : public RuntimeObject
{
public:
	// System.String GoogleMobileAds.Api.ServerSideVerificationOptions::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_0;
	// System.String GoogleMobileAds.Api.ServerSideVerificationOptions::<CustomData>k__BackingField
	String_t* ___U3CCustomDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148, ___U3CUserIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_0() const { return ___U3CUserIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_0() { return &___U3CUserIdU3Ek__BackingField_0; }
	inline void set_U3CUserIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserIdU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCustomDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148, ___U3CCustomDataU3Ek__BackingField_1)); }
	inline String_t* get_U3CCustomDataU3Ek__BackingField_1() const { return ___U3CCustomDataU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCustomDataU3Ek__BackingField_1() { return &___U3CCustomDataU3Ek__BackingField_1; }
	inline void set_U3CCustomDataU3Ek__BackingField_1(String_t* value)
	{
		___U3CCustomDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCustomDataU3Ek__BackingField_1), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// GoogleMobileAds.Common.Utils
struct Utils_tE8647868519A26A49DB2EF9B1370B5947011B491  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0
struct U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityEvent GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::eventParam
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___eventParam_0;

public:
	inline static int32_t get_offset_of_eventParam_0() { return static_cast<int32_t>(offsetof(U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24, ___eventParam_0)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_eventParam_0() const { return ___eventParam_0; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_eventParam_0() { return &___eventParam_0; }
	inline void set_eventParam_0(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___eventParam_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventParam_0), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Action>
struct Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435, ___list_0)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_list_0() const { return ___list_0; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435, ___current_3)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_current_3() const { return ___current_3; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// GoogleMobileAds.Common.AdErrorClientEventArgs
struct AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Common.AdErrorClientEventArgs::<AdErrorClient>k__BackingField
	RuntimeObject* ___U3CAdErrorClientU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAdErrorClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549, ___U3CAdErrorClientU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CAdErrorClientU3Ek__BackingField_1() const { return ___U3CAdErrorClientU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CAdErrorClientU3Ek__BackingField_1() { return &___U3CAdErrorClientU3Ek__BackingField_1; }
	inline void set_U3CAdErrorClientU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CAdErrorClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAdErrorClientU3Ek__BackingField_1), (void*)value);
	}
};


// GoogleMobileAds.Common.AdInspectorErrorClientEventArgs
struct AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// GoogleMobileAds.Common.IAdInspectorErrorClient GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::<AdErrorClient>k__BackingField
	RuntimeObject* ___U3CAdErrorClientU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAdErrorClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE, ___U3CAdErrorClientU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CAdErrorClientU3Ek__BackingField_1() const { return ___U3CAdErrorClientU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CAdErrorClientU3Ek__BackingField_1() { return &___U3CAdErrorClientU3Ek__BackingField_1; }
	inline void set_U3CAdErrorClientU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CAdErrorClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAdErrorClientU3Ek__BackingField_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// GoogleMobileAds.Common.LoadAdErrorClientEventArgs
struct LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// GoogleMobileAds.Common.ILoadAdErrorClient GoogleMobileAds.Common.LoadAdErrorClientEventArgs::<LoadAdErrorClient>k__BackingField
	RuntimeObject* ___U3CLoadAdErrorClientU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLoadAdErrorClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A, ___U3CLoadAdErrorClientU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CLoadAdErrorClientU3Ek__BackingField_1() const { return ___U3CLoadAdErrorClientU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CLoadAdErrorClientU3Ek__BackingField_1() { return &___U3CLoadAdErrorClientU3Ek__BackingField_1; }
	inline void set_U3CLoadAdErrorClientU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CLoadAdErrorClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLoadAdErrorClientU3Ek__BackingField_1), (void*)value);
	}
};


// System.Reflection.MethodBase
struct MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// GoogleMobileAds.Api.Reward
struct Reward_tD7F06295563E1242E649B33C6DB13848322402B6  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// System.String GoogleMobileAds.Api.Reward::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Double GoogleMobileAds.Api.Reward::<Amount>k__BackingField
	double ___U3CAmountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Reward_tD7F06295563E1242E649B33C6DB13848322402B6, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTypeU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Reward_tD7F06295563E1242E649B33C6DB13848322402B6, ___U3CAmountU3Ek__BackingField_2)); }
	inline double get_U3CAmountU3Ek__BackingField_2() const { return ___U3CAmountU3Ek__BackingField_2; }
	inline double* get_address_of_U3CAmountU3Ek__BackingField_2() { return &___U3CAmountU3Ek__BackingField_2; }
	inline void set_U3CAmountU3Ek__BackingField_2(double value)
	{
		___U3CAmountU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.HideFlags
struct HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// GoogleMobileAds.Common.ResponseInfoClientType
struct ResponseInfoClientType_t4BB774B57BDFA7D345E725B876850AB33F538CDB 
{
public:
	// System.Int32 GoogleMobileAds.Common.ResponseInfoClientType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResponseInfoClientType_t4BB774B57BDFA7D345E725B876850AB33F538CDB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ScreenOrientation
struct ScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>
struct EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>
struct EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>
struct EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230  : public MulticastDelegate_t
{
public:

public:
};


// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// GoogleMobileAds.Common.MobileAdsEventExecutor
struct MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields
{
public:
	// GoogleMobileAds.Common.MobileAdsEventExecutor GoogleMobileAds.Common.MobileAdsEventExecutor::instance
	MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * ___instance_4;
	// System.Collections.Generic.List`1<System.Action> GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueue
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___adEventsQueue_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueueEmpty
	bool ___adEventsQueueEmpty_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields, ___instance_4)); }
	inline MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * get_instance_4() const { return ___instance_4; }
	inline MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_adEventsQueue_5() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields, ___adEventsQueue_5)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_adEventsQueue_5() const { return ___adEventsQueue_5; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_adEventsQueue_5() { return &___adEventsQueue_5; }
	inline void set_adEventsQueue_5(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___adEventsQueue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adEventsQueue_5), (void*)value);
	}

	inline static int32_t get_offset_of_adEventsQueueEmpty_6() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields, ___adEventsQueueEmpty_6)); }
	inline bool get_adEventsQueueEmpty_6() const { return ___adEventsQueueEmpty_6; }
	inline bool* get_address_of_adEventsQueueEmpty_6() { return &___adEventsQueueEmpty_6; }
	inline void set_adEventsQueueEmpty_6(bool value)
	{
		___adEventsQueueEmpty_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m6465DEF706EB529B4227F2AF79338419D517EDF9_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1_Invoke_mEAFD7F9E52E7DF356F3C4F0262BCFBA7769C83C0_gshared (EventHandler_1_tFA1C30E54FA1061D79E711F65F9A174BFBD8CDCB * __this, RuntimeObject * ___sender0, RuntimeObject * ___e1, const RuntimeMethod* method);

// System.Void System.EventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571 (EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Boolean GoogleMobileAds.Common.MobileAdsEventExecutor::IsActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E (const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<GoogleMobileAds.Common.MobileAdsEventExecutor>()
inline MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * GameObject_AddComponent_TisMobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_m01D25401BAE61A73AD6E6A4A4C448947930228D9 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
inline void List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9 (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeInUpdateU3Ec__AnonStorey0__ctor_mC8F379C8356A1F1FE4E62393D402C0ABB7102F11 (U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::ExecuteInUpdate(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_ExecuteInUpdate_m8A940774825EEDE73D61C609EB12B68FC81A4E4B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___action0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
inline void List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_mC32923569CAAE589250C6AD9DCEEBFEC2AE11335 (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m6465DEF706EB529B4227F2AF79338419D517EDF9_gshared)(__this, ___collection0, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::Clear()
inline void List_1_Clear_mAFD3E92217FFE1CC0A595FC5C1394D30FECC3BC4 (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Action>::GetEnumerator()
inline Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435  List_1_GetEnumerator_mE95B13B5EC29D2F9B4766626876B29BE769A3AB3 (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435  (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Action>::get_Current()
inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * Enumerator_get_Current_m37474172B0E9F5DD721C7B801409358615A00441_inline (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 * __this, const RuntimeMethod* method)
{
	return ((  Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * (*) (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Object System.Delegate::get_Target()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Delegate_get_Target_mA4C35D598EE379F0F1D49EA8670620792D25EAB1_inline (Delegate_t * __this, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action>::MoveNext()
inline bool Enumerator_MoveNext_m3D2AE71694710C857CE1B64AA938846F1EDEB50A (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action>::Dispose()
inline void Enumerator_Dispose_mFAB75CBB7CF407D418704DAC8F325645927793B1 (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void System.EventHandler`1<System.EventArgs>::Invoke(System.Object,!0)
inline void EventHandler_1_Invoke_mF69BA60CAF322C4A19F99D91FA94DC3E842035DF (EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * __this, RuntimeObject * ___sender0, EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___e1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *, RuntimeObject *, EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA *, const RuntimeMethod*))EventHandler_1_Invoke_mEAFD7F9E52E7DF356F3C4F0262BCFBA7769C83C0_gshared)(__this, ___sender0, ___e1, method);
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_Initialize_m1448806DA8253632AFD68C0B29AEF16C287FF5D6 (const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___tex0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.AdErrorClientEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdErrorClientEventArgs__ctor_m93239FDB158D4F24A099957AB1C9EA4E229CFE1B (AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571(__this, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Common.AdErrorClientEventArgs::get_AdErrorClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdErrorClientEventArgs_get_AdErrorClient_m4440FC4F2048A9051F6D26340A22D653D9C284D1 (AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CAdErrorClientU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void GoogleMobileAds.Common.AdErrorClientEventArgs::set_AdErrorClient(GoogleMobileAds.Common.IAdErrorClient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdErrorClientEventArgs_set_AdErrorClient_m3E63E09455836EED9523184A16C40F346ABB18BE (AdErrorClientEventArgs_tB274856D2970370818325A72964ACBCC0150D549 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CAdErrorClientU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdInspectorErrorClientEventArgs__ctor_mA7484DD53EB3BDD93F5B63EBF462917BAEE3A17A (AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571(__this, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.IAdInspectorErrorClient GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::get_AdErrorClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdInspectorErrorClientEventArgs_get_AdErrorClient_m22EC00E2D1C837C26202248647B4E7C2E45756F6 (AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CAdErrorClientU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::set_AdErrorClient(GoogleMobileAds.Common.IAdInspectorErrorClient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdInspectorErrorClientEventArgs_set_AdErrorClient_m338465B56A77301A4A22D6A770097F96AB3EF3BC (AdInspectorErrorClientEventArgs_tD4C109BC6B03237B0094836C41FFCBE558A9D1EE * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CAdErrorClientU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdLoaded_m309C884953361C82DB73B162DA53D47C4F663BD4 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdLoaded_m8CD62FE6C0D8679B6F262A2CADB9133AC2A50839 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdFailedToLoad_m7728823A53F14F214F33C5AD27B74F85386C8C53 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdFailedToLoad_m11F3806FF1BC289468F7443A213BB9FE935319F7 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnPaidEvent_mDC509BEAC7A54B488907E4D787D1AD7B8CF31090 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_2();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnPaidEvent_mBD2A81EF4EC41FA1AA9ECAD4505D59CE98A2D99C (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_2();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m34634DE2135690A838D2E19265EC8002B9DF3E75 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_3();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m2489B2EF5FCC0827D9A9E7309572011B89881BF6 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_3();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdDidPresentFullScreenContent_mDB41C6BE0B1F82383C15F8AF15FA4B2973C5C030 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_4();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdDidPresentFullScreenContent_m7802D2403D1AAAAD879BBA70F11EDB172DBCF21C (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_4();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdDidDismissFullScreenContent_m1FE6317467AC357AF806662FD12A2D5E96561C7B (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2B63C84A7AF691B024D9660E3BBEDC9B4F039015 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_add_OnAdDidRecordImpression_mF7CD3F4F1A6E6EA25E9A1EA565D06B788A29CA00 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_remove_OnAdDidRecordImpression_m6905A5257B15830DC7BE26D6CABB25F5665F4B42 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::CreateAppOpenAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest,UnityEngine.ScreenOrientation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, String_t* ___adUnitID0, AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2 * ___request1, int32_t ___orientation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::Show()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::DestroyAppOpenAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.AppOpenAdAdDummyClient::GetResponseInfoClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53 (AppOpenAdAdDummyClient_t314360324B027D25F07276468A0F856A46529A89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return (RuntimeObject*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.LoadAdErrorClientEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadAdErrorClientEventArgs__ctor_mE45381F3D10AF46E60928BB3540FE9A05B56E70E (LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571(__this, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.ILoadAdErrorClient GoogleMobileAds.Common.LoadAdErrorClientEventArgs::get_LoadAdErrorClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoadAdErrorClientEventArgs_get_LoadAdErrorClient_mF8379F2375AB2F5BD4F0C47371DED5E32A72115B (LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CLoadAdErrorClientU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void GoogleMobileAds.Common.LoadAdErrorClientEventArgs::set_LoadAdErrorClient(GoogleMobileAds.Common.ILoadAdErrorClient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadAdErrorClientEventArgs_set_LoadAdErrorClient_mE9FFC8AD6E3337EEA9C1296C657CCD8F326BF256 (LoadAdErrorClientEventArgs_t8A40C29E27174A02407452A656DFB39DBD4BC34A * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CLoadAdErrorClientU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor__ctor_m5C94414047CAFBE0DA34C863D65C83E64B6F9F1A (MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_Initialize_m1448806DA8253632AFD68C0B29AEF16C287FF5D6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisMobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_m01D25401BAE61A73AD6E6A4A4C448947930228D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A8FD152DF02205C2FBF8ACC2505613B83477A5C);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		bool L_0;
		L_0 = MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144(L_1, _stringLiteral1A8FD152DF02205C2FBF8ACC2505613B83477A5C, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = V_0;
		NullCheck(L_2);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_2, ((int32_t)61), /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = V_0;
		NullCheck(L_4);
		MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * L_5;
		L_5 = GameObject_AddComponent_TisMobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_m01D25401BAE61A73AD6E6A4A4C448947930228D9(L_4, /*hidden argument*/GameObject_AddComponent_TisMobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_m01D25401BAE61A73AD6E6A4A4C448947930228D9_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_instance_4(L_5);
		return;
	}
}
// System.Boolean GoogleMobileAds.Common.MobileAdsEventExecutor::IsActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * L_0 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_Awake_m95585B833F8DA4129ED6EFB2EDD76BC60255D26B (MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::ExecuteInUpdate(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_ExecuteInUpdate_m8A940774825EEDE73D61C609EB12B68FC81A4E4B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueue_5();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2(L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_2 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueue_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = ___action0;
		NullCheck(L_2);
		List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9(L_2, L_3, /*hidden argument*/List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		il2cpp_codegen_memory_barrier();
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_adEventsQueueEmpty_6(0);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A(L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
	}

IL_002b:
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::InvokeInUpdate(UnityEngine.Events.UnityEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_InvokeInUpdate_m786542D0978DC8034D345A9BF095CEC26B751900 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___eventParam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * V_0 = NULL;
	{
		U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * L_0 = (U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 *)il2cpp_codegen_object_new(U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24_il2cpp_TypeInfo_var);
		U3CInvokeInUpdateU3Ec__AnonStorey0__ctor_mC8F379C8356A1F1FE4E62393D402C0ABB7102F11(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * L_1 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = ___eventParam0;
		NullCheck(L_1);
		L_1->set_eventParam_0(L_2);
		U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * L_3 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_4 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_4, L_3, (intptr_t)((intptr_t)U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		MobileAdsEventExecutor_ExecuteInUpdate_m8A940774825EEDE73D61C609EB12B68FC81A4E4B(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_Update_mB3B2E0F578A69796B8751DFAD1D8B86BB7E82126 (MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mFAB75CBB7CF407D418704DAC8F325645927793B1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m3D2AE71694710C857CE1B64AA938846F1EDEB50A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m37474172B0E9F5DD721C7B801409358615A00441_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_mC32923569CAAE589250C6AD9DCEEBFEC2AE11335_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mAFD3E92217FFE1CC0A595FC5C1394D30FECC3BC4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mE95B13B5EC29D2F9B4766626876B29BE769A3AB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_2 = NULL;
	Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		bool L_0 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueueEmpty_6();
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_1 = (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *)il2cpp_codegen_object_new(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB(L_1, /*hidden argument*/List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_2 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueue_5();
		V_1 = L_2;
		RuntimeObject * L_3 = V_1;
		Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2(L_3, /*hidden argument*/NULL);
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_5 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueue_5();
		NullCheck(L_4);
		List_1_AddRange_mC32923569CAAE589250C6AD9DCEEBFEC2AE11335(L_4, L_5, /*hidden argument*/List_1_AddRange_mC32923569CAAE589250C6AD9DCEEBFEC2AE11335_RuntimeMethod_var);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_6 = ((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->get_adEventsQueue_5();
		NullCheck(L_6);
		List_1_Clear_mAFD3E92217FFE1CC0A595FC5C1394D30FECC3BC4(L_6, /*hidden argument*/List_1_Clear_mAFD3E92217FFE1CC0A595FC5C1394D30FECC3BC4_RuntimeMethod_var);
		il2cpp_codegen_memory_barrier();
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_adEventsQueueEmpty_6(1);
		IL2CPP_LEAVE(0x48, FINALLY_0041);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_1;
		Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A(L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
	}

IL_0048:
	{
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_8 = V_0;
		NullCheck(L_8);
		Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435  L_9;
		L_9 = List_1_GetEnumerator_mE95B13B5EC29D2F9B4766626876B29BE769A3AB3(L_8, /*hidden argument*/List_1_GetEnumerator_mE95B13B5EC29D2F9B4766626876B29BE769A3AB3_RuntimeMethod_var);
		V_3 = L_9;
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006d;
		}

IL_0054:
		{
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_10;
			L_10 = Enumerator_get_Current_m37474172B0E9F5DD721C7B801409358615A00441_inline((Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m37474172B0E9F5DD721C7B801409358615A00441_RuntimeMethod_var);
			V_2 = L_10;
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_11 = V_2;
			NullCheck(L_11);
			RuntimeObject * L_12;
			L_12 = Delegate_get_Target_mA4C35D598EE379F0F1D49EA8670620792D25EAB1_inline(L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_006d;
			}
		}

IL_0067:
		{
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_13 = V_2;
			NullCheck(L_13);
			Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_13, /*hidden argument*/NULL);
		}

IL_006d:
		{
			bool L_14;
			L_14 = Enumerator_MoveNext_m3D2AE71694710C857CE1B64AA938846F1EDEB50A((Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m3D2AE71694710C857CE1B64AA938846F1EDEB50A_RuntimeMethod_var);
			if (L_14)
			{
				goto IL_0054;
			}
		}

IL_0079:
		{
			IL2CPP_LEAVE(0x8C, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mFAB75CBB7CF407D418704DAC8F325645927793B1((Enumerator_tE666A9296F1B52A9BF4204E6138AB695F6F7E435 *)(&V_3), /*hidden argument*/Enumerator_Dispose_mFAB75CBB7CF407D418704DAC8F325645927793B1_RuntimeMethod_var);
		IL2CPP_END_FINALLY(126)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
	}

IL_008c:
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor_OnDisable_mE2756A2E6860A83452361BF52DED618EBF5FC323 (MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_instance_4((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 *)NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MobileAdsEventExecutor__cctor_mA46D12A46E0E0C8EBEC865AA1BF29513FA55EDA2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_instance_4((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10 *)NULL);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *)il2cpp_codegen_object_new(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB(L_0, /*hidden argument*/List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_adEventsQueue_5(L_0);
		il2cpp_codegen_memory_barrier();
		((MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_StaticFields*)il2cpp_codegen_static_fields_for(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var))->set_adEventsQueueEmpty_6(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdLoaded_m18EEB16C76E34313A182246DCAAD17CC967FDC66 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdLoaded_m8FDB4BDD09573E33DF962B1A1623274F4CB322C0 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdFailedToLoad_mB5A1AA8A6664682B380934076662A362A6A72F2D (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdFailedToLoad_m3CE4C2667B82106FDC1F3C1478BB1E64DB66FADA (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnUserEarnedReward_mAC506289B1E5C551AF3DAC147B7A044104CA01CB (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_0 = NULL;
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_1 = NULL;
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_0 = __this->get_OnUserEarnedReward_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** L_2 = __this->get_address_of_OnUserEarnedReward_2();
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_3 = V_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_6 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *>((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 **)L_2, ((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_8 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_8) == ((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnUserEarnedReward_m3398B13A054E69E7D64BB13510AA7CCF88FC95FA (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_0 = NULL;
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_1 = NULL;
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_0 = __this->get_OnUserEarnedReward_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** L_2 = __this->get_address_of_OnUserEarnedReward_2();
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_3 = V_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_6 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *>((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 **)L_2, ((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_8 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_8) == ((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnPaidEvent_mC744005D4D20ECE86DA7D02635569A8648366EF5 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_3();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnPaidEvent_m7AFE340D4FBCE5431CB59D682152EECE72551057 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_3();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m716763228696903449AFA07A532C66F4DF9520D3 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_4();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m1E8E146DBF6C6DCA419A29BBD8B3FA96AD59F0A0 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_4();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdDidPresentFullScreenContent_mF029D975810A792BB7749553B7851DA2B9736DFC (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdDidPresentFullScreenContent_mE66FF1E03C6601CED2B8291D702B086F8E5F8ECC (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdDidDismissFullScreenContent_mD74F37CCCEA460E1E57864FFE4389A23D136D02B (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdDidDismissFullScreenContent_mA6CD8B693C3D7087715A179D6F8B96EE7FBFB71A (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_add_OnAdDidRecordImpression_mCC0F4BF042C5BDCF89D149ED7A91C7FFD61FDDF2 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_7();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_remove_OnAdDidRecordImpression_m05C094862F16C31C13752A059462351A373D43FF (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_7();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::CreateRewardedAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, String_t* ___adUnitId0, AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2 * ___request1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_Invoke_mF69BA60CAF322C4A19F99D91FA94DC3E842035DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = __this->get_OnAdLoaded_0();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = __this->get_OnAdLoaded_0();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * L_5 = ((EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(L_4);
		EventHandler_1_Invoke_mF69BA60CAF322C4A19F99D91FA94DC3E842035DF(L_4, __this, L_5, /*hidden argument*/EventHandler_1_Invoke_mF69BA60CAF322C4A19F99D91FA94DC3E842035DF_RuntimeMethod_var);
	}

IL_0035:
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::Show()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Api.Reward GoogleMobileAds.Common.RewardedAdDummyClient::GetRewardItem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Reward_tD7F06295563E1242E649B33C6DB13848322402B6 * RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return (Reward_tD7F06295563E1242E649B33C6DB13848322402B6 *)NULL;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148 * ___serverSideVerificationOptions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedAdDummyClient::DestroyRewardedAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.RewardedAdDummyClient::GetResponseInfoClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85 (RewardedAdDummyClient_tD5E029693D0963A245114B6DA2F931DCB5FDB4E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return (RuntimeObject*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdLoaded_mB94D58D69016376BCD0201F00169984E1BD45ACF (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdLoaded_m93C328456448580FD6285912D1C9BA75317C558F (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdLoaded_0();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdFailedToLoad_mB3BA795652E1ECCB25E758D64FDD96D9E235FAF6 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdFailedToLoad_m1E445E210D2292F18487D7A53CED6F7E259FBBD3 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_0 = NULL;
	EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * V_1 = NULL;
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_0 = __this->get_OnAdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 ** L_2 = __this->get_address_of_OnAdFailedToLoad_1();
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_3 = V_1;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_6 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *>((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 **)L_2, ((EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_8 = V_0;
		EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3F280C24BFFA1F032DA3C628516C6CA0497B60B7 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnUserEarnedReward_mDCDB6E058DE644FF7935BA64D8F70CD94DF31A15 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_0 = NULL;
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_1 = NULL;
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_0 = __this->get_OnUserEarnedReward_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** L_2 = __this->get_address_of_OnUserEarnedReward_2();
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_3 = V_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_6 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *>((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 **)L_2, ((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_8 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_8) == ((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnUserEarnedReward_mFFEDD3E13D839FBFC3C7DE671CB9F963F6BBB7BC (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_0 = NULL;
	EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * V_1 = NULL;
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_0 = __this->get_OnUserEarnedReward_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 ** L_2 = __this->get_address_of_OnUserEarnedReward_2();
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_3 = V_1;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_6 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *>((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 **)L_2, ((EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_8 = V_0;
		EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_8) == ((RuntimeObject*)(EventHandler_1_tD337AACDA7BD69F30588EFFD58D3F0DD07B33230 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnPaidEvent_mF76B2A38E9B493992BD6FF34FB62822C8992A6BC (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_3();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnPaidEvent_mC398ED83432D6CE1A90E7C19D90A1CF46FF10830 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_0 = NULL;
	EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * V_1 = NULL;
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_0 = __this->get_OnPaidEvent_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 ** L_2 = __this->get_address_of_OnPaidEvent_3();
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_3 = V_1;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_6 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *>((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 **)L_2, ((EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_8 = V_0;
		EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_8) == ((RuntimeObject*)(EventHandler_1_t996E9F4D6ACC0E62DCB6129A804B31D7BBB73D36 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m50CDDC72D9D6BC3DBE2AD262E7DB295BC27A2483 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_4();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_mE9F580E20049BE17BE16F3F0C6E2BC5736B28BA6 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_0 = NULL;
	EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * V_1 = NULL;
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_0 = __this->get_OnAdFailedToPresentFullScreenContent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB ** L_2 = __this->get_address_of_OnAdFailedToPresentFullScreenContent_4();
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_3 = V_1;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_6 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *>((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB **)L_2, ((EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_8 = V_0;
		EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_8) == ((RuntimeObject*)(EventHandler_1_tD7F1FCCC669FFEC2AC8F018E0D3673841C379CFB *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdDidPresentFullScreenContent_mA4E39F1B1CAAEA898A869178906E99EF62BABEF4 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdDidPresentFullScreenContent_mC5830CA0BB47C5054A1007066CC1E7E776CB7A25 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidPresentFullScreenContent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidPresentFullScreenContent_5();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdDidDismissFullScreenContent_m33E58EE8BC7BF37AA405C981EF14F28D6E3D413C (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2411DE6CA699D2BE18690D1691B99F44343FA5A4 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidDismissFullScreenContent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidDismissFullScreenContent_6();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_add_OnAdDidRecordImpression_m2EAF100D93ABE9093CE22BED4D5FE8452AD60693 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_7();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_remove_OnAdDidRecordImpression_m90D509D7D361B214DD81B902F1AD9D8ABB5AC2F0 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_0 = NULL;
	EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * V_1 = NULL;
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_0 = __this->get_OnAdDidRecordImpression_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 ** L_2 = __this->get_address_of_OnAdDidRecordImpression_7();
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_3 = V_1;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_4 = ___value0;
		Delegate_t * L_5;
		L_5 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_6 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_7;
		L_7 = InterlockedCompareExchangeImpl<EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *>((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 **)L_2, ((EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_8 = V_0;
		EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_8) == ((RuntimeObject*)(EventHandler_1_tA707D618BF8F29DB61DFD553B055AE4E516C1722 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::CreateRewardedInterstitialAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, String_t* ___adUnitID0, AdRequest_tE64CA6699EE4572F9D6651EED0BFE0244DAE34A2 * ___request1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Api.Reward GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::GetRewardItem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Reward_tD7F06295563E1242E649B33C6DB13848322402B6 * RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return (Reward_tD7F06295563E1242E649B33C6DB13848322402B6 *)NULL;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::Show()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, ServerSideVerificationOptions_t0D528D78DC5D73DB61D0D8B6E40006B416EBA148 * ___serverSideVerificationOptions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::DestroyRewardedInterstitialAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060 (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return;
	}
}
// GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::GetResponseInfoClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C (RewardedInterstitialAdDummyClient_tDDC7D091B9BF0FBB58D57C7594ECD8CFDCEEADC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0;
		L_0 = il2cpp_codegen_get_method_object(RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C_RuntimeMethod_var);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral3C81F186092FD937E80B1B266D4706CC0D65FAF8, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		return (RuntimeObject*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.Utils::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils__ctor_m14AAC251D5294F487EC89EA0A097AC98F37CA892 (Utils_tE8647868519A26A49DB2EF9B1370B5947011B491 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.Utils::CheckInitialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_CheckInitialization_m7E335C1BEE73E87BE067FED40013CE55C9661327 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB977C8ABD5DC51A9CAB375AB6B9292A8DE3E94BB);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		bool L_0;
		L_0 = MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB977C8ABD5DC51A9CAB375AB6B9292A8DE3E94BB, /*hidden argument*/NULL);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MobileAdsEventExecutor_t76B9939F500A2D7AFC60397590EA21ACC91FFC10_il2cpp_TypeInfo_var);
		MobileAdsEventExecutor_Initialize_m1448806DA8253632AFD68C0B29AEF16C287FF5D6(/*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D GoogleMobileAds.Common.Utils::GetTexture2DFromByteArray(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * Utils_GetTexture2DFromByteArray_m8CDFB66C3562334923A1F7933602821DAC7F0A2B (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___img0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * V_0 = NULL;
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4(L_0, 1, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_1 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___img0;
		bool L_3;
		L_3 = ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_4 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_4, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralFE4B457907B315DC803DC1665C4DBE73F1BC72D4)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Utils_GetTexture2DFromByteArray_m8CDFB66C3562334923A1F7933602821DAC7F0A2B_RuntimeMethod_var)));
	}

IL_001f:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeInUpdateU3Ec__AnonStorey0__ctor_mC8F379C8356A1F1FE4E62393D402C0ABB7102F11 (U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::<>m__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7 (U3CInvokeInUpdateU3Ec__AnonStorey0_tFEBA2F8041FDB768908B2315C16F5B8306579B24 * __this, const RuntimeMethod* method)
{
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_eventParam_0();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Delegate_get_Target_mA4C35D598EE379F0F1D49EA8670620792D25EAB1_inline (Delegate_t * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_m_target_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}

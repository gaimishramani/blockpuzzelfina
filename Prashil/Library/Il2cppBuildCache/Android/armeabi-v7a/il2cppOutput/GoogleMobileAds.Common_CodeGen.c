﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GoogleMobileAds.Common.AdErrorClientEventArgs::.ctor()
extern void AdErrorClientEventArgs__ctor_m93239FDB158D4F24A099957AB1C9EA4E229CFE1B (void);
// 0x00000002 GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Common.AdErrorClientEventArgs::get_AdErrorClient()
extern void AdErrorClientEventArgs_get_AdErrorClient_m4440FC4F2048A9051F6D26340A22D653D9C284D1 (void);
// 0x00000003 System.Void GoogleMobileAds.Common.AdErrorClientEventArgs::set_AdErrorClient(GoogleMobileAds.Common.IAdErrorClient)
extern void AdErrorClientEventArgs_set_AdErrorClient_m3E63E09455836EED9523184A16C40F346ABB18BE (void);
// 0x00000004 System.Void GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::.ctor()
extern void AdInspectorErrorClientEventArgs__ctor_mA7484DD53EB3BDD93F5B63EBF462917BAEE3A17A (void);
// 0x00000005 GoogleMobileAds.Common.IAdInspectorErrorClient GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::get_AdErrorClient()
extern void AdInspectorErrorClientEventArgs_get_AdErrorClient_m22EC00E2D1C837C26202248647B4E7C2E45756F6 (void);
// 0x00000006 System.Void GoogleMobileAds.Common.AdInspectorErrorClientEventArgs::set_AdErrorClient(GoogleMobileAds.Common.IAdInspectorErrorClient)
extern void AdInspectorErrorClientEventArgs_set_AdErrorClient_m338465B56A77301A4A22D6A770097F96AB3EF3BC (void);
// 0x00000007 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::.ctor()
extern void AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321 (void);
// 0x00000008 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdLoaded_m309C884953361C82DB73B162DA53D47C4F663BD4 (void);
// 0x00000009 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdLoaded_m8CD62FE6C0D8679B6F262A2CADB9133AC2A50839 (void);
// 0x0000000A System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdFailedToLoad_m7728823A53F14F214F33C5AD27B74F85386C8C53 (void);
// 0x0000000B System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdFailedToLoad_m11F3806FF1BC289468F7443A213BB9FE935319F7 (void);
// 0x0000000C System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void AppOpenAdAdDummyClient_add_OnPaidEvent_mDC509BEAC7A54B488907E4D787D1AD7B8CF31090 (void);
// 0x0000000D System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnPaidEvent_mBD2A81EF4EC41FA1AA9ECAD4505D59CE98A2D99C (void);
// 0x0000000E System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m34634DE2135690A838D2E19265EC8002B9DF3E75 (void);
// 0x0000000F System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m2489B2EF5FCC0827D9A9E7309572011B89881BF6 (void);
// 0x00000010 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdDidPresentFullScreenContent_mDB41C6BE0B1F82383C15F8AF15FA4B2973C5C030 (void);
// 0x00000011 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdDidPresentFullScreenContent_m7802D2403D1AAAAD879BBA70F11EDB172DBCF21C (void);
// 0x00000012 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdDidDismissFullScreenContent_m1FE6317467AC357AF806662FD12A2D5E96561C7B (void);
// 0x00000013 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2B63C84A7AF691B024D9660E3BBEDC9B4F039015 (void);
// 0x00000014 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_add_OnAdDidRecordImpression_mF7CD3F4F1A6E6EA25E9A1EA565D06B788A29CA00 (void);
// 0x00000015 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void AppOpenAdAdDummyClient_remove_OnAdDidRecordImpression_m6905A5257B15830DC7BE26D6CABB25F5665F4B42 (void);
// 0x00000016 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::CreateAppOpenAd()
extern void AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41 (void);
// 0x00000017 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest,UnityEngine.ScreenOrientation)
extern void AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D (void);
// 0x00000018 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::Show()
extern void AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1 (void);
// 0x00000019 System.Void GoogleMobileAds.Common.AppOpenAdAdDummyClient::DestroyAppOpenAd()
extern void AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46 (void);
// 0x0000001A GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.AppOpenAdAdDummyClient::GetResponseInfoClient()
extern void AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53 (void);
// 0x0000001B System.Int32 GoogleMobileAds.Common.IAdErrorClient::GetCode()
// 0x0000001C System.String GoogleMobileAds.Common.IAdErrorClient::GetDomain()
// 0x0000001D System.String GoogleMobileAds.Common.IAdErrorClient::GetMessage()
// 0x0000001E GoogleMobileAds.Common.IAdErrorClient GoogleMobileAds.Common.IAdErrorClient::GetCause()
// 0x0000001F System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000020 System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000021 System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000022 System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000023 System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000024 System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000025 System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000026 System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000027 System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000028 System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000029 System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000002A System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000002B System.Void GoogleMobileAds.Common.IAppOpenAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000002C System.Void GoogleMobileAds.Common.IAppOpenAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000002D System.Void GoogleMobileAds.Common.IAppOpenAdClient::CreateAppOpenAd()
// 0x0000002E System.Void GoogleMobileAds.Common.IAppOpenAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest,UnityEngine.ScreenOrientation)
// 0x0000002F System.Void GoogleMobileAds.Common.IAppOpenAdClient::Show()
// 0x00000030 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.IAppOpenAdClient::GetResponseInfoClient()
// 0x00000031 System.Void GoogleMobileAds.Common.IAppOpenAdClient::DestroyAppOpenAd()
// 0x00000032 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000033 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000034 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000035 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000036 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x00000037 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x00000038 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000039 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x0000003A System.Void GoogleMobileAds.Common.IBannerClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x0000003B System.Void GoogleMobileAds.Common.IBannerClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x0000003C System.Void GoogleMobileAds.Common.IBannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
// 0x0000003D System.Void GoogleMobileAds.Common.IBannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
// 0x0000003E System.Void GoogleMobileAds.Common.IBannerClient::LoadAd(GoogleMobileAds.Api.AdRequest)
// 0x0000003F System.Void GoogleMobileAds.Common.IBannerClient::ShowBannerView()
// 0x00000040 System.Void GoogleMobileAds.Common.IBannerClient::HideBannerView()
// 0x00000041 System.Void GoogleMobileAds.Common.IBannerClient::DestroyBannerView()
// 0x00000042 System.Single GoogleMobileAds.Common.IBannerClient::GetHeightInPixels()
// 0x00000043 System.Single GoogleMobileAds.Common.IBannerClient::GetWidthInPixels()
// 0x00000044 System.Void GoogleMobileAds.Common.IBannerClient::SetPosition(GoogleMobileAds.Api.AdPosition)
// 0x00000045 System.Void GoogleMobileAds.Common.IBannerClient::SetPosition(System.Int32,System.Int32)
// 0x00000046 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.IBannerClient::GetResponseInfoClient()
// 0x00000047 GoogleMobileAds.Common.IAppOpenAdClient GoogleMobileAds.IClientFactory::BuildAppOpenAdClient()
// 0x00000048 GoogleMobileAds.Common.IBannerClient GoogleMobileAds.IClientFactory::BuildBannerClient()
// 0x00000049 GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.IClientFactory::BuildInterstitialClient()
// 0x0000004A GoogleMobileAds.Common.IRewardedAdClient GoogleMobileAds.IClientFactory::BuildRewardedAdClient()
// 0x0000004B GoogleMobileAds.Common.IRewardedInterstitialAdClient GoogleMobileAds.IClientFactory::BuildRewardedInterstitialAdClient()
// 0x0000004C GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.IClientFactory::MobileAdsInstance()
// 0x0000004D GoogleMobileAds.Api.AdapterStatus GoogleMobileAds.Common.IInitializationStatusClient::getAdapterStatusForClassName(System.String)
// 0x0000004E System.Collections.Generic.Dictionary`2<System.String,GoogleMobileAds.Api.AdapterStatus> GoogleMobileAds.Common.IInitializationStatusClient::getAdapterStatusMap()
// 0x0000004F System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000050 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000051 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000052 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000053 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000054 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000055 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000056 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000057 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000058 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000059 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000005A System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000005B System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000005C System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000005D System.Void GoogleMobileAds.Common.IInterstitialClient::CreateInterstitialAd()
// 0x0000005E System.Void GoogleMobileAds.Common.IInterstitialClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
// 0x0000005F System.Void GoogleMobileAds.Common.IInterstitialClient::Show()
// 0x00000060 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.IInterstitialClient::GetResponseInfoClient()
// 0x00000061 System.Void GoogleMobileAds.Common.IInterstitialClient::DestroyInterstitial()
// 0x00000062 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.ILoadAdErrorClient::GetResponseInfoClient()
// 0x00000063 System.Void GoogleMobileAds.Common.IMobileAdsClient::Initialize(System.Action`1<GoogleMobileAds.Common.IInitializationStatusClient>)
// 0x00000064 System.Void GoogleMobileAds.Common.IMobileAdsClient::DisableMediationInitialization()
// 0x00000065 System.Void GoogleMobileAds.Common.IMobileAdsClient::SetApplicationVolume(System.Single)
// 0x00000066 System.Void GoogleMobileAds.Common.IMobileAdsClient::SetApplicationMuted(System.Boolean)
// 0x00000067 System.Void GoogleMobileAds.Common.IMobileAdsClient::SetiOSAppPauseOnBackground(System.Boolean)
// 0x00000068 System.Single GoogleMobileAds.Common.IMobileAdsClient::GetDeviceScale()
// 0x00000069 System.Int32 GoogleMobileAds.Common.IMobileAdsClient::GetDeviceSafeWidth()
// 0x0000006A System.Void GoogleMobileAds.Common.IMobileAdsClient::SetRequestConfiguration(GoogleMobileAds.Api.RequestConfiguration)
// 0x0000006B GoogleMobileAds.Api.RequestConfiguration GoogleMobileAds.Common.IMobileAdsClient::GetRequestConfiguration()
// 0x0000006C System.Void GoogleMobileAds.Common.IMobileAdsClient::OpenAdInspector(System.Action`1<GoogleMobileAds.Common.AdInspectorErrorClientEventArgs>)
// 0x0000006D System.String GoogleMobileAds.Common.IResponseInfoClient::GetMediationAdapterClassName()
// 0x0000006E System.String GoogleMobileAds.Common.IResponseInfoClient::GetResponseId()
// 0x0000006F System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000070 System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000071 System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000072 System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000073 System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000074 System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x00000075 System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x00000076 System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x00000077 System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000078 System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000079 System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000007A System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000007B System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000007C System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x0000007D System.Void GoogleMobileAds.Common.IRewardedAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000007E System.Void GoogleMobileAds.Common.IRewardedAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x0000007F System.Void GoogleMobileAds.Common.IRewardedAdClient::CreateRewardedAd()
// 0x00000080 System.Void GoogleMobileAds.Common.IRewardedAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
// 0x00000081 GoogleMobileAds.Api.Reward GoogleMobileAds.Common.IRewardedAdClient::GetRewardItem()
// 0x00000082 System.Void GoogleMobileAds.Common.IRewardedAdClient::Show()
// 0x00000083 System.Void GoogleMobileAds.Common.IRewardedAdClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
// 0x00000084 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.IRewardedAdClient::GetResponseInfoClient()
// 0x00000085 System.Void GoogleMobileAds.Common.IRewardedAdClient::DestroyRewardedAd()
// 0x00000086 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000087 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000088 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x00000089 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
// 0x0000008A System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x0000008B System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
// 0x0000008C System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x0000008D System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x0000008E System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x0000008F System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
// 0x00000090 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000091 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000092 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000093 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
// 0x00000094 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x00000095 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
// 0x00000096 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::CreateRewardedInterstitialAd()
// 0x00000097 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
// 0x00000098 GoogleMobileAds.Api.Reward GoogleMobileAds.Common.IRewardedInterstitialAdClient::GetRewardItem()
// 0x00000099 System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::Show()
// 0x0000009A System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
// 0x0000009B GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.IRewardedInterstitialAdClient::GetResponseInfoClient()
// 0x0000009C System.Void GoogleMobileAds.Common.IRewardedInterstitialAdClient::DestroyRewardedInterstitialAd()
// 0x0000009D System.Void GoogleMobileAds.Common.LoadAdErrorClientEventArgs::.ctor()
extern void LoadAdErrorClientEventArgs__ctor_mE45381F3D10AF46E60928BB3540FE9A05B56E70E (void);
// 0x0000009E GoogleMobileAds.Common.ILoadAdErrorClient GoogleMobileAds.Common.LoadAdErrorClientEventArgs::get_LoadAdErrorClient()
extern void LoadAdErrorClientEventArgs_get_LoadAdErrorClient_mF8379F2375AB2F5BD4F0C47371DED5E32A72115B (void);
// 0x0000009F System.Void GoogleMobileAds.Common.LoadAdErrorClientEventArgs::set_LoadAdErrorClient(GoogleMobileAds.Common.ILoadAdErrorClient)
extern void LoadAdErrorClientEventArgs_set_LoadAdErrorClient_mE9FFC8AD6E3337EEA9C1296C657CCD8F326BF256 (void);
// 0x000000A0 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::.ctor()
extern void MobileAdsEventExecutor__ctor_m5C94414047CAFBE0DA34C863D65C83E64B6F9F1A (void);
// 0x000000A1 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Initialize()
extern void MobileAdsEventExecutor_Initialize_m1448806DA8253632AFD68C0B29AEF16C287FF5D6 (void);
// 0x000000A2 System.Boolean GoogleMobileAds.Common.MobileAdsEventExecutor::IsActive()
extern void MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E (void);
// 0x000000A3 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Awake()
extern void MobileAdsEventExecutor_Awake_m95585B833F8DA4129ED6EFB2EDD76BC60255D26B (void);
// 0x000000A4 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::ExecuteInUpdate(System.Action)
extern void MobileAdsEventExecutor_ExecuteInUpdate_m8A940774825EEDE73D61C609EB12B68FC81A4E4B (void);
// 0x000000A5 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::InvokeInUpdate(UnityEngine.Events.UnityEvent)
extern void MobileAdsEventExecutor_InvokeInUpdate_m786542D0978DC8034D345A9BF095CEC26B751900 (void);
// 0x000000A6 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::Update()
extern void MobileAdsEventExecutor_Update_mB3B2E0F578A69796B8751DFAD1D8B86BB7E82126 (void);
// 0x000000A7 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::OnDisable()
extern void MobileAdsEventExecutor_OnDisable_mE2756A2E6860A83452361BF52DED618EBF5FC323 (void);
// 0x000000A8 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor::.cctor()
extern void MobileAdsEventExecutor__cctor_mA46D12A46E0E0C8EBEC865AA1BF29513FA55EDA2 (void);
// 0x000000A9 System.Void GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::.ctor()
extern void U3CInvokeInUpdateU3Ec__AnonStorey0__ctor_mC8F379C8356A1F1FE4E62393D402C0ABB7102F11 (void);
// 0x000000AA System.Void GoogleMobileAds.Common.MobileAdsEventExecutor/<InvokeInUpdate>c__AnonStorey0::<>m__0()
extern void U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7 (void);
// 0x000000AB System.Void GoogleMobileAds.Common.RewardedAdDummyClient::.ctor()
extern void RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C (void);
// 0x000000AC System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_add_OnAdLoaded_m18EEB16C76E34313A182246DCAAD17CC967FDC66 (void);
// 0x000000AD System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_remove_OnAdLoaded_m8FDB4BDD09573E33DF962B1A1623274F4CB322C0 (void);
// 0x000000AE System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedAdDummyClient_add_OnAdFailedToLoad_mB5A1AA8A6664682B380934076662A362A6A72F2D (void);
// 0x000000AF System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedAdDummyClient_remove_OnAdFailedToLoad_m3CE4C2667B82106FDC1F3C1478BB1E64DB66FADA (void);
// 0x000000B0 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAdDummyClient_add_OnUserEarnedReward_mAC506289B1E5C551AF3DAC147B7A044104CA01CB (void);
// 0x000000B1 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedAdDummyClient_remove_OnUserEarnedReward_m3398B13A054E69E7D64BB13510AA7CCF88FC95FA (void);
// 0x000000B2 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAdDummyClient_add_OnPaidEvent_mC744005D4D20ECE86DA7D02635569A8648366EF5 (void);
// 0x000000B3 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedAdDummyClient_remove_OnPaidEvent_m7AFE340D4FBCE5431CB59D682152EECE72551057 (void);
// 0x000000B4 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m716763228696903449AFA07A532C66F4DF9520D3 (void);
// 0x000000B5 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m1E8E146DBF6C6DCA419A29BBD8B3FA96AD59F0A0 (void);
// 0x000000B6 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_add_OnAdDidPresentFullScreenContent_mF029D975810A792BB7749553B7851DA2B9736DFC (void);
// 0x000000B7 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_remove_OnAdDidPresentFullScreenContent_mE66FF1E03C6601CED2B8291D702B086F8E5F8ECC (void);
// 0x000000B8 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_add_OnAdDidDismissFullScreenContent_mD74F37CCCEA460E1E57864FFE4389A23D136D02B (void);
// 0x000000B9 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_remove_OnAdDidDismissFullScreenContent_mA6CD8B693C3D7087715A179D6F8B96EE7FBFB71A (void);
// 0x000000BA System.Void GoogleMobileAds.Common.RewardedAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_add_OnAdDidRecordImpression_mCC0F4BF042C5BDCF89D149ED7A91C7FFD61FDDF2 (void);
// 0x000000BB System.Void GoogleMobileAds.Common.RewardedAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedAdDummyClient_remove_OnAdDidRecordImpression_m05C094862F16C31C13752A059462351A373D43FF (void);
// 0x000000BC System.Void GoogleMobileAds.Common.RewardedAdDummyClient::CreateRewardedAd()
extern void RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2 (void);
// 0x000000BD System.Void GoogleMobileAds.Common.RewardedAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
extern void RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC (void);
// 0x000000BE System.Void GoogleMobileAds.Common.RewardedAdDummyClient::Show()
extern void RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8 (void);
// 0x000000BF GoogleMobileAds.Api.Reward GoogleMobileAds.Common.RewardedAdDummyClient::GetRewardItem()
extern void RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F (void);
// 0x000000C0 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
extern void RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213 (void);
// 0x000000C1 System.Void GoogleMobileAds.Common.RewardedAdDummyClient::DestroyRewardedAd()
extern void RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC (void);
// 0x000000C2 GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.RewardedAdDummyClient::GetResponseInfoClient()
extern void RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85 (void);
// 0x000000C3 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::.ctor()
extern void RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1 (void);
// 0x000000C4 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdLoaded_mB94D58D69016376BCD0201F00169984E1BD45ACF (void);
// 0x000000C5 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdLoaded_m93C328456448580FD6285912D1C9BA75317C558F (void);
// 0x000000C6 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdFailedToLoad_mB3BA795652E1ECCB25E758D64FDD96D9E235FAF6 (void);
// 0x000000C7 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Common.LoadAdErrorClientEventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdFailedToLoad_m1E445E210D2292F18487D7A53CED6F7E259FBBD3 (void);
// 0x000000C8 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedInterstitialAdDummyClient_add_OnUserEarnedReward_mDCDB6E058DE644FF7935BA64D8F70CD94DF31A15 (void);
// 0x000000C9 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnUserEarnedReward(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardedInterstitialAdDummyClient_remove_OnUserEarnedReward_mFFEDD3E13D839FBFC3C7DE671CB9F963F6BBB7BC (void);
// 0x000000CA System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnPaidEvent_mF76B2A38E9B493992BD6FF34FB62822C8992A6BC (void);
// 0x000000CB System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnPaidEvent(System.EventHandler`1<GoogleMobileAds.Api.AdValueEventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnPaidEvent_mC398ED83432D6CE1A90E7C19D90A1CF46FF10830 (void);
// 0x000000CC System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m50CDDC72D9D6BC3DBE2AD262E7DB295BC27A2483 (void);
// 0x000000CD System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdFailedToPresentFullScreenContent(System.EventHandler`1<GoogleMobileAds.Common.AdErrorClientEventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_mE9F580E20049BE17BE16F3F0C6E2BC5736B28BA6 (void);
// 0x000000CE System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdDidPresentFullScreenContent_mA4E39F1B1CAAEA898A869178906E99EF62BABEF4 (void);
// 0x000000CF System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidPresentFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdDidPresentFullScreenContent_mC5830CA0BB47C5054A1007066CC1E7E776CB7A25 (void);
// 0x000000D0 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdDidDismissFullScreenContent_m33E58EE8BC7BF37AA405C981EF14F28D6E3D413C (void);
// 0x000000D1 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidDismissFullScreenContent(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2411DE6CA699D2BE18690D1691B99F44343FA5A4 (void);
// 0x000000D2 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::add_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_add_OnAdDidRecordImpression_m2EAF100D93ABE9093CE22BED4D5FE8452AD60693 (void);
// 0x000000D3 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::remove_OnAdDidRecordImpression(System.EventHandler`1<System.EventArgs>)
extern void RewardedInterstitialAdDummyClient_remove_OnAdDidRecordImpression_m90D509D7D361B214DD81B902F1AD9D8ABB5AC2F0 (void);
// 0x000000D4 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::CreateRewardedInterstitialAd()
extern void RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2 (void);
// 0x000000D5 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::LoadAd(System.String,GoogleMobileAds.Api.AdRequest)
extern void RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC (void);
// 0x000000D6 GoogleMobileAds.Api.Reward GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::GetRewardItem()
extern void RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6 (void);
// 0x000000D7 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::Show()
extern void RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4 (void);
// 0x000000D8 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::SetServerSideVerificationOptions(GoogleMobileAds.Api.ServerSideVerificationOptions)
extern void RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C (void);
// 0x000000D9 System.Void GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::DestroyRewardedInterstitialAd()
extern void RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060 (void);
// 0x000000DA GoogleMobileAds.Common.IResponseInfoClient GoogleMobileAds.Common.RewardedInterstitialAdDummyClient::GetResponseInfoClient()
extern void RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C (void);
// 0x000000DB System.Void GoogleMobileAds.Common.Utils::.ctor()
extern void Utils__ctor_m14AAC251D5294F487EC89EA0A097AC98F37CA892 (void);
// 0x000000DC System.Void GoogleMobileAds.Common.Utils::CheckInitialization()
extern void Utils_CheckInitialization_m7E335C1BEE73E87BE067FED40013CE55C9661327 (void);
// 0x000000DD UnityEngine.Texture2D GoogleMobileAds.Common.Utils::GetTexture2DFromByteArray(System.Byte[])
extern void Utils_GetTexture2DFromByteArray_m8CDFB66C3562334923A1F7933602821DAC7F0A2B (void);
static Il2CppMethodPointer s_methodPointers[221] = 
{
	AdErrorClientEventArgs__ctor_m93239FDB158D4F24A099957AB1C9EA4E229CFE1B,
	AdErrorClientEventArgs_get_AdErrorClient_m4440FC4F2048A9051F6D26340A22D653D9C284D1,
	AdErrorClientEventArgs_set_AdErrorClient_m3E63E09455836EED9523184A16C40F346ABB18BE,
	AdInspectorErrorClientEventArgs__ctor_mA7484DD53EB3BDD93F5B63EBF462917BAEE3A17A,
	AdInspectorErrorClientEventArgs_get_AdErrorClient_m22EC00E2D1C837C26202248647B4E7C2E45756F6,
	AdInspectorErrorClientEventArgs_set_AdErrorClient_m338465B56A77301A4A22D6A770097F96AB3EF3BC,
	AppOpenAdAdDummyClient__ctor_mC8C223B930FFD35B3F88044F3BA72B1BFDEF8321,
	AppOpenAdAdDummyClient_add_OnAdLoaded_m309C884953361C82DB73B162DA53D47C4F663BD4,
	AppOpenAdAdDummyClient_remove_OnAdLoaded_m8CD62FE6C0D8679B6F262A2CADB9133AC2A50839,
	AppOpenAdAdDummyClient_add_OnAdFailedToLoad_m7728823A53F14F214F33C5AD27B74F85386C8C53,
	AppOpenAdAdDummyClient_remove_OnAdFailedToLoad_m11F3806FF1BC289468F7443A213BB9FE935319F7,
	AppOpenAdAdDummyClient_add_OnPaidEvent_mDC509BEAC7A54B488907E4D787D1AD7B8CF31090,
	AppOpenAdAdDummyClient_remove_OnPaidEvent_mBD2A81EF4EC41FA1AA9ECAD4505D59CE98A2D99C,
	AppOpenAdAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m34634DE2135690A838D2E19265EC8002B9DF3E75,
	AppOpenAdAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m2489B2EF5FCC0827D9A9E7309572011B89881BF6,
	AppOpenAdAdDummyClient_add_OnAdDidPresentFullScreenContent_mDB41C6BE0B1F82383C15F8AF15FA4B2973C5C030,
	AppOpenAdAdDummyClient_remove_OnAdDidPresentFullScreenContent_m7802D2403D1AAAAD879BBA70F11EDB172DBCF21C,
	AppOpenAdAdDummyClient_add_OnAdDidDismissFullScreenContent_m1FE6317467AC357AF806662FD12A2D5E96561C7B,
	AppOpenAdAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2B63C84A7AF691B024D9660E3BBEDC9B4F039015,
	AppOpenAdAdDummyClient_add_OnAdDidRecordImpression_mF7CD3F4F1A6E6EA25E9A1EA565D06B788A29CA00,
	AppOpenAdAdDummyClient_remove_OnAdDidRecordImpression_m6905A5257B15830DC7BE26D6CABB25F5665F4B42,
	AppOpenAdAdDummyClient_CreateAppOpenAd_m37B243878D1684D7D9B6251F8842ADD119A6BC41,
	AppOpenAdAdDummyClient_LoadAd_m96AF941D6C8B57C410B4E43B6D85AA9F56E7EC7D,
	AppOpenAdAdDummyClient_Show_mEFB758411ADA2B3961AF58C53551763445761FF1,
	AppOpenAdAdDummyClient_DestroyAppOpenAd_mD583015A9FCD1BFAF299559FED22C6C285883A46,
	AppOpenAdAdDummyClient_GetResponseInfoClient_m4B6120C5026E68D6B11A0D01D168B0DEA9079E53,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LoadAdErrorClientEventArgs__ctor_mE45381F3D10AF46E60928BB3540FE9A05B56E70E,
	LoadAdErrorClientEventArgs_get_LoadAdErrorClient_mF8379F2375AB2F5BD4F0C47371DED5E32A72115B,
	LoadAdErrorClientEventArgs_set_LoadAdErrorClient_mE9FFC8AD6E3337EEA9C1296C657CCD8F326BF256,
	MobileAdsEventExecutor__ctor_m5C94414047CAFBE0DA34C863D65C83E64B6F9F1A,
	MobileAdsEventExecutor_Initialize_m1448806DA8253632AFD68C0B29AEF16C287FF5D6,
	MobileAdsEventExecutor_IsActive_mEB458352CA7B0AFEFBB705171037FB13782D310E,
	MobileAdsEventExecutor_Awake_m95585B833F8DA4129ED6EFB2EDD76BC60255D26B,
	MobileAdsEventExecutor_ExecuteInUpdate_m8A940774825EEDE73D61C609EB12B68FC81A4E4B,
	MobileAdsEventExecutor_InvokeInUpdate_m786542D0978DC8034D345A9BF095CEC26B751900,
	MobileAdsEventExecutor_Update_mB3B2E0F578A69796B8751DFAD1D8B86BB7E82126,
	MobileAdsEventExecutor_OnDisable_mE2756A2E6860A83452361BF52DED618EBF5FC323,
	MobileAdsEventExecutor__cctor_mA46D12A46E0E0C8EBEC865AA1BF29513FA55EDA2,
	U3CInvokeInUpdateU3Ec__AnonStorey0__ctor_mC8F379C8356A1F1FE4E62393D402C0ABB7102F11,
	U3CInvokeInUpdateU3Ec__AnonStorey0_U3CU3Em__0_mE65AD8C9D1FDCFFB2415170E9FCB2789E0EE07F7,
	RewardedAdDummyClient__ctor_m92B9ED10A21B41EA3FC056411288C84D3C6EE13C,
	RewardedAdDummyClient_add_OnAdLoaded_m18EEB16C76E34313A182246DCAAD17CC967FDC66,
	RewardedAdDummyClient_remove_OnAdLoaded_m8FDB4BDD09573E33DF962B1A1623274F4CB322C0,
	RewardedAdDummyClient_add_OnAdFailedToLoad_mB5A1AA8A6664682B380934076662A362A6A72F2D,
	RewardedAdDummyClient_remove_OnAdFailedToLoad_m3CE4C2667B82106FDC1F3C1478BB1E64DB66FADA,
	RewardedAdDummyClient_add_OnUserEarnedReward_mAC506289B1E5C551AF3DAC147B7A044104CA01CB,
	RewardedAdDummyClient_remove_OnUserEarnedReward_m3398B13A054E69E7D64BB13510AA7CCF88FC95FA,
	RewardedAdDummyClient_add_OnPaidEvent_mC744005D4D20ECE86DA7D02635569A8648366EF5,
	RewardedAdDummyClient_remove_OnPaidEvent_m7AFE340D4FBCE5431CB59D682152EECE72551057,
	RewardedAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m716763228696903449AFA07A532C66F4DF9520D3,
	RewardedAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_m1E8E146DBF6C6DCA419A29BBD8B3FA96AD59F0A0,
	RewardedAdDummyClient_add_OnAdDidPresentFullScreenContent_mF029D975810A792BB7749553B7851DA2B9736DFC,
	RewardedAdDummyClient_remove_OnAdDidPresentFullScreenContent_mE66FF1E03C6601CED2B8291D702B086F8E5F8ECC,
	RewardedAdDummyClient_add_OnAdDidDismissFullScreenContent_mD74F37CCCEA460E1E57864FFE4389A23D136D02B,
	RewardedAdDummyClient_remove_OnAdDidDismissFullScreenContent_mA6CD8B693C3D7087715A179D6F8B96EE7FBFB71A,
	RewardedAdDummyClient_add_OnAdDidRecordImpression_mCC0F4BF042C5BDCF89D149ED7A91C7FFD61FDDF2,
	RewardedAdDummyClient_remove_OnAdDidRecordImpression_m05C094862F16C31C13752A059462351A373D43FF,
	RewardedAdDummyClient_CreateRewardedAd_m098BA5D3AE42A1B308E0340932B2BB359A1318E2,
	RewardedAdDummyClient_LoadAd_m2B2ECBD0AA861937D8792E8E15EC91ABC5AC58CC,
	RewardedAdDummyClient_Show_mE2841B00146D94113932C4B751ED754A40E6C8C8,
	RewardedAdDummyClient_GetRewardItem_mC327F6CA7803DA3F8D75946C5E73D82B34CCD46F,
	RewardedAdDummyClient_SetServerSideVerificationOptions_m9FF0D4C9EF65381DFF29A3B9772DCBE31D005213,
	RewardedAdDummyClient_DestroyRewardedAd_m8C719D3B173AFA960C1D69FD764B77C74FFE5AEC,
	RewardedAdDummyClient_GetResponseInfoClient_m3CD737C161E42AB0FBAB5CE6C10CDB8CA6FC8D85,
	RewardedInterstitialAdDummyClient__ctor_mD062BED66BA45DA6A0EA0A824AE5BA90E09571F1,
	RewardedInterstitialAdDummyClient_add_OnAdLoaded_mB94D58D69016376BCD0201F00169984E1BD45ACF,
	RewardedInterstitialAdDummyClient_remove_OnAdLoaded_m93C328456448580FD6285912D1C9BA75317C558F,
	RewardedInterstitialAdDummyClient_add_OnAdFailedToLoad_mB3BA795652E1ECCB25E758D64FDD96D9E235FAF6,
	RewardedInterstitialAdDummyClient_remove_OnAdFailedToLoad_m1E445E210D2292F18487D7A53CED6F7E259FBBD3,
	RewardedInterstitialAdDummyClient_add_OnUserEarnedReward_mDCDB6E058DE644FF7935BA64D8F70CD94DF31A15,
	RewardedInterstitialAdDummyClient_remove_OnUserEarnedReward_mFFEDD3E13D839FBFC3C7DE671CB9F963F6BBB7BC,
	RewardedInterstitialAdDummyClient_add_OnPaidEvent_mF76B2A38E9B493992BD6FF34FB62822C8992A6BC,
	RewardedInterstitialAdDummyClient_remove_OnPaidEvent_mC398ED83432D6CE1A90E7C19D90A1CF46FF10830,
	RewardedInterstitialAdDummyClient_add_OnAdFailedToPresentFullScreenContent_m50CDDC72D9D6BC3DBE2AD262E7DB295BC27A2483,
	RewardedInterstitialAdDummyClient_remove_OnAdFailedToPresentFullScreenContent_mE9F580E20049BE17BE16F3F0C6E2BC5736B28BA6,
	RewardedInterstitialAdDummyClient_add_OnAdDidPresentFullScreenContent_mA4E39F1B1CAAEA898A869178906E99EF62BABEF4,
	RewardedInterstitialAdDummyClient_remove_OnAdDidPresentFullScreenContent_mC5830CA0BB47C5054A1007066CC1E7E776CB7A25,
	RewardedInterstitialAdDummyClient_add_OnAdDidDismissFullScreenContent_m33E58EE8BC7BF37AA405C981EF14F28D6E3D413C,
	RewardedInterstitialAdDummyClient_remove_OnAdDidDismissFullScreenContent_m2411DE6CA699D2BE18690D1691B99F44343FA5A4,
	RewardedInterstitialAdDummyClient_add_OnAdDidRecordImpression_m2EAF100D93ABE9093CE22BED4D5FE8452AD60693,
	RewardedInterstitialAdDummyClient_remove_OnAdDidRecordImpression_m90D509D7D361B214DD81B902F1AD9D8ABB5AC2F0,
	RewardedInterstitialAdDummyClient_CreateRewardedInterstitialAd_m9371CEE1097CCF75AA23914677F6C3525131B2B2,
	RewardedInterstitialAdDummyClient_LoadAd_m360B56AFCA218F30DF56378F3BEAEFDB7FF2F6DC,
	RewardedInterstitialAdDummyClient_GetRewardItem_mBB876339BCA2A4AC3591FD0847337262903074C6,
	RewardedInterstitialAdDummyClient_Show_m8231B1503252B5FD955D418C6F5865070C6B35E4,
	RewardedInterstitialAdDummyClient_SetServerSideVerificationOptions_m0F3644AB4B63BEEE8880F3701FB71E1CF07A650C,
	RewardedInterstitialAdDummyClient_DestroyRewardedInterstitialAd_mBA529728FE725B4BA06267D08F63AE3E5A85E060,
	RewardedInterstitialAdDummyClient_GetResponseInfoClient_m8C7988F7FB10E1F62ACB0F7F3381EA0F3BF0770C,
	Utils__ctor_m14AAC251D5294F487EC89EA0A097AC98F37CA892,
	Utils_CheckInitialization_m7E335C1BEE73E87BE067FED40013CE55C9661327,
	Utils_GetTexture2DFromByteArray_m8CDFB66C3562334923A1F7933602821DAC7F0A2B,
};
static const int32_t s_InvokerIndices[221] = 
{
	3068,
	3002,
	2563,
	3068,
	3002,
	2563,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1031,
	3068,
	3068,
	3002,
	2985,
	3002,
	3002,
	3002,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1031,
	3068,
	3002,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	1031,
	661,
	2563,
	3068,
	3068,
	3068,
	3029,
	3029,
	2548,
	1443,
	3002,
	3002,
	3002,
	3002,
	3002,
	3002,
	3002,
	2029,
	3002,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3068,
	3002,
	3068,
	3002,
	2563,
	3068,
	2588,
	2585,
	2585,
	3029,
	2985,
	2563,
	3002,
	2563,
	3002,
	3002,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3002,
	3068,
	2563,
	3002,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3002,
	3068,
	2563,
	3002,
	3068,
	3068,
	3002,
	2563,
	3068,
	4807,
	4797,
	3068,
	4745,
	4745,
	3068,
	3068,
	4807,
	3068,
	3068,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3068,
	3002,
	2563,
	3068,
	3002,
	3068,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	2563,
	3068,
	1567,
	3002,
	3068,
	2563,
	3068,
	3002,
	3068,
	4807,
	4601,
};
extern const CustomAttributesCacheGenerator g_GoogleMobileAds_Common_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_GoogleMobileAds_Common_CodeGenModule;
const Il2CppCodeGenModule g_GoogleMobileAds_Common_CodeGenModule = 
{
	"GoogleMobileAds.Common.dll",
	221,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_GoogleMobileAds_Common_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

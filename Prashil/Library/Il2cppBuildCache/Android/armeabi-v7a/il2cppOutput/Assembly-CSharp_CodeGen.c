﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AdManager::Start()
extern void AdManager_Start_m07F7C8DA24A1908D9E6AC811C2574154A31F68C4 (void);
// 0x00000002 System.Void AdManager::Update()
extern void AdManager_Update_m4267B5FCE42EEA0752333421FC184C58CB4BD345 (void);
// 0x00000003 System.Void AdManager::.ctor()
extern void AdManager__ctor_mDEF91F5263438BACE36D66E7434579BEDEC9A3FC (void);
// 0x00000004 System.Void BaseGamePlay::OnEnable()
extern void BaseGamePlay_OnEnable_mAA0998F3144F6980C6193F660CBDD8A297B4CB84 (void);
// 0x00000005 System.Void BaseGamePlay::OnDisable()
extern void BaseGamePlay_OnDisable_m4EE3FC52274B7CFAF90F4047084CFCC5AE6861F0 (void);
// 0x00000006 System.Void BaseGamePlay::Init()
extern void BaseGamePlay_Init_m46BC30ADD8AF0451776052BE9E5C8994BBADDC8E (void);
// 0x00000007 System.Void BaseGamePlay::UpdateScore(System.Int32)
extern void BaseGamePlay_UpdateScore_mB561621F15C01A419E64409C9BACDAEA05ADFA8A (void);
// 0x00000008 System.Void BaseGamePlay::ResetScore()
extern void BaseGamePlay_ResetScore_mD7CBD6578D25E684A3503148B175F3E2085E770A (void);
// 0x00000009 System.Void BaseGamePlay::SaveScore()
extern void BaseGamePlay_SaveScore_m18D189283B6E0444406554577531B24C84A67B72 (void);
// 0x0000000A System.Void BaseGamePlay::DisplayScore()
extern void BaseGamePlay_DisplayScore_m9E4D47B502DA1B84DC76F8B468F6940CBD23EA5B (void);
// 0x0000000B System.Void BaseGamePlay::SetSoundImage()
extern void BaseGamePlay_SetSoundImage_m2A22718636BDA668AF76F0324616D67898D63955 (void);
// 0x0000000C System.Collections.IEnumerator BaseGamePlay::ScoreAddAnimationCo(System.Int32,System.Int32)
extern void BaseGamePlay_ScoreAddAnimationCo_m3C24F4BF626C5B7AC26219B79F212308C9B5FE81 (void);
// 0x0000000D System.Void BaseGamePlay::OnSettingClick()
extern void BaseGamePlay_OnSettingClick_m51B0F072492A082470CD06A06F68460CA2536EAB (void);
// 0x0000000E System.Void BaseGamePlay::OnDummyButtonClick()
extern void BaseGamePlay_OnDummyButtonClick_mC62A0BE57CC97FB7FA7BBA9AD61FD5723FEDADE0 (void);
// 0x0000000F System.Collections.IEnumerator BaseGamePlay::SettingPannelSetActive()
extern void BaseGamePlay_SettingPannelSetActive_m0BFC6B7C772CFE426E248E7103D7F94EC3523203 (void);
// 0x00000010 System.Void BaseGamePlay::OnToggleSoundClick()
extern void BaseGamePlay_OnToggleSoundClick_mAE358699BA8170B19ADAF87A598E3A5E28FC086A (void);
// 0x00000011 System.Void BaseGamePlay::HomeClick()
extern void BaseGamePlay_HomeClick_mBAA4DC2CF2343383105936EA3C728218810A46E1 (void);
// 0x00000012 System.Void BaseGamePlay::OnRestartClick()
extern void BaseGamePlay_OnRestartClick_mDAD8C6D79A7A9B66457A5D2F19EB2093AEEEA3B5 (void);
// 0x00000013 System.Void BaseGamePlay::OnBackBttonClick()
extern void BaseGamePlay_OnBackBttonClick_m1CA4B4FFA3FA2DE878FC7B72F59C585BA9552806 (void);
// 0x00000014 System.Void BaseGamePlay::.ctor()
extern void BaseGamePlay__ctor_m1BB77C9BF6D46A2F86BF015E8A1232390850F410 (void);
// 0x00000015 System.Void BaseGamePlay/<ScoreAddAnimationCo>d__18::.ctor(System.Int32)
extern void U3CScoreAddAnimationCoU3Ed__18__ctor_m8EC82519287ECAAFBA39C5CEE63FD15688248F04 (void);
// 0x00000016 System.Void BaseGamePlay/<ScoreAddAnimationCo>d__18::System.IDisposable.Dispose()
extern void U3CScoreAddAnimationCoU3Ed__18_System_IDisposable_Dispose_m666951E2C45AA724446E55B254071A4D1EED0B72 (void);
// 0x00000017 System.Boolean BaseGamePlay/<ScoreAddAnimationCo>d__18::MoveNext()
extern void U3CScoreAddAnimationCoU3Ed__18_MoveNext_m1EFE665E5C93A44233C391FB6750C3F22DD36C02 (void);
// 0x00000018 System.Object BaseGamePlay/<ScoreAddAnimationCo>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScoreAddAnimationCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF83A5DBD4F110E0F7FFCA7334F7C7A14443B7A45 (void);
// 0x00000019 System.Void BaseGamePlay/<ScoreAddAnimationCo>d__18::System.Collections.IEnumerator.Reset()
extern void U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_Reset_m5504B8BF34108E89426ED5A1242AFFC96C8BE280 (void);
// 0x0000001A System.Object BaseGamePlay/<ScoreAddAnimationCo>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_get_Current_m7686727706784EA45AC761E89C88713861A91708 (void);
// 0x0000001B System.Void BaseGamePlay/<SettingPannelSetActive>d__21::.ctor(System.Int32)
extern void U3CSettingPannelSetActiveU3Ed__21__ctor_m670E0B3DDFD7550DDFF55DA58A34E83E355128F6 (void);
// 0x0000001C System.Void BaseGamePlay/<SettingPannelSetActive>d__21::System.IDisposable.Dispose()
extern void U3CSettingPannelSetActiveU3Ed__21_System_IDisposable_Dispose_m34D2D4427AAC05311069E68B47F303E3A62815DF (void);
// 0x0000001D System.Boolean BaseGamePlay/<SettingPannelSetActive>d__21::MoveNext()
extern void U3CSettingPannelSetActiveU3Ed__21_MoveNext_m7AD0FFBD51C2A60CA7942EC8AB0E369C288136C6 (void);
// 0x0000001E System.Object BaseGamePlay/<SettingPannelSetActive>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSettingPannelSetActiveU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B6FD2D99C4DEDED021B9555C09DE73BE432BFE9 (void);
// 0x0000001F System.Void BaseGamePlay/<SettingPannelSetActive>d__21::System.Collections.IEnumerator.Reset()
extern void U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_Reset_mFA1700E5C0151AC8623508B6489EFC0ADE514F52 (void);
// 0x00000020 System.Object BaseGamePlay/<SettingPannelSetActive>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_get_Current_m211E5C8CA6F25079022FB27F7454CCAB6C34977C (void);
// 0x00000021 System.Void GoogleMobileAdsDemoScript::Start()
extern void GoogleMobileAdsDemoScript_Start_m02AB0806DAA8316AA8909B3D78DE4ED44D98D6F7 (void);
// 0x00000022 System.Void GoogleMobileAdsDemoScript::RequestBanner()
extern void GoogleMobileAdsDemoScript_RequestBanner_m704F024CB070432AD4CC6736A01E1938AF1A0A1F (void);
// 0x00000023 System.Void GoogleMobileAdsDemoScript::HandleOnAdLoaded(System.Object,System.EventArgs)
extern void GoogleMobileAdsDemoScript_HandleOnAdLoaded_mB5543F20721EA0A6B23B7F9174E6F38A1CFCD7D3 (void);
// 0x00000024 System.Void GoogleMobileAdsDemoScript::HandleOnAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void GoogleMobileAdsDemoScript_HandleOnAdFailedToLoad_m639EE664F1CAF4C4E590E37A3C4DC6F1977ED3F7 (void);
// 0x00000025 System.Void GoogleMobileAdsDemoScript::HandleOnAdOpened(System.Object,System.EventArgs)
extern void GoogleMobileAdsDemoScript_HandleOnAdOpened_mA1C660C9D16C689A64DFF81EE49184D6574BE089 (void);
// 0x00000026 System.Void GoogleMobileAdsDemoScript::HandleOnAdClosed(System.Object,System.EventArgs)
extern void GoogleMobileAdsDemoScript_HandleOnAdClosed_mAA5C0F9B1B17B6544BDC4A63231E443BD0688BD3 (void);
// 0x00000027 System.Void GoogleMobileAdsDemoScript::.ctor()
extern void GoogleMobileAdsDemoScript__ctor_m48B3E925F49AB203248C2D0AF43ED4AAF39F6531 (void);
// 0x00000028 System.Void GoogleMobileAdsDemoScript/<>c::.cctor()
extern void U3CU3Ec__cctor_mB2CE7DD41E0B041142CE81C310E345ECAE487CFB (void);
// 0x00000029 System.Void GoogleMobileAdsDemoScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m3729A42D4168E754229DF8B5D779AA8DE43A1E76 (void);
// 0x0000002A System.Void GoogleMobileAdsDemoScript/<>c::<Start>b__1_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CStartU3Eb__1_0_m29D30F558CF35CCB5107553D8A049915ACB5F755 (void);
// 0x0000002B System.Void Admanager::Awake()
extern void Admanager_Awake_m994F2EF9DD47E84170F824B432BA1694BC195123 (void);
// 0x0000002C System.Void Admanager::RequestInterstitial()
extern void Admanager_RequestInterstitial_mA96E258BF9631AFB4330117A82F830D4B7E8D82B (void);
// 0x0000002D System.Void Admanager::LoadAd()
extern void Admanager_LoadAd_m40CFD75061280A7D8DDAC35D807F00C9D8156BF0 (void);
// 0x0000002E System.Void Admanager::ShowAd()
extern void Admanager_ShowAd_m80C7774EA7D5668E6DC37670687434CC2EDFC20A (void);
// 0x0000002F System.Void Admanager::HandleOnAdLoaded(System.Object,System.EventArgs)
extern void Admanager_HandleOnAdLoaded_mADC93D3182B8492F399AB6E23E90F1CCF8E700F8 (void);
// 0x00000030 System.Void Admanager::HandleOnAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void Admanager_HandleOnAdFailedToLoad_m8CE3B47C31412A98A179AB7945006863E870BF87 (void);
// 0x00000031 System.Void Admanager::HandleOnAdOpening(System.Object,System.EventArgs)
extern void Admanager_HandleOnAdOpening_mC793EF0E2D583D3C297655B9D36A7E5602D8E670 (void);
// 0x00000032 System.Void Admanager::HandleOnAdClosed(System.Object,System.EventArgs)
extern void Admanager_HandleOnAdClosed_m11E399723614B8D9833C52A6A11693E3E868B2C4 (void);
// 0x00000033 System.Void Admanager::.ctor()
extern void Admanager__ctor_mE86D433FC77298E184F06642D2497EC31FBB5154 (void);
// 0x00000034 System.Void TimerDataSO::.ctor()
extern void TimerDataSO__ctor_mCFFF8170646C213FEA250930F75C2B997734CBF6 (void);
// 0x00000035 System.Void ScoreAnimation::PlayAnimation()
extern void ScoreAnimation_PlayAnimation_mB6BB2399F37F9C5F1AC6ADD6DDEED49ECD0B8D5D (void);
// 0x00000036 System.Void ScoreAnimation::SetData(System.Int32,UnityEngine.Sprite)
extern void ScoreAnimation_SetData_mDE1AFFED2C7196B46D3609969E5EE1C80BA69559 (void);
// 0x00000037 System.Collections.IEnumerator ScoreAnimation::WaitAndStopAnimation(System.Single)
extern void ScoreAnimation_WaitAndStopAnimation_mE9E35EA4A41BE2E5A27363BEA368687D29635B7F (void);
// 0x00000038 System.Void ScoreAnimation::.ctor()
extern void ScoreAnimation__ctor_m4A591596A8CB4641D00F884AFCC95DEA0A7365DA (void);
// 0x00000039 System.Void ScoreAnimation/<WaitAndStopAnimation>d__5::.ctor(System.Int32)
extern void U3CWaitAndStopAnimationU3Ed__5__ctor_m56D36877C16C8DBEAFECDD08F7BBF531EBD56CA7 (void);
// 0x0000003A System.Void ScoreAnimation/<WaitAndStopAnimation>d__5::System.IDisposable.Dispose()
extern void U3CWaitAndStopAnimationU3Ed__5_System_IDisposable_Dispose_mFEDE48E928634E276BAD35BD2DBB38B27E8783C3 (void);
// 0x0000003B System.Boolean ScoreAnimation/<WaitAndStopAnimation>d__5::MoveNext()
extern void U3CWaitAndStopAnimationU3Ed__5_MoveNext_m4D0EE38FCDC6B4293FAF404AB5BD43BF7B30E846 (void);
// 0x0000003C System.Object ScoreAnimation/<WaitAndStopAnimation>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndStopAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE3B1F8C83B81B6EA4CB171EF8596703449793D (void);
// 0x0000003D System.Void ScoreAnimation/<WaitAndStopAnimation>d__5::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_Reset_mF3496B9B1A7A6533F3E659B5CA2DCC025B1F012D (void);
// 0x0000003E System.Object ScoreAnimation/<WaitAndStopAnimation>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m545B77BD53146A74099EEF30F7C835C85F83A85E (void);
// 0x0000003F System.Int32 TimerHandler::GetMin(System.Int32)
extern void TimerHandler_GetMin_mE36ED07C66E755A81882B90F500576DB71C9367B (void);
// 0x00000040 System.Int32 TimerHandler::GetSec(System.Int32)
extern void TimerHandler_GetSec_m41A8604C19812E14E4BCB8EC9971DCA5598B4F2A (void);
// 0x00000041 System.Void TimerHandler::.ctor()
extern void TimerHandler__ctor_mEE66FD03CB1DDA4916EC02B4C8C8A07E4D8CEDB6 (void);
// 0x00000042 System.Void BaseHomeView::OnEnable()
extern void BaseHomeView_OnEnable_m1F6CF69B934B70401616C4470F8869F761F9BAB8 (void);
// 0x00000043 System.Void BaseHomeView::OnPlayClick()
extern void BaseHomeView_OnPlayClick_m2695522E45BED28349F89683352EF47225252878 (void);
// 0x00000044 System.Void BaseHomeView::OnTimerBasedPlayModeClick()
extern void BaseHomeView_OnTimerBasedPlayModeClick_m69A6C49B7C6E7DFDE80585805CDB8F518D7D3A97 (void);
// 0x00000045 System.Void BaseHomeView::OnBackButtonClick()
extern void BaseHomeView_OnBackButtonClick_mCF7B15156AF683E37960A74539D70440B456152B (void);
// 0x00000046 System.Void BaseHomeView::.ctor()
extern void BaseHomeView__ctor_mC281A81B58862CB7AF15BDA8D618E78A2C087E1A (void);
// 0x00000047 System.Void BaseHomeView/<>c::.cctor()
extern void U3CU3Ec__cctor_m3F1E0943958DE0281EA255D8478EDBD95B6CDD4F (void);
// 0x00000048 System.Void BaseHomeView/<>c::.ctor()
extern void U3CU3Ec__ctor_m7B21D08B9DDE2736FFF31CA2F3EC84E889FE86FE (void);
// 0x00000049 System.Void BaseHomeView/<>c::<OnPlayClick>b__2_0()
extern void U3CU3Ec_U3COnPlayClickU3Eb__2_0_m6ABEB7A16FE3ED9C291266E3B2F4E269AB3B3EA9 (void);
// 0x0000004A System.Void BaseHomeView/<>c::<OnTimerBasedPlayModeClick>b__3_0()
extern void U3CU3Ec_U3COnTimerBasedPlayModeClickU3Eb__3_0_m9DC5349999B089134ECEFE2593CF79154A40EB95 (void);
// 0x0000004B System.Void ChristmasHomeView::.ctor()
extern void ChristmasHomeView__ctor_m57B97446C2A1D1C173A871B9556428E7836A590D (void);
// 0x0000004C System.Void EsterHomePlayView::.ctor()
extern void EsterHomePlayView__ctor_mDBF81ECE34306A007349D8BBA630E87543296726 (void);
// 0x0000004D System.Void GameMainHomeView::OnEnable()
extern void GameMainHomeView_OnEnable_mEDDC8E2139D6F63F80273CC5B853C0FEF7ACEC5E (void);
// 0x0000004E System.Void GameMainHomeView::ShowView()
extern void GameMainHomeView_ShowView_mBC7D43F499548A5EFA2B2131DFAFBEBB4C1102C0 (void);
// 0x0000004F System.Void GameMainHomeView::HideView()
extern void GameMainHomeView_HideView_m88F6D2C6D181D78447F7A70CF657F5471F891C8D (void);
// 0x00000050 System.Void GameMainHomeView::OnPlayButtonClick()
extern void GameMainHomeView_OnPlayButtonClick_m60508682231FB07EAC3DCE482931DB6818AAB4A6 (void);
// 0x00000051 System.Void GameMainHomeView::SetSoundImage()
extern void GameMainHomeView_SetSoundImage_m5234E02C8C53F1F4854FC80E630BB040F99B8D8A (void);
// 0x00000052 System.Void GameMainHomeView::OnSoundButtonClick()
extern void GameMainHomeView_OnSoundButtonClick_m6582DDBD3784BAF21F092C7BBF287ED4A7530F61 (void);
// 0x00000053 System.Void GameMainHomeView::.ctor()
extern void GameMainHomeView__ctor_m443F786AF74746CF0403457BB0002A88A4F7A959 (void);
// 0x00000054 System.Void GridUIManager::SetCellSpriteById(GamePlay.GamePlayType)
extern void GridUIManager_SetCellSpriteById_m8EC6C6799174265301A2096A2690B2BEE673D1BE (void);
// 0x00000055 UnityEngine.Sprite GridUIManager::GetSpriteByID(GamePlay.GamePlayType)
extern void GridUIManager_GetSpriteByID_mA135EC81A6C7E57F156B206BEF0E106B4B511C9A (void);
// 0x00000056 UnityEngine.Sprite GridUIManager::GetBGSpriteByID(GamePlay.GamePlayType)
extern void GridUIManager_GetBGSpriteByID_m82D2A58AF2CE88065212F1B133608138340618B8 (void);
// 0x00000057 UnityEngine.Sprite GridUIManager::GetBGPoliminoSpriteByID(GamePlay.GamePlayType)
extern void GridUIManager_GetBGPoliminoSpriteByID_m28DF8783C8655F623E4EED3E3DAC9AB9852A17EE (void);
// 0x00000058 System.Void GridUIManager::.ctor()
extern void GridUIManager__ctor_m2BCD9966548ACA1B2661A3C2F212C5F15368BAF9 (void);
// 0x00000059 System.Void GridUIManager/CellSpriteData::.ctor()
extern void CellSpriteData__ctor_m13D15A36B669E8276D14B38B2E96D1486BDBA564 (void);
// 0x0000005A System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3::get_Item(T1)
// 0x0000005B System.Boolean MultiKeyDictionary`3::ContainsKey(T1,T2)
// 0x0000005C System.Void MultiKeyDictionary`3::.ctor()
// 0x0000005D System.Void Images::.ctor()
extern void Images__ctor_m14A3FB738DACEED08F4A9D237663E10B92DFC6E8 (void);
// 0x0000005E System.Single Reporter::get_TotalMemUsage()
extern void Reporter_get_TotalMemUsage_m243BEA1AE109392B1031128E60269F20F4E26E82 (void);
// 0x0000005F System.Void Reporter::Awake()
extern void Reporter_Awake_mE6F45EFB980C87A9E27CDCD430803F1028EB46C4 (void);
// 0x00000060 System.Void Reporter::OnDestroy()
extern void Reporter_OnDestroy_m7ED1EDEDE1B329113E330D967BE7F17018CB066E (void);
// 0x00000061 System.Void Reporter::OnEnable()
extern void Reporter_OnEnable_mAD6DBD51FE170B358222640F7ECE25D6337E95C8 (void);
// 0x00000062 System.Void Reporter::OnDisable()
extern void Reporter_OnDisable_m9F636B3BB1C92B9FA0FD1BBD42DA6DAE8DEF846A (void);
// 0x00000063 System.Void Reporter::addSample()
extern void Reporter_addSample_m540E57AEF6028B6CCBDCCA2C1E68C9D719A78C13 (void);
// 0x00000064 System.Void Reporter::Initialize()
extern void Reporter_Initialize_m4E1F8BE7BF77E47634224D1550980FED99C92046 (void);
// 0x00000065 System.Void Reporter::initializeStyle()
extern void Reporter_initializeStyle_mC794732561B7004D88F0B48786322E94A6F72283 (void);
// 0x00000066 System.Void Reporter::Start()
extern void Reporter_Start_m3A74F6C9AA0BE2D06875F449D39E0BD4463EC11E (void);
// 0x00000067 System.Void Reporter::clear()
extern void Reporter_clear_mDB88F6175A6FE89797697F75F3AC167FB6CA7B51 (void);
// 0x00000068 System.Void Reporter::calculateCurrentLog()
extern void Reporter_calculateCurrentLog_m4D9B9A224D33F529E4B755DD14CB4F8FF563B517 (void);
// 0x00000069 System.Void Reporter::DrawInfo()
extern void Reporter_DrawInfo_m1CAE6609921984149B08D4A99E656C89BC5E2B23 (void);
// 0x0000006A System.Void Reporter::drawInfo_enableDisableToolBarButtons()
extern void Reporter_drawInfo_enableDisableToolBarButtons_m0DDB7E27F9A60F519DAD7D1F170D6093B5280A17 (void);
// 0x0000006B System.Void Reporter::DrawReport()
extern void Reporter_DrawReport_m33ACD652C1A1E7517A4F665777E0EF2799007763 (void);
// 0x0000006C System.Void Reporter::drawToolBar()
extern void Reporter_drawToolBar_m9BFB40BFDF691AF3C12A1F6BDFD757C0E43912B2 (void);
// 0x0000006D System.Void Reporter::DrawLogs()
extern void Reporter_DrawLogs_m4CC63AF0D5D889B46D3033673FAFCCD2DFBEBC68 (void);
// 0x0000006E System.Void Reporter::drawGraph()
extern void Reporter_drawGraph_m1BD6F1B2EDC9FF5E25DD1E40A3443581C5681255 (void);
// 0x0000006F System.Void Reporter::drawStack()
extern void Reporter_drawStack_m01CD93112D4A5D7BF9F797BA1BEFFEC3FD062129 (void);
// 0x00000070 System.Void Reporter::OnGUIDraw()
extern void Reporter_OnGUIDraw_m644E89CBF9C4CD6412A326028F47FAF64BBF5F2D (void);
// 0x00000071 System.Boolean Reporter::isGestureDone()
extern void Reporter_isGestureDone_mC926F2861566102F24258988EE14224CB5F5DAC5 (void);
// 0x00000072 System.Boolean Reporter::isDoubleClickDone()
extern void Reporter_isDoubleClickDone_m4E6E93701D7D56C9A6C49B4F1B07C03EBD0EA416 (void);
// 0x00000073 UnityEngine.Vector2 Reporter::getDownPos()
extern void Reporter_getDownPos_m0AB81FEFEAB7187DCC834459069B5F1D470BDFB7 (void);
// 0x00000074 UnityEngine.Vector2 Reporter::getDrag()
extern void Reporter_getDrag_m42B62313D8BE37D00FB209823BF771D4587BE5A7 (void);
// 0x00000075 System.Void Reporter::calculateStartIndex()
extern void Reporter_calculateStartIndex_mBE0FB265258688F620643A2137A4B556DA25768D (void);
// 0x00000076 System.Void Reporter::doShow()
extern void Reporter_doShow_m08A729528D3F2E6A67CB2259A59669241533A295 (void);
// 0x00000077 System.Void Reporter::Update()
extern void Reporter_Update_m2A0AE08146B30EA0FEE90C1D46CC6D9445D1DCE2 (void);
// 0x00000078 System.Void Reporter::CaptureLog(System.String,System.String,UnityEngine.LogType)
extern void Reporter_CaptureLog_mA5060A0B6B0FC335DA1A99D5AFE8CD5D98C673D0 (void);
// 0x00000079 System.Void Reporter::AddLog(System.String,System.String,UnityEngine.LogType)
extern void Reporter_AddLog_m88361FCA66586C88414BA239A6E1F4427E30E98E (void);
// 0x0000007A System.Void Reporter::CaptureLogThread(System.String,System.String,UnityEngine.LogType)
extern void Reporter_CaptureLogThread_mC9EA896A90AD6457E27D6C7DF888384B6F5930E4 (void);
// 0x0000007B System.Void Reporter::_OnLevelWasLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void Reporter__OnLevelWasLoaded_m50AD021E279F55B6916BF79D32B89D1A3BDEAB10 (void);
// 0x0000007C System.Void Reporter::OnApplicationQuit()
extern void Reporter_OnApplicationQuit_m08A565D8A9AB73B4C4BE358C713527431B5D5C05 (void);
// 0x0000007D System.Collections.IEnumerator Reporter::readInfo()
extern void Reporter_readInfo_m96DE3344D84AEC06C476B18794B3A3C82E58E737 (void);
// 0x0000007E System.Void Reporter::SaveLogsToDevice()
extern void Reporter_SaveLogsToDevice_mD7A2C496C00461F06EC2D61401106C1186B6128A (void);
// 0x0000007F System.Void Reporter::.ctor()
extern void Reporter__ctor_mA4BCF10B880CCC842438711687A14B8CFF02CE05 (void);
// 0x00000080 System.Void Reporter::.cctor()
extern void Reporter__cctor_mFF6F7DC4AC2280BA9A6732870B83F6229DD40A4C (void);
// 0x00000081 System.Single Reporter/Sample::MemSize()
extern void Sample_MemSize_m4D369E2F8B0B0132737E082D352A4C7638E6D4A2 (void);
// 0x00000082 System.String Reporter/Sample::GetSceneName()
extern void Sample_GetSceneName_mF375AB4530FB46DA810DDB0FF874F4E59506D476 (void);
// 0x00000083 System.Void Reporter/Sample::.ctor()
extern void Sample__ctor_m02F7676D47E82DAEE97735C65576B49B506D2453 (void);
// 0x00000084 Reporter/Log Reporter/Log::CreateCopy()
extern void Log_CreateCopy_mFCC38494B5E8A6C1E44BC61FAB7F46077A63711A (void);
// 0x00000085 System.Single Reporter/Log::GetMemoryUsage()
extern void Log_GetMemoryUsage_mED699CB4309A05ED9108938A7534568A51EBD415 (void);
// 0x00000086 System.Void Reporter/Log::.ctor()
extern void Log__ctor_m7BF33C0496F355EDC3D884812786A0D3DFA9522D (void);
// 0x00000087 System.Void Reporter/<readInfo>d__188::.ctor(System.Int32)
extern void U3CreadInfoU3Ed__188__ctor_m1074CD57ECB5282678BE9366813C0FD134F3C04A (void);
// 0x00000088 System.Void Reporter/<readInfo>d__188::System.IDisposable.Dispose()
extern void U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m7FA47F089E3E44C215AA02F56A71F03F179AE492 (void);
// 0x00000089 System.Boolean Reporter/<readInfo>d__188::MoveNext()
extern void U3CreadInfoU3Ed__188_MoveNext_mC9E975802B890843F8384228D002418014CAA613 (void);
// 0x0000008A System.Object Reporter/<readInfo>d__188::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6964659507DB03DC202BE8B05A79F8E228D1807 (void);
// 0x0000008B System.Void Reporter/<readInfo>d__188::System.Collections.IEnumerator.Reset()
extern void U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_m7950A1C07E9941FC6F83BD1E1DF4B14FA26A7C2E (void);
// 0x0000008C System.Object Reporter/<readInfo>d__188::System.Collections.IEnumerator.get_Current()
extern void U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m5A3DFB2378273D241AE9C3EF98DF3FAC754AA2EA (void);
// 0x0000008D System.Void ReporterGUI::Awake()
extern void ReporterGUI_Awake_m75AE591F9E4CB78A427C89E9278BE4D56CCF7CD6 (void);
// 0x0000008E System.Void ReporterGUI::OnGUI()
extern void ReporterGUI_OnGUI_mFC33C895684D1EC6CDA24AAC340C69294EC08516 (void);
// 0x0000008F System.Void ReporterGUI::.ctor()
extern void ReporterGUI__ctor_mC6456038C4EAB8D290C4253256F59AB0A550BD86 (void);
// 0x00000090 System.Void ReporterMessageReceiver::Start()
extern void ReporterMessageReceiver_Start_mD918A8392396CE96564A12434294C1544997624E (void);
// 0x00000091 System.Void ReporterMessageReceiver::OnPreStart()
extern void ReporterMessageReceiver_OnPreStart_mADCE71C11DE96AB870DCB30AEC273F58EA46B482 (void);
// 0x00000092 System.Void ReporterMessageReceiver::OnHideReporter()
extern void ReporterMessageReceiver_OnHideReporter_m3DCF2E6157DCF519F9277DC72034259BA235441E (void);
// 0x00000093 System.Void ReporterMessageReceiver::OnShowReporter()
extern void ReporterMessageReceiver_OnShowReporter_m74CEFEFF9B4EE67915D983C769DBC95E79B09DCB (void);
// 0x00000094 System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern void ReporterMessageReceiver_OnLog_mA0DD9085AA99D585E89AE26ECBBC79F0ED602BBA (void);
// 0x00000095 System.Void ReporterMessageReceiver::.ctor()
extern void ReporterMessageReceiver__ctor_m02BC4C5416EF8CB9013E5BD52B7391C3964C6A7B (void);
// 0x00000096 System.Void Rotate::Start()
extern void Rotate_Start_m260F3DA22D8BEE3571FB61596065ECC238B60968 (void);
// 0x00000097 System.Void Rotate::Update()
extern void Rotate_Update_m66D07F3686A017A63E869D8294C08E68F4A2FBAB (void);
// 0x00000098 System.Void Rotate::.ctor()
extern void Rotate__ctor_mAB2884DA9234D7A6485C5662D97205C92CA9B9C4 (void);
// 0x00000099 System.Void TestReporter::Start()
extern void TestReporter_Start_m6397556902788BB52CAF60730ECE79D25CED86F0 (void);
// 0x0000009A System.Void TestReporter::OnDestroy()
extern void TestReporter_OnDestroy_m4A73BE669965BDB08596934A124368035D4BCDE7 (void);
// 0x0000009B System.Void TestReporter::threadLogTest()
extern void TestReporter_threadLogTest_m631A52116F32D171C27CF3B75EF33B423E8B5304 (void);
// 0x0000009C System.Void TestReporter::Update()
extern void TestReporter_Update_m6BA9D5AA04F97B05FE21726E148C234F246B35A9 (void);
// 0x0000009D System.Void TestReporter::OnGUI()
extern void TestReporter_OnGUI_m1F0C8C7C08B940C51976BE835235F6F2BF8E9AA5 (void);
// 0x0000009E System.Void TestReporter::.ctor()
extern void TestReporter__ctor_mF94083FCF4F1102F7780790D06809C17D1EE8F20 (void);
// 0x0000009F System.Void raw::Start()
extern void raw_Start_mF6389E4D52FC7AC3A89EEE61938955A9180FED9A (void);
// 0x000000A0 System.Void raw::Update()
extern void raw_Update_m7FA2C1BB7B5A85871E73229AD2578C2186D5CB5C (void);
// 0x000000A1 System.Void raw::.ctor()
extern void raw__ctor_mBFDA0F7002BEC6D84DC3F7090B049F50EE285D73 (void);
// 0x000000A2 System.Void Tag.CoreGame.SafeArea::Awake()
extern void SafeArea_Awake_m84A2CD2A994D90CF9EAF58C2F89022D096C4B06D (void);
// 0x000000A3 System.Void Tag.CoreGame.SafeArea::ApplySafeArea()
extern void SafeArea_ApplySafeArea_mF59008F67597542E8D1EBCFD6CC943E1B8390139 (void);
// 0x000000A4 System.Void Tag.CoreGame.SafeArea::.ctor()
extern void SafeArea__ctor_mA3BB2BD560BE0F759582FD7151D2B7226B649894 (void);
// 0x000000A5 System.Void GamePlay.Constants::.ctor()
extern void Constants__ctor_mA7C60AE52C906371D3E1FDB1AE9D9982E6B0B1D2 (void);
// 0x000000A6 System.Void GamePlay.Constants::.cctor()
extern void Constants__cctor_m29EDCBEA52F8B30E77505804367D8A2D62FFD69D (void);
// 0x000000A7 System.Void GamePlay.GridAndHolderData::Awake()
extern void GridAndHolderData_Awake_m93BE19B0B47DF6EF055E65580EEF9981B2EA047D (void);
// 0x000000A8 System.Void GamePlay.GridAndHolderData::OnApplicationQuit()
extern void GridAndHolderData_OnApplicationQuit_m7ADBAD28CB75D06985EFDAB274815E7E7297B43C (void);
// 0x000000A9 GamePlay.SpriteTypes[0...,0...] GamePlay.GridAndHolderData::GetGameData()
extern void GridAndHolderData_GetGameData_m4A86CD5CAED5908E1AB1C580ADD3FDF1401F4D42 (void);
// 0x000000AA System.Void GamePlay.GridAndHolderData::SetGameData()
extern void GridAndHolderData_SetGameData_m760417E17C4E91D2D93253627C4992A97D0A222E (void);
// 0x000000AB GamePlay.SpriteTypes[] GamePlay.GridAndHolderData::To1DArray(GamePlay.SpriteTypes[0...,0...])
extern void GridAndHolderData_To1DArray_m43E4023CF9CC672D2A7CB2CF70AFBF5746227570 (void);
// 0x000000AC GamePlay.SpriteTypes[0...,0...] GamePlay.GridAndHolderData::To2DArray(GamePlay.SpriteTypes[])
extern void GridAndHolderData_To2DArray_mAEB251D1376187F93D29B5C2E0699AB5C144B64E (void);
// 0x000000AD System.Void GamePlay.GridAndHolderData::.ctor()
extern void GridAndHolderData__ctor_m9A502808C28FAA3772B9789149154BAC97307DB4 (void);
// 0x000000AE System.Void GamePlay.GridAndHolderData::.cctor()
extern void GridAndHolderData__cctor_mDEBB237E2F7DF4A7098589553C8A9257CACC199E (void);
// 0x000000AF System.Void GamePlay.BaseBlock::.ctor()
extern void BaseBlock__ctor_mA4EACC9138C84297CC28670A74B90DEAD769DA23 (void);
// 0x000000B0 System.Void GamePlay.BasePolyomino::Start()
extern void BasePolyomino_Start_m83AE1E16962FE32362F81F8ADE5FBCE855B1EB32 (void);
// 0x000000B1 System.Void GamePlay.BasePolyomino::Init()
extern void BasePolyomino_Init_mE57212EAE30309E2B16A63FA024BB43EC160D5CB (void);
// 0x000000B2 System.Void GamePlay.BasePolyomino::CheckCollideRule()
extern void BasePolyomino_CheckCollideRule_m260BFD5E798AA3323B3296667797FF8D7DC70DFB (void);
// 0x000000B3 GamePlay.Cell GamePlay.BasePolyomino::GetCollideCell(UnityEngine.Transform)
extern void BasePolyomino_GetCollideCell_mD76DCF275249E35B61DA2D776B18B17755595BC0 (void);
// 0x000000B4 System.Void GamePlay.BasePolyomino::SetMainCollider(System.Boolean)
extern void BasePolyomino_SetMainCollider_m25C8D7E6F7F32D1E0E3E2727C184A1FAA95BD8C0 (void);
// 0x000000B5 System.Void GamePlay.BasePolyomino::OnDrag()
extern void BasePolyomino_OnDrag_mCCC43FE43F27FD82E06736CF46B6C514F25AC364 (void);
// 0x000000B6 System.Void GamePlay.BasePolyomino::OnDrop(System.Boolean)
extern void BasePolyomino_OnDrop_m257CF10AD7AA5BA928F163F061F7850BB694A875 (void);
// 0x000000B7 System.Void GamePlay.BasePolyomino::CheckPlaceAvailable()
extern void BasePolyomino_CheckPlaceAvailable_mDCAF127A34454E9AB8F257632B3AFC8853CEB693 (void);
// 0x000000B8 System.Void GamePlay.BasePolyomino::StartAnimation(UnityEngine.Transform,GamePlay.CustomTransform,System.Action)
extern void BasePolyomino_StartAnimation_m44343BFEBBEA6D1938AFBFC1180637B7BF836825 (void);
// 0x000000B9 System.Void GamePlay.BasePolyomino::WaitAndDestroy()
extern void BasePolyomino_WaitAndDestroy_mFCC3CB6882B308ADD5881118453487E8C10EB995 (void);
// 0x000000BA System.Collections.IEnumerator GamePlay.BasePolyomino::LearpObject(UnityEngine.Transform,GamePlay.CustomTransform,System.Action)
extern void BasePolyomino_LearpObject_m1E73D023FAFB6FBE91413439AE665FD2797A72F0 (void);
// 0x000000BB System.Collections.IEnumerator GamePlay.BasePolyomino::WaitAndDestroyCO()
extern void BasePolyomino_WaitAndDestroyCO_m21B75FEF605879F8BA9E42FE2CE1C72F8742C02F (void);
// 0x000000BC System.Void GamePlay.BasePolyomino::.ctor()
extern void BasePolyomino__ctor_mF4F8C66AB854AACEB49B2A35EA60D716DF380CCD (void);
// 0x000000BD System.Void GamePlay.BasePolyomino/<LearpObject>d__14::.ctor(System.Int32)
extern void U3CLearpObjectU3Ed__14__ctor_m9C780924431DA2807CB8A33945EC0E5FBA9E740B (void);
// 0x000000BE System.Void GamePlay.BasePolyomino/<LearpObject>d__14::System.IDisposable.Dispose()
extern void U3CLearpObjectU3Ed__14_System_IDisposable_Dispose_m4657DFB93555CB9FB7B11CA58BD68707F96B9D7B (void);
// 0x000000BF System.Boolean GamePlay.BasePolyomino/<LearpObject>d__14::MoveNext()
extern void U3CLearpObjectU3Ed__14_MoveNext_m20EF7DCA4525F4EDB0EBA4BB9A6F256BEBBA7D23 (void);
// 0x000000C0 System.Object GamePlay.BasePolyomino/<LearpObject>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLearpObjectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA34BF89B49E1F65604B719E972471D5670115E0 (void);
// 0x000000C1 System.Void GamePlay.BasePolyomino/<LearpObject>d__14::System.Collections.IEnumerator.Reset()
extern void U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_Reset_m6C440EB8EC4B4D64DA392E5471EFE768DA7BB43D (void);
// 0x000000C2 System.Object GamePlay.BasePolyomino/<LearpObject>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_get_Current_m698F9838A2D8A51C7F991185F04BED54EB77AC11 (void);
// 0x000000C3 System.Void GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::.ctor(System.Int32)
extern void U3CWaitAndDestroyCOU3Ed__15__ctor_m0D93DF3C044D855E7EB240C71A66DB98B8D67862 (void);
// 0x000000C4 System.Void GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::System.IDisposable.Dispose()
extern void U3CWaitAndDestroyCOU3Ed__15_System_IDisposable_Dispose_m9E461FEC944FCCB2BA19375FD03BAD8463CD97B1 (void);
// 0x000000C5 System.Boolean GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::MoveNext()
extern void U3CWaitAndDestroyCOU3Ed__15_MoveNext_mDD2D92437DD09E10939FC2F47AAD5DB8EE2C9E48 (void);
// 0x000000C6 System.Object GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndDestroyCOU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B6C2664DA2E0F1C690B6C6DD52864E10B36058C (void);
// 0x000000C7 System.Void GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_Reset_m048D08D06E80BB24A37CCE345F2524C94B1F9E02 (void);
// 0x000000C8 System.Object GamePlay.BasePolyomino/<WaitAndDestroyCO>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_get_Current_m14BF06A405A57EF85B5FDE966414B3C2751A031C (void);
// 0x000000C9 System.Void GamePlay.Block::.ctor()
extern void Block__ctor_mB9C9E69045B02084BF7EE26376D7286CA41A0E2D (void);
// 0x000000CA System.Boolean GamePlay.Cell::IsEmpty()
extern void Cell_IsEmpty_mE366ED5E389F064391562CBECD8F88018BA69C15 (void);
// 0x000000CB System.Void GamePlay.Cell::SetTemporarySprite(GamePlay.SpriteTypes,System.Single,System.Boolean)
extern void Cell_SetTemporarySprite_m5FED258799F05FD2FC7EB16F9E180735B197073C (void);
// 0x000000CC System.Void GamePlay.Cell::ResetTemporarySprite()
extern void Cell_ResetTemporarySprite_m60459030068DFA8F0C408A6BC057D3B1178B4CFF (void);
// 0x000000CD System.Void GamePlay.Cell::DestroyBlock()
extern void Cell_DestroyBlock_m12C756759137F23EAFAC988970F64B6D2C377C9B (void);
// 0x000000CE System.Void GamePlay.Cell::DestroyBlock(GamePlay.SpriteTypes)
extern void Cell_DestroyBlock_mCCE3F8B22A4301D8454ECB3D11CAB53823A38A68 (void);
// 0x000000CF System.Void GamePlay.Cell::ResetData()
extern void Cell_ResetData_m601D0B3B136441357A74C591A28361B0FCC0E469 (void);
// 0x000000D0 System.Void GamePlay.Cell::SetBlock(GamePlay.SpriteTypes)
extern void Cell_SetBlock_mBE8EEBE1008E8CD1D27469A1046CA3347FF60072 (void);
// 0x000000D1 System.Void GamePlay.Cell::ChangeCellEffectLayer(System.Int32)
extern void Cell_ChangeCellEffectLayer_m518CC7E29F310B5686F0A662BF0443FA92DCC900 (void);
// 0x000000D2 System.Void GamePlay.Cell::ActivateCellEffect()
extern void Cell_ActivateCellEffect_m5BE44C59FAD24E845887AEB0115B14AD4BE1346D (void);
// 0x000000D3 System.Void GamePlay.Cell::ActivateCellBorderEffect()
extern void Cell_ActivateCellBorderEffect_mA736A0D8EAA1862511A0F6AF73BA3B9366E709B1 (void);
// 0x000000D4 System.Void GamePlay.Cell::DeactivateCellEffect()
extern void Cell_DeactivateCellEffect_m8224A03E58EBB0C7D38878ED940F43B67D37D91B (void);
// 0x000000D5 System.Void GamePlay.Cell::PlayHintFX()
extern void Cell_PlayHintFX_m226A201D38515B365BBC049147720218CD8B04DF (void);
// 0x000000D6 System.Void GamePlay.Cell::StopHintFX()
extern void Cell_StopHintFX_m85B1584418746D71E2782DB317C6A79D6799B4F5 (void);
// 0x000000D7 System.Void GamePlay.Cell::ResetBlockSprite()
extern void Cell_ResetBlockSprite_m0C064DE9B509D78079F7343C07902CD41BCF4A3C (void);
// 0x000000D8 System.Void GamePlay.Cell::PlayDestroyAnimation()
extern void Cell_PlayDestroyAnimation_mE2D318A1371D9DC120EC83729B3FC5B65DD15124 (void);
// 0x000000D9 System.Void GamePlay.Cell::.ctor()
extern void Cell__ctor_mD02F71AF09C714ABB8742DC17386661820EBF8FC (void);
// 0x000000DA System.Void GamePlay.Cell/<>c::.cctor()
extern void U3CU3Ec__cctor_mC839ECB2C685BA8C1A05D4C85C5BFBD88972E435 (void);
// 0x000000DB System.Void GamePlay.Cell/<>c::.ctor()
extern void U3CU3Ec__ctor_m7F502F331C7AD922955FFCA91C52D306A2607E21 (void);
// 0x000000DC System.Void GamePlay.Cell/<>c::<PlayHintFX>b__18_0(UnityEngine.ParticleSystem)
extern void U3CU3Ec_U3CPlayHintFXU3Eb__18_0_m51F4DFD11AF53AA6AA6C7DB7F90C8670706271A1 (void);
// 0x000000DD System.Void GamePlay.Cell/<>c::<StopHintFX>b__19_0(UnityEngine.ParticleSystem)
extern void U3CU3Ec_U3CStopHintFXU3Eb__19_0_m41064ADD56C13834DDF4F4B372B271F99594F5D3 (void);
// 0x000000DE System.Void GamePlay.Cell/<>c::<PlayDestroyAnimation>b__21_0(UnityEngine.ParticleSystem)
extern void U3CU3Ec_U3CPlayDestroyAnimationU3Eb__21_0_m5758C266BD8696F60D8731FC083346FB8F1B7282 (void);
// 0x000000DF System.Void GamePlay.Grid::Start()
extern void Grid_Start_mA3991118B50CBC4D7C9F7CE9EA370651A2BACCF2 (void);
// 0x000000E0 System.Void GamePlay.Grid::SetPolyominoInCell(GamePlay.SimplePolyomino)
extern void Grid_SetPolyominoInCell_m512A0AE52AE71E80A5EFDFF86226A5AEEB4C9490 (void);
// 0x000000E1 System.Void GamePlay.Grid::PlayHintPS(GamePlay.SimplePolyomino)
extern void Grid_PlayHintPS_m582A35A34859812158F2C38997D61107EBD87B29 (void);
// 0x000000E2 System.Void GamePlay.Grid::StopHintPS(GamePlay.SimplePolyomino)
extern void Grid_StopHintPS_mB110CB2F277AB4BB0D62697C4BEECDC5F5C330C7 (void);
// 0x000000E3 System.Boolean GamePlay.Grid::IsItRowFull(System.Int32)
extern void Grid_IsItRowFull_m122B0D33B58A621E3CC1EE4FC943436E9427FECE (void);
// 0x000000E4 System.Boolean GamePlay.Grid::IsItColFull(System.Int32)
extern void Grid_IsItColFull_mD9280C188A7EB3C14E23499C422DFFC99C6929E4 (void);
// 0x000000E5 System.Void GamePlay.Grid::DestroyBlocks(System.Collections.Generic.List`1<GamePlay.DestroyCellData>,GamePlay.SpriteTypes)
extern void Grid_DestroyBlocks_mDD2F6ACCE806E05D8C617893BF30D782780350C7 (void);
// 0x000000E6 System.Void GamePlay.Grid::SetCell()
extern void Grid_SetCell_m127F5FAA1AA57202B28641B8185D0B7CC4ECC5EE (void);
// 0x000000E7 System.Void GamePlay.Grid::SetBlockFromData(GamePlay.SpriteTypes[0...,0...])
extern void Grid_SetBlockFromData_m876D07B297681650E56003CF1D5842312DC730B9 (void);
// 0x000000E8 System.Void GamePlay.Grid::OnGameOver()
extern void Grid_OnGameOver_m34A224853641E572DCD63E88F2C029ADE7061623 (void);
// 0x000000E9 System.Void GamePlay.Grid::StartActiveCellEffect()
extern void Grid_StartActiveCellEffect_m2C379E11C1B84B39D0916FB95A77751141F9149D (void);
// 0x000000EA System.Void GamePlay.Grid::StopActiveCellEffect()
extern void Grid_StopActiveCellEffect_mEE141186B8DD3D7ABA588953F7D374ECE1BDBD8B (void);
// 0x000000EB System.Void GamePlay.Grid::SetCellEffectsByRow(System.Int32)
extern void Grid_SetCellEffectsByRow_m468843A66F6CF4585CF255F86009C7768A3F6E7E (void);
// 0x000000EC System.Void GamePlay.Grid::SetCellEffectsByCol(System.Int32)
extern void Grid_SetCellEffectsByCol_mAEB92DFE8C55F02A0DA2861FA78E688F895AC0E9 (void);
// 0x000000ED System.Void GamePlay.Grid::DestroyRandomBlocks()
extern void Grid_DestroyRandomBlocks_mBE3A6B55803E5864B85D627EBE71B9117660091D (void);
// 0x000000EE System.Void GamePlay.Grid::ResetAllCells()
extern void Grid_ResetAllCells_mC60B39E5C71B8B35F33278469A2303375E9712E1 (void);
// 0x000000EF System.Boolean GamePlay.Grid::IsGridEmpty()
extern void Grid_IsGridEmpty_m40CB429865784A486CACA9D1EBA1E9B9AEB5DB50 (void);
// 0x000000F0 System.Void GamePlay.Grid::PerformActionAfterDestroyBlock()
extern void Grid_PerformActionAfterDestroyBlock_m4966B547173335539E511A3EF5543BF2EE862171 (void);
// 0x000000F1 System.Collections.IEnumerator GamePlay.Grid::WaitAndSetSprite()
extern void Grid_WaitAndSetSprite_m316CE6E8DC6664F84EED514C63EE471CF8318EA6 (void);
// 0x000000F2 System.Collections.IEnumerator GamePlay.Grid::DestroyBlockCO(GamePlay.DestroyCellData,GamePlay.SpriteTypes)
extern void Grid_DestroyBlockCO_m1D911C9BE4279B4450C6BC18D2D76E4807594B95 (void);
// 0x000000F3 System.Void GamePlay.Grid::.ctor()
extern void Grid__ctor_m89459B22582B1D9D3B6AE44D7CED61E9246D14AD (void);
// 0x000000F4 System.Void GamePlay.Grid::<DestroyBlocks>b__8_0()
extern void Grid_U3CDestroyBlocksU3Eb__8_0_m5253B5681B09948DFC858E3B6D56BF568D3A1C2C (void);
// 0x000000F5 System.Void GamePlay.Grid::<SetCell>b__9_0(GamePlay.Cell)
extern void Grid_U3CSetCellU3Eb__9_0_m89A6DF9BC810C8151061BC242CC103CF3FF1AAE3 (void);
// 0x000000F6 System.Void GamePlay.Grid/<>c::.cctor()
extern void U3CU3Ec__cctor_m773EEFEE87D00A6705010AC86FC85D6FF26E12A3 (void);
// 0x000000F7 System.Void GamePlay.Grid/<>c::.ctor()
extern void U3CU3Ec__ctor_m86C68B7A88AFB413C069325CB942C841ECDE6598 (void);
// 0x000000F8 System.Void GamePlay.Grid/<>c::<StartActiveCellEffect>b__12_0(GamePlay.Cell)
extern void U3CU3Ec_U3CStartActiveCellEffectU3Eb__12_0_m8BE9EB32DD6D72898461564C0F185ECCA817ED94 (void);
// 0x000000F9 System.Void GamePlay.Grid/<>c::<StopActiveCellEffect>b__13_0(GamePlay.Cell)
extern void U3CU3Ec_U3CStopActiveCellEffectU3Eb__13_0_mAC37DA0E14F4B97B690DD5A19AC8633E03155C03 (void);
// 0x000000FA System.Void GamePlay.Grid/<>c::<ResetAllCells>b__17_0(GamePlay.Cell)
extern void U3CU3Ec_U3CResetAllCellsU3Eb__17_0_m9157EA4A09490782C84C8953D37280DAA7FE2D17 (void);
// 0x000000FB System.Single GamePlay.Grid/<>c::<WaitAndSetSprite>b__20_0(GamePlay.Cell)
extern void U3CU3Ec_U3CWaitAndSetSpriteU3Eb__20_0_m4C97B309EA2BB676A3A222FA1597C52B19ABADED (void);
// 0x000000FC System.Void GamePlay.Grid/<WaitAndSetSprite>d__20::.ctor(System.Int32)
extern void U3CWaitAndSetSpriteU3Ed__20__ctor_m8B991F753CEBCCE65339C24F6B4C3F4BE799FB1B (void);
// 0x000000FD System.Void GamePlay.Grid/<WaitAndSetSprite>d__20::System.IDisposable.Dispose()
extern void U3CWaitAndSetSpriteU3Ed__20_System_IDisposable_Dispose_m94B304D9922828FE66F1CBA3B1322BEE2CC514DC (void);
// 0x000000FE System.Boolean GamePlay.Grid/<WaitAndSetSprite>d__20::MoveNext()
extern void U3CWaitAndSetSpriteU3Ed__20_MoveNext_m58D3A5230A48802FF60AA4B2F6357BD1673E19CE (void);
// 0x000000FF System.Object GamePlay.Grid/<WaitAndSetSprite>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndSetSpriteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8E8B6C83FDCA0C021EF13E15DB317AF200BACC (void);
// 0x00000100 System.Void GamePlay.Grid/<WaitAndSetSprite>d__20::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_Reset_m2F3B06310784C6ADECB39F0CCBDD9C940B5F25CE (void);
// 0x00000101 System.Object GamePlay.Grid/<WaitAndSetSprite>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_get_Current_m47C74139557B5ED8AD836B7B376879EC7F6C6422 (void);
// 0x00000102 System.Void GamePlay.Grid/<DestroyBlockCO>d__21::.ctor(System.Int32)
extern void U3CDestroyBlockCOU3Ed__21__ctor_m2657C6103D4DA7EECA4CB3DABBAB7A9958226226 (void);
// 0x00000103 System.Void GamePlay.Grid/<DestroyBlockCO>d__21::System.IDisposable.Dispose()
extern void U3CDestroyBlockCOU3Ed__21_System_IDisposable_Dispose_m9404FFD07E53A22FD55D8E0CBDB23054491CC476 (void);
// 0x00000104 System.Boolean GamePlay.Grid/<DestroyBlockCO>d__21::MoveNext()
extern void U3CDestroyBlockCOU3Ed__21_MoveNext_mA1F00460B38FDA08BBAD418098FA14CB25208B5D (void);
// 0x00000105 System.Void GamePlay.Grid/<DestroyBlockCO>d__21::<>m__Finally1()
extern void U3CDestroyBlockCOU3Ed__21_U3CU3Em__Finally1_m25514FBDA451A6610CAF65F7F009B8A5E37E9D5F (void);
// 0x00000106 System.Object GamePlay.Grid/<DestroyBlockCO>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyBlockCOU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2B4BF82C64017FBF76034F6FDEEB7F8654E0E70 (void);
// 0x00000107 System.Void GamePlay.Grid/<DestroyBlockCO>d__21::System.Collections.IEnumerator.Reset()
extern void U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_Reset_m3E4B139B7532EB2465B72E6396E6CDB180F27BD2 (void);
// 0x00000108 System.Object GamePlay.Grid/<DestroyBlockCO>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_get_Current_m591535103E9317CB93B8745E6660AC4315822E36 (void);
// 0x00000109 System.Void GamePlay.PolyominoBlock::.ctor()
extern void PolyominoBlock__ctor_m8BB6A04826749E3612082B370D119F493DFACD33 (void);
// 0x0000010A System.Void GamePlay.PolyominoSlot::SetPolyomino(GamePlay.BasePolyomino)
extern void PolyominoSlot_SetPolyomino_m0AE8754429FCD162276CF4A7ADC6B29AD15F626B (void);
// 0x0000010B System.Void GamePlay.PolyominoSlot::SetBoosterPolyomino()
extern void PolyominoSlot_SetBoosterPolyomino_m570F6C305D9E5AE9583844579B611D9A44661A9B (void);
// 0x0000010C GamePlay.BasePolyomino GamePlay.PolyominoSlot::GetPolyomino()
extern void PolyominoSlot_GetPolyomino_m5EE104871F9D78F6E33BE40BBB29A4F1BEDA9986 (void);
// 0x0000010D System.Boolean GamePlay.PolyominoSlot::IsEmpty()
extern void PolyominoSlot_IsEmpty_mB749B0B1A41213C8BBE3BB1A6CAEFC7D39C58BBF (void);
// 0x0000010E System.Void GamePlay.PolyominoSlot::ResetPolyomino()
extern void PolyominoSlot_ResetPolyomino_mB335BD31E82053863FBEB0E07D1A1E641FC558F3 (void);
// 0x0000010F System.Void GamePlay.PolyominoSlot::CheckPlaceAvailable()
extern void PolyominoSlot_CheckPlaceAvailable_m9F47BEF097D4C8D2E2EB33DF7FA1D43576879267 (void);
// 0x00000110 System.Void GamePlay.PolyominoSlot::SaveLastPolyomino()
extern void PolyominoSlot_SaveLastPolyomino_mA0E027BC26DE871F64F6093E7A03424AF204BA04 (void);
// 0x00000111 System.Void GamePlay.PolyominoSlot::SetLastPolyomino()
extern void PolyominoSlot_SetLastPolyomino_mA12B9448A9DA59B14A4A94DA0022E2365ACEE302 (void);
// 0x00000112 System.Void GamePlay.PolyominoSlot::ResetLastPolyomino()
extern void PolyominoSlot_ResetLastPolyomino_m178621A70461C88B27A2E2FC2783A1CA56ACF1A9 (void);
// 0x00000113 System.Void GamePlay.PolyominoSlot::.ctor()
extern void PolyominoSlot__ctor_m769AC6B2BF7308D954B5A7F662A4F850A49FB026 (void);
// 0x00000114 System.Void GamePlay.SimplePolyomino::Start()
extern void SimplePolyomino_Start_mEE89DF27AAC634C5855B8EB7774978804FC3B771 (void);
// 0x00000115 System.Void GamePlay.SimplePolyomino::Update()
extern void SimplePolyomino_Update_mC15E0A8DA1BDAE56896CBD7C048986891ABC6DF8 (void);
// 0x00000116 System.Void GamePlay.SimplePolyomino::Init()
extern void SimplePolyomino_Init_mB7572FD3FE530406E6A4DDE5FA7445BC930A1BE8 (void);
// 0x00000117 System.Void GamePlay.SimplePolyomino::OnDrag()
extern void SimplePolyomino_OnDrag_m6EBA1DA973983A95A1C8B1721C70360A1293BAD0 (void);
// 0x00000118 System.Void GamePlay.SimplePolyomino::OnDrop(System.Boolean)
extern void SimplePolyomino_OnDrop_mF63256C456381AE1E4E447DE50DA0B6FA5F93F83 (void);
// 0x00000119 System.Void GamePlay.SimplePolyomino::SetSprite(GamePlay.SpriteTypes)
extern void SimplePolyomino_SetSprite_m909B20754920040AEFDA560014BA5112175B991C (void);
// 0x0000011A System.Void GamePlay.SimplePolyomino::SetUnplacedSprite()
extern void SimplePolyomino_SetUnplacedSprite_mCB0F4B80735744A45FE48C67C31E1AAC0199116B (void);
// 0x0000011B System.Void GamePlay.SimplePolyomino::SetBlockSize(System.Single)
extern void SimplePolyomino_SetBlockSize_mE6CAFA53356A097E2A5947F952AC3DECEA1A62BB (void);
// 0x0000011C System.Void GamePlay.SimplePolyomino::CheckPlaceAvailable()
extern void SimplePolyomino_CheckPlaceAvailable_m0BBE996FB47A4B042A22E7CEC453E154A727672E (void);
// 0x0000011D System.Void GamePlay.SimplePolyomino::CheckCollideRule()
extern void SimplePolyomino_CheckCollideRule_m2606B4CC6B738DF5E745C1350506203B218A6206 (void);
// 0x0000011E System.Void GamePlay.SimplePolyomino::SetIfPlaceable(GamePlay.SpriteTypes)
extern void SimplePolyomino_SetIfPlaceable_mBFC486DD9DAF377BD2DE942ED4B38C9C06450025 (void);
// 0x0000011F System.Collections.IEnumerator GamePlay.SimplePolyomino::WaitAndHint(System.Single)
extern void SimplePolyomino_WaitAndHint_mFE2AB7C6FE5E037F2EEB0C1633897F45492F3068 (void);
// 0x00000120 System.Void GamePlay.SimplePolyomino::SetUp()
extern void SimplePolyomino_SetUp_mD425ECAC66C97FEFFD381FF1EDA3C45E5158482F (void);
// 0x00000121 System.Void GamePlay.SimplePolyomino::PolyominoBlockPosSetUp()
extern void SimplePolyomino_PolyominoBlockPosSetUp_m80E2F0978B150BF93858BD4B2772547F4AA29D7D (void);
// 0x00000122 System.Void GamePlay.SimplePolyomino::SetRowWiseData()
extern void SimplePolyomino_SetRowWiseData_mB460A72A02E08102063B5DC4024E1B28A8153E22 (void);
// 0x00000123 System.Void GamePlay.SimplePolyomino::SetColWiseData()
extern void SimplePolyomino_SetColWiseData_m34EBA914661EAB43146C9E5E23EEE79CD52C3AED (void);
// 0x00000124 System.Boolean GamePlay.SimplePolyomino::DoesContainInRowList(System.Int32)
extern void SimplePolyomino_DoesContainInRowList_mF8BB3A02DCC47DBBAB85114ACEB7108B5C9615D0 (void);
// 0x00000125 System.Boolean GamePlay.SimplePolyomino::DoesContainInColList(System.Int32)
extern void SimplePolyomino_DoesContainInColList_m36DD883F2122B9614E063964CE7CAC9F554D41C4 (void);
// 0x00000126 System.Void GamePlay.SimplePolyomino::.ctor()
extern void SimplePolyomino__ctor_m8EE45BE252053A8D77AE7942583C099AA247F22C (void);
// 0x00000127 System.Void GamePlay.SimplePolyomino::<OnDrop>b__13_0()
extern void SimplePolyomino_U3COnDropU3Eb__13_0_m393E9055C749DADD1826522B462FEB9D6BA4BF55 (void);
// 0x00000128 System.Boolean GamePlay.SimplePolyomino::<OnDrop>b__13_2(GamePlay.PolyominoSlot)
extern void SimplePolyomino_U3COnDropU3Eb__13_2_mFE1FED896F0D6218C9A3064E5E4DD2AD647CED5C (void);
// 0x00000129 System.Void GamePlay.SimplePolyomino::<OnDrop>b__13_1()
extern void SimplePolyomino_U3COnDropU3Eb__13_1_m34A618CE2D1E31F800CAEDEF62DA355D0D13E0F0 (void);
// 0x0000012A System.Void GamePlay.SimplePolyomino/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m4E5C1C19C93E4E39BB6EEC732D2CF19C9DE3FB6B (void);
// 0x0000012B System.Void GamePlay.SimplePolyomino/<>c__DisplayClass16_0::<SetBlockSize>b__0(GamePlay.PolyominoBlock)
extern void U3CU3Ec__DisplayClass16_0_U3CSetBlockSizeU3Eb__0_m99CB239220F82555B0AAFC20B6AEF8DDFADFE1DD (void);
// 0x0000012C System.Void GamePlay.SimplePolyomino/<WaitAndHint>d__20::.ctor(System.Int32)
extern void U3CWaitAndHintU3Ed__20__ctor_m8AE8440CB025905B9A795A658BCA376BE72D3EF3 (void);
// 0x0000012D System.Void GamePlay.SimplePolyomino/<WaitAndHint>d__20::System.IDisposable.Dispose()
extern void U3CWaitAndHintU3Ed__20_System_IDisposable_Dispose_mFE3F40C5A75650729FDF2FA8D5122E448F216CB1 (void);
// 0x0000012E System.Boolean GamePlay.SimplePolyomino/<WaitAndHint>d__20::MoveNext()
extern void U3CWaitAndHintU3Ed__20_MoveNext_mB3166E12D0D9478F354AF073D8365D579A4C2C69 (void);
// 0x0000012F System.Object GamePlay.SimplePolyomino/<WaitAndHint>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndHintU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0680968ACA99E565FAA2AE7DF6B0332309C98C (void);
// 0x00000130 System.Void GamePlay.SimplePolyomino/<WaitAndHint>d__20::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_Reset_m7CB3DAD8F63D190EA692853C0D3CAA843040569A (void);
// 0x00000131 System.Object GamePlay.SimplePolyomino/<WaitAndHint>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_get_Current_mA9C88B63A4ED03143C2B09622F5930DA10263ADF (void);
// 0x00000132 System.Void GamePlay.SimplePolyomino/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mE0BA7E3D37F32ABFB841FB5D6D5E233F06DFA9F0 (void);
// 0x00000133 System.Boolean GamePlay.SimplePolyomino/<>c__DisplayClass23_0::<SetRowWiseData>b__0(GamePlay.PolyominoDistributeData)
extern void U3CU3Ec__DisplayClass23_0_U3CSetRowWiseDataU3Eb__0_mCCCFB5AEF16181111654D1A10BC41FD641C826EB (void);
// 0x00000134 System.Void GamePlay.SimplePolyomino/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m110F7B1A83D4478B6C372AF65FF9BCEF87AF3408 (void);
// 0x00000135 System.Boolean GamePlay.SimplePolyomino/<>c__DisplayClass24_0::<SetColWiseData>b__0(GamePlay.PolyominoDistributeData)
extern void U3CU3Ec__DisplayClass24_0_U3CSetColWiseDataU3Eb__0_mE31312FFEE7B990AB0E2B03C0474DEEA370A3462 (void);
// 0x00000136 System.Void GamePlay.SimplePolyomino/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mA004A2BBE648440255D52481CA9D7D2AC33B03E3 (void);
// 0x00000137 System.Boolean GamePlay.SimplePolyomino/<>c__DisplayClass25_0::<DoesContainInRowList>b__0(GamePlay.PolyominoDistributeData)
extern void U3CU3Ec__DisplayClass25_0_U3CDoesContainInRowListU3Eb__0_mE5AD66A05511269CD0E540D59A9719A04C4E51D8 (void);
// 0x00000138 System.Void GamePlay.SimplePolyomino/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m96C5F12609C33401F205C2C44AD6B77E6251FADA (void);
// 0x00000139 System.Boolean GamePlay.SimplePolyomino/<>c__DisplayClass26_0::<DoesContainInColList>b__0(GamePlay.PolyominoDistributeData)
extern void U3CU3Ec__DisplayClass26_0_U3CDoesContainInColListU3Eb__0_m150D09728A95A16A73A01FD7EFE033CE957745B4 (void);
// 0x0000013A System.Void GamePlay.PolyominoDistributeData::.ctor()
extern void PolyominoDistributeData__ctor_m9315638EE64182F5552AE9C60DB14608E8EB0B0C (void);
// 0x0000013B System.Void GamePlay.TimerBasedGamePlay::OnDisable()
extern void TimerBasedGamePlay_OnDisable_m04A3C90D5D4B267CF7A28BB3BAC212B1BB96E867 (void);
// 0x0000013C System.Void GamePlay.TimerBasedGamePlay::Init()
extern void TimerBasedGamePlay_Init_m92BA66D3BD2DDB544896D96C2AD7848BEFD35E27 (void);
// 0x0000013D System.Void GamePlay.TimerBasedGamePlay::ResetScore()
extern void TimerBasedGamePlay_ResetScore_mDEE3AEE3884DC1176EDAB8BBA3683B2A7DFA5011 (void);
// 0x0000013E System.Collections.IEnumerator GamePlay.TimerBasedGamePlay::TimerCo()
extern void TimerBasedGamePlay_TimerCo_mB4235BDB64294813F17CA6C8468A6E7573350AC9 (void);
// 0x0000013F System.Void GamePlay.TimerBasedGamePlay::OnTimerOver()
extern void TimerBasedGamePlay_OnTimerOver_m524D8B7084B7FE0899460F90AF78D75C963ED020 (void);
// 0x00000140 System.Void GamePlay.TimerBasedGamePlay::StartCorutine()
extern void TimerBasedGamePlay_StartCorutine_m1DD45C81CDA2F7B89BAB44E91A5B4335656C7FA2 (void);
// 0x00000141 System.Void GamePlay.TimerBasedGamePlay::.ctor()
extern void TimerBasedGamePlay__ctor_m6FB10216A889737A51636F397C29DEA9D8C7ABF8 (void);
// 0x00000142 System.Void GamePlay.TimerBasedGamePlay/<TimerCo>d__5::.ctor(System.Int32)
extern void U3CTimerCoU3Ed__5__ctor_m7C0CAF6DFDC89E73368768DCE0A7D0891C8909C4 (void);
// 0x00000143 System.Void GamePlay.TimerBasedGamePlay/<TimerCo>d__5::System.IDisposable.Dispose()
extern void U3CTimerCoU3Ed__5_System_IDisposable_Dispose_m56059E0A8A9CFB18236BDCB78DF67BCF116D57D6 (void);
// 0x00000144 System.Boolean GamePlay.TimerBasedGamePlay/<TimerCo>d__5::MoveNext()
extern void U3CTimerCoU3Ed__5_MoveNext_mE4159120292B018DE23ED789D10D9794CD553847 (void);
// 0x00000145 System.Object GamePlay.TimerBasedGamePlay/<TimerCo>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimerCoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DA6D2BE5096A517728D1CE497E334E0555196F (void);
// 0x00000146 System.Void GamePlay.TimerBasedGamePlay/<TimerCo>d__5::System.Collections.IEnumerator.Reset()
extern void U3CTimerCoU3Ed__5_System_Collections_IEnumerator_Reset_mB3FA62DE93F2E10646F763FD243FF892CCB3CAF2 (void);
// 0x00000147 System.Object GamePlay.TimerBasedGamePlay/<TimerCo>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CTimerCoU3Ed__5_System_Collections_IEnumerator_get_Current_m8DD692E1E16E3D5191F940125B6CE7543963737B (void);
// 0x00000148 System.Void GamePlay.CameraManager::Awake()
extern void CameraManager_Awake_m4606FCB47B2BBC94EEB4649B6B75EFB7B30A5042 (void);
// 0x00000149 System.Void GamePlay.CameraManager::Start()
extern void CameraManager_Start_mC82EA0E4AED180C1A5C20BD6FDC5C002016EC623 (void);
// 0x0000014A System.Void GamePlay.CameraManager::OnInitWorldWithForCamera()
extern void CameraManager_OnInitWorldWithForCamera_m370F666BEB9594286A5E1D2B974868A930213490 (void);
// 0x0000014B System.Void GamePlay.CameraManager::SetWorld()
extern void CameraManager_SetWorld_m9571911B49FC1775117A49639AA5557722CCC70B (void);
// 0x0000014C System.Void GamePlay.CameraManager::.ctor()
extern void CameraManager__ctor_m2F200E3CFD2E61FCC9B00D088EF5CC306F7AB28A (void);
// 0x0000014D System.Void GamePlay.DataAnalyster::Awake()
extern void DataAnalyster_Awake_m888B3EB3F63235BF5528B3532625546B2F735F81 (void);
// 0x0000014E System.Void GamePlay.DataAnalyster::SetInt(System.String,System.Int32)
extern void DataAnalyster_SetInt_mAF9EE5598E14AA3357E868E8AD63FC854602D558 (void);
// 0x0000014F System.Void GamePlay.DataAnalyster::SetFloat(System.String,System.Single)
extern void DataAnalyster_SetFloat_m81E37053713D7992077D3A37E1264685BF0C3D84 (void);
// 0x00000150 System.Void GamePlay.DataAnalyster::SetString(System.String,System.String)
extern void DataAnalyster_SetString_m1EE02161D5EC28B452C572E25D6C35BE58C73073 (void);
// 0x00000151 System.Int32 GamePlay.DataAnalyster::GetInt(System.String,System.Int32)
extern void DataAnalyster_GetInt_mAA9784F6E3BAEFCBB0E11BBDDBC99A1A7E1AC53B (void);
// 0x00000152 System.Single GamePlay.DataAnalyster::GetFloat(System.String,System.Single)
extern void DataAnalyster_GetFloat_mF7950EA4BF383BB1E92ECE3D8C89E7D4FA811811 (void);
// 0x00000153 System.String GamePlay.DataAnalyster::GetString(System.String,System.String)
extern void DataAnalyster_GetString_m4264B3E4EEF118272074E1BCCE0F94E8EFE67F6D (void);
// 0x00000154 System.Void GamePlay.DataAnalyster::SetDataOnGameComplite(System.Int32)
extern void DataAnalyster_SetDataOnGameComplite_m6A312A4DDB5F2D7BC098D1EA6BB3477D196C32ED (void);
// 0x00000155 System.Void GamePlay.DataAnalyster::IncrementSpinCount()
extern void DataAnalyster_IncrementSpinCount_m43A9A01630329FBDE4EA055CB408747F950DED79 (void);
// 0x00000156 System.Void GamePlay.DataAnalyster::IncrementContinueGameCount()
extern void DataAnalyster_IncrementContinueGameCount_m8B9620BF7D287616EB3991B6F204C6A038CB6B83 (void);
// 0x00000157 System.Void GamePlay.DataAnalyster::IncrementOneBlockBoosterUseCount()
extern void DataAnalyster_IncrementOneBlockBoosterUseCount_mDD1538085A460510B4CF09D6CED24BFCDA65039B (void);
// 0x00000158 System.Void GamePlay.DataAnalyster::IncrementRainbowBlockBoosterUseCount()
extern void DataAnalyster_IncrementRainbowBlockBoosterUseCount_mEEF22954BEDF1C21A056EA7C7644B64A6DF12CC4 (void);
// 0x00000159 System.Void GamePlay.DataAnalyster::IncrementBigBombBoosterBoosterUseCount()
extern void DataAnalyster_IncrementBigBombBoosterBoosterUseCount_m67A9B614D8E6E21614579E41EC8214AC829E2E8B (void);
// 0x0000015A System.Void GamePlay.DataAnalyster::SaveGameData()
extern void DataAnalyster_SaveGameData_mB6BA37B13CCCA209D78D872CA627EE18BADE723C (void);
// 0x0000015B System.String GamePlay.DataAnalyster::GetGameData()
extern void DataAnalyster_GetGameData_m5B94B0378A669BF9EF5E658763F8E5B27055D972 (void);
// 0x0000015C System.Void GamePlay.DataAnalyster::LoadData(System.String)
extern void DataAnalyster_LoadData_mC42ECD650AE5C169C142C6EE6C288BFD51CE7E9A (void);
// 0x0000015D System.Void GamePlay.DataAnalyster::.ctor()
extern void DataAnalyster__ctor_mD929F8E2DAAC54F05BD838F264941AA5FFD289A4 (void);
// 0x0000015E System.Void GamePlay.BreakingBlockGameData::.ctor()
extern void BreakingBlockGameData__ctor_m0A277E4379FCC6ED622583DF7A4925A8A76F3C90 (void);
// 0x0000015F System.String GamePlay.DataManager::get_LastTimeToSpin()
extern void DataManager_get_LastTimeToSpin_mC21B3B7A3A698EA62000399CBFAD23E4CEA4E19F (void);
// 0x00000160 System.Void GamePlay.DataManager::set_LastTimeToSpin(System.String)
extern void DataManager_set_LastTimeToSpin_m6C986DDB8773E5BA876EB5B48EE9522C35AE9C0F (void);
// 0x00000161 System.Boolean GamePlay.DataManager::get_IsFreeSpinAvailable()
extern void DataManager_get_IsFreeSpinAvailable_mAF5E17FFE54B52E8346B1B2B508CDD0F8FBEBB44 (void);
// 0x00000162 System.Boolean GamePlay.DataManager::get_IsContinueGame()
extern void DataManager_get_IsContinueGame_mC174103E96A256F747902A8D150116E4DEE59D05 (void);
// 0x00000163 System.Void GamePlay.DataManager::Awake()
extern void DataManager_Awake_mB3EE29372BD42A7217F8D72C3760CDC14A18D072 (void);
// 0x00000164 System.Void GamePlay.DataManager::ResetPolyominoGeneratorCount()
extern void DataManager_ResetPolyominoGeneratorCount_m711FEB8D3C3818E8C3F4609AC4BDD612F73729AA (void);
// 0x00000165 System.Void GamePlay.DataManager::SetFreeSpinAvailable()
extern void DataManager_SetFreeSpinAvailable_mD687EFF50CB27B880B8D8C329BF9CB1470ADF0D2 (void);
// 0x00000166 System.Void GamePlay.DataManager::ResetFreeSpinAvailable()
extern void DataManager_ResetFreeSpinAvailable_m86F40FD9ED3E71DF177DB5981F4DC9288706DD0B (void);
// 0x00000167 System.Boolean GamePlay.DataManager::IsItStaringPolyominoPhase()
extern void DataManager_IsItStaringPolyominoPhase_m11A6683C61FE3F5C2781A920C5761B349B70CA09 (void);
// 0x00000168 System.Void GamePlay.DataManager::.ctor()
extern void DataManager__ctor_mCD6709C0ABCFF08FC7A5B9B8D4AFDE5655AC51CF (void);
// 0x00000169 System.Void GamePlay.GameManager::Awake()
extern void GameManager_Awake_m2502394043A2161755FB1E69C0C8C4E5E52704B1 (void);
// 0x0000016A System.Void GamePlay.GameManager::Start()
extern void GameManager_Start_m74C3FE7266249F33355E5A2D00B0EC6279ACCBA3 (void);
// 0x0000016B System.Void GamePlay.GameManager::Init()
extern void GameManager_Init_mD0719BBAA0C4A3BC1921659EEB098C08AEB08A50 (void);
// 0x0000016C System.Void GamePlay.GameManager::RestartGame()
extern void GameManager_RestartGame_mB360FB8E9B1B707B528354B1A8D62B9C808DF29E (void);
// 0x0000016D System.Void GamePlay.GameManager::OnResumeGame()
extern void GameManager_OnResumeGame_mFC4CAA3A72DEE8F194F881C1E1F9C6D43F437803 (void);
// 0x0000016E System.Void GamePlay.GameManager::SetColorDataByGamePlay(GamePlay.GamePlayType)
extern void GameManager_SetColorDataByGamePlay_mFEF0B6A60B2868F353D750480CF44B413A146D52 (void);
// 0x0000016F System.Void GamePlay.GameManager::.ctor()
extern void GameManager__ctor_mECAA9584696B570432522A3381E37FE3D8A49428 (void);
// 0x00000170 System.Void GamePlay.InputManager::Awake()
extern void InputManager_Awake_mACAA154DFD76ADD1F44064EAD321FCA1D3978175 (void);
// 0x00000171 System.Void GamePlay.InputManager::Start()
extern void InputManager_Start_m4312832FC12025D4987598E5151C8D9F6DED6506 (void);
// 0x00000172 System.Void GamePlay.InputManager::Update()
extern void InputManager_Update_m60614A6440667AF28BA0ADDF130D05CD61AF93C8 (void);
// 0x00000173 System.Void GamePlay.InputManager::PlayerInput()
extern void InputManager_PlayerInput_m8F195913E18E2AB6CB25DCBA2F5A0E94FB65A148 (void);
// 0x00000174 System.Void GamePlay.InputManager::CheckHitObject()
extern void InputManager_CheckHitObject_mA7133BEAABC47EFD58E808D0F6E058CA11109B01 (void);
// 0x00000175 System.Void GamePlay.InputManager::DragObject()
extern void InputManager_DragObject_m2BAD71E6AB54C7B760B0CA1A81EC81238F1F4DBD (void);
// 0x00000176 System.Void GamePlay.InputManager::DropObject()
extern void InputManager_DropObject_m3CCA923BB95C3CC7F8449D91F00CBE0E486EBB46 (void);
// 0x00000177 System.Boolean GamePlay.InputManager::IsValidMove(GamePlay.BasePolyomino)
extern void InputManager_IsValidMove_mC15A02E6C1EC783DA836D5F9715ECFDFE8EC8B6C (void);
// 0x00000178 System.Void GamePlay.InputManager::.ctor()
extern void InputManager__ctor_mB1098C74D4BF458C11074E0907C7EA1D80752B44 (void);
// 0x00000179 System.Void GamePlay.InputManager::<CheckHitObject>b__11_0()
extern void InputManager_U3CCheckHitObjectU3Eb__11_0_m3839724C17AF4AEB0F23BB4D8C2069F702567F01 (void);
// 0x0000017A System.Void GamePlay.CustomTransform::.ctor()
extern void CustomTransform__ctor_m9A840035FC11C6B99E070DC5D32B81435D6D07ED (void);
// 0x0000017B System.Void GamePlay.CustomTransform::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CustomTransform__ctor_m29965ABCCB0D8FE806CEFC8AEC2D3865330BD69E (void);
// 0x0000017C T[] GamePlay.JsonHelper::FromJson(System.String)
// 0x0000017D System.String GamePlay.JsonHelper::ToJson(T[])
// 0x0000017E System.String GamePlay.JsonHelper::ToJson(T[],System.Boolean)
// 0x0000017F System.Void GamePlay.JsonHelper/Wrapper`1::.ctor()
// 0x00000180 System.Void GamePlay.PolyominoGenerator::Awake()
extern void PolyominoGenerator_Awake_m64A26E188CFABD62A96E45E6D403F2B1C33F673C (void);
// 0x00000181 GamePlay.BasePolyomino GamePlay.PolyominoGenerator::GetPolyomino(UnityEngine.Transform)
extern void PolyominoGenerator_GetPolyomino_m89D6D17A9963495889B6C2383BCF1693C4E6A3B4 (void);
// 0x00000182 GamePlay.BasePolyomino GamePlay.PolyominoGenerator::GetStartingPolyomino(UnityEngine.Transform)
extern void PolyominoGenerator_GetStartingPolyomino_mAF432D0F1D89C6DAAB69931632B5A30FD7AEC064 (void);
// 0x00000183 System.Void GamePlay.PolyominoGenerator::.ctor()
extern void PolyominoGenerator__ctor_mB0B4EE424A4C033C2E3D9433F57D104353E13FB4 (void);
// 0x00000184 System.Void GamePlay.PolyominoHolder::Awake()
extern void PolyominoHolder_Awake_mD67D36D0E0DD0C844D1B099C869CC3C29F0AAE22 (void);
// 0x00000185 System.Void GamePlay.PolyominoHolder::SetPolyominoInSlot()
extern void PolyominoHolder_SetPolyominoInSlot_m11C9CC202C296D40654066DA3A74308508158E57 (void);
// 0x00000186 System.Void GamePlay.PolyominoHolder::CheckForPolyominoGenerate()
extern void PolyominoHolder_CheckForPolyominoGenerate_m821CBD0C90EF092F4C34B28900E308E88CC9A50D (void);
// 0x00000187 System.Void GamePlay.PolyominoHolder::SaveLastPolyomino()
extern void PolyominoHolder_SaveLastPolyomino_m3BBC7C511DF40D50CE91C4285328FF852D5E8DC7 (void);
// 0x00000188 System.Void GamePlay.PolyominoHolder::SetLastPolyominoInSlot()
extern void PolyominoHolder_SetLastPolyominoInSlot_mF117D4394450A5387CE3020C2D2461F319481CF5 (void);
// 0x00000189 System.Void GamePlay.PolyominoHolder::ResetLastPolyomino()
extern void PolyominoHolder_ResetLastPolyomino_m60644530A1A1916FA2DC49E4CDB42A2BF3FC2C07 (void);
// 0x0000018A System.Void GamePlay.PolyominoHolder::ClearAllPolyomino()
extern void PolyominoHolder_ClearAllPolyomino_mA5D1DF64A47472C8B496A7CB0EC962FF46A83276 (void);
// 0x0000018B System.Void GamePlay.PolyominoHolder::CheckForUnplacedPolyomino()
extern void PolyominoHolder_CheckForUnplacedPolyomino_mE5F507354B57646167A278EA37BA4B80EEAF4FD0 (void);
// 0x0000018C System.Boolean GamePlay.PolyominoHolder::IsAllSlotEmpty()
extern void PolyominoHolder_IsAllSlotEmpty_mB95F0E924E00CF5D4E4BEFF965541264B5FCFD8D (void);
// 0x0000018D System.Void GamePlay.PolyominoHolder::CheckForGameOver()
extern void PolyominoHolder_CheckForGameOver_mF7D8CEA427FD5F864D343BD59E68146FA44F8570 (void);
// 0x0000018E System.Void GamePlay.PolyominoHolder::.ctor()
extern void PolyominoHolder__ctor_m74B6A341371567C633B4727DAB73A29B487A5ECF (void);
// 0x0000018F System.Void GamePlay.PolyominoHolder/<>c::.cctor()
extern void U3CU3Ec__cctor_mA9C365BE581656A37ADCCE60831A1A37BE4F261A (void);
// 0x00000190 System.Void GamePlay.PolyominoHolder/<>c::.ctor()
extern void U3CU3Ec__ctor_m497D8B64003FAD56492369F6E414C198D422681D (void);
// 0x00000191 System.Void GamePlay.PolyominoHolder/<>c::<SaveLastPolyomino>b__6_0(GamePlay.PolyominoSlot)
extern void U3CU3Ec_U3CSaveLastPolyominoU3Eb__6_0_m048C2921210FB5D1CB83A41D720BA58CA01BDBB0 (void);
// 0x00000192 System.Void GamePlay.PolyominoHolder/<>c::<SetLastPolyominoInSlot>b__7_0(GamePlay.PolyominoSlot)
extern void U3CU3Ec_U3CSetLastPolyominoInSlotU3Eb__7_0_m366B6897365851216B936DA4C7D55BD0C1BCA40A (void);
// 0x00000193 System.Void GamePlay.PolyominoHolder/<>c::<ResetLastPolyomino>b__8_0(GamePlay.PolyominoSlot)
extern void U3CU3Ec_U3CResetLastPolyominoU3Eb__8_0_m818EE64C3FC7B149E863D63553BA287C850D0211 (void);
// 0x00000194 System.Void GamePlay.PolyominoHolder/<>c::<CheckForUnplacedPolyomino>b__10_0(GamePlay.PolyominoSlot)
extern void U3CU3Ec_U3CCheckForUnplacedPolyominoU3Eb__10_0_m317C43BA19911743A59D35E9C0AA178732594E52 (void);
// 0x00000195 System.Void GamePlay.RuleManager::Awake()
extern void RuleManager_Awake_mCA977616EC20FAAAD27666949D060A282867E485 (void);
// 0x00000196 System.Void GamePlay.RuleManager::CheckDropRule(GamePlay.SimplePolyomino)
extern void RuleManager_CheckDropRule_m9B85DA55383F53A0E98F357EE5AD57820CBD21D3 (void);
// 0x00000197 System.Void GamePlay.RuleManager::Drop(GamePlay.SimplePolyomino)
extern void RuleManager_Drop_m816D594D6D1C0A00A0FE141F9B48A3C5516D6DD3 (void);
// 0x00000198 System.Void GamePlay.RuleManager::DestroyCells(GamePlay.SpriteTypes)
extern void RuleManager_DestroyCells_m6F32B4441E59BB187DACAF352C100DA06BB26234 (void);
// 0x00000199 System.Collections.Generic.List`1<GamePlay.DestroyCellData> GamePlay.RuleManager::CheckDestroyRule(GamePlay.PolyominoBlock,GamePlay.SimplePolyomino)
extern void RuleManager_CheckDestroyRule_m6D87280B1963A38771142F306B14B41BF54D0A39 (void);
// 0x0000019A System.Void GamePlay.RuleManager::CheckEntryRule(GamePlay.SimplePolyomino)
extern void RuleManager_CheckEntryRule_m60BCD9D2E6676C018FC0A73063C9F8A921C704C7 (void);
// 0x0000019B System.Void GamePlay.RuleManager::SetTemporarySprite(GamePlay.SimplePolyomino,System.Int32,System.Int32)
extern void RuleManager_SetTemporarySprite_m1420559025017015B3C6D940B20660DF7C11034E (void);
// 0x0000019C System.Void GamePlay.RuleManager::SetTemporaryRowSprite(GamePlay.SimplePolyomino,System.Int32,System.Int32)
extern void RuleManager_SetTemporaryRowSprite_m9A53EDDF0387648A76481131A452F3872293B3B4 (void);
// 0x0000019D System.Void GamePlay.RuleManager::SetTemporaryColSprite(GamePlay.SimplePolyomino,System.Int32,System.Int32)
extern void RuleManager_SetTemporaryColSprite_m3A0FD743E067ED9EC7F86C2C09F6CDB7F5C830A7 (void);
// 0x0000019E System.Boolean GamePlay.RuleManager::IsAnyEmptyCell(System.Collections.Generic.List`1<GamePlay.Cell>)
extern void RuleManager_IsAnyEmptyCell_m94461FD78DD382A87C957E48C3FA852968F59E99 (void);
// 0x0000019F System.Void GamePlay.RuleManager::CheckExitRule(GamePlay.Cell)
extern void RuleManager_CheckExitRule_mB9928300DDFE6B662373E22E62471035FC052C13 (void);
// 0x000001A0 System.Void GamePlay.RuleManager::ResetTemporarySprite()
extern void RuleManager_ResetTemporarySprite_mBD8C3BB7E5C0DFA8142E503F47AB3B466643FEF9 (void);
// 0x000001A1 System.Boolean GamePlay.RuleManager::IsPlaceAvailable(GamePlay.BasePolyomino)
extern void RuleManager_IsPlaceAvailable_mDCD03CBA7A91E303BBDD895BE97637DAA19A8F90 (void);
// 0x000001A2 System.Boolean GamePlay.RuleManager::IsPlaceAvailableForSimplePolyomino(GamePlay.SimplePolyomino)
extern void RuleManager_IsPlaceAvailableForSimplePolyomino_mA6AEB13F086AF5862C81E436E2337B52C6A2A79F (void);
// 0x000001A3 System.Boolean GamePlay.RuleManager::IsItValidIndexFroRowAndCol(System.Int32)
extern void RuleManager_IsItValidIndexFroRowAndCol_mAFABD4CAC919E0E4822E9680D49568D9010A82A9 (void);
// 0x000001A4 System.Void GamePlay.RuleManager::CheckEntryRuleForBomb(System.Collections.Generic.List`1<GamePlay.Cell>)
extern void RuleManager_CheckEntryRuleForBomb_mBC536885A47EAF111A9A6C062B2C1DC509F9910E (void);
// 0x000001A5 System.Void GamePlay.RuleManager::CheckExitRuleForBomb()
extern void RuleManager_CheckExitRuleForBomb_m7C7A96400025516C9B8D79E4EF10BC1B31A94387 (void);
// 0x000001A6 System.Void GamePlay.RuleManager::.ctor()
extern void RuleManager__ctor_m3C95CFE017DFCA490077924A43B859ADAFED9988 (void);
// 0x000001A7 System.Void GamePlay.RuleManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m8D66AFBE9290CA6BA2A3A12B27843E7EDCC38B74 (void);
// 0x000001A8 System.Void GamePlay.RuleManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m46BE214EAFE6672F468A826763F84E34F6906159 (void);
// 0x000001A9 System.Void GamePlay.RuleManager/<>c::<ResetTemporarySprite>b__15_0(GamePlay.Cell)
extern void U3CU3Ec_U3CResetTemporarySpriteU3Eb__15_0_mA952794DE872EDAAA06A2C9B4217F7D310398E48 (void);
// 0x000001AA System.Void GamePlay.RuleManager/<>c::<CheckExitRuleForBomb>b__20_0(GamePlay.Cell)
extern void U3CU3Ec_U3CCheckExitRuleForBombU3Eb__20_0_mA2730DF77505F8041A185CB69B6433CBE0AC5FD1 (void);
// 0x000001AB System.Void GamePlay.DestroyCellData::Add(GamePlay.Cell)
extern void DestroyCellData_Add_m1D7CD59CCFE94E7A67AE3D99AA516DE16742769F (void);
// 0x000001AC System.Void GamePlay.DestroyCellData::.ctor()
extern void DestroyCellData__ctor_mA90649A6B725CC2F3131625BD9BC87A19F4A1522 (void);
// 0x000001AD System.Void GamePlay.ScoreAnimationHandler::Awake()
extern void ScoreAnimationHandler_Awake_mE6479F22FF60A3FC25426D3C8FE3C6743A291E18 (void);
// 0x000001AE System.Void GamePlay.ScoreAnimationHandler::SetCellTransform(UnityEngine.Transform)
extern void ScoreAnimationHandler_SetCellTransform_m459923D0B3F07D0CE935D974F52A1076FF5BEE5C (void);
// 0x000001AF System.Void GamePlay.ScoreAnimationHandler::ActivateEffects(System.Int32)
extern void ScoreAnimationHandler_ActivateEffects_mE10B7BCB8722E53D7E14FEF6E118A83CC6F282CF (void);
// 0x000001B0 System.Void GamePlay.ScoreAnimationHandler::.ctor()
extern void ScoreAnimationHandler__ctor_mC17DA5A898049ABBF55672A28D9C36B94A80A264 (void);
// 0x000001B1 System.Boolean GamePlay.SoundManager::get_Sound()
extern void SoundManager_get_Sound_m5C850EB289DA582F6CBAEBED0EAE782DC144C9E8 (void);
// 0x000001B2 System.Void GamePlay.SoundManager::set_Sound(System.Boolean)
extern void SoundManager_set_Sound_mF6A449C3C379306EB39FA56647B42F882CCAF3F5 (void);
// 0x000001B3 System.Void GamePlay.SoundManager::Awake()
extern void SoundManager_Awake_m2D7E9CC0BA595286C0E58EDB243623E166DF1B13 (void);
// 0x000001B4 System.Void GamePlay.SoundManager::ToggleSound()
extern void SoundManager_ToggleSound_mE4D0327D31B796A8343116C11125003D4FB09C21 (void);
// 0x000001B5 System.Void GamePlay.SoundManager::SetSound()
extern void SoundManager_SetSound_mB6ECEA55E33681820FDCDD04B31F8575B40B6D4A (void);
// 0x000001B6 System.Void GamePlay.SoundManager::PlayBackgroundMusic()
extern void SoundManager_PlayBackgroundMusic_m67D53D47466B65B8364E270CF7CAF7E0B63CD5F4 (void);
// 0x000001B7 System.Void GamePlay.SoundManager::PlayBackgroundMusicHalloween()
extern void SoundManager_PlayBackgroundMusicHalloween_mEA7C884B5E29BB8297BACEEA0641BC949892DDC9 (void);
// 0x000001B8 System.Void GamePlay.SoundManager::PlayBackgroundMusicChristmas()
extern void SoundManager_PlayBackgroundMusicChristmas_mC928462F86C76A4D8EAA10344C6544170A148EA4 (void);
// 0x000001B9 System.Void GamePlay.SoundManager::PlayBackgroundMusicEster()
extern void SoundManager_PlayBackgroundMusicEster_mB0CB2E591F792D0EFEF5B1B832D5ED7118EA1737 (void);
// 0x000001BA System.Void GamePlay.SoundManager::PlayButtonClickSFX()
extern void SoundManager_PlayButtonClickSFX_mAD6709D65C990FBDF2AE32079C5AD27C12DCDCCA (void);
// 0x000001BB System.Void GamePlay.SoundManager::PlayGameStart()
extern void SoundManager_PlayGameStart_mA12E712D9C9835AF8DCD9993EB8A5C2C87118755 (void);
// 0x000001BC System.Void GamePlay.SoundManager::PlayGameWinSFX()
extern void SoundManager_PlayGameWinSFX_m6DBBB43DD6FC59C2D74435B7D35B9178D43E65BA (void);
// 0x000001BD System.Void GamePlay.SoundManager::PlayGameLooseSFX()
extern void SoundManager_PlayGameLooseSFX_m0E1A6073A6793E09098F3B872FB1FF1B21FDE10E (void);
// 0x000001BE System.Void GamePlay.SoundManager::PlayNotEnoughBoosterSFX()
extern void SoundManager_PlayNotEnoughBoosterSFX_mCD95ECE3FF1D4F636028FF2C982C12AD1A6BF453 (void);
// 0x000001BF System.Void GamePlay.SoundManager::PlayDestorySFX()
extern void SoundManager_PlayDestorySFX_mC13407C5B9ACEFC4599694C13923E7B38E5956DC (void);
// 0x000001C0 System.Void GamePlay.SoundManager::PlayDropPolyomino()
extern void SoundManager_PlayDropPolyomino_mDF62494743050C0D5EB57F92C622561C38059598 (void);
// 0x000001C1 System.Void GamePlay.SoundManager::PlayBombSound()
extern void SoundManager_PlayBombSound_m6AB1A0B07E118987869B73B3779D3BE55E4DE6BB (void);
// 0x000001C2 System.Void GamePlay.SoundManager::PlayRainbowSound()
extern void SoundManager_PlayRainbowSound_m056520FD0CF4C3D60E14A3077149D174F23B7B74 (void);
// 0x000001C3 System.Void GamePlay.SoundManager::PlayCoolSound()
extern void SoundManager_PlayCoolSound_m60BF1333427BDACB866620E46D46DE12DB6FBF06 (void);
// 0x000001C4 System.Void GamePlay.SoundManager::PlayGoodSound()
extern void SoundManager_PlayGoodSound_m23D0B64D08296908B820A2E3C1E4A3CA1023003E (void);
// 0x000001C5 System.Void GamePlay.SoundManager::PlayAmazingSound()
extern void SoundManager_PlayAmazingSound_m86F388CF2D65FDA2B62A93FFC8A1B555EBFD8C72 (void);
// 0x000001C6 System.Void GamePlay.SoundManager::PlaySpinWheelSound()
extern void SoundManager_PlaySpinWheelSound_mC57A15A4FBC9BAA32D086C3E1D8F3298685F54A8 (void);
// 0x000001C7 System.Void GamePlay.SoundManager::StopSpinWheelSound()
extern void SoundManager_StopSpinWheelSound_m7953A9B149E7F49F7313D78B0C64D7EAA105EB51 (void);
// 0x000001C8 System.Void GamePlay.SoundManager::PlaySpinRewardSound()
extern void SoundManager_PlaySpinRewardSound_mED9C4DB1C10D803643A58D5545BA760AEDEA5AE7 (void);
// 0x000001C9 System.Void GamePlay.SoundManager::PlaySpinRewardClime()
extern void SoundManager_PlaySpinRewardClime_m75B3FE8B2C7F9F264B1B4DF446A4076301FE4C2F (void);
// 0x000001CA System.Void GamePlay.SoundManager::StopBGSound()
extern void SoundManager_StopBGSound_m76ACF3F820A22A5839F4E66147E40A12448C7730 (void);
// 0x000001CB System.Void GamePlay.SoundManager::StopHaloweenBGSound()
extern void SoundManager_StopHaloweenBGSound_m83912052CF71C681EDA328E956378FEC11F8393B (void);
// 0x000001CC System.Void GamePlay.SoundManager::StopChristmasBGSound()
extern void SoundManager_StopChristmasBGSound_m2A64B42695056F5539D7D288993889FFA2BC3D9D (void);
// 0x000001CD System.Void GamePlay.SoundManager::StopEsterBGSound()
extern void SoundManager_StopEsterBGSound_m7A1CE696A6BE429717CDA624A8F09337A9EA403A (void);
// 0x000001CE System.Void GamePlay.SoundManager::.ctor()
extern void SoundManager__ctor_mAC6A45846C0AD7DF0E1355A56369785800C09D70 (void);
// 0x000001CF System.Void GamePlay.TimeHandler::SetLastCollectTime()
extern void TimeHandler_SetLastCollectTime_m625E8800E45BDD163407420EC76C3D21748D4023 (void);
// 0x000001D0 System.TimeSpan GamePlay.TimeHandler::GetTimeDifference(System.String)
extern void TimeHandler_GetTimeDifference_mF02E163B8B69CF9565A20F6D0354916D698C1C76 (void);
// 0x000001D1 System.TimeSpan GamePlay.TimeHandler::GetRemainingTime(System.DateTime)
extern void TimeHandler_GetRemainingTime_mAB62640052D6C97818019CF1218E37B30D14E12A (void);
// 0x000001D2 System.Single GamePlay.TimeHandler::GetRemainingSeconds(System.Single,System.String)
extern void TimeHandler_GetRemainingSeconds_m8C4D1ECD9655BC2276782AF513A5C5FB097CF834 (void);
// 0x000001D3 System.String GamePlay.TimeHandler::SerializeDate(System.DateTime)
extern void TimeHandler_SerializeDate_mD0F47D0679B32A2127C27E1C216ECF44337372DE (void);
// 0x000001D4 System.DateTime GamePlay.TimeHandler::DeserializeDate(System.String)
extern void TimeHandler_DeserializeDate_m7E25D2230AB97AE008D712BA8C54E46DDCD30568 (void);
// 0x000001D5 System.Void GamePlay.TimeHandler::.ctor()
extern void TimeHandler__ctor_mDD08C91ED0C13E5B11B1631B246ACCBE552BDC7F (void);
// 0x000001D6 System.Void GamePlay.UiManager::Awake()
extern void UiManager_Awake_mBFDAFF1B2685678ABCEBAA8BAFC23AFE40A79EF7 (void);
// 0x000001D7 GamePlay.GamePlayView GamePlay.UiManager::GetGamePlayViewByType(GamePlay.GamePlayType)
extern void UiManager_GetGamePlayViewByType_m9BCD3A7781C7CAC90214E75675E5D30E06B03E3A (void);
// 0x000001D8 GamePlay.TimerBasedGamePlay GamePlay.UiManager::GetTimerBasedGamePlayViewByType(GamePlay.GamePlayType)
extern void UiManager_GetTimerBasedGamePlayViewByType_m01B1720EC67525E1A8840538E0EC18666730BD4B (void);
// 0x000001D9 BaseHomeView GamePlay.UiManager::GetHomeViewByType(GamePlay.GamePlayType)
extern void UiManager_GetHomeViewByType_mAD2BBF1DCCD31718561080CDFB52DF00C926A058 (void);
// 0x000001DA GamePlay.GameOverView GamePlay.UiManager::GetGameOverViewByType(GamePlay.GamePlayType)
extern void UiManager_GetGameOverViewByType_m9E860157C12424DA68D605CF11A19A10582F02FF (void);
// 0x000001DB System.Boolean GamePlay.UiManager::isAnyTimerGameplyOn()
extern void UiManager_isAnyTimerGameplyOn_m1C454C4DD3EA9A3C0A5764B74CE76B117044791B (void);
// 0x000001DC System.Void GamePlay.UiManager::ButtonSound()
extern void UiManager_ButtonSound_m271CE43BAE6DA6ECC68261721C847809922DD3FE (void);
// 0x000001DD System.Void GamePlay.UiManager::.ctor()
extern void UiManager__ctor_mD4D5F4E710CFB2EA6BDCA3F24A932AB5038AB010 (void);
// 0x000001DE UnityEngine.Sprite GamePlay.ColorData::GetSprite(GamePlay.SpriteTypes)
extern void ColorData_GetSprite_m86E13C52B3E5971C81704A0EF15B53D19A960790 (void);
// 0x000001DF GamePlay.SpriteTypes GamePlay.ColorData::GetRandomColorType()
extern void ColorData_GetRandomColorType_mC07C182BDF9F15046A835D24CADE8C867068EFB0 (void);
// 0x000001E0 System.Void GamePlay.ColorData::.ctor()
extern void ColorData__ctor_m4EC8DEAAC82977EB88E33D457E715954AC34FDB0 (void);
// 0x000001E1 System.Void GamePlay.ColorData/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mB413EF553BE89145593BE749848EFAA395AE1D4B (void);
// 0x000001E2 System.Boolean GamePlay.ColorData/<>c__DisplayClass2_0::<GetSprite>b__0(GamePlay.SquareSpriteData)
extern void U3CU3Ec__DisplayClass2_0_U3CGetSpriteU3Eb__0_mC30673F2046726F3E3B81A564517CE673E3E3101 (void);
// 0x000001E3 System.Void GamePlay.SquareSpriteData::.ctor()
extern void SquareSpriteData__ctor_mA77D59335A6D0FB3BBC47D3268D36B4244477389 (void);
// 0x000001E4 UnityEngine.Material GamePlay.MaterialDataSO::GetMaterial(GamePlay.SpriteTypes)
extern void MaterialDataSO_GetMaterial_m2CCFFFC45987E5C632607CD732B9D3AD59233AAA (void);
// 0x000001E5 System.Void GamePlay.MaterialDataSO::.ctor()
extern void MaterialDataSO__ctor_m53F6BA5FDB59BEDA4BEC9179D81BC6249CF3C06D (void);
// 0x000001E6 System.Void GamePlay.MaterialDataSO/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m02250A0E8534B5A56A9F8BA9B95A34914EDB4294 (void);
// 0x000001E7 System.Boolean GamePlay.MaterialDataSO/<>c__DisplayClass1_0::<GetMaterial>b__0(GamePlay.MaterialData)
extern void U3CU3Ec__DisplayClass1_0_U3CGetMaterialU3Eb__0_m53EE49B1A63F8B02E2F85C5DF295D03ABC774B9C (void);
// 0x000001E8 System.Void GamePlay.MaterialData::.ctor()
extern void MaterialData__ctor_m2372F1FDB87F89A363FDE080703676D04FE9AF26 (void);
// 0x000001E9 GamePlay.SimplePolyomino GamePlay.PolyominoData::GetRandomPolyomino()
extern void PolyominoData_GetRandomPolyomino_m4CE4A84537C3874C2ED3DA94DD7334586AC49CA3 (void);
// 0x000001EA GamePlay.SimplePolyomino GamePlay.PolyominoData::GetStartingPolyomino()
extern void PolyominoData_GetStartingPolyomino_m0AC72DA7FC9F2794635C9EFCCD34C8902E9F155D (void);
// 0x000001EB System.Void GamePlay.PolyominoData::.ctor()
extern void PolyominoData__ctor_m3E6918447F988A27B04BBBE7E862337FD30B14AF (void);
// 0x000001EC System.Void GamePlay.FeedBackView::OnSubmitClick()
extern void FeedBackView_OnSubmitClick_mF769E8548BB42D7B9302DD39360D0512BC3C1E99 (void);
// 0x000001ED System.Void GamePlay.FeedBackView::OnCloseClick()
extern void FeedBackView_OnCloseClick_m41469B3A8AD0348D6BD3204DB5217F8C12AAE2F7 (void);
// 0x000001EE System.Void GamePlay.FeedBackView::.ctor()
extern void FeedBackView__ctor_m48864C291C7FC3D7F5D7DE70A9DADE0B848D1C9C (void);
// 0x000001EF System.Void GamePlay.GameLossView::SetContinueGameReward()
extern void GameLossView_SetContinueGameReward_m68D41ECB01C67A852E1C077CB21C88BF118A3878 (void);
// 0x000001F0 System.Void GamePlay.GameLossView::OnCloseClick()
extern void GameLossView_OnCloseClick_m00EAFBF103CD87455DB3819F737BEE454CE1F6D0 (void);
// 0x000001F1 System.Void GamePlay.GameLossView::.ctor()
extern void GameLossView__ctor_mD7A836660BE79586198DAC12046427B93BCA34E4 (void);
// 0x000001F2 System.Void GamePlay.GameOverView::OnEnable()
extern void GameOverView_OnEnable_mBC3B9C808E36D336A23E503AF7EF9FABDA3D7ADD (void);
// 0x000001F3 System.Void GamePlay.GameOverView::OnDisable()
extern void GameOverView_OnDisable_mE3026F334B807E42C7F3537B3791EE75350E9396 (void);
// 0x000001F4 System.Void GamePlay.GameOverView::DisplayScores()
extern void GameOverView_DisplayScores_mE0B43CF138CFD37FE22B535C20BE3EEA97D80109 (void);
// 0x000001F5 System.Void GamePlay.GameOverView::SetHeading(System.Boolean)
extern void GameOverView_SetHeading_mC3E491E0ED5AF7951EA98D1D4AB90A8B4ACAE043 (void);
// 0x000001F6 System.Void GamePlay.GameOverView::OnHomeClick()
extern void GameOverView_OnHomeClick_mB45447F3801E8CCC880D747C0399FEA5BF966D30 (void);
// 0x000001F7 System.Void GamePlay.GameOverView::OnRestart()
extern void GameOverView_OnRestart_m697E49CA82A865D4118C46A69E1ABC8A0E7FEE8C (void);
// 0x000001F8 System.Void GamePlay.GameOverView::OnQuitClick()
extern void GameOverView_OnQuitClick_m2A88F24D19879BE059ACC7D40079018ECFA4DB60 (void);
// 0x000001F9 System.Void GamePlay.GameOverView::.ctor()
extern void GameOverView__ctor_m8DB03784603F47152E4341DC136EFD8A252A6661 (void);
// 0x000001FA System.Void GamePlay.GamePlayView::.ctor()
extern void GamePlayView__ctor_m715E2DB7F6454551ABDD5DC62BC7523B90AC5826 (void);
// 0x000001FB System.Void GamePlay.GameQuitView::OnCloseClick()
extern void GameQuitView_OnCloseClick_m1E0A8E4CEB7DBA695D5913214A2650DF2F95056B (void);
// 0x000001FC System.Void GamePlay.GameQuitView::OnResumeClick()
extern void GameQuitView_OnResumeClick_m2881F88023C92A91C211CCC032301883B6AE46B6 (void);
// 0x000001FD System.Void GamePlay.GameQuitView::OnQuitClick()
extern void GameQuitView_OnQuitClick_m9622FCB3B76DD0021BEBB63955B2BBFA2787CEA2 (void);
// 0x000001FE System.Void GamePlay.GameQuitView::.ctor()
extern void GameQuitView__ctor_m69A83A224C18C81BC7BA88E5C7D7036EC79D4622 (void);
// 0x000001FF System.Collections.IEnumerator GamePlay.HalloweenHomeView::ShareAndroidText()
extern void HalloweenHomeView_ShareAndroidText_mA26D80EDCEE60FA4BB6174D054FE7C68FB8FCCB5 (void);
// 0x00000200 System.Void GamePlay.HalloweenHomeView::.ctor()
extern void HalloweenHomeView__ctor_m36B264FA55D6456903E8E972E7B4DBAB55E118FC (void);
// 0x00000201 System.Void GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::.ctor(System.Int32)
extern void U3CShareAndroidTextU3Ed__0__ctor_m48577E7AEE8D5EF769B60039753B00923B4F4D0D (void);
// 0x00000202 System.Void GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::System.IDisposable.Dispose()
extern void U3CShareAndroidTextU3Ed__0_System_IDisposable_Dispose_m83E9BB33539619ABF1F7D0FD653F0A3C82873C79 (void);
// 0x00000203 System.Boolean GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::MoveNext()
extern void U3CShareAndroidTextU3Ed__0_MoveNext_m0EF1D7E8A6EB0A5B5C74BD64A5AAF58300AB341E (void);
// 0x00000204 System.Object GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShareAndroidTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7293609AAF42E7BEEA3D708D3870D5539196F74 (void);
// 0x00000205 System.Void GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::System.Collections.IEnumerator.Reset()
extern void U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_Reset_mCBEF07A86217C7241C3922903833A00169C5A61B (void);
// 0x00000206 System.Object GamePlay.HalloweenHomeView/<ShareAndroidText>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_get_Current_mE6506CB9E172A57BE3F6F7F673D200C2164B9D6E (void);
// 0x00000207 System.Void GamePlay.LeaderBoardView::OnEnable()
extern void LeaderBoardView_OnEnable_m877F771EC4C46A168EE51EAB8166463478E6EC2C (void);
// 0x00000208 System.Void GamePlay.LeaderBoardView::Update()
extern void LeaderBoardView_Update_mBD09903FAC671EAD74192007908626593167A26B (void);
// 0x00000209 System.Void GamePlay.LeaderBoardView::SetInternetMessage(System.String)
extern void LeaderBoardView_SetInternetMessage_mA00C8C0EB2D851CF67559024997C18434AD40133 (void);
// 0x0000020A System.Void GamePlay.LeaderBoardView::OnCloseClick()
extern void LeaderBoardView_OnCloseClick_m441737A29AB1D2D5A5090714B43169A9BE884CAF (void);
// 0x0000020B System.Void GamePlay.LeaderBoardView::.ctor()
extern void LeaderBoardView__ctor_m7A2403BBB73449051B70A2AF6E345E75FEE2799E (void);
// 0x0000020C System.Void GamePlay.RateUsView::OnStartClick(System.Int32)
extern void RateUsView_OnStartClick_m403DE4FB5F9BF3AB9817774180478DA98C32218F (void);
// 0x0000020D System.Void GamePlay.RateUsView::OnCloseClick()
extern void RateUsView_OnCloseClick_m07D7E7C9EE71E03A7E8992DEF6C3F0FFB58E00E7 (void);
// 0x0000020E System.Void GamePlay.RateUsView::.ctor()
extern void RateUsView__ctor_m58DF86CF5A249ECF287A0DC44A18C9755642AF99 (void);
// 0x0000020F System.Void GamePlay.SettingView::OnCloseClick()
extern void SettingView_OnCloseClick_mBE77635ACB06A76BADEC043387D36977CFB6F37B (void);
// 0x00000210 System.Void GamePlay.SettingView::OnSoundClick()
extern void SettingView_OnSoundClick_m9197E11A07F725E41D0BAA8FDCAD03FE2AC13F90 (void);
// 0x00000211 System.Void GamePlay.SettingView::RateUsClick()
extern void SettingView_RateUsClick_mCC622A53DE1E5BC5E3AA1A4B022A8B744F3B84D0 (void);
// 0x00000212 System.Void GamePlay.SettingView::OnShareClick()
extern void SettingView_OnShareClick_m8CA230E2A152B39DC51D534205256DE38CD1BE73 (void);
// 0x00000213 System.Void GamePlay.SettingView::.ctor()
extern void SettingView__ctor_mBC5B00A8CAB85CA11939F3E44B29DDE0C909CBC9 (void);
// 0x00000214 System.Void GamePlay.ThemeSelectionView::OnEnable()
extern void ThemeSelectionView_OnEnable_mD9C9CA16AF762C3F004BD47BB8259E9F8BE8F547 (void);
// 0x00000215 System.Void GamePlay.ThemeSelectionView::ShowView()
extern void ThemeSelectionView_ShowView_mD9A177847D38C5D3356B86B374F3CBA25C50DE0F (void);
// 0x00000216 System.Void GamePlay.ThemeSelectionView::HideView()
extern void ThemeSelectionView_HideView_m2A8A6B6958886DD912D1609512F5719D2D1937DE (void);
// 0x00000217 System.Void GamePlay.ThemeSelectionView::OnHalloweenPlayClick()
extern void ThemeSelectionView_OnHalloweenPlayClick_mA04349556A4F654CB503C63C1B2314FF74EB320E (void);
// 0x00000218 System.Void GamePlay.ThemeSelectionView::OnChristmasPlayClick()
extern void ThemeSelectionView_OnChristmasPlayClick_m343AE6470BB693A8475D479EB8DC3D09BEB0CD37 (void);
// 0x00000219 System.Void GamePlay.ThemeSelectionView::OnEsterPlayClick()
extern void ThemeSelectionView_OnEsterPlayClick_m5DD8BD254912F2212DB08DDE757616398E2830D1 (void);
// 0x0000021A System.Void GamePlay.ThemeSelectionView::OnBackBUttonClick()
extern void ThemeSelectionView_OnBackBUttonClick_m5D4D6740C757DB63843D0033924901928A077ECC (void);
// 0x0000021B System.Void GamePlay.ThemeSelectionView::ShowHellowinBlockBg()
extern void ThemeSelectionView_ShowHellowinBlockBg_m20CD9F3B8DEFDEF60EBB7B503CCBCD79629B1442 (void);
// 0x0000021C System.Void GamePlay.ThemeSelectionView::ShowChismasBlockBg()
extern void ThemeSelectionView_ShowChismasBlockBg_mC48F94AD432D4143BFA03A7595FDB0253220E82C (void);
// 0x0000021D System.Void GamePlay.ThemeSelectionView::ShowEasterBlockBg()
extern void ThemeSelectionView_ShowEasterBlockBg_mC384910AF5A314C5B8FA8096E21A801360023605 (void);
// 0x0000021E System.Void GamePlay.ThemeSelectionView::.ctor()
extern void ThemeSelectionView__ctor_m731452F7E389978E2FDB705B860AE3888F007B4D (void);
// 0x0000021F System.Void CoreGame.AdManager::Awake()
extern void AdManager_Awake_m2B718DE5E5519D8964323C6F4D603E611E0DE92E (void);
// 0x00000220 System.Void CoreGame.AdManager::Init()
extern void AdManager_Init_mD33EFD608A1D2179E5B4F37540BCD5E3CAFB663E (void);
// 0x00000221 System.Void CoreGame.AdManager::SelfInitialize()
extern void AdManager_SelfInitialize_m29F4D9B489A2CFE90AF0E5C29BBA2C840AAC79CC (void);
// 0x00000222 System.Void CoreGame.AdManager::ShowRewardAd(CoreGame.RewardAdData)
extern void AdManager_ShowRewardAd_m79195807AE21A78462C1EC35D64949397A7C8027 (void);
// 0x00000223 System.Void CoreGame.AdManager::ShowInterstitialAd()
extern void AdManager_ShowInterstitialAd_mBCE81C636091F07EEE7A2F29BB4DAA591FC239C5 (void);
// 0x00000224 System.Void CoreGame.AdManager::LoadAd()
extern void AdManager_LoadAd_m7B82016EBAD04558EF333496B3E8DFBC15189E63 (void);
// 0x00000225 System.Void CoreGame.AdManager::SetGDPRConsentMethod()
extern void AdManager_SetGDPRConsentMethod_mCC187B47512D64FEA32DEAF7D1EBAC3B596F29E4 (void);
// 0x00000226 System.Void CoreGame.AdManager::.ctor()
extern void AdManager__ctor_mEBCE9C16722BC950561C32F32C6136C9976915BE (void);
// 0x00000227 System.Void CoreGame.AdManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m658C0B3E45C7CD6DD9C5A4EB4EFD56BD485D092F (void);
// 0x00000228 System.Void CoreGame.AdManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m8114653AC79D21B4E4D6E17D3E39260018C1EA05 (void);
// 0x00000229 System.Void CoreGame.AdManager/<>c::<Init>b__6_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CInitU3Eb__6_0_m5B6CC776968273AA042425FDB03CDB0ABFBCFDF2 (void);
// 0x0000022A System.Void CoreGame.InterstitialAdMono::LoadInterstitialAd()
extern void InterstitialAdMono_LoadInterstitialAd_mB279200B1071B2B47886BD310AE6B2867F89A4AB (void);
// 0x0000022B System.Void CoreGame.InterstitialAdMono::ShowAd()
extern void InterstitialAdMono_ShowAd_m2B0F881A6DDDB326C041657016B2CA5212EED735 (void);
// 0x0000022C System.Boolean CoreGame.InterstitialAdMono::IsAdLoaded()
extern void InterstitialAdMono_IsAdLoaded_m87DF62608E62F030DFBCE5CC1DC5116435037DF1 (void);
// 0x0000022D System.Void CoreGame.InterstitialAdMono::ShowTestAd()
extern void InterstitialAdMono_ShowTestAd_m191668B7782C3A4A77E308CC6BBA2D04739E2CC2 (void);
// 0x0000022E GoogleMobileAds.Api.AdRequest CoreGame.InterstitialAdMono::CreateAdRequest()
extern void InterstitialAdMono_CreateAdRequest_m65608BE1086785B3BBD37864EF9038DA3B10EF5E (void);
// 0x0000022F System.Void CoreGame.InterstitialAdMono::HandleInterstitialLoaded(System.Object,System.EventArgs)
extern void InterstitialAdMono_HandleInterstitialLoaded_mF23BB10D73C2B5AE5569C51BC09380A51DF76B46 (void);
// 0x00000230 System.Void CoreGame.InterstitialAdMono::HandleInterstitialFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void InterstitialAdMono_HandleInterstitialFailedToLoad_m664786B0943917BECF112F0BA5907CB84E8A0194 (void);
// 0x00000231 System.Void CoreGame.InterstitialAdMono::HandleInterstitialOpened(System.Object,System.EventArgs)
extern void InterstitialAdMono_HandleInterstitialOpened_m292F3AE4654411BFBD5F199DF215B831743D56B9 (void);
// 0x00000232 System.Void CoreGame.InterstitialAdMono::HandleInterstitialClosed(System.Object,System.EventArgs)
extern void InterstitialAdMono_HandleInterstitialClosed_mB4090D52D542611C3E86B44CBF042A2A23F038FE (void);
// 0x00000233 System.Void CoreGame.InterstitialAdMono::HandleInterstitialLeftApplication(System.Object,System.EventArgs)
extern void InterstitialAdMono_HandleInterstitialLeftApplication_m5D70D01B845EC8CCC4729F4698B84E7091F63558 (void);
// 0x00000234 System.Void CoreGame.InterstitialAdMono::HandleRewardPaidEvent(System.Object,GoogleMobileAds.Api.AdValueEventArgs)
extern void InterstitialAdMono_HandleRewardPaidEvent_mCF5386303855326AA90F07FC5A43468160EA11F0 (void);
// 0x00000235 System.Void CoreGame.InterstitialAdMono::.ctor()
extern void InterstitialAdMono__ctor_mABC905CD8C67271373554D177D42E0E49387888E (void);
// 0x00000236 System.Boolean CoreGame.RewardAd::get_IsInWaitingForLoadAd()
extern void RewardAd_get_IsInWaitingForLoadAd_m5114D97AE42B31734F05691958CB51593107846A (void);
// 0x00000237 System.Void CoreGame.RewardAd::set_IsInWaitingForLoadAd(System.Boolean)
extern void RewardAd_set_IsInWaitingForLoadAd_mF7FB0DCE6E53F38285C568767983FBE17A6DD40F (void);
// 0x00000238 System.Void CoreGame.RewardAd::LoadRewardAd()
extern void RewardAd_LoadRewardAd_m770829CDBA737E20255DA0ECBA7310551896B093 (void);
// 0x00000239 System.Void CoreGame.RewardAd::ShowAd(CoreGame.RewardAdData)
extern void RewardAd_ShowAd_m526EFD6909564894A92343C4545131A14F4196C5 (void);
// 0x0000023A System.Boolean CoreGame.RewardAd::IsAdLoaded()
extern void RewardAd_IsAdLoaded_mD618AF4FEEEBDF32AE79E39C586BCCAA45583C3D (void);
// 0x0000023B System.Void CoreGame.RewardAd::ShowTestAd()
extern void RewardAd_ShowTestAd_mA2F44534FD9BAAF0093E3D080135E587EC7DB164 (void);
// 0x0000023C GoogleMobileAds.Api.AdRequest CoreGame.RewardAd::CreateAdRequest()
extern void RewardAd_CreateAdRequest_m46DF0C9AB6CB270C203E71D3A4D430B7EB380C17 (void);
// 0x0000023D System.Collections.IEnumerator CoreGame.RewardAd::HandleRewardedAd()
extern void RewardAd_HandleRewardedAd_m9F7FD689AFDA9120DB3361E106EBFC55492B5DE7 (void);
// 0x0000023E System.Void CoreGame.RewardAd::HandleRewardedAdLoaded(System.Object,System.EventArgs)
extern void RewardAd_HandleRewardedAdLoaded_mFC7AE1C4ED37BAF49B3006B0B7C782B6AE877560 (void);
// 0x0000023F System.Void CoreGame.RewardAd::HandleRewardedAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void RewardAd_HandleRewardedAdFailedToLoad_mB18E95D0A006D4082CB2EC49FFC56A94D236965D (void);
// 0x00000240 System.Void CoreGame.RewardAd::HandleRewardedAdOpening(System.Object,System.EventArgs)
extern void RewardAd_HandleRewardedAdOpening_m813B0F9D49F93F0D68D24FA7FF69DDBB4B3D9686 (void);
// 0x00000241 System.Void CoreGame.RewardAd::HandleRewardedAdFailedToShow(System.Object,GoogleMobileAds.Api.AdErrorEventArgs)
extern void RewardAd_HandleRewardedAdFailedToShow_m9889D607D41BE3EB3D83E65C3D0A8043857A3D43 (void);
// 0x00000242 System.Void CoreGame.RewardAd::HandleRewardedAdClosed(System.Object,System.EventArgs)
extern void RewardAd_HandleRewardedAdClosed_m3A3ACFE254D8558EC61FECDA2BC473B2BF7D7B72 (void);
// 0x00000243 System.Void CoreGame.RewardAd::HandleUserEarnedReward(System.Object,GoogleMobileAds.Api.Reward)
extern void RewardAd_HandleUserEarnedReward_mDAE0CEC4E3CC8AABCE00E1546989409AEB016191 (void);
// 0x00000244 System.Void CoreGame.RewardAd::HandleRewardPaidEvent(System.Object,GoogleMobileAds.Api.AdValueEventArgs)
extern void RewardAd_HandleRewardPaidEvent_mD39A652C4AA78D7E07499655E54389635F5F1271 (void);
// 0x00000245 System.Void CoreGame.RewardAd::.ctor()
extern void RewardAd__ctor_m157850AA462D3676E53E0D8B56C461FDEC33A75C (void);
// 0x00000246 System.Void CoreGame.RewardAd/<HandleRewardedAd>d__13::.ctor(System.Int32)
extern void U3CHandleRewardedAdU3Ed__13__ctor_m02BE4FDF06BBE5BD0CE25900C3140BD75B5023F5 (void);
// 0x00000247 System.Void CoreGame.RewardAd/<HandleRewardedAd>d__13::System.IDisposable.Dispose()
extern void U3CHandleRewardedAdU3Ed__13_System_IDisposable_Dispose_mB5B980BA7922C6E36C6A04D346E8C48ED7039468 (void);
// 0x00000248 System.Boolean CoreGame.RewardAd/<HandleRewardedAd>d__13::MoveNext()
extern void U3CHandleRewardedAdU3Ed__13_MoveNext_mB6E03E7C0952C25DF9B47A57FB2ED09324F2FDFA (void);
// 0x00000249 System.Object CoreGame.RewardAd/<HandleRewardedAd>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHandleRewardedAdU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFF8EF072C53C6BB648D0A57E0CCE523D2CD075B (void);
// 0x0000024A System.Void CoreGame.RewardAd/<HandleRewardedAd>d__13::System.Collections.IEnumerator.Reset()
extern void U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_Reset_m617931E8D8470D9FBA9537D3D4EBD5880B76B845 (void);
// 0x0000024B System.Object CoreGame.RewardAd/<HandleRewardedAd>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_get_Current_m062DD8F084A5E3FAADCCC5E78DF6A77B0BEF2326 (void);
// 0x0000024C System.Void CoreGame.RewardAdData::.ctor()
extern void RewardAdData__ctor_m98DE60BFC79F5F046B078DB500C1252D0AB89B0E (void);
// 0x0000024D System.Void CoreGame.IAPManager::add_OnPurchaseSuccess(CoreGame.IAPManager/PurchaseSuccess)
extern void IAPManager_add_OnPurchaseSuccess_m5770136C682BCBD528FA184FA5CDB28E0271E657 (void);
// 0x0000024E System.Void CoreGame.IAPManager::remove_OnPurchaseSuccess(CoreGame.IAPManager/PurchaseSuccess)
extern void IAPManager_remove_OnPurchaseSuccess_m976C6D79491D93B215CFEEE7C510A252D3C7282B (void);
// 0x0000024F System.Void CoreGame.IAPManager::RaisePurchaseSuccess(CoreGame.IAPProductData)
extern void IAPManager_RaisePurchaseSuccess_m29BF0EE8571ED80B5B0F45A074CC8A5673FB028A (void);
// 0x00000250 System.Void CoreGame.IAPManager::add_OnPurchaseCancel(CoreGame.IAPManager/PurchaseCancel)
extern void IAPManager_add_OnPurchaseCancel_mF70A1C83B724A55EB6AA44148393A1E86AE26DFC (void);
// 0x00000251 System.Void CoreGame.IAPManager::remove_OnPurchaseCancel(CoreGame.IAPManager/PurchaseCancel)
extern void IAPManager_remove_OnPurchaseCancel_m80BA81676142BD8AAFBE953E060FF9DD24E05290 (void);
// 0x00000252 System.Void CoreGame.IAPManager::RaisePurchaseCancel(CoreGame.IAPProductData)
extern void IAPManager_RaisePurchaseCancel_mFA3209F5BE0BE16A617C8DF88A8443ACFCAC540D (void);
// 0x00000253 System.Void CoreGame.IAPManager::Start()
extern void IAPManager_Start_mA14B10A6337D93827364297EE1D26BE9B1677664 (void);
// 0x00000254 System.Void CoreGame.IAPManager::Awake()
extern void IAPManager_Awake_mAB98EA00323B1EA23BA2E40E9E3805895195CEC9 (void);
// 0x00000255 UnityEngine.Coroutine CoreGame.IAPManager::OnInitManager()
extern void IAPManager_OnInitManager_mDF151BC6D3AF48508EDA1AC07107C93C72E063D7 (void);
// 0x00000256 System.Collections.IEnumerator CoreGame.IAPManager::InitRoutine()
extern void IAPManager_InitRoutine_m491B305A643C997913E216D26127A2A3A9CE8D7F (void);
// 0x00000257 System.Void CoreGame.IAPManager::InitializePurchasing()
extern void IAPManager_InitializePurchasing_mDEEA97C211CE83F569242F862967A0C0CB366086 (void);
// 0x00000258 System.Boolean CoreGame.IAPManager::IsInitialized()
extern void IAPManager_IsInitialized_mC639CA5610747E0E194CCB1AEBAF3E2CCC05F355 (void);
// 0x00000259 CoreGame.IAPProductData CoreGame.IAPManager::GetProductData(CoreGame.IAPProductType)
extern void IAPManager_GetProductData_mFC0FEDE0BF7C0A46510CF0662818096F305F2F86 (void);
// 0x0000025A CoreGame.IAPProductData CoreGame.IAPManager::GetProductData(System.String)
extern void IAPManager_GetProductData_m1C72BF1683F856905BB18D68184183892ED3EC99 (void);
// 0x0000025B System.Void CoreGame.IAPManager::PurchaseProduct(System.String)
extern void IAPManager_PurchaseProduct_m3887B622A2D12CE93B4EB22CC146B8D84A78EA6F (void);
// 0x0000025C System.Void CoreGame.IAPManager::PurchaseProduct(CoreGame.IAPProductType)
extern void IAPManager_PurchaseProduct_mE3763FF67350281FEF3D21A4A4BF2C4E9E563182 (void);
// 0x0000025D System.String CoreGame.IAPManager::GetIAPPrice(System.String)
extern void IAPManager_GetIAPPrice_mA1D6E9E0274B5E82CF84CD23E8E95BB1AAE30BBF (void);
// 0x0000025E System.Double CoreGame.IAPManager::GetIAPPriceInDouble(CoreGame.IAPProductType)
extern void IAPManager_GetIAPPriceInDouble_mF937798A612A89C25B346969CA2C07690AB815B6 (void);
// 0x0000025F System.String CoreGame.IAPManager::GetIAPCurrency(CoreGame.IAPProductType)
extern void IAPManager_GetIAPCurrency_m24E97E858E8D563F44F38B9686A2D0B4C3B5CD3C (void);
// 0x00000260 System.Boolean CoreGame.IAPManager::IsProductAlreadyBought(System.String)
extern void IAPManager_IsProductAlreadyBought_m103A2108D6EA02DE681A468BF97306CF300C59EA (void);
// 0x00000261 System.Boolean CoreGame.IAPManager::IsAnyAdProductPurchased()
extern void IAPManager_IsAnyAdProductPurchased_mBD4DC24AFD9DF0D662EE6A42224F3D09608D10AE (void);
// 0x00000262 System.Void CoreGame.IAPManager::BuyProductID(System.String)
extern void IAPManager_BuyProductID_mA3A17B7F6A17681D896BFF43EEA3E8DD047B03DA (void);
// 0x00000263 System.Void CoreGame.IAPManager::RestorePurchases()
extern void IAPManager_RestorePurchases_mC9BB5A04BE460A6A8AC896B287F97BC5403648E7 (void);
// 0x00000264 System.Void CoreGame.IAPManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void IAPManager_OnInitialized_m2C401760A130804612287440BDE93F37F9D435AA (void);
// 0x00000265 System.Void CoreGame.IAPManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void IAPManager_OnInitializeFailed_mE7AD16A44DF66198D05E523BD67CC9E95A36B748 (void);
// 0x00000266 UnityEngine.Purchasing.PurchaseProcessingResult CoreGame.IAPManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPManager_ProcessPurchase_m19E3073671FE0F380C70059AE2FFAB84C4A6A64E (void);
// 0x00000267 System.Void CoreGame.IAPManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPManager_OnPurchaseFailed_mB9B60DBF2AD30B745FB8A1868EEC3C8956DFDBC4 (void);
// 0x00000268 System.Collections.IEnumerator CoreGame.IAPManager::WaitForInternetCheckBoughtProducts()
extern void IAPManager_WaitForInternetCheckBoughtProducts_mE7920CF75BB949A0A8793B65579E6CD29E1A1E43 (void);
// 0x00000269 System.String CoreGame.IAPManager::GetProductPurchasedReceipt(CoreGame.IAPProductType)
extern void IAPManager_GetProductPurchasedReceipt_mAF58A3E47C15139EAC0697848851767E043608ED (void);
// 0x0000026A System.Void CoreGame.IAPManager::.ctor()
extern void IAPManager__ctor_m0E9B3C5F41938E3F71B051C0ADE2260397581720 (void);
// 0x0000026B System.Void CoreGame.IAPManager/PurchaseSuccess::.ctor(System.Object,System.IntPtr)
extern void PurchaseSuccess__ctor_m4907490BD2E6C025A5E27D109B3EB0CFEE2A62B3 (void);
// 0x0000026C System.Void CoreGame.IAPManager/PurchaseSuccess::Invoke(CoreGame.IAPProductData)
extern void PurchaseSuccess_Invoke_m731222809B25FD0F29A20CC1A60ABDAB4F8A5430 (void);
// 0x0000026D System.IAsyncResult CoreGame.IAPManager/PurchaseSuccess::BeginInvoke(CoreGame.IAPProductData,System.AsyncCallback,System.Object)
extern void PurchaseSuccess_BeginInvoke_m6E6B2B2781A35C15610E8966704256CBB3DCA494 (void);
// 0x0000026E System.Void CoreGame.IAPManager/PurchaseSuccess::EndInvoke(System.IAsyncResult)
extern void PurchaseSuccess_EndInvoke_mE776416AC59EF86A7CECEBBA19091569FF5D4BE8 (void);
// 0x0000026F System.Void CoreGame.IAPManager/PurchaseCancel::.ctor(System.Object,System.IntPtr)
extern void PurchaseCancel__ctor_mA6965819D2F928BD25A0BB3CF63066875521C937 (void);
// 0x00000270 System.Void CoreGame.IAPManager/PurchaseCancel::Invoke(CoreGame.IAPProductData)
extern void PurchaseCancel_Invoke_m36FC0DB22883E9BE893A9B4B0C4AD3335DEA2EB8 (void);
// 0x00000271 System.IAsyncResult CoreGame.IAPManager/PurchaseCancel::BeginInvoke(CoreGame.IAPProductData,System.AsyncCallback,System.Object)
extern void PurchaseCancel_BeginInvoke_mCC3F20564A920F299538D7B1CDC5279774FA773D (void);
// 0x00000272 System.Void CoreGame.IAPManager/PurchaseCancel::EndInvoke(System.IAsyncResult)
extern void PurchaseCancel_EndInvoke_m9D17D0BB39E735F40E1BC4C34B05716C2FFC287A (void);
// 0x00000273 System.Void CoreGame.IAPManager/<Awake>d__19::MoveNext()
extern void U3CAwakeU3Ed__19_MoveNext_mF7D8CA8A5F9BDB4CBDD1BC61D0878686BB7ED1F9 (void);
// 0x00000274 System.Void CoreGame.IAPManager/<Awake>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B (void);
// 0x00000275 System.Void CoreGame.IAPManager/<InitRoutine>d__21::.ctor(System.Int32)
extern void U3CInitRoutineU3Ed__21__ctor_mAEFF8084A9F8B86448E061EF04B768F1437D4AC9 (void);
// 0x00000276 System.Void CoreGame.IAPManager/<InitRoutine>d__21::System.IDisposable.Dispose()
extern void U3CInitRoutineU3Ed__21_System_IDisposable_Dispose_m711DF51248BAA1DED0B7C429C1FE4F3C0FD4952C (void);
// 0x00000277 System.Boolean CoreGame.IAPManager/<InitRoutine>d__21::MoveNext()
extern void U3CInitRoutineU3Ed__21_MoveNext_m36B7A41A625A795BB5BC5AE5CD7B8D2F231745E3 (void);
// 0x00000278 System.Object CoreGame.IAPManager/<InitRoutine>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitRoutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C0EB3766FB22F1052F0A8B652861373D6836697 (void);
// 0x00000279 System.Void CoreGame.IAPManager/<InitRoutine>d__21::System.Collections.IEnumerator.Reset()
extern void U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_Reset_m44D85717BECADB3DF040C3E30DB95EBBF5156493 (void);
// 0x0000027A System.Object CoreGame.IAPManager/<InitRoutine>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA6E8778BFB06FF1C798CB6A051649066FA1B369B (void);
// 0x0000027B System.Void CoreGame.IAPManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m879EC86BD41F61D1C20E9C5A037231BA40F0C798 (void);
// 0x0000027C System.Void CoreGame.IAPManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mB7FDDADD6DC9C42439F8CEEC714C35B1990539E5 (void);
// 0x0000027D System.Void CoreGame.IAPManager/<>c::<RestorePurchases>b__34_0(System.Boolean)
extern void U3CU3Ec_U3CRestorePurchasesU3Eb__34_0_mC79C8511C171909B53D13412A0F3746C93A9B06A (void);
// 0x0000027E System.Void CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::.ctor(System.Int32)
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39__ctor_m71B9CEDF53BDC71546A62B488B91351CC449F4D9 (void);
// 0x0000027F System.Void CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::System.IDisposable.Dispose()
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_IDisposable_Dispose_m57524CDD0E864C5A6581FCF33725B5A134D353D2 (void);
// 0x00000280 System.Boolean CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::MoveNext()
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39_MoveNext_mA9A7115F2B24B35774D9E5E8FE3C720743E3E353 (void);
// 0x00000281 System.Object CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m526879C01B18A1C91F35EF33F1D10246AA88BF62 (void);
// 0x00000282 System.Void CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::System.Collections.IEnumerator.Reset()
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_Reset_mA9D2ED022B84D573757B25E8D10C3BA19FC84C76 (void);
// 0x00000283 System.Object CoreGame.IAPManager/<WaitForInternetCheckBoughtProducts>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_get_Current_m703564928F8B8E4DC10D7B3F53259ABCFDF1F602 (void);
// 0x00000284 System.Boolean CoreGame.IAPProductData::Equals(CoreGame.IAPProductData)
extern void IAPProductData_Equals_mF101CCFB0D427113E1F8E64AB818BEB9C9CCF9AB (void);
// 0x00000285 System.Void CoreGame.IAPProductData::.ctor()
extern void IAPProductData__ctor_mF5A2D6EDE27CED6D9D5ACEEC6C49E3884EF98CF4 (void);
// 0x00000286 System.Void CoreGame.ShopManager::Awake()
extern void ShopManager_Awake_mEA499061859D292A39793E6A7842D10B9EFF96F7 (void);
// 0x00000287 System.Void CoreGame.ShopManager::OnEnable()
extern void ShopManager_OnEnable_m8F21B586F3032A9394521B5701649C4680218346 (void);
// 0x00000288 System.Void CoreGame.ShopManager::OnDisable()
extern void ShopManager_OnDisable_m8ABFB920D209BD83F3B88F726E083F9CB76FE769 (void);
// 0x00000289 System.Void CoreGame.ShopManager::OnIapButtonClick(CoreGame.IAPProductType)
extern void ShopManager_OnIapButtonClick_mEA9DA53CE24132E26B500070220D3330F02E7B4A (void);
// 0x0000028A System.Void CoreGame.ShopManager::PurchaseProductIfInternetConnected(System.Boolean)
extern void ShopManager_PurchaseProductIfInternetConnected_m5272A230EABEE435E50B2A52AC37A9A7CDA5432C (void);
// 0x0000028B System.Void CoreGame.ShopManager::AddIapToData(CoreGame.IAPProductType)
extern void ShopManager_AddIapToData_mBD9350E1CDDA88444402B6F842ED28986FF62C95 (void);
// 0x0000028C System.Void CoreGame.ShopManager::Event_OnPurchaseSuccess(CoreGame.IAPProductData)
extern void ShopManager_Event_OnPurchaseSuccess_mD88CFCB60662959A0F82DBF9B54D1271DB57E471 (void);
// 0x0000028D System.Void CoreGame.ShopManager::Event_OnPurchaseCancle(CoreGame.IAPProductData)
extern void ShopManager_Event_OnPurchaseCancle_mC03789D2595558F8FC460064984442569D5B01FC (void);
// 0x0000028E System.Void CoreGame.ShopManager::.ctor()
extern void ShopManager__ctor_m57071D1D97330875E2A06ABA66E03B1A8FE4DCA9 (void);
static Il2CppMethodPointer s_methodPointers[654] = 
{
	AdManager_Start_m07F7C8DA24A1908D9E6AC811C2574154A31F68C4,
	AdManager_Update_m4267B5FCE42EEA0752333421FC184C58CB4BD345,
	AdManager__ctor_mDEF91F5263438BACE36D66E7434579BEDEC9A3FC,
	BaseGamePlay_OnEnable_mAA0998F3144F6980C6193F660CBDD8A297B4CB84,
	BaseGamePlay_OnDisable_m4EE3FC52274B7CFAF90F4047084CFCC5AE6861F0,
	BaseGamePlay_Init_m46BC30ADD8AF0451776052BE9E5C8994BBADDC8E,
	BaseGamePlay_UpdateScore_mB561621F15C01A419E64409C9BACDAEA05ADFA8A,
	BaseGamePlay_ResetScore_mD7CBD6578D25E684A3503148B175F3E2085E770A,
	BaseGamePlay_SaveScore_m18D189283B6E0444406554577531B24C84A67B72,
	BaseGamePlay_DisplayScore_m9E4D47B502DA1B84DC76F8B468F6940CBD23EA5B,
	BaseGamePlay_SetSoundImage_m2A22718636BDA668AF76F0324616D67898D63955,
	BaseGamePlay_ScoreAddAnimationCo_m3C24F4BF626C5B7AC26219B79F212308C9B5FE81,
	BaseGamePlay_OnSettingClick_m51B0F072492A082470CD06A06F68460CA2536EAB,
	BaseGamePlay_OnDummyButtonClick_mC62A0BE57CC97FB7FA7BBA9AD61FD5723FEDADE0,
	BaseGamePlay_SettingPannelSetActive_m0BFC6B7C772CFE426E248E7103D7F94EC3523203,
	BaseGamePlay_OnToggleSoundClick_mAE358699BA8170B19ADAF87A598E3A5E28FC086A,
	BaseGamePlay_HomeClick_mBAA4DC2CF2343383105936EA3C728218810A46E1,
	BaseGamePlay_OnRestartClick_mDAD8C6D79A7A9B66457A5D2F19EB2093AEEEA3B5,
	BaseGamePlay_OnBackBttonClick_m1CA4B4FFA3FA2DE878FC7B72F59C585BA9552806,
	BaseGamePlay__ctor_m1BB77C9BF6D46A2F86BF015E8A1232390850F410,
	U3CScoreAddAnimationCoU3Ed__18__ctor_m8EC82519287ECAAFBA39C5CEE63FD15688248F04,
	U3CScoreAddAnimationCoU3Ed__18_System_IDisposable_Dispose_m666951E2C45AA724446E55B254071A4D1EED0B72,
	U3CScoreAddAnimationCoU3Ed__18_MoveNext_m1EFE665E5C93A44233C391FB6750C3F22DD36C02,
	U3CScoreAddAnimationCoU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF83A5DBD4F110E0F7FFCA7334F7C7A14443B7A45,
	U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_Reset_m5504B8BF34108E89426ED5A1242AFFC96C8BE280,
	U3CScoreAddAnimationCoU3Ed__18_System_Collections_IEnumerator_get_Current_m7686727706784EA45AC761E89C88713861A91708,
	U3CSettingPannelSetActiveU3Ed__21__ctor_m670E0B3DDFD7550DDFF55DA58A34E83E355128F6,
	U3CSettingPannelSetActiveU3Ed__21_System_IDisposable_Dispose_m34D2D4427AAC05311069E68B47F303E3A62815DF,
	U3CSettingPannelSetActiveU3Ed__21_MoveNext_m7AD0FFBD51C2A60CA7942EC8AB0E369C288136C6,
	U3CSettingPannelSetActiveU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B6FD2D99C4DEDED021B9555C09DE73BE432BFE9,
	U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_Reset_mFA1700E5C0151AC8623508B6489EFC0ADE514F52,
	U3CSettingPannelSetActiveU3Ed__21_System_Collections_IEnumerator_get_Current_m211E5C8CA6F25079022FB27F7454CCAB6C34977C,
	GoogleMobileAdsDemoScript_Start_m02AB0806DAA8316AA8909B3D78DE4ED44D98D6F7,
	GoogleMobileAdsDemoScript_RequestBanner_m704F024CB070432AD4CC6736A01E1938AF1A0A1F,
	GoogleMobileAdsDemoScript_HandleOnAdLoaded_mB5543F20721EA0A6B23B7F9174E6F38A1CFCD7D3,
	GoogleMobileAdsDemoScript_HandleOnAdFailedToLoad_m639EE664F1CAF4C4E590E37A3C4DC6F1977ED3F7,
	GoogleMobileAdsDemoScript_HandleOnAdOpened_mA1C660C9D16C689A64DFF81EE49184D6574BE089,
	GoogleMobileAdsDemoScript_HandleOnAdClosed_mAA5C0F9B1B17B6544BDC4A63231E443BD0688BD3,
	GoogleMobileAdsDemoScript__ctor_m48B3E925F49AB203248C2D0AF43ED4AAF39F6531,
	U3CU3Ec__cctor_mB2CE7DD41E0B041142CE81C310E345ECAE487CFB,
	U3CU3Ec__ctor_m3729A42D4168E754229DF8B5D779AA8DE43A1E76,
	U3CU3Ec_U3CStartU3Eb__1_0_m29D30F558CF35CCB5107553D8A049915ACB5F755,
	Admanager_Awake_m994F2EF9DD47E84170F824B432BA1694BC195123,
	Admanager_RequestInterstitial_mA96E258BF9631AFB4330117A82F830D4B7E8D82B,
	Admanager_LoadAd_m40CFD75061280A7D8DDAC35D807F00C9D8156BF0,
	Admanager_ShowAd_m80C7774EA7D5668E6DC37670687434CC2EDFC20A,
	Admanager_HandleOnAdLoaded_mADC93D3182B8492F399AB6E23E90F1CCF8E700F8,
	Admanager_HandleOnAdFailedToLoad_m8CE3B47C31412A98A179AB7945006863E870BF87,
	Admanager_HandleOnAdOpening_mC793EF0E2D583D3C297655B9D36A7E5602D8E670,
	Admanager_HandleOnAdClosed_m11E399723614B8D9833C52A6A11693E3E868B2C4,
	Admanager__ctor_mE86D433FC77298E184F06642D2497EC31FBB5154,
	TimerDataSO__ctor_mCFFF8170646C213FEA250930F75C2B997734CBF6,
	ScoreAnimation_PlayAnimation_mB6BB2399F37F9C5F1AC6ADD6DDEED49ECD0B8D5D,
	ScoreAnimation_SetData_mDE1AFFED2C7196B46D3609969E5EE1C80BA69559,
	ScoreAnimation_WaitAndStopAnimation_mE9E35EA4A41BE2E5A27363BEA368687D29635B7F,
	ScoreAnimation__ctor_m4A591596A8CB4641D00F884AFCC95DEA0A7365DA,
	U3CWaitAndStopAnimationU3Ed__5__ctor_m56D36877C16C8DBEAFECDD08F7BBF531EBD56CA7,
	U3CWaitAndStopAnimationU3Ed__5_System_IDisposable_Dispose_mFEDE48E928634E276BAD35BD2DBB38B27E8783C3,
	U3CWaitAndStopAnimationU3Ed__5_MoveNext_m4D0EE38FCDC6B4293FAF404AB5BD43BF7B30E846,
	U3CWaitAndStopAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE3B1F8C83B81B6EA4CB171EF8596703449793D,
	U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_Reset_mF3496B9B1A7A6533F3E659B5CA2DCC025B1F012D,
	U3CWaitAndStopAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m545B77BD53146A74099EEF30F7C835C85F83A85E,
	TimerHandler_GetMin_mE36ED07C66E755A81882B90F500576DB71C9367B,
	TimerHandler_GetSec_m41A8604C19812E14E4BCB8EC9971DCA5598B4F2A,
	TimerHandler__ctor_mEE66FD03CB1DDA4916EC02B4C8C8A07E4D8CEDB6,
	BaseHomeView_OnEnable_m1F6CF69B934B70401616C4470F8869F761F9BAB8,
	BaseHomeView_OnPlayClick_m2695522E45BED28349F89683352EF47225252878,
	BaseHomeView_OnTimerBasedPlayModeClick_m69A6C49B7C6E7DFDE80585805CDB8F518D7D3A97,
	BaseHomeView_OnBackButtonClick_mCF7B15156AF683E37960A74539D70440B456152B,
	BaseHomeView__ctor_mC281A81B58862CB7AF15BDA8D618E78A2C087E1A,
	U3CU3Ec__cctor_m3F1E0943958DE0281EA255D8478EDBD95B6CDD4F,
	U3CU3Ec__ctor_m7B21D08B9DDE2736FFF31CA2F3EC84E889FE86FE,
	U3CU3Ec_U3COnPlayClickU3Eb__2_0_m6ABEB7A16FE3ED9C291266E3B2F4E269AB3B3EA9,
	U3CU3Ec_U3COnTimerBasedPlayModeClickU3Eb__3_0_m9DC5349999B089134ECEFE2593CF79154A40EB95,
	ChristmasHomeView__ctor_m57B97446C2A1D1C173A871B9556428E7836A590D,
	EsterHomePlayView__ctor_mDBF81ECE34306A007349D8BBA630E87543296726,
	GameMainHomeView_OnEnable_mEDDC8E2139D6F63F80273CC5B853C0FEF7ACEC5E,
	GameMainHomeView_ShowView_mBC7D43F499548A5EFA2B2131DFAFBEBB4C1102C0,
	GameMainHomeView_HideView_m88F6D2C6D181D78447F7A70CF657F5471F891C8D,
	GameMainHomeView_OnPlayButtonClick_m60508682231FB07EAC3DCE482931DB6818AAB4A6,
	GameMainHomeView_SetSoundImage_m5234E02C8C53F1F4854FC80E630BB040F99B8D8A,
	GameMainHomeView_OnSoundButtonClick_m6582DDBD3784BAF21F092C7BBF287ED4A7530F61,
	GameMainHomeView__ctor_m443F786AF74746CF0403457BB0002A88A4F7A959,
	GridUIManager_SetCellSpriteById_m8EC6C6799174265301A2096A2690B2BEE673D1BE,
	GridUIManager_GetSpriteByID_mA135EC81A6C7E57F156B206BEF0E106B4B511C9A,
	GridUIManager_GetBGSpriteByID_m82D2A58AF2CE88065212F1B133608138340618B8,
	GridUIManager_GetBGPoliminoSpriteByID_m28DF8783C8655F623E4EED3E3DAC9AB9852A17EE,
	GridUIManager__ctor_m2BCD9966548ACA1B2661A3C2F212C5F15368BAF9,
	CellSpriteData__ctor_m13D15A36B669E8276D14B38B2E96D1486BDBA564,
	NULL,
	NULL,
	NULL,
	Images__ctor_m14A3FB738DACEED08F4A9D237663E10B92DFC6E8,
	Reporter_get_TotalMemUsage_m243BEA1AE109392B1031128E60269F20F4E26E82,
	Reporter_Awake_mE6F45EFB980C87A9E27CDCD430803F1028EB46C4,
	Reporter_OnDestroy_m7ED1EDEDE1B329113E330D967BE7F17018CB066E,
	Reporter_OnEnable_mAD6DBD51FE170B358222640F7ECE25D6337E95C8,
	Reporter_OnDisable_m9F636B3BB1C92B9FA0FD1BBD42DA6DAE8DEF846A,
	Reporter_addSample_m540E57AEF6028B6CCBDCCA2C1E68C9D719A78C13,
	Reporter_Initialize_m4E1F8BE7BF77E47634224D1550980FED99C92046,
	Reporter_initializeStyle_mC794732561B7004D88F0B48786322E94A6F72283,
	Reporter_Start_m3A74F6C9AA0BE2D06875F449D39E0BD4463EC11E,
	Reporter_clear_mDB88F6175A6FE89797697F75F3AC167FB6CA7B51,
	Reporter_calculateCurrentLog_m4D9B9A224D33F529E4B755DD14CB4F8FF563B517,
	Reporter_DrawInfo_m1CAE6609921984149B08D4A99E656C89BC5E2B23,
	Reporter_drawInfo_enableDisableToolBarButtons_m0DDB7E27F9A60F519DAD7D1F170D6093B5280A17,
	Reporter_DrawReport_m33ACD652C1A1E7517A4F665777E0EF2799007763,
	Reporter_drawToolBar_m9BFB40BFDF691AF3C12A1F6BDFD757C0E43912B2,
	Reporter_DrawLogs_m4CC63AF0D5D889B46D3033673FAFCCD2DFBEBC68,
	Reporter_drawGraph_m1BD6F1B2EDC9FF5E25DD1E40A3443581C5681255,
	Reporter_drawStack_m01CD93112D4A5D7BF9F797BA1BEFFEC3FD062129,
	Reporter_OnGUIDraw_m644E89CBF9C4CD6412A326028F47FAF64BBF5F2D,
	Reporter_isGestureDone_mC926F2861566102F24258988EE14224CB5F5DAC5,
	Reporter_isDoubleClickDone_m4E6E93701D7D56C9A6C49B4F1B07C03EBD0EA416,
	Reporter_getDownPos_m0AB81FEFEAB7187DCC834459069B5F1D470BDFB7,
	Reporter_getDrag_m42B62313D8BE37D00FB209823BF771D4587BE5A7,
	Reporter_calculateStartIndex_mBE0FB265258688F620643A2137A4B556DA25768D,
	Reporter_doShow_m08A729528D3F2E6A67CB2259A59669241533A295,
	Reporter_Update_m2A0AE08146B30EA0FEE90C1D46CC6D9445D1DCE2,
	Reporter_CaptureLog_mA5060A0B6B0FC335DA1A99D5AFE8CD5D98C673D0,
	Reporter_AddLog_m88361FCA66586C88414BA239A6E1F4427E30E98E,
	Reporter_CaptureLogThread_mC9EA896A90AD6457E27D6C7DF888384B6F5930E4,
	Reporter__OnLevelWasLoaded_m50AD021E279F55B6916BF79D32B89D1A3BDEAB10,
	Reporter_OnApplicationQuit_m08A565D8A9AB73B4C4BE358C713527431B5D5C05,
	Reporter_readInfo_m96DE3344D84AEC06C476B18794B3A3C82E58E737,
	Reporter_SaveLogsToDevice_mD7A2C496C00461F06EC2D61401106C1186B6128A,
	Reporter__ctor_mA4BCF10B880CCC842438711687A14B8CFF02CE05,
	Reporter__cctor_mFF6F7DC4AC2280BA9A6732870B83F6229DD40A4C,
	Sample_MemSize_m4D369E2F8B0B0132737E082D352A4C7638E6D4A2,
	Sample_GetSceneName_mF375AB4530FB46DA810DDB0FF874F4E59506D476,
	Sample__ctor_m02F7676D47E82DAEE97735C65576B49B506D2453,
	Log_CreateCopy_mFCC38494B5E8A6C1E44BC61FAB7F46077A63711A,
	Log_GetMemoryUsage_mED699CB4309A05ED9108938A7534568A51EBD415,
	Log__ctor_m7BF33C0496F355EDC3D884812786A0D3DFA9522D,
	U3CreadInfoU3Ed__188__ctor_m1074CD57ECB5282678BE9366813C0FD134F3C04A,
	U3CreadInfoU3Ed__188_System_IDisposable_Dispose_m7FA47F089E3E44C215AA02F56A71F03F179AE492,
	U3CreadInfoU3Ed__188_MoveNext_mC9E975802B890843F8384228D002418014CAA613,
	U3CreadInfoU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6964659507DB03DC202BE8B05A79F8E228D1807,
	U3CreadInfoU3Ed__188_System_Collections_IEnumerator_Reset_m7950A1C07E9941FC6F83BD1E1DF4B14FA26A7C2E,
	U3CreadInfoU3Ed__188_System_Collections_IEnumerator_get_Current_m5A3DFB2378273D241AE9C3EF98DF3FAC754AA2EA,
	ReporterGUI_Awake_m75AE591F9E4CB78A427C89E9278BE4D56CCF7CD6,
	ReporterGUI_OnGUI_mFC33C895684D1EC6CDA24AAC340C69294EC08516,
	ReporterGUI__ctor_mC6456038C4EAB8D290C4253256F59AB0A550BD86,
	ReporterMessageReceiver_Start_mD918A8392396CE96564A12434294C1544997624E,
	ReporterMessageReceiver_OnPreStart_mADCE71C11DE96AB870DCB30AEC273F58EA46B482,
	ReporterMessageReceiver_OnHideReporter_m3DCF2E6157DCF519F9277DC72034259BA235441E,
	ReporterMessageReceiver_OnShowReporter_m74CEFEFF9B4EE67915D983C769DBC95E79B09DCB,
	ReporterMessageReceiver_OnLog_mA0DD9085AA99D585E89AE26ECBBC79F0ED602BBA,
	ReporterMessageReceiver__ctor_m02BC4C5416EF8CB9013E5BD52B7391C3964C6A7B,
	Rotate_Start_m260F3DA22D8BEE3571FB61596065ECC238B60968,
	Rotate_Update_m66D07F3686A017A63E869D8294C08E68F4A2FBAB,
	Rotate__ctor_mAB2884DA9234D7A6485C5662D97205C92CA9B9C4,
	TestReporter_Start_m6397556902788BB52CAF60730ECE79D25CED86F0,
	TestReporter_OnDestroy_m4A73BE669965BDB08596934A124368035D4BCDE7,
	TestReporter_threadLogTest_m631A52116F32D171C27CF3B75EF33B423E8B5304,
	TestReporter_Update_m6BA9D5AA04F97B05FE21726E148C234F246B35A9,
	TestReporter_OnGUI_m1F0C8C7C08B940C51976BE835235F6F2BF8E9AA5,
	TestReporter__ctor_mF94083FCF4F1102F7780790D06809C17D1EE8F20,
	raw_Start_mF6389E4D52FC7AC3A89EEE61938955A9180FED9A,
	raw_Update_m7FA2C1BB7B5A85871E73229AD2578C2186D5CB5C,
	raw__ctor_mBFDA0F7002BEC6D84DC3F7090B049F50EE285D73,
	SafeArea_Awake_m84A2CD2A994D90CF9EAF58C2F89022D096C4B06D,
	SafeArea_ApplySafeArea_mF59008F67597542E8D1EBCFD6CC943E1B8390139,
	SafeArea__ctor_mA3BB2BD560BE0F759582FD7151D2B7226B649894,
	Constants__ctor_mA7C60AE52C906371D3E1FDB1AE9D9982E6B0B1D2,
	Constants__cctor_m29EDCBEA52F8B30E77505804367D8A2D62FFD69D,
	GridAndHolderData_Awake_m93BE19B0B47DF6EF055E65580EEF9981B2EA047D,
	GridAndHolderData_OnApplicationQuit_m7ADBAD28CB75D06985EFDAB274815E7E7297B43C,
	GridAndHolderData_GetGameData_m4A86CD5CAED5908E1AB1C580ADD3FDF1401F4D42,
	GridAndHolderData_SetGameData_m760417E17C4E91D2D93253627C4992A97D0A222E,
	GridAndHolderData_To1DArray_m43E4023CF9CC672D2A7CB2CF70AFBF5746227570,
	GridAndHolderData_To2DArray_mAEB251D1376187F93D29B5C2E0699AB5C144B64E,
	GridAndHolderData__ctor_m9A502808C28FAA3772B9789149154BAC97307DB4,
	GridAndHolderData__cctor_mDEBB237E2F7DF4A7098589553C8A9257CACC199E,
	BaseBlock__ctor_mA4EACC9138C84297CC28670A74B90DEAD769DA23,
	BasePolyomino_Start_m83AE1E16962FE32362F81F8ADE5FBCE855B1EB32,
	BasePolyomino_Init_mE57212EAE30309E2B16A63FA024BB43EC160D5CB,
	BasePolyomino_CheckCollideRule_m260BFD5E798AA3323B3296667797FF8D7DC70DFB,
	BasePolyomino_GetCollideCell_mD76DCF275249E35B61DA2D776B18B17755595BC0,
	BasePolyomino_SetMainCollider_m25C8D7E6F7F32D1E0E3E2727C184A1FAA95BD8C0,
	BasePolyomino_OnDrag_mCCC43FE43F27FD82E06736CF46B6C514F25AC364,
	BasePolyomino_OnDrop_m257CF10AD7AA5BA928F163F061F7850BB694A875,
	BasePolyomino_CheckPlaceAvailable_mDCAF127A34454E9AB8F257632B3AFC8853CEB693,
	BasePolyomino_StartAnimation_m44343BFEBBEA6D1938AFBFC1180637B7BF836825,
	BasePolyomino_WaitAndDestroy_mFCC3CB6882B308ADD5881118453487E8C10EB995,
	BasePolyomino_LearpObject_m1E73D023FAFB6FBE91413439AE665FD2797A72F0,
	BasePolyomino_WaitAndDestroyCO_m21B75FEF605879F8BA9E42FE2CE1C72F8742C02F,
	BasePolyomino__ctor_mF4F8C66AB854AACEB49B2A35EA60D716DF380CCD,
	U3CLearpObjectU3Ed__14__ctor_m9C780924431DA2807CB8A33945EC0E5FBA9E740B,
	U3CLearpObjectU3Ed__14_System_IDisposable_Dispose_m4657DFB93555CB9FB7B11CA58BD68707F96B9D7B,
	U3CLearpObjectU3Ed__14_MoveNext_m20EF7DCA4525F4EDB0EBA4BB9A6F256BEBBA7D23,
	U3CLearpObjectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA34BF89B49E1F65604B719E972471D5670115E0,
	U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_Reset_m6C440EB8EC4B4D64DA392E5471EFE768DA7BB43D,
	U3CLearpObjectU3Ed__14_System_Collections_IEnumerator_get_Current_m698F9838A2D8A51C7F991185F04BED54EB77AC11,
	U3CWaitAndDestroyCOU3Ed__15__ctor_m0D93DF3C044D855E7EB240C71A66DB98B8D67862,
	U3CWaitAndDestroyCOU3Ed__15_System_IDisposable_Dispose_m9E461FEC944FCCB2BA19375FD03BAD8463CD97B1,
	U3CWaitAndDestroyCOU3Ed__15_MoveNext_mDD2D92437DD09E10939FC2F47AAD5DB8EE2C9E48,
	U3CWaitAndDestroyCOU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B6C2664DA2E0F1C690B6C6DD52864E10B36058C,
	U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_Reset_m048D08D06E80BB24A37CCE345F2524C94B1F9E02,
	U3CWaitAndDestroyCOU3Ed__15_System_Collections_IEnumerator_get_Current_m14BF06A405A57EF85B5FDE966414B3C2751A031C,
	Block__ctor_mB9C9E69045B02084BF7EE26376D7286CA41A0E2D,
	Cell_IsEmpty_mE366ED5E389F064391562CBECD8F88018BA69C15,
	Cell_SetTemporarySprite_m5FED258799F05FD2FC7EB16F9E180735B197073C,
	Cell_ResetTemporarySprite_m60459030068DFA8F0C408A6BC057D3B1178B4CFF,
	Cell_DestroyBlock_m12C756759137F23EAFAC988970F64B6D2C377C9B,
	Cell_DestroyBlock_mCCE3F8B22A4301D8454ECB3D11CAB53823A38A68,
	Cell_ResetData_m601D0B3B136441357A74C591A28361B0FCC0E469,
	Cell_SetBlock_mBE8EEBE1008E8CD1D27469A1046CA3347FF60072,
	Cell_ChangeCellEffectLayer_m518CC7E29F310B5686F0A662BF0443FA92DCC900,
	Cell_ActivateCellEffect_m5BE44C59FAD24E845887AEB0115B14AD4BE1346D,
	Cell_ActivateCellBorderEffect_mA736A0D8EAA1862511A0F6AF73BA3B9366E709B1,
	Cell_DeactivateCellEffect_m8224A03E58EBB0C7D38878ED940F43B67D37D91B,
	Cell_PlayHintFX_m226A201D38515B365BBC049147720218CD8B04DF,
	Cell_StopHintFX_m85B1584418746D71E2782DB317C6A79D6799B4F5,
	Cell_ResetBlockSprite_m0C064DE9B509D78079F7343C07902CD41BCF4A3C,
	Cell_PlayDestroyAnimation_mE2D318A1371D9DC120EC83729B3FC5B65DD15124,
	Cell__ctor_mD02F71AF09C714ABB8742DC17386661820EBF8FC,
	U3CU3Ec__cctor_mC839ECB2C685BA8C1A05D4C85C5BFBD88972E435,
	U3CU3Ec__ctor_m7F502F331C7AD922955FFCA91C52D306A2607E21,
	U3CU3Ec_U3CPlayHintFXU3Eb__18_0_m51F4DFD11AF53AA6AA6C7DB7F90C8670706271A1,
	U3CU3Ec_U3CStopHintFXU3Eb__19_0_m41064ADD56C13834DDF4F4B372B271F99594F5D3,
	U3CU3Ec_U3CPlayDestroyAnimationU3Eb__21_0_m5758C266BD8696F60D8731FC083346FB8F1B7282,
	Grid_Start_mA3991118B50CBC4D7C9F7CE9EA370651A2BACCF2,
	Grid_SetPolyominoInCell_m512A0AE52AE71E80A5EFDFF86226A5AEEB4C9490,
	Grid_PlayHintPS_m582A35A34859812158F2C38997D61107EBD87B29,
	Grid_StopHintPS_mB110CB2F277AB4BB0D62697C4BEECDC5F5C330C7,
	Grid_IsItRowFull_m122B0D33B58A621E3CC1EE4FC943436E9427FECE,
	Grid_IsItColFull_mD9280C188A7EB3C14E23499C422DFFC99C6929E4,
	Grid_DestroyBlocks_mDD2F6ACCE806E05D8C617893BF30D782780350C7,
	Grid_SetCell_m127F5FAA1AA57202B28641B8185D0B7CC4ECC5EE,
	Grid_SetBlockFromData_m876D07B297681650E56003CF1D5842312DC730B9,
	Grid_OnGameOver_m34A224853641E572DCD63E88F2C029ADE7061623,
	Grid_StartActiveCellEffect_m2C379E11C1B84B39D0916FB95A77751141F9149D,
	Grid_StopActiveCellEffect_mEE141186B8DD3D7ABA588953F7D374ECE1BDBD8B,
	Grid_SetCellEffectsByRow_m468843A66F6CF4585CF255F86009C7768A3F6E7E,
	Grid_SetCellEffectsByCol_mAEB92DFE8C55F02A0DA2861FA78E688F895AC0E9,
	Grid_DestroyRandomBlocks_mBE3A6B55803E5864B85D627EBE71B9117660091D,
	Grid_ResetAllCells_mC60B39E5C71B8B35F33278469A2303375E9712E1,
	Grid_IsGridEmpty_m40CB429865784A486CACA9D1EBA1E9B9AEB5DB50,
	Grid_PerformActionAfterDestroyBlock_m4966B547173335539E511A3EF5543BF2EE862171,
	Grid_WaitAndSetSprite_m316CE6E8DC6664F84EED514C63EE471CF8318EA6,
	Grid_DestroyBlockCO_m1D911C9BE4279B4450C6BC18D2D76E4807594B95,
	Grid__ctor_m89459B22582B1D9D3B6AE44D7CED61E9246D14AD,
	Grid_U3CDestroyBlocksU3Eb__8_0_m5253B5681B09948DFC858E3B6D56BF568D3A1C2C,
	Grid_U3CSetCellU3Eb__9_0_m89A6DF9BC810C8151061BC242CC103CF3FF1AAE3,
	U3CU3Ec__cctor_m773EEFEE87D00A6705010AC86FC85D6FF26E12A3,
	U3CU3Ec__ctor_m86C68B7A88AFB413C069325CB942C841ECDE6598,
	U3CU3Ec_U3CStartActiveCellEffectU3Eb__12_0_m8BE9EB32DD6D72898461564C0F185ECCA817ED94,
	U3CU3Ec_U3CStopActiveCellEffectU3Eb__13_0_mAC37DA0E14F4B97B690DD5A19AC8633E03155C03,
	U3CU3Ec_U3CResetAllCellsU3Eb__17_0_m9157EA4A09490782C84C8953D37280DAA7FE2D17,
	U3CU3Ec_U3CWaitAndSetSpriteU3Eb__20_0_m4C97B309EA2BB676A3A222FA1597C52B19ABADED,
	U3CWaitAndSetSpriteU3Ed__20__ctor_m8B991F753CEBCCE65339C24F6B4C3F4BE799FB1B,
	U3CWaitAndSetSpriteU3Ed__20_System_IDisposable_Dispose_m94B304D9922828FE66F1CBA3B1322BEE2CC514DC,
	U3CWaitAndSetSpriteU3Ed__20_MoveNext_m58D3A5230A48802FF60AA4B2F6357BD1673E19CE,
	U3CWaitAndSetSpriteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8E8B6C83FDCA0C021EF13E15DB317AF200BACC,
	U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_Reset_m2F3B06310784C6ADECB39F0CCBDD9C940B5F25CE,
	U3CWaitAndSetSpriteU3Ed__20_System_Collections_IEnumerator_get_Current_m47C74139557B5ED8AD836B7B376879EC7F6C6422,
	U3CDestroyBlockCOU3Ed__21__ctor_m2657C6103D4DA7EECA4CB3DABBAB7A9958226226,
	U3CDestroyBlockCOU3Ed__21_System_IDisposable_Dispose_m9404FFD07E53A22FD55D8E0CBDB23054491CC476,
	U3CDestroyBlockCOU3Ed__21_MoveNext_mA1F00460B38FDA08BBAD418098FA14CB25208B5D,
	U3CDestroyBlockCOU3Ed__21_U3CU3Em__Finally1_m25514FBDA451A6610CAF65F7F009B8A5E37E9D5F,
	U3CDestroyBlockCOU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2B4BF82C64017FBF76034F6FDEEB7F8654E0E70,
	U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_Reset_m3E4B139B7532EB2465B72E6396E6CDB180F27BD2,
	U3CDestroyBlockCOU3Ed__21_System_Collections_IEnumerator_get_Current_m591535103E9317CB93B8745E6660AC4315822E36,
	PolyominoBlock__ctor_m8BB6A04826749E3612082B370D119F493DFACD33,
	PolyominoSlot_SetPolyomino_m0AE8754429FCD162276CF4A7ADC6B29AD15F626B,
	PolyominoSlot_SetBoosterPolyomino_m570F6C305D9E5AE9583844579B611D9A44661A9B,
	PolyominoSlot_GetPolyomino_m5EE104871F9D78F6E33BE40BBB29A4F1BEDA9986,
	PolyominoSlot_IsEmpty_mB749B0B1A41213C8BBE3BB1A6CAEFC7D39C58BBF,
	PolyominoSlot_ResetPolyomino_mB335BD31E82053863FBEB0E07D1A1E641FC558F3,
	PolyominoSlot_CheckPlaceAvailable_m9F47BEF097D4C8D2E2EB33DF7FA1D43576879267,
	PolyominoSlot_SaveLastPolyomino_mA0E027BC26DE871F64F6093E7A03424AF204BA04,
	PolyominoSlot_SetLastPolyomino_mA12B9448A9DA59B14A4A94DA0022E2365ACEE302,
	PolyominoSlot_ResetLastPolyomino_m178621A70461C88B27A2E2FC2783A1CA56ACF1A9,
	PolyominoSlot__ctor_m769AC6B2BF7308D954B5A7F662A4F850A49FB026,
	SimplePolyomino_Start_mEE89DF27AAC634C5855B8EB7774978804FC3B771,
	SimplePolyomino_Update_mC15E0A8DA1BDAE56896CBD7C048986891ABC6DF8,
	SimplePolyomino_Init_mB7572FD3FE530406E6A4DDE5FA7445BC930A1BE8,
	SimplePolyomino_OnDrag_m6EBA1DA973983A95A1C8B1721C70360A1293BAD0,
	SimplePolyomino_OnDrop_mF63256C456381AE1E4E447DE50DA0B6FA5F93F83,
	SimplePolyomino_SetSprite_m909B20754920040AEFDA560014BA5112175B991C,
	SimplePolyomino_SetUnplacedSprite_mCB0F4B80735744A45FE48C67C31E1AAC0199116B,
	SimplePolyomino_SetBlockSize_mE6CAFA53356A097E2A5947F952AC3DECEA1A62BB,
	SimplePolyomino_CheckPlaceAvailable_m0BBE996FB47A4B042A22E7CEC453E154A727672E,
	SimplePolyomino_CheckCollideRule_m2606B4CC6B738DF5E745C1350506203B218A6206,
	SimplePolyomino_SetIfPlaceable_mBFC486DD9DAF377BD2DE942ED4B38C9C06450025,
	SimplePolyomino_WaitAndHint_mFE2AB7C6FE5E037F2EEB0C1633897F45492F3068,
	SimplePolyomino_SetUp_mD425ECAC66C97FEFFD381FF1EDA3C45E5158482F,
	SimplePolyomino_PolyominoBlockPosSetUp_m80E2F0978B150BF93858BD4B2772547F4AA29D7D,
	SimplePolyomino_SetRowWiseData_mB460A72A02E08102063B5DC4024E1B28A8153E22,
	SimplePolyomino_SetColWiseData_m34EBA914661EAB43146C9E5E23EEE79CD52C3AED,
	SimplePolyomino_DoesContainInRowList_mF8BB3A02DCC47DBBAB85114ACEB7108B5C9615D0,
	SimplePolyomino_DoesContainInColList_m36DD883F2122B9614E063964CE7CAC9F554D41C4,
	SimplePolyomino__ctor_m8EE45BE252053A8D77AE7942583C099AA247F22C,
	SimplePolyomino_U3COnDropU3Eb__13_0_m393E9055C749DADD1826522B462FEB9D6BA4BF55,
	SimplePolyomino_U3COnDropU3Eb__13_2_mFE1FED896F0D6218C9A3064E5E4DD2AD647CED5C,
	SimplePolyomino_U3COnDropU3Eb__13_1_m34A618CE2D1E31F800CAEDEF62DA355D0D13E0F0,
	U3CU3Ec__DisplayClass16_0__ctor_m4E5C1C19C93E4E39BB6EEC732D2CF19C9DE3FB6B,
	U3CU3Ec__DisplayClass16_0_U3CSetBlockSizeU3Eb__0_m99CB239220F82555B0AAFC20B6AEF8DDFADFE1DD,
	U3CWaitAndHintU3Ed__20__ctor_m8AE8440CB025905B9A795A658BCA376BE72D3EF3,
	U3CWaitAndHintU3Ed__20_System_IDisposable_Dispose_mFE3F40C5A75650729FDF2FA8D5122E448F216CB1,
	U3CWaitAndHintU3Ed__20_MoveNext_mB3166E12D0D9478F354AF073D8365D579A4C2C69,
	U3CWaitAndHintU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0680968ACA99E565FAA2AE7DF6B0332309C98C,
	U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_Reset_m7CB3DAD8F63D190EA692853C0D3CAA843040569A,
	U3CWaitAndHintU3Ed__20_System_Collections_IEnumerator_get_Current_mA9C88B63A4ED03143C2B09622F5930DA10263ADF,
	U3CU3Ec__DisplayClass23_0__ctor_mE0BA7E3D37F32ABFB841FB5D6D5E233F06DFA9F0,
	U3CU3Ec__DisplayClass23_0_U3CSetRowWiseDataU3Eb__0_mCCCFB5AEF16181111654D1A10BC41FD641C826EB,
	U3CU3Ec__DisplayClass24_0__ctor_m110F7B1A83D4478B6C372AF65FF9BCEF87AF3408,
	U3CU3Ec__DisplayClass24_0_U3CSetColWiseDataU3Eb__0_mE31312FFEE7B990AB0E2B03C0474DEEA370A3462,
	U3CU3Ec__DisplayClass25_0__ctor_mA004A2BBE648440255D52481CA9D7D2AC33B03E3,
	U3CU3Ec__DisplayClass25_0_U3CDoesContainInRowListU3Eb__0_mE5AD66A05511269CD0E540D59A9719A04C4E51D8,
	U3CU3Ec__DisplayClass26_0__ctor_m96C5F12609C33401F205C2C44AD6B77E6251FADA,
	U3CU3Ec__DisplayClass26_0_U3CDoesContainInColListU3Eb__0_m150D09728A95A16A73A01FD7EFE033CE957745B4,
	PolyominoDistributeData__ctor_m9315638EE64182F5552AE9C60DB14608E8EB0B0C,
	TimerBasedGamePlay_OnDisable_m04A3C90D5D4B267CF7A28BB3BAC212B1BB96E867,
	TimerBasedGamePlay_Init_m92BA66D3BD2DDB544896D96C2AD7848BEFD35E27,
	TimerBasedGamePlay_ResetScore_mDEE3AEE3884DC1176EDAB8BBA3683B2A7DFA5011,
	TimerBasedGamePlay_TimerCo_mB4235BDB64294813F17CA6C8468A6E7573350AC9,
	TimerBasedGamePlay_OnTimerOver_m524D8B7084B7FE0899460F90AF78D75C963ED020,
	TimerBasedGamePlay_StartCorutine_m1DD45C81CDA2F7B89BAB44E91A5B4335656C7FA2,
	TimerBasedGamePlay__ctor_m6FB10216A889737A51636F397C29DEA9D8C7ABF8,
	U3CTimerCoU3Ed__5__ctor_m7C0CAF6DFDC89E73368768DCE0A7D0891C8909C4,
	U3CTimerCoU3Ed__5_System_IDisposable_Dispose_m56059E0A8A9CFB18236BDCB78DF67BCF116D57D6,
	U3CTimerCoU3Ed__5_MoveNext_mE4159120292B018DE23ED789D10D9794CD553847,
	U3CTimerCoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DA6D2BE5096A517728D1CE497E334E0555196F,
	U3CTimerCoU3Ed__5_System_Collections_IEnumerator_Reset_mB3FA62DE93F2E10646F763FD243FF892CCB3CAF2,
	U3CTimerCoU3Ed__5_System_Collections_IEnumerator_get_Current_m8DD692E1E16E3D5191F940125B6CE7543963737B,
	CameraManager_Awake_m4606FCB47B2BBC94EEB4649B6B75EFB7B30A5042,
	CameraManager_Start_mC82EA0E4AED180C1A5C20BD6FDC5C002016EC623,
	CameraManager_OnInitWorldWithForCamera_m370F666BEB9594286A5E1D2B974868A930213490,
	CameraManager_SetWorld_m9571911B49FC1775117A49639AA5557722CCC70B,
	CameraManager__ctor_m2F200E3CFD2E61FCC9B00D088EF5CC306F7AB28A,
	DataAnalyster_Awake_m888B3EB3F63235BF5528B3532625546B2F735F81,
	DataAnalyster_SetInt_mAF9EE5598E14AA3357E868E8AD63FC854602D558,
	DataAnalyster_SetFloat_m81E37053713D7992077D3A37E1264685BF0C3D84,
	DataAnalyster_SetString_m1EE02161D5EC28B452C572E25D6C35BE58C73073,
	DataAnalyster_GetInt_mAA9784F6E3BAEFCBB0E11BBDDBC99A1A7E1AC53B,
	DataAnalyster_GetFloat_mF7950EA4BF383BB1E92ECE3D8C89E7D4FA811811,
	DataAnalyster_GetString_m4264B3E4EEF118272074E1BCCE0F94E8EFE67F6D,
	DataAnalyster_SetDataOnGameComplite_m6A312A4DDB5F2D7BC098D1EA6BB3477D196C32ED,
	DataAnalyster_IncrementSpinCount_m43A9A01630329FBDE4EA055CB408747F950DED79,
	DataAnalyster_IncrementContinueGameCount_m8B9620BF7D287616EB3991B6F204C6A038CB6B83,
	DataAnalyster_IncrementOneBlockBoosterUseCount_mDD1538085A460510B4CF09D6CED24BFCDA65039B,
	DataAnalyster_IncrementRainbowBlockBoosterUseCount_mEEF22954BEDF1C21A056EA7C7644B64A6DF12CC4,
	DataAnalyster_IncrementBigBombBoosterBoosterUseCount_m67A9B614D8E6E21614579E41EC8214AC829E2E8B,
	DataAnalyster_SaveGameData_mB6BA37B13CCCA209D78D872CA627EE18BADE723C,
	DataAnalyster_GetGameData_m5B94B0378A669BF9EF5E658763F8E5B27055D972,
	DataAnalyster_LoadData_mC42ECD650AE5C169C142C6EE6C288BFD51CE7E9A,
	DataAnalyster__ctor_mD929F8E2DAAC54F05BD838F264941AA5FFD289A4,
	BreakingBlockGameData__ctor_m0A277E4379FCC6ED622583DF7A4925A8A76F3C90,
	DataManager_get_LastTimeToSpin_mC21B3B7A3A698EA62000399CBFAD23E4CEA4E19F,
	DataManager_set_LastTimeToSpin_m6C986DDB8773E5BA876EB5B48EE9522C35AE9C0F,
	DataManager_get_IsFreeSpinAvailable_mAF5E17FFE54B52E8346B1B2B508CDD0F8FBEBB44,
	DataManager_get_IsContinueGame_mC174103E96A256F747902A8D150116E4DEE59D05,
	DataManager_Awake_mB3EE29372BD42A7217F8D72C3760CDC14A18D072,
	DataManager_ResetPolyominoGeneratorCount_m711FEB8D3C3818E8C3F4609AC4BDD612F73729AA,
	DataManager_SetFreeSpinAvailable_mD687EFF50CB27B880B8D8C329BF9CB1470ADF0D2,
	DataManager_ResetFreeSpinAvailable_m86F40FD9ED3E71DF177DB5981F4DC9288706DD0B,
	DataManager_IsItStaringPolyominoPhase_m11A6683C61FE3F5C2781A920C5761B349B70CA09,
	DataManager__ctor_mCD6709C0ABCFF08FC7A5B9B8D4AFDE5655AC51CF,
	GameManager_Awake_m2502394043A2161755FB1E69C0C8C4E5E52704B1,
	GameManager_Start_m74C3FE7266249F33355E5A2D00B0EC6279ACCBA3,
	GameManager_Init_mD0719BBAA0C4A3BC1921659EEB098C08AEB08A50,
	GameManager_RestartGame_mB360FB8E9B1B707B528354B1A8D62B9C808DF29E,
	GameManager_OnResumeGame_mFC4CAA3A72DEE8F194F881C1E1F9C6D43F437803,
	GameManager_SetColorDataByGamePlay_mFEF0B6A60B2868F353D750480CF44B413A146D52,
	GameManager__ctor_mECAA9584696B570432522A3381E37FE3D8A49428,
	InputManager_Awake_mACAA154DFD76ADD1F44064EAD321FCA1D3978175,
	InputManager_Start_m4312832FC12025D4987598E5151C8D9F6DED6506,
	InputManager_Update_m60614A6440667AF28BA0ADDF130D05CD61AF93C8,
	InputManager_PlayerInput_m8F195913E18E2AB6CB25DCBA2F5A0E94FB65A148,
	InputManager_CheckHitObject_mA7133BEAABC47EFD58E808D0F6E058CA11109B01,
	InputManager_DragObject_m2BAD71E6AB54C7B760B0CA1A81EC81238F1F4DBD,
	InputManager_DropObject_m3CCA923BB95C3CC7F8449D91F00CBE0E486EBB46,
	InputManager_IsValidMove_mC15A02E6C1EC783DA836D5F9715ECFDFE8EC8B6C,
	InputManager__ctor_mB1098C74D4BF458C11074E0907C7EA1D80752B44,
	InputManager_U3CCheckHitObjectU3Eb__11_0_m3839724C17AF4AEB0F23BB4D8C2069F702567F01,
	CustomTransform__ctor_m9A840035FC11C6B99E070DC5D32B81435D6D07ED,
	CustomTransform__ctor_m29965ABCCB0D8FE806CEFC8AEC2D3865330BD69E,
	NULL,
	NULL,
	NULL,
	NULL,
	PolyominoGenerator_Awake_m64A26E188CFABD62A96E45E6D403F2B1C33F673C,
	PolyominoGenerator_GetPolyomino_m89D6D17A9963495889B6C2383BCF1693C4E6A3B4,
	PolyominoGenerator_GetStartingPolyomino_mAF432D0F1D89C6DAAB69931632B5A30FD7AEC064,
	PolyominoGenerator__ctor_mB0B4EE424A4C033C2E3D9433F57D104353E13FB4,
	PolyominoHolder_Awake_mD67D36D0E0DD0C844D1B099C869CC3C29F0AAE22,
	PolyominoHolder_SetPolyominoInSlot_m11C9CC202C296D40654066DA3A74308508158E57,
	PolyominoHolder_CheckForPolyominoGenerate_m821CBD0C90EF092F4C34B28900E308E88CC9A50D,
	PolyominoHolder_SaveLastPolyomino_m3BBC7C511DF40D50CE91C4285328FF852D5E8DC7,
	PolyominoHolder_SetLastPolyominoInSlot_mF117D4394450A5387CE3020C2D2461F319481CF5,
	PolyominoHolder_ResetLastPolyomino_m60644530A1A1916FA2DC49E4CDB42A2BF3FC2C07,
	PolyominoHolder_ClearAllPolyomino_mA5D1DF64A47472C8B496A7CB0EC962FF46A83276,
	PolyominoHolder_CheckForUnplacedPolyomino_mE5F507354B57646167A278EA37BA4B80EEAF4FD0,
	PolyominoHolder_IsAllSlotEmpty_mB95F0E924E00CF5D4E4BEFF965541264B5FCFD8D,
	PolyominoHolder_CheckForGameOver_mF7D8CEA427FD5F864D343BD59E68146FA44F8570,
	PolyominoHolder__ctor_m74B6A341371567C633B4727DAB73A29B487A5ECF,
	U3CU3Ec__cctor_mA9C365BE581656A37ADCCE60831A1A37BE4F261A,
	U3CU3Ec__ctor_m497D8B64003FAD56492369F6E414C198D422681D,
	U3CU3Ec_U3CSaveLastPolyominoU3Eb__6_0_m048C2921210FB5D1CB83A41D720BA58CA01BDBB0,
	U3CU3Ec_U3CSetLastPolyominoInSlotU3Eb__7_0_m366B6897365851216B936DA4C7D55BD0C1BCA40A,
	U3CU3Ec_U3CResetLastPolyominoU3Eb__8_0_m818EE64C3FC7B149E863D63553BA287C850D0211,
	U3CU3Ec_U3CCheckForUnplacedPolyominoU3Eb__10_0_m317C43BA19911743A59D35E9C0AA178732594E52,
	RuleManager_Awake_mCA977616EC20FAAAD27666949D060A282867E485,
	RuleManager_CheckDropRule_m9B85DA55383F53A0E98F357EE5AD57820CBD21D3,
	RuleManager_Drop_m816D594D6D1C0A00A0FE141F9B48A3C5516D6DD3,
	RuleManager_DestroyCells_m6F32B4441E59BB187DACAF352C100DA06BB26234,
	RuleManager_CheckDestroyRule_m6D87280B1963A38771142F306B14B41BF54D0A39,
	RuleManager_CheckEntryRule_m60BCD9D2E6676C018FC0A73063C9F8A921C704C7,
	RuleManager_SetTemporarySprite_m1420559025017015B3C6D940B20660DF7C11034E,
	RuleManager_SetTemporaryRowSprite_m9A53EDDF0387648A76481131A452F3872293B3B4,
	RuleManager_SetTemporaryColSprite_m3A0FD743E067ED9EC7F86C2C09F6CDB7F5C830A7,
	RuleManager_IsAnyEmptyCell_m94461FD78DD382A87C957E48C3FA852968F59E99,
	RuleManager_CheckExitRule_mB9928300DDFE6B662373E22E62471035FC052C13,
	RuleManager_ResetTemporarySprite_mBD8C3BB7E5C0DFA8142E503F47AB3B466643FEF9,
	RuleManager_IsPlaceAvailable_mDCD03CBA7A91E303BBDD895BE97637DAA19A8F90,
	RuleManager_IsPlaceAvailableForSimplePolyomino_mA6AEB13F086AF5862C81E436E2337B52C6A2A79F,
	RuleManager_IsItValidIndexFroRowAndCol_mAFABD4CAC919E0E4822E9680D49568D9010A82A9,
	RuleManager_CheckEntryRuleForBomb_mBC536885A47EAF111A9A6C062B2C1DC509F9910E,
	RuleManager_CheckExitRuleForBomb_m7C7A96400025516C9B8D79E4EF10BC1B31A94387,
	RuleManager__ctor_m3C95CFE017DFCA490077924A43B859ADAFED9988,
	U3CU3Ec__cctor_m8D66AFBE9290CA6BA2A3A12B27843E7EDCC38B74,
	U3CU3Ec__ctor_m46BE214EAFE6672F468A826763F84E34F6906159,
	U3CU3Ec_U3CResetTemporarySpriteU3Eb__15_0_mA952794DE872EDAAA06A2C9B4217F7D310398E48,
	U3CU3Ec_U3CCheckExitRuleForBombU3Eb__20_0_mA2730DF77505F8041A185CB69B6433CBE0AC5FD1,
	DestroyCellData_Add_m1D7CD59CCFE94E7A67AE3D99AA516DE16742769F,
	DestroyCellData__ctor_mA90649A6B725CC2F3131625BD9BC87A19F4A1522,
	ScoreAnimationHandler_Awake_mE6479F22FF60A3FC25426D3C8FE3C6743A291E18,
	ScoreAnimationHandler_SetCellTransform_m459923D0B3F07D0CE935D974F52A1076FF5BEE5C,
	ScoreAnimationHandler_ActivateEffects_mE10B7BCB8722E53D7E14FEF6E118A83CC6F282CF,
	ScoreAnimationHandler__ctor_mC17DA5A898049ABBF55672A28D9C36B94A80A264,
	SoundManager_get_Sound_m5C850EB289DA582F6CBAEBED0EAE782DC144C9E8,
	SoundManager_set_Sound_mF6A449C3C379306EB39FA56647B42F882CCAF3F5,
	SoundManager_Awake_m2D7E9CC0BA595286C0E58EDB243623E166DF1B13,
	SoundManager_ToggleSound_mE4D0327D31B796A8343116C11125003D4FB09C21,
	SoundManager_SetSound_mB6ECEA55E33681820FDCDD04B31F8575B40B6D4A,
	SoundManager_PlayBackgroundMusic_m67D53D47466B65B8364E270CF7CAF7E0B63CD5F4,
	SoundManager_PlayBackgroundMusicHalloween_mEA7C884B5E29BB8297BACEEA0641BC949892DDC9,
	SoundManager_PlayBackgroundMusicChristmas_mC928462F86C76A4D8EAA10344C6544170A148EA4,
	SoundManager_PlayBackgroundMusicEster_mB0CB2E591F792D0EFEF5B1B832D5ED7118EA1737,
	SoundManager_PlayButtonClickSFX_mAD6709D65C990FBDF2AE32079C5AD27C12DCDCCA,
	SoundManager_PlayGameStart_mA12E712D9C9835AF8DCD9993EB8A5C2C87118755,
	SoundManager_PlayGameWinSFX_m6DBBB43DD6FC59C2D74435B7D35B9178D43E65BA,
	SoundManager_PlayGameLooseSFX_m0E1A6073A6793E09098F3B872FB1FF1B21FDE10E,
	SoundManager_PlayNotEnoughBoosterSFX_mCD95ECE3FF1D4F636028FF2C982C12AD1A6BF453,
	SoundManager_PlayDestorySFX_mC13407C5B9ACEFC4599694C13923E7B38E5956DC,
	SoundManager_PlayDropPolyomino_mDF62494743050C0D5EB57F92C622561C38059598,
	SoundManager_PlayBombSound_m6AB1A0B07E118987869B73B3779D3BE55E4DE6BB,
	SoundManager_PlayRainbowSound_m056520FD0CF4C3D60E14A3077149D174F23B7B74,
	SoundManager_PlayCoolSound_m60BF1333427BDACB866620E46D46DE12DB6FBF06,
	SoundManager_PlayGoodSound_m23D0B64D08296908B820A2E3C1E4A3CA1023003E,
	SoundManager_PlayAmazingSound_m86F388CF2D65FDA2B62A93FFC8A1B555EBFD8C72,
	SoundManager_PlaySpinWheelSound_mC57A15A4FBC9BAA32D086C3E1D8F3298685F54A8,
	SoundManager_StopSpinWheelSound_m7953A9B149E7F49F7313D78B0C64D7EAA105EB51,
	SoundManager_PlaySpinRewardSound_mED9C4DB1C10D803643A58D5545BA760AEDEA5AE7,
	SoundManager_PlaySpinRewardClime_m75B3FE8B2C7F9F264B1B4DF446A4076301FE4C2F,
	SoundManager_StopBGSound_m76ACF3F820A22A5839F4E66147E40A12448C7730,
	SoundManager_StopHaloweenBGSound_m83912052CF71C681EDA328E956378FEC11F8393B,
	SoundManager_StopChristmasBGSound_m2A64B42695056F5539D7D288993889FFA2BC3D9D,
	SoundManager_StopEsterBGSound_m7A1CE696A6BE429717CDA624A8F09337A9EA403A,
	SoundManager__ctor_mAC6A45846C0AD7DF0E1355A56369785800C09D70,
	TimeHandler_SetLastCollectTime_m625E8800E45BDD163407420EC76C3D21748D4023,
	TimeHandler_GetTimeDifference_mF02E163B8B69CF9565A20F6D0354916D698C1C76,
	TimeHandler_GetRemainingTime_mAB62640052D6C97818019CF1218E37B30D14E12A,
	TimeHandler_GetRemainingSeconds_m8C4D1ECD9655BC2276782AF513A5C5FB097CF834,
	TimeHandler_SerializeDate_mD0F47D0679B32A2127C27E1C216ECF44337372DE,
	TimeHandler_DeserializeDate_m7E25D2230AB97AE008D712BA8C54E46DDCD30568,
	TimeHandler__ctor_mDD08C91ED0C13E5B11B1631B246ACCBE552BDC7F,
	UiManager_Awake_mBFDAFF1B2685678ABCEBAA8BAFC23AFE40A79EF7,
	UiManager_GetGamePlayViewByType_m9BCD3A7781C7CAC90214E75675E5D30E06B03E3A,
	UiManager_GetTimerBasedGamePlayViewByType_m01B1720EC67525E1A8840538E0EC18666730BD4B,
	UiManager_GetHomeViewByType_mAD2BBF1DCCD31718561080CDFB52DF00C926A058,
	UiManager_GetGameOverViewByType_m9E860157C12424DA68D605CF11A19A10582F02FF,
	UiManager_isAnyTimerGameplyOn_m1C454C4DD3EA9A3C0A5764B74CE76B117044791B,
	UiManager_ButtonSound_m271CE43BAE6DA6ECC68261721C847809922DD3FE,
	UiManager__ctor_mD4D5F4E710CFB2EA6BDCA3F24A932AB5038AB010,
	ColorData_GetSprite_m86E13C52B3E5971C81704A0EF15B53D19A960790,
	ColorData_GetRandomColorType_mC07C182BDF9F15046A835D24CADE8C867068EFB0,
	ColorData__ctor_m4EC8DEAAC82977EB88E33D457E715954AC34FDB0,
	U3CU3Ec__DisplayClass2_0__ctor_mB413EF553BE89145593BE749848EFAA395AE1D4B,
	U3CU3Ec__DisplayClass2_0_U3CGetSpriteU3Eb__0_mC30673F2046726F3E3B81A564517CE673E3E3101,
	SquareSpriteData__ctor_mA77D59335A6D0FB3BBC47D3268D36B4244477389,
	MaterialDataSO_GetMaterial_m2CCFFFC45987E5C632607CD732B9D3AD59233AAA,
	MaterialDataSO__ctor_m53F6BA5FDB59BEDA4BEC9179D81BC6249CF3C06D,
	U3CU3Ec__DisplayClass1_0__ctor_m02250A0E8534B5A56A9F8BA9B95A34914EDB4294,
	U3CU3Ec__DisplayClass1_0_U3CGetMaterialU3Eb__0_m53EE49B1A63F8B02E2F85C5DF295D03ABC774B9C,
	MaterialData__ctor_m2372F1FDB87F89A363FDE080703676D04FE9AF26,
	PolyominoData_GetRandomPolyomino_m4CE4A84537C3874C2ED3DA94DD7334586AC49CA3,
	PolyominoData_GetStartingPolyomino_m0AC72DA7FC9F2794635C9EFCCD34C8902E9F155D,
	PolyominoData__ctor_m3E6918447F988A27B04BBBE7E862337FD30B14AF,
	FeedBackView_OnSubmitClick_mF769E8548BB42D7B9302DD39360D0512BC3C1E99,
	FeedBackView_OnCloseClick_m41469B3A8AD0348D6BD3204DB5217F8C12AAE2F7,
	FeedBackView__ctor_m48864C291C7FC3D7F5D7DE70A9DADE0B848D1C9C,
	GameLossView_SetContinueGameReward_m68D41ECB01C67A852E1C077CB21C88BF118A3878,
	GameLossView_OnCloseClick_m00EAFBF103CD87455DB3819F737BEE454CE1F6D0,
	GameLossView__ctor_mD7A836660BE79586198DAC12046427B93BCA34E4,
	GameOverView_OnEnable_mBC3B9C808E36D336A23E503AF7EF9FABDA3D7ADD,
	GameOverView_OnDisable_mE3026F334B807E42C7F3537B3791EE75350E9396,
	GameOverView_DisplayScores_mE0B43CF138CFD37FE22B535C20BE3EEA97D80109,
	GameOverView_SetHeading_mC3E491E0ED5AF7951EA98D1D4AB90A8B4ACAE043,
	GameOverView_OnHomeClick_mB45447F3801E8CCC880D747C0399FEA5BF966D30,
	GameOverView_OnRestart_m697E49CA82A865D4118C46A69E1ABC8A0E7FEE8C,
	GameOverView_OnQuitClick_m2A88F24D19879BE059ACC7D40079018ECFA4DB60,
	GameOverView__ctor_m8DB03784603F47152E4341DC136EFD8A252A6661,
	GamePlayView__ctor_m715E2DB7F6454551ABDD5DC62BC7523B90AC5826,
	GameQuitView_OnCloseClick_m1E0A8E4CEB7DBA695D5913214A2650DF2F95056B,
	GameQuitView_OnResumeClick_m2881F88023C92A91C211CCC032301883B6AE46B6,
	GameQuitView_OnQuitClick_m9622FCB3B76DD0021BEBB63955B2BBFA2787CEA2,
	GameQuitView__ctor_m69A83A224C18C81BC7BA88E5C7D7036EC79D4622,
	HalloweenHomeView_ShareAndroidText_mA26D80EDCEE60FA4BB6174D054FE7C68FB8FCCB5,
	HalloweenHomeView__ctor_m36B264FA55D6456903E8E972E7B4DBAB55E118FC,
	U3CShareAndroidTextU3Ed__0__ctor_m48577E7AEE8D5EF769B60039753B00923B4F4D0D,
	U3CShareAndroidTextU3Ed__0_System_IDisposable_Dispose_m83E9BB33539619ABF1F7D0FD653F0A3C82873C79,
	U3CShareAndroidTextU3Ed__0_MoveNext_m0EF1D7E8A6EB0A5B5C74BD64A5AAF58300AB341E,
	U3CShareAndroidTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7293609AAF42E7BEEA3D708D3870D5539196F74,
	U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_Reset_mCBEF07A86217C7241C3922903833A00169C5A61B,
	U3CShareAndroidTextU3Ed__0_System_Collections_IEnumerator_get_Current_mE6506CB9E172A57BE3F6F7F673D200C2164B9D6E,
	LeaderBoardView_OnEnable_m877F771EC4C46A168EE51EAB8166463478E6EC2C,
	LeaderBoardView_Update_mBD09903FAC671EAD74192007908626593167A26B,
	LeaderBoardView_SetInternetMessage_mA00C8C0EB2D851CF67559024997C18434AD40133,
	LeaderBoardView_OnCloseClick_m441737A29AB1D2D5A5090714B43169A9BE884CAF,
	LeaderBoardView__ctor_m7A2403BBB73449051B70A2AF6E345E75FEE2799E,
	RateUsView_OnStartClick_m403DE4FB5F9BF3AB9817774180478DA98C32218F,
	RateUsView_OnCloseClick_m07D7E7C9EE71E03A7E8992DEF6C3F0FFB58E00E7,
	RateUsView__ctor_m58DF86CF5A249ECF287A0DC44A18C9755642AF99,
	SettingView_OnCloseClick_mBE77635ACB06A76BADEC043387D36977CFB6F37B,
	SettingView_OnSoundClick_m9197E11A07F725E41D0BAA8FDCAD03FE2AC13F90,
	SettingView_RateUsClick_mCC622A53DE1E5BC5E3AA1A4B022A8B744F3B84D0,
	SettingView_OnShareClick_m8CA230E2A152B39DC51D534205256DE38CD1BE73,
	SettingView__ctor_mBC5B00A8CAB85CA11939F3E44B29DDE0C909CBC9,
	ThemeSelectionView_OnEnable_mD9C9CA16AF762C3F004BD47BB8259E9F8BE8F547,
	ThemeSelectionView_ShowView_mD9A177847D38C5D3356B86B374F3CBA25C50DE0F,
	ThemeSelectionView_HideView_m2A8A6B6958886DD912D1609512F5719D2D1937DE,
	ThemeSelectionView_OnHalloweenPlayClick_mA04349556A4F654CB503C63C1B2314FF74EB320E,
	ThemeSelectionView_OnChristmasPlayClick_m343AE6470BB693A8475D479EB8DC3D09BEB0CD37,
	ThemeSelectionView_OnEsterPlayClick_m5DD8BD254912F2212DB08DDE757616398E2830D1,
	ThemeSelectionView_OnBackBUttonClick_m5D4D6740C757DB63843D0033924901928A077ECC,
	ThemeSelectionView_ShowHellowinBlockBg_m20CD9F3B8DEFDEF60EBB7B503CCBCD79629B1442,
	ThemeSelectionView_ShowChismasBlockBg_mC48F94AD432D4143BFA03A7595FDB0253220E82C,
	ThemeSelectionView_ShowEasterBlockBg_mC384910AF5A314C5B8FA8096E21A801360023605,
	ThemeSelectionView__ctor_m731452F7E389978E2FDB705B860AE3888F007B4D,
	AdManager_Awake_m2B718DE5E5519D8964323C6F4D603E611E0DE92E,
	AdManager_Init_mD33EFD608A1D2179E5B4F37540BCD5E3CAFB663E,
	AdManager_SelfInitialize_m29F4D9B489A2CFE90AF0E5C29BBA2C840AAC79CC,
	AdManager_ShowRewardAd_m79195807AE21A78462C1EC35D64949397A7C8027,
	AdManager_ShowInterstitialAd_mBCE81C636091F07EEE7A2F29BB4DAA591FC239C5,
	AdManager_LoadAd_m7B82016EBAD04558EF333496B3E8DFBC15189E63,
	AdManager_SetGDPRConsentMethod_mCC187B47512D64FEA32DEAF7D1EBAC3B596F29E4,
	AdManager__ctor_mEBCE9C16722BC950561C32F32C6136C9976915BE,
	U3CU3Ec__cctor_m658C0B3E45C7CD6DD9C5A4EB4EFD56BD485D092F,
	U3CU3Ec__ctor_m8114653AC79D21B4E4D6E17D3E39260018C1EA05,
	U3CU3Ec_U3CInitU3Eb__6_0_m5B6CC776968273AA042425FDB03CDB0ABFBCFDF2,
	InterstitialAdMono_LoadInterstitialAd_mB279200B1071B2B47886BD310AE6B2867F89A4AB,
	InterstitialAdMono_ShowAd_m2B0F881A6DDDB326C041657016B2CA5212EED735,
	InterstitialAdMono_IsAdLoaded_m87DF62608E62F030DFBCE5CC1DC5116435037DF1,
	InterstitialAdMono_ShowTestAd_m191668B7782C3A4A77E308CC6BBA2D04739E2CC2,
	InterstitialAdMono_CreateAdRequest_m65608BE1086785B3BBD37864EF9038DA3B10EF5E,
	InterstitialAdMono_HandleInterstitialLoaded_mF23BB10D73C2B5AE5569C51BC09380A51DF76B46,
	InterstitialAdMono_HandleInterstitialFailedToLoad_m664786B0943917BECF112F0BA5907CB84E8A0194,
	InterstitialAdMono_HandleInterstitialOpened_m292F3AE4654411BFBD5F199DF215B831743D56B9,
	InterstitialAdMono_HandleInterstitialClosed_mB4090D52D542611C3E86B44CBF042A2A23F038FE,
	InterstitialAdMono_HandleInterstitialLeftApplication_m5D70D01B845EC8CCC4729F4698B84E7091F63558,
	InterstitialAdMono_HandleRewardPaidEvent_mCF5386303855326AA90F07FC5A43468160EA11F0,
	InterstitialAdMono__ctor_mABC905CD8C67271373554D177D42E0E49387888E,
	RewardAd_get_IsInWaitingForLoadAd_m5114D97AE42B31734F05691958CB51593107846A,
	RewardAd_set_IsInWaitingForLoadAd_mF7FB0DCE6E53F38285C568767983FBE17A6DD40F,
	RewardAd_LoadRewardAd_m770829CDBA737E20255DA0ECBA7310551896B093,
	RewardAd_ShowAd_m526EFD6909564894A92343C4545131A14F4196C5,
	RewardAd_IsAdLoaded_mD618AF4FEEEBDF32AE79E39C586BCCAA45583C3D,
	RewardAd_ShowTestAd_mA2F44534FD9BAAF0093E3D080135E587EC7DB164,
	RewardAd_CreateAdRequest_m46DF0C9AB6CB270C203E71D3A4D430B7EB380C17,
	RewardAd_HandleRewardedAd_m9F7FD689AFDA9120DB3361E106EBFC55492B5DE7,
	RewardAd_HandleRewardedAdLoaded_mFC7AE1C4ED37BAF49B3006B0B7C782B6AE877560,
	RewardAd_HandleRewardedAdFailedToLoad_mB18E95D0A006D4082CB2EC49FFC56A94D236965D,
	RewardAd_HandleRewardedAdOpening_m813B0F9D49F93F0D68D24FA7FF69DDBB4B3D9686,
	RewardAd_HandleRewardedAdFailedToShow_m9889D607D41BE3EB3D83E65C3D0A8043857A3D43,
	RewardAd_HandleRewardedAdClosed_m3A3ACFE254D8558EC61FECDA2BC473B2BF7D7B72,
	RewardAd_HandleUserEarnedReward_mDAE0CEC4E3CC8AABCE00E1546989409AEB016191,
	RewardAd_HandleRewardPaidEvent_mD39A652C4AA78D7E07499655E54389635F5F1271,
	RewardAd__ctor_m157850AA462D3676E53E0D8B56C461FDEC33A75C,
	U3CHandleRewardedAdU3Ed__13__ctor_m02BE4FDF06BBE5BD0CE25900C3140BD75B5023F5,
	U3CHandleRewardedAdU3Ed__13_System_IDisposable_Dispose_mB5B980BA7922C6E36C6A04D346E8C48ED7039468,
	U3CHandleRewardedAdU3Ed__13_MoveNext_mB6E03E7C0952C25DF9B47A57FB2ED09324F2FDFA,
	U3CHandleRewardedAdU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFF8EF072C53C6BB648D0A57E0CCE523D2CD075B,
	U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_Reset_m617931E8D8470D9FBA9537D3D4EBD5880B76B845,
	U3CHandleRewardedAdU3Ed__13_System_Collections_IEnumerator_get_Current_m062DD8F084A5E3FAADCCC5E78DF6A77B0BEF2326,
	RewardAdData__ctor_m98DE60BFC79F5F046B078DB500C1252D0AB89B0E,
	IAPManager_add_OnPurchaseSuccess_m5770136C682BCBD528FA184FA5CDB28E0271E657,
	IAPManager_remove_OnPurchaseSuccess_m976C6D79491D93B215CFEEE7C510A252D3C7282B,
	IAPManager_RaisePurchaseSuccess_m29BF0EE8571ED80B5B0F45A074CC8A5673FB028A,
	IAPManager_add_OnPurchaseCancel_mF70A1C83B724A55EB6AA44148393A1E86AE26DFC,
	IAPManager_remove_OnPurchaseCancel_m80BA81676142BD8AAFBE953E060FF9DD24E05290,
	IAPManager_RaisePurchaseCancel_mFA3209F5BE0BE16A617C8DF88A8443ACFCAC540D,
	IAPManager_Start_mA14B10A6337D93827364297EE1D26BE9B1677664,
	IAPManager_Awake_mAB98EA00323B1EA23BA2E40E9E3805895195CEC9,
	IAPManager_OnInitManager_mDF151BC6D3AF48508EDA1AC07107C93C72E063D7,
	IAPManager_InitRoutine_m491B305A643C997913E216D26127A2A3A9CE8D7F,
	IAPManager_InitializePurchasing_mDEEA97C211CE83F569242F862967A0C0CB366086,
	IAPManager_IsInitialized_mC639CA5610747E0E194CCB1AEBAF3E2CCC05F355,
	IAPManager_GetProductData_mFC0FEDE0BF7C0A46510CF0662818096F305F2F86,
	IAPManager_GetProductData_m1C72BF1683F856905BB18D68184183892ED3EC99,
	IAPManager_PurchaseProduct_m3887B622A2D12CE93B4EB22CC146B8D84A78EA6F,
	IAPManager_PurchaseProduct_mE3763FF67350281FEF3D21A4A4BF2C4E9E563182,
	IAPManager_GetIAPPrice_mA1D6E9E0274B5E82CF84CD23E8E95BB1AAE30BBF,
	IAPManager_GetIAPPriceInDouble_mF937798A612A89C25B346969CA2C07690AB815B6,
	IAPManager_GetIAPCurrency_m24E97E858E8D563F44F38B9686A2D0B4C3B5CD3C,
	IAPManager_IsProductAlreadyBought_m103A2108D6EA02DE681A468BF97306CF300C59EA,
	IAPManager_IsAnyAdProductPurchased_mBD4DC24AFD9DF0D662EE6A42224F3D09608D10AE,
	IAPManager_BuyProductID_mA3A17B7F6A17681D896BFF43EEA3E8DD047B03DA,
	IAPManager_RestorePurchases_mC9BB5A04BE460A6A8AC896B287F97BC5403648E7,
	IAPManager_OnInitialized_m2C401760A130804612287440BDE93F37F9D435AA,
	IAPManager_OnInitializeFailed_mE7AD16A44DF66198D05E523BD67CC9E95A36B748,
	IAPManager_ProcessPurchase_m19E3073671FE0F380C70059AE2FFAB84C4A6A64E,
	IAPManager_OnPurchaseFailed_mB9B60DBF2AD30B745FB8A1868EEC3C8956DFDBC4,
	IAPManager_WaitForInternetCheckBoughtProducts_mE7920CF75BB949A0A8793B65579E6CD29E1A1E43,
	IAPManager_GetProductPurchasedReceipt_mAF58A3E47C15139EAC0697848851767E043608ED,
	IAPManager__ctor_m0E9B3C5F41938E3F71B051C0ADE2260397581720,
	PurchaseSuccess__ctor_m4907490BD2E6C025A5E27D109B3EB0CFEE2A62B3,
	PurchaseSuccess_Invoke_m731222809B25FD0F29A20CC1A60ABDAB4F8A5430,
	PurchaseSuccess_BeginInvoke_m6E6B2B2781A35C15610E8966704256CBB3DCA494,
	PurchaseSuccess_EndInvoke_mE776416AC59EF86A7CECEBBA19091569FF5D4BE8,
	PurchaseCancel__ctor_mA6965819D2F928BD25A0BB3CF63066875521C937,
	PurchaseCancel_Invoke_m36FC0DB22883E9BE893A9B4B0C4AD3335DEA2EB8,
	PurchaseCancel_BeginInvoke_mCC3F20564A920F299538D7B1CDC5279774FA773D,
	PurchaseCancel_EndInvoke_m9D17D0BB39E735F40E1BC4C34B05716C2FFC287A,
	U3CAwakeU3Ed__19_MoveNext_mF7D8CA8A5F9BDB4CBDD1BC61D0878686BB7ED1F9,
	U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B,
	U3CInitRoutineU3Ed__21__ctor_mAEFF8084A9F8B86448E061EF04B768F1437D4AC9,
	U3CInitRoutineU3Ed__21_System_IDisposable_Dispose_m711DF51248BAA1DED0B7C429C1FE4F3C0FD4952C,
	U3CInitRoutineU3Ed__21_MoveNext_m36B7A41A625A795BB5BC5AE5CD7B8D2F231745E3,
	U3CInitRoutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C0EB3766FB22F1052F0A8B652861373D6836697,
	U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_Reset_m44D85717BECADB3DF040C3E30DB95EBBF5156493,
	U3CInitRoutineU3Ed__21_System_Collections_IEnumerator_get_Current_mA6E8778BFB06FF1C798CB6A051649066FA1B369B,
	U3CU3Ec__cctor_m879EC86BD41F61D1C20E9C5A037231BA40F0C798,
	U3CU3Ec__ctor_mB7FDDADD6DC9C42439F8CEEC714C35B1990539E5,
	U3CU3Ec_U3CRestorePurchasesU3Eb__34_0_mC79C8511C171909B53D13412A0F3746C93A9B06A,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39__ctor_m71B9CEDF53BDC71546A62B488B91351CC449F4D9,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_IDisposable_Dispose_m57524CDD0E864C5A6581FCF33725B5A134D353D2,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_MoveNext_mA9A7115F2B24B35774D9E5E8FE3C720743E3E353,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m526879C01B18A1C91F35EF33F1D10246AA88BF62,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_Reset_mA9D2ED022B84D573757B25E8D10C3BA19FC84C76,
	U3CWaitForInternetCheckBoughtProductsU3Ed__39_System_Collections_IEnumerator_get_Current_m703564928F8B8E4DC10D7B3F53259ABCFDF1F602,
	IAPProductData_Equals_mF101CCFB0D427113E1F8E64AB818BEB9C9CCF9AB,
	IAPProductData__ctor_mF5A2D6EDE27CED6D9D5ACEEC6C49E3884EF98CF4,
	ShopManager_Awake_mEA499061859D292A39793E6A7842D10B9EFF96F7,
	ShopManager_OnEnable_m8F21B586F3032A9394521B5701649C4680218346,
	ShopManager_OnDisable_m8ABFB920D209BD83F3B88F726E083F9CB76FE769,
	ShopManager_OnIapButtonClick_mEA9DA53CE24132E26B500070220D3330F02E7B4A,
	ShopManager_PurchaseProductIfInternetConnected_m5272A230EABEE435E50B2A52AC37A9A7CDA5432C,
	ShopManager_AddIapToData_mBD9350E1CDDA88444402B6F842ED28986FF62C95,
	ShopManager_Event_OnPurchaseSuccess_mD88CFCB60662959A0F82DBF9B54D1271DB57E471,
	ShopManager_Event_OnPurchaseCancle_mC03789D2595558F8FC460064984442569D5B01FC,
	ShopManager__ctor_m57071D1D97330875E2A06ABA66E03B1A8FE4DCA9,
};
extern void U3CAwakeU3Ed__19_MoveNext_mF7D8CA8A5F9BDB4CBDD1BC61D0878686BB7ED1F9_AdjustorThunk (void);
extern void U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000273, U3CAwakeU3Ed__19_MoveNext_mF7D8CA8A5F9BDB4CBDD1BC61D0878686BB7ED1F9_AdjustorThunk },
	{ 0x06000274, U3CAwakeU3Ed__19_SetStateMachine_m99FFC241F5A67CC079CA7D19A5F1A80766595E5B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[654] = 
{
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2548,
	3068,
	3068,
	3068,
	3068,
	1180,
	3068,
	3068,
	3002,
	3068,
	3068,
	3068,
	3068,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	3068,
	1567,
	1567,
	1567,
	1567,
	3068,
	4807,
	3068,
	2563,
	3068,
	3068,
	3068,
	3068,
	1567,
	1567,
	1567,
	1567,
	3068,
	3068,
	3068,
	1458,
	2033,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	1873,
	1873,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	4807,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2548,
	2025,
	2025,
	2025,
	3068,
	3068,
	-1,
	-1,
	-1,
	3068,
	3029,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3026,
	3026,
	3062,
	3062,
	3068,
	3068,
	3068,
	1031,
	1031,
	1031,
	1582,
	3068,
	3002,
	3068,
	3068,
	4807,
	4800,
	3002,
	3068,
	3002,
	3029,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2563,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	4807,
	3068,
	3068,
	3002,
	3068,
	2029,
	2029,
	3068,
	4807,
	3068,
	3068,
	3068,
	3068,
	2029,
	2585,
	3068,
	2585,
	3068,
	1033,
	3068,
	832,
	3002,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	3026,
	1003,
	3068,
	3068,
	2548,
	3068,
	2548,
	2548,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	4807,
	3068,
	2563,
	2563,
	2563,
	3068,
	2563,
	2563,
	2563,
	2222,
	2222,
	1564,
	3068,
	2563,
	3068,
	3068,
	3068,
	2548,
	2548,
	3068,
	3068,
	3026,
	3068,
	3002,
	1191,
	3068,
	3068,
	2563,
	4807,
	3068,
	2563,
	2563,
	2563,
	2349,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	2548,
	3068,
	3026,
	3068,
	3002,
	3068,
	3002,
	3068,
	2563,
	3068,
	3002,
	3026,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2585,
	2548,
	3068,
	2588,
	3068,
	3068,
	2548,
	2033,
	3068,
	3068,
	3068,
	3068,
	2222,
	2222,
	3068,
	3068,
	2239,
	3068,
	3068,
	2563,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	2239,
	3068,
	2239,
	3068,
	2239,
	3068,
	2239,
	3068,
	3068,
	3068,
	3068,
	3002,
	3068,
	3068,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	1564,
	1570,
	1567,
	1113,
	1316,
	1192,
	2548,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3002,
	2563,
	3068,
	3068,
	3002,
	2563,
	3026,
	3026,
	3068,
	3068,
	3068,
	3068,
	3026,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2548,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2239,
	3068,
	3068,
	3068,
	1596,
	-1,
	-1,
	-1,
	-1,
	3068,
	2029,
	2029,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3026,
	3068,
	3068,
	4807,
	3068,
	2563,
	2563,
	2563,
	2563,
	3068,
	2563,
	2563,
	2548,
	1192,
	2563,
	1021,
	1021,
	1021,
	2239,
	2563,
	3068,
	2239,
	2239,
	2222,
	2563,
	3068,
	3068,
	4807,
	3068,
	2563,
	2563,
	2563,
	3068,
	3068,
	2563,
	2548,
	3068,
	3026,
	2585,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	4807,
	4723,
	4720,
	4309,
	4588,
	4472,
	3068,
	3068,
	2025,
	2025,
	2025,
	2025,
	3026,
	3068,
	3068,
	2025,
	2985,
	3068,
	3068,
	2239,
	3068,
	2025,
	3068,
	3068,
	2239,
	3068,
	3002,
	3002,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2585,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3002,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	3068,
	2563,
	3068,
	3068,
	2548,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2563,
	3068,
	3068,
	3068,
	3068,
	4807,
	3068,
	2563,
	3068,
	3068,
	3026,
	3068,
	3002,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	3068,
	3026,
	2585,
	3068,
	2563,
	3026,
	3068,
	3002,
	3002,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	1567,
	3068,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	3068,
	4745,
	4745,
	2563,
	4745,
	4745,
	2563,
	3068,
	3068,
	3002,
	3002,
	3068,
	3026,
	2025,
	2029,
	2563,
	2548,
	2029,
	1753,
	2025,
	2239,
	3026,
	2563,
	3068,
	1567,
	2548,
	1887,
	1564,
	3002,
	2025,
	3068,
	1566,
	2563,
	832,
	2563,
	1566,
	2563,
	832,
	2563,
	3068,
	2563,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	4807,
	3068,
	2585,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	2239,
	3068,
	3068,
	3068,
	3068,
	2548,
	2585,
	2548,
	2563,
	2563,
	3068,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x02000014, { 0, 8 } },
	{ 0x0600017C, { 8, 1 } },
	{ 0x0600017D, { 9, 2 } },
	{ 0x0600017E, { 11, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[13] = 
{
	{ (Il2CppRGCTXDataType)3, 3189 },
	{ (Il2CppRGCTXDataType)2, 1875 },
	{ (Il2CppRGCTXDataType)3, 3221 },
	{ (Il2CppRGCTXDataType)3, 3187 },
	{ (Il2CppRGCTXDataType)3, 3210 },
	{ (Il2CppRGCTXDataType)3, 3222 },
	{ (Il2CppRGCTXDataType)3, 3186 },
	{ (Il2CppRGCTXDataType)2, 1860 },
	{ (Il2CppRGCTXDataType)3, 23436 },
	{ (Il2CppRGCTXDataType)2, 4651 },
	{ (Il2CppRGCTXDataType)3, 19917 },
	{ (Il2CppRGCTXDataType)2, 4652 },
	{ (Il2CppRGCTXDataType)3, 19918 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	654,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	13,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

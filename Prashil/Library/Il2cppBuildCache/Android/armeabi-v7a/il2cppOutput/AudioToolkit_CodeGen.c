﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AudioController::set_DisableAudio(System.Boolean)
extern void AudioController_set_DisableAudio_m295462AF48B514A279B22F7A65A0D277206ACC7B (void);
// 0x00000002 System.Boolean AudioController::get_DisableAudio()
extern void AudioController_get_DisableAudio_m597B19459A2F1F001DB3EECDDCC97721CD5D8896 (void);
// 0x00000003 System.Boolean AudioController::get_isAdditionalAudioController()
extern void AudioController_get_isAdditionalAudioController_mEB7B0AB2C7D9FB339CB03685B2922F6290DB8DB8 (void);
// 0x00000004 System.Void AudioController::set_isAdditionalAudioController(System.Boolean)
extern void AudioController_set_isAdditionalAudioController_mA80630A3E3569DB85A7B7D4F193A34F72379D199 (void);
// 0x00000005 System.Single AudioController::get_Volume()
extern void AudioController_get_Volume_mC5F1B0459B1EE3C6F6701C7F8F03530C0411CC89 (void);
// 0x00000006 System.Void AudioController::set_Volume(System.Single)
extern void AudioController_set_Volume_m1D7ED0C201F482C6472C6DC3BA1F1FB01A253619 (void);
// 0x00000007 System.Boolean AudioController::get_musicEnabled()
extern void AudioController_get_musicEnabled_m8BDE5B4C8A0978FCE880A5B296472CB80A0B8B30 (void);
// 0x00000008 System.Void AudioController::set_musicEnabled(System.Boolean)
extern void AudioController_set_musicEnabled_m65386EAB8EF87FED1D170BF0DFB210B5C1B9939B (void);
// 0x00000009 System.Boolean AudioController::get_ambienceSoundEnabled()
extern void AudioController_get_ambienceSoundEnabled_m0823A620254DA9EB78ADE7FF74B6A6AD9D67FDC9 (void);
// 0x0000000A System.Void AudioController::set_ambienceSoundEnabled(System.Boolean)
extern void AudioController_set_ambienceSoundEnabled_mF50228ED3F4EAF97B3251D08856B981385C3CA4E (void);
// 0x0000000B System.Boolean AudioController::get_soundMuted()
extern void AudioController_get_soundMuted_m668DF129821C846D678564C93E19AC1A73F7B5E1 (void);
// 0x0000000C System.Void AudioController::set_soundMuted(System.Boolean)
extern void AudioController_set_soundMuted_m6041BF17A04B093927F7E0054B80FC2EB046B9BD (void);
// 0x0000000D System.Single AudioController::get_musicCrossFadeTime_In()
extern void AudioController_get_musicCrossFadeTime_In_mB4950C27B87B8CEE715514EF71FE68E1A76629DE (void);
// 0x0000000E System.Void AudioController::set_musicCrossFadeTime_In(System.Single)
extern void AudioController_set_musicCrossFadeTime_In_m80917995680C2ECC0CEC6ACED75E19C94AE0963C (void);
// 0x0000000F System.Single AudioController::get_musicCrossFadeTime_Out()
extern void AudioController_get_musicCrossFadeTime_Out_m9940A28243D69E595F2B243EBD0293E18C4602A8 (void);
// 0x00000010 System.Void AudioController::set_musicCrossFadeTime_Out(System.Single)
extern void AudioController_set_musicCrossFadeTime_Out_mE45BB7C5BF27FBBAECE5F891C62C7BEBFF7DC502 (void);
// 0x00000011 System.Single AudioController::get_ambienceSoundCrossFadeTime_In()
extern void AudioController_get_ambienceSoundCrossFadeTime_In_m748B2EE6E00FC8009FF84EB5376639E287885E94 (void);
// 0x00000012 System.Void AudioController::set_ambienceSoundCrossFadeTime_In(System.Single)
extern void AudioController_set_ambienceSoundCrossFadeTime_In_mFC6FB0595FDEE15C62441DA35B70E9FC22B80ED5 (void);
// 0x00000013 System.Single AudioController::get_ambienceSoundCrossFadeTime_Out()
extern void AudioController_get_ambienceSoundCrossFadeTime_Out_mB3DF6B8D722DA539EBB91882580BC5E6FF1AE5D3 (void);
// 0x00000014 System.Void AudioController::set_ambienceSoundCrossFadeTime_Out(System.Single)
extern void AudioController_set_ambienceSoundCrossFadeTime_Out_mC8A46AA2413E7BC159BA13A36BEFF7F53E1589BB (void);
// 0x00000015 System.Double AudioController::get_systemTime()
extern void AudioController_get_systemTime_m494F4E9476D0E721018B85D396745B45B4C0926D (void);
// 0x00000016 System.Double AudioController::get_systemDeltaTime()
extern void AudioController_get_systemDeltaTime_mFA763378659DE5CFFC0C03A67F1B87750ED1C875 (void);
// 0x00000017 System.Void AudioController::set_musicParent(UnityEngine.Transform)
extern void AudioController_set_musicParent_m6EDD0A0A7104E4468E0FAA96260FF231BCD7742C (void);
// 0x00000018 UnityEngine.Transform AudioController::get_musicParent()
extern void AudioController_get_musicParent_m814A96325AE3A6611017552F27DAB200AB563256 (void);
// 0x00000019 System.Void AudioController::set_ambienceParent(UnityEngine.Transform)
extern void AudioController_set_ambienceParent_m59EC177F59C61C7D7980E0D66C18BE74C44C4C4A (void);
// 0x0000001A UnityEngine.Transform AudioController::get_ambienceParent()
extern void AudioController_get_ambienceParent_m2C30FD86590D662DFE3F1FF88970D16BD5FB65A8 (void);
// 0x0000001B ClockStone.AudioObject AudioController::PlayMusic(System.String,System.Single,System.Single,System.Single)
extern void AudioController_PlayMusic_m482EBCA3BDF1843F026B88EB07C07B473DEE6E34 (void);
// 0x0000001C ClockStone.AudioObject AudioController::PlayMusic(System.String,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_PlayMusic_m8024CA856B4E8D64F4EA685B6168B23DC103AA63 (void);
// 0x0000001D ClockStone.AudioObject AudioController::PlayMusic(System.String,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_PlayMusic_m0E426FF87EAA28F44943473A88D3471DAB4F51FC (void);
// 0x0000001E System.Boolean AudioController::StopMusic()
extern void AudioController_StopMusic_m2A2C7082F53E6F359533B4ADFAB6DC80EDF150D0 (void);
// 0x0000001F System.Boolean AudioController::StopMusic(System.Single)
extern void AudioController_StopMusic_m6034EF055E2385A49F2BD4261BD746A886276A74 (void);
// 0x00000020 System.Boolean AudioController::PauseMusic(System.Single)
extern void AudioController_PauseMusic_mFC1C5F498122990BD5E8BC6F0357DF5576779B81 (void);
// 0x00000021 System.Boolean AudioController::IsMusicPaused()
extern void AudioController_IsMusicPaused_m37DA8D3596511ECB7ED8327739729634B2ECE741 (void);
// 0x00000022 System.Boolean AudioController::UnpauseMusic(System.Single)
extern void AudioController_UnpauseMusic_m4129185AB4275627319452CDE83678B1DA28AF80 (void);
// 0x00000023 ClockStone.AudioObject AudioController::PlayAmbienceSound(System.String,System.Single,System.Single,System.Single)
extern void AudioController_PlayAmbienceSound_mF1A9705DC376A95631003986D617DC10DD64D7F7 (void);
// 0x00000024 ClockStone.AudioObject AudioController::PlayAmbienceSound(System.String,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_PlayAmbienceSound_mB9DBB0ECD24ABECCE3EAAE1AF31C89EFDE6FDB7E (void);
// 0x00000025 ClockStone.AudioObject AudioController::PlayAmbienceSound(System.String,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_PlayAmbienceSound_m297559B4EB494EF8CC94EA90990223F33B4DF2D3 (void);
// 0x00000026 System.Boolean AudioController::StopAmbienceSound()
extern void AudioController_StopAmbienceSound_m871EBF1B106DB083A2DC7414C91540C46D1FB49F (void);
// 0x00000027 System.Boolean AudioController::StopAmbienceSound(System.Single)
extern void AudioController_StopAmbienceSound_m5E1CD4A36F947AD0944D783DF3431DB56173388B (void);
// 0x00000028 System.Boolean AudioController::PauseAmbienceSound(System.Single)
extern void AudioController_PauseAmbienceSound_mAD9BD0FB011E255206F394713141F0C17DCC4B8F (void);
// 0x00000029 System.Boolean AudioController::IsAmbienceSoundPaused()
extern void AudioController_IsAmbienceSoundPaused_m0B5EC151F775499153AB71A9BC3CDA2778EA8B2E (void);
// 0x0000002A System.Boolean AudioController::UnpauseAmbienceSound(System.Single)
extern void AudioController_UnpauseAmbienceSound_mE058348877FFA74441D37DD6F449F7D60F6CA635 (void);
// 0x0000002B System.Int32 AudioController::EnqueueMusic(System.String)
extern void AudioController_EnqueueMusic_m3098EA923E8ADEE146DB0F5667D74C7D2C437005 (void);
// 0x0000002C ClockStone.Playlist AudioController::_GetCurrentPlaylist()
extern void AudioController__GetCurrentPlaylist_mB77BE8AD3FAAB01FFE4A4E4E98C521E1AFE6762B (void);
// 0x0000002D ClockStone.Playlist AudioController::GetPlaylistByName(System.String)
extern void AudioController_GetPlaylistByName_m89C7AC165C3A7B5598CB26414C6AE197E9EE9BF7 (void);
// 0x0000002E System.String[] AudioController::GetMusicPlaylist(System.String)
extern void AudioController_GetMusicPlaylist_mDEC93E65326944F36A09B52C5F37015A0E0DF286 (void);
// 0x0000002F System.Boolean AudioController::SetCurrentMusicPlaylist(System.String)
extern void AudioController_SetCurrentMusicPlaylist_mE9D141ED1922E7CE979BD17873A1E39ADAC43941 (void);
// 0x00000030 ClockStone.AudioObject AudioController::PlayMusicPlaylist(System.String)
extern void AudioController_PlayMusicPlaylist_m87CA056EBD775A831AE63648EFE1A5B44488367C (void);
// 0x00000031 ClockStone.AudioObject AudioController::PlayNextMusicOnPlaylist()
extern void AudioController_PlayNextMusicOnPlaylist_m7F170CBF479F06FDEEA7921F5D766D2BAD9226C2 (void);
// 0x00000032 ClockStone.AudioObject AudioController::PlayPreviousMusicOnPlaylist()
extern void AudioController_PlayPreviousMusicOnPlaylist_m35AFAE185D7127288CE54D88D8BF9FF074482176 (void);
// 0x00000033 System.Boolean AudioController::IsPlaylistPlaying()
extern void AudioController_IsPlaylistPlaying_m17E58AC6179961CE02D27167EB32FB1FD2F97F2A (void);
// 0x00000034 System.Void AudioController::ClearPlaylists()
extern void AudioController_ClearPlaylists_m92555392BE58F316523C0BE570A079CA4499D49A (void);
// 0x00000035 System.Void AudioController::AddPlaylist(System.String,System.String[])
extern void AudioController_AddPlaylist_m82BEA18EFCEEF73A7EE01CCA3F39800C36555E3D (void);
// 0x00000036 ClockStone.AudioObject AudioController::Play(System.String)
extern void AudioController_Play_m123199DEFD5FBB4B38E69D36C0DCB384E593641E (void);
// 0x00000037 ClockStone.AudioObject AudioController::Play(System.String,System.Single,System.Single,System.Single)
extern void AudioController_Play_m1C28BA01681BA3F6E07E29A328D76BCA515044BA (void);
// 0x00000038 ClockStone.AudioObject AudioController::Play(System.String,UnityEngine.Transform)
extern void AudioController_Play_mFEC54A6857032C457243D0A7F1F75C5F56892752 (void);
// 0x00000039 ClockStone.AudioObject AudioController::Play(System.String,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_Play_m2983C1AAF496D16B202DB1E9A300F20D8106F8AC (void);
// 0x0000003A ClockStone.AudioObject AudioController::Play(System.String,UnityEngine.Vector3,UnityEngine.Transform)
extern void AudioController_Play_m237616BA746863A4843ABFCF56AC2B213CA4ADA4 (void);
// 0x0000003B ClockStone.AudioObject AudioController::Play(System.String,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController_Play_m41F6CD5F387B1D5F5B84E4A47858D37B49C679C5 (void);
// 0x0000003C ClockStone.AudioObject AudioController::PlayScheduled(System.String,System.Double,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single)
extern void AudioController_PlayScheduled_mA8C31B12A2530892AFC7FD36C0619D6D87683ADB (void);
// 0x0000003D ClockStone.AudioObject AudioController::PlayAfter(System.String,ClockStone.AudioObject,System.Double,System.Single,System.Single)
extern void AudioController_PlayAfter_mC002DD4EFE1067C0FE73D9F277A29CC125CA91DF (void);
// 0x0000003E System.Boolean AudioController::Stop(System.String,System.Single)
extern void AudioController_Stop_m8D45A372C1B8F756B76861B732C9BF5777C0A67E (void);
// 0x0000003F System.Boolean AudioController::Stop(System.String)
extern void AudioController_Stop_m1D09E9179DC6D11DB6C19CEEE141870EAB1928EA (void);
// 0x00000040 System.Void AudioController::StopAll(System.Single)
extern void AudioController_StopAll_m2A6505718A6F3E6D7B241A524BA18D829F8516EA (void);
// 0x00000041 System.Void AudioController::StopAll()
extern void AudioController_StopAll_m20BCF9359A00CBC63B8E00D2CFDDB93A79902ADC (void);
// 0x00000042 System.Void AudioController::PauseAll(System.Single)
extern void AudioController_PauseAll_mBD74772389C3579B83A1D4D90D8A534FE9121B39 (void);
// 0x00000043 System.Void AudioController::UnpauseAll(System.Single)
extern void AudioController_UnpauseAll_m0207E757C9B874E96177A2A11FBC7F22DC51038D (void);
// 0x00000044 System.Void AudioController::PauseCategory(System.String,System.Single)
extern void AudioController_PauseCategory_mD371BE130C9C7461AB6CE4793144258E1FF0B3F6 (void);
// 0x00000045 System.Void AudioController::UnpauseCategory(System.String,System.Single)
extern void AudioController_UnpauseCategory_mBBD59D9DC2EA41BCEC033331160A7BEE37750E4F (void);
// 0x00000046 System.Void AudioController::StopCategory(System.String,System.Single)
extern void AudioController_StopCategory_mF187C966C3DD5BE6E69951717C2E63DDE04980F1 (void);
// 0x00000047 System.Boolean AudioController::IsPlaying(System.String)
extern void AudioController_IsPlaying_m52697A76ABB12E37C64F055DB2C03E2DFBD30138 (void);
// 0x00000048 System.Collections.Generic.List`1<ClockStone.AudioObject> AudioController::GetPlayingAudioObjects(System.String,System.Boolean)
extern void AudioController_GetPlayingAudioObjects_m083C5E263EFDB1D14617D0BB4E2CEDB92BCEE8C7 (void);
// 0x00000049 System.Collections.Generic.List`1<ClockStone.AudioObject> AudioController::GetPlayingAudioObjectsInCategory(System.String,System.Boolean)
extern void AudioController_GetPlayingAudioObjectsInCategory_m4866203456FF37B473BA0CDC157EEB468F082C49 (void);
// 0x0000004A System.Collections.Generic.List`1<ClockStone.AudioObject> AudioController::GetPlayingAudioObjects(System.Boolean)
extern void AudioController_GetPlayingAudioObjects_m11D79A5F8F90A49735F6BE2CD3C387AA6A38476A (void);
// 0x0000004B System.Int32 AudioController::GetPlayingAudioObjectsCount(System.String,System.Boolean)
extern void AudioController_GetPlayingAudioObjectsCount_mE7AD0D99E5F5B13D898B3CE470764C81F2C44C60 (void);
// 0x0000004C System.Void AudioController::EnableMusic(System.Boolean)
extern void AudioController_EnableMusic_m56B9FCF57FAB083D86BF6A3792FC58244F064816 (void);
// 0x0000004D System.Void AudioController::EnableAmbienceSound(System.Boolean)
extern void AudioController_EnableAmbienceSound_m3EE7D636F6BF291BB2FB3851D153A1E5943C5A20 (void);
// 0x0000004E System.Void AudioController::MuteSound(System.Boolean)
extern void AudioController_MuteSound_mBEFC13772A7D5601E317E27AD175A01E0CB14A5C (void);
// 0x0000004F System.Boolean AudioController::IsMusicEnabled()
extern void AudioController_IsMusicEnabled_m32D131BE3DC5FF134C4AC111865DD3C8D11A5497 (void);
// 0x00000050 System.Boolean AudioController::IsAmbienceSoundEnabled()
extern void AudioController_IsAmbienceSoundEnabled_m6CF0C699F21BB238E6C077371AFF90403DD74D7F (void);
// 0x00000051 System.Boolean AudioController::IsSoundMuted()
extern void AudioController_IsSoundMuted_m0ABD23FCCC1DE80620F824ED3CFF427F1A9B1FE8 (void);
// 0x00000052 UnityEngine.AudioListener AudioController::GetCurrentAudioListener()
extern void AudioController_GetCurrentAudioListener_mE0617F3C93ECC5B545B008740B1BD0534328058E (void);
// 0x00000053 ClockStone.AudioObject AudioController::GetCurrentMusic()
extern void AudioController_GetCurrentMusic_m79ACF97E79AA223EA5B83D4110B0D5A90AADA492 (void);
// 0x00000054 ClockStone.AudioObject AudioController::GetCurrentAmbienceSound()
extern void AudioController_GetCurrentAmbienceSound_m920DB6EF34C1B39CA5772445DF5C430593537F27 (void);
// 0x00000055 ClockStone.AudioCategory AudioController::GetCategory(System.String)
extern void AudioController_GetCategory_mAFE4AFA000CE187E9AC655EE2FF064A060B14A97 (void);
// 0x00000056 System.Void AudioController::SetCategoryVolume(System.String,System.Single)
extern void AudioController_SetCategoryVolume_m881B91F2C5439C19DA908ECBBB7A44584145857F (void);
// 0x00000057 System.Single AudioController::GetCategoryVolume(System.String)
extern void AudioController_GetCategoryVolume_m763C4DCA2DECF03CD5CB8DE77491B93E3AA83F18 (void);
// 0x00000058 System.Void AudioController::FadeOutCategory(System.String,System.Single,System.Single)
extern void AudioController_FadeOutCategory_m63FB9CC73121D5F9516CB7547C7A2CC2E7896142 (void);
// 0x00000059 System.Void AudioController::FadeInCategory(System.String,System.Single,System.Boolean)
extern void AudioController_FadeInCategory_m271FF67702CE8F90E4465051E45DF2E86FEA8850 (void);
// 0x0000005A System.Void AudioController::SetGlobalVolume(System.Single)
extern void AudioController_SetGlobalVolume_mF74923143285431C8E65E6126B770F9AE7F6F6A2 (void);
// 0x0000005B System.Single AudioController::GetGlobalVolume()
extern void AudioController_GetGlobalVolume_m45AB41B17355297BB828F53FC82E77233F713EBD (void);
// 0x0000005C ClockStone.AudioCategory AudioController::NewCategory(System.String)
extern void AudioController_NewCategory_mF3414A42AEEB4CDC76DA76C22BA5283759E5B078 (void);
// 0x0000005D System.Void AudioController::RemoveCategory(System.String)
extern void AudioController_RemoveCategory_mE54A12EA93F35F2AEDE8866F1FF4F0A646DE9773 (void);
// 0x0000005E System.Void AudioController::AddToCategory(ClockStone.AudioCategory,ClockStone.AudioItem)
extern void AudioController_AddToCategory_m84197E4C3CE5991B301B09605BCED7A68CAE4764 (void);
// 0x0000005F ClockStone.AudioItem AudioController::AddToCategory(ClockStone.AudioCategory,UnityEngine.AudioClip,System.String)
extern void AudioController_AddToCategory_m36BBE2E56B8E543543741423F445857217859397 (void);
// 0x00000060 System.Boolean AudioController::RemoveAudioItem(System.String)
extern void AudioController_RemoveAudioItem_mDD50029C13DE8265B4120190BC91F340C6642CF0 (void);
// 0x00000061 System.Boolean AudioController::IsValidAudioID(System.String)
extern void AudioController_IsValidAudioID_m63ECE8BA90CEC294E016DDA06A670AFC91D26D16 (void);
// 0x00000062 ClockStone.AudioItem AudioController::GetAudioItem(System.String)
extern void AudioController_GetAudioItem_mE6C4344C17CD4F2B58DABFA0B33E9C906D5B1F79 (void);
// 0x00000063 System.Void AudioController::DetachAllAudios(UnityEngine.GameObject)
extern void AudioController_DetachAllAudios_m8EE8F26D5FD9CB58E9E2FB49D7A5C335187CA147 (void);
// 0x00000064 System.Single AudioController::GetAudioItemMaxDistance(System.String)
extern void AudioController_GetAudioItemMaxDistance_mA3F56641F604FBB85EAB65CCB190D5408B46C35C (void);
// 0x00000065 System.Void AudioController::UnloadAllAudioClips()
extern void AudioController_UnloadAllAudioClips_mE7645242ECB7B53B0CFE900734B867A5F07AFD01 (void);
// 0x00000066 System.Void AudioController::set__currentMusic(ClockStone.AudioObject)
extern void AudioController_set__currentMusic_mB8DF0925536F6BADF2679E67C8FBABA5BF1BFA6B (void);
// 0x00000067 ClockStone.AudioObject AudioController::get__currentMusic()
extern void AudioController_get__currentMusic_mD95BC00BC979E0AC7D25672AC24D589782F6D739 (void);
// 0x00000068 System.Void AudioController::set__currentAmbienceSound(ClockStone.AudioObject)
extern void AudioController_set__currentAmbienceSound_m21C624C8A8CE79EA6388829B23E9572AE1E27919 (void);
// 0x00000069 ClockStone.AudioObject AudioController::get__currentAmbienceSound()
extern void AudioController_get__currentAmbienceSound_m044706F14050DDF41A946D6BBC6EEA9C9CD877C4 (void);
// 0x0000006A System.Void AudioController::_ApplyVolumeChange()
extern void AudioController__ApplyVolumeChange_m5F3A39CC1F16D858E99AD1CEB59F8DB759362B20 (void);
// 0x0000006B ClockStone.AudioItem AudioController::_GetAudioItem(System.String)
extern void AudioController__GetAudioItem_m513C80870DFDA93ADEE36E5B128978CDA9CD3637 (void);
// 0x0000006C ClockStone.AudioObject AudioController::_PlayMusic(System.String,System.Single,System.Single,System.Single)
extern void AudioController__PlayMusic_m50404041B9FF41BA6B826CE6B9A3DDE5086C0118 (void);
// 0x0000006D ClockStone.AudioObject AudioController::_PlayAmbienceSound(System.String,System.Single,System.Single,System.Single)
extern void AudioController__PlayAmbienceSound_m3263BB7433BEC78B297F6A35237F893B6D1FAE11 (void);
// 0x0000006E System.Boolean AudioController::_StopMusic(System.Single)
extern void AudioController__StopMusic_m7FE0EBB83A4ECE7885D8478639EA63D3D602FF0A (void);
// 0x0000006F System.Boolean AudioController::_PauseMusic(System.Single)
extern void AudioController__PauseMusic_mB4AB1A62CCAC9B67A7F3A2729920BD9EB66724E3 (void);
// 0x00000070 System.Boolean AudioController::_StopAmbienceSound(System.Single)
extern void AudioController__StopAmbienceSound_mCC471D17095B3FF18E1F5A44B84BF2C5D414FF15 (void);
// 0x00000071 System.Boolean AudioController::_PauseAmbienceSound(System.Single)
extern void AudioController__PauseAmbienceSound_mB7183135C5A01916178865FC0F48C294891509DD (void);
// 0x00000072 ClockStone.AudioObject AudioController::_PlayMusic(System.String,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController__PlayMusic_mE4EA2492EAAA5499FD29E5985F92AF9312D99C1A (void);
// 0x00000073 ClockStone.AudioObject AudioController::_PlayAmbienceSound(System.String,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void AudioController__PlayAmbienceSound_m7C8489D595CFAACE867A9FCB107FD4B3FA78C2FB (void);
// 0x00000074 System.Int32 AudioController::_EnqueueMusic(System.String)
extern void AudioController__EnqueueMusic_m7EF535DCCA9CBD73FA25BC12FB2857882D29859C (void);
// 0x00000075 ClockStone.AudioObject AudioController::_PlayMusicPlaylist()
extern void AudioController__PlayMusicPlaylist_mEA5069CA5CC87E5551621752BB11A56F2747D04B (void);
// 0x00000076 ClockStone.AudioObject AudioController::_PlayMusicTrackWithID(System.Int32,System.Single,System.Boolean)
extern void AudioController__PlayMusicTrackWithID_mAC2C6FBBC4D3965105A9E5D57B2A75D2AA988670 (void);
// 0x00000077 ClockStone.AudioObject AudioController::_PlayNextMusicOnPlaylist(System.Single)
extern void AudioController__PlayNextMusicOnPlaylist_m90E1888D7CD66AA46785FB544C226B2BCE546270 (void);
// 0x00000078 ClockStone.AudioObject AudioController::_PlayPreviousMusicOnPlaylist(System.Single)
extern void AudioController__PlayPreviousMusicOnPlaylist_m2347CD04E54C0E58EA6C2DE9BDD24E5F69D6C204 (void);
// 0x00000079 System.Void AudioController::_ResetLastPlayedList()
extern void AudioController__ResetLastPlayedList_m16138A0E8A544724F1C90993422E2082A68F5765 (void);
// 0x0000007A System.Int32 AudioController::_GetNextMusicTrack()
extern void AudioController__GetNextMusicTrack_mCA218047D42CF9539961C1E4F3987CBA5E46AD3E (void);
// 0x0000007B System.Int32 AudioController::_GetPreviousMusicTrack()
extern void AudioController__GetPreviousMusicTrack_m349F522494425EEA2AD31EA3EE28A1DF3A091D03 (void);
// 0x0000007C System.Int32 AudioController::_GetPreviousMusicTrackShuffled()
extern void AudioController__GetPreviousMusicTrackShuffled_m3AEFBC35AE6A82DBDDF9FF74765EAE64F00D74A6 (void);
// 0x0000007D System.Void AudioController::_RemoveLastPlayedOnList()
extern void AudioController__RemoveLastPlayedOnList_mE502C5E8FA0610FE6C1236E131FF4236E9F62DD9 (void);
// 0x0000007E System.Int32 AudioController::_GetNextMusicTrackShuffled()
extern void AudioController__GetNextMusicTrackShuffled_mBCCF02A977878C0265EE2165CF68A5D33D612256 (void);
// 0x0000007F System.Int32 AudioController::_GetNextMusicTrackInOrder()
extern void AudioController__GetNextMusicTrackInOrder_m4FC46D045122ADCA4AF0965A2099D81134773EAA (void);
// 0x00000080 System.Int32 AudioController::_GetPreviousMusicTrackInOrder()
extern void AudioController__GetPreviousMusicTrackInOrder_m18FB811DDD1E97C1F8D30BB2684F8DAFFFE82778 (void);
// 0x00000081 ClockStone.AudioObject AudioController::_PlayEx(System.String,AudioChannelType,System.Single,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Boolean,System.Double,ClockStone.AudioObject,System.Single)
extern void AudioController__PlayEx_mF576955AC35F19CAACB55AE246637C17E5DECA0B (void);
// 0x00000082 ClockStone.AudioObject AudioController::PlayAudioItem(ClockStone.AudioItem,System.Single,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Boolean,ClockStone.AudioObject,System.Double,AudioChannelType,System.Single)
extern void AudioController_PlayAudioItem_mAAD24E42B8E0077252698974E18159670A1653B6 (void);
// 0x00000083 ClockStone.AudioCategory AudioController::_GetCategory(System.String)
extern void AudioController__GetCategory_mE2649060C97060F0EC8DC2A0FEF97AF63F527EBE (void);
// 0x00000084 System.Void AudioController::Update()
extern void AudioController_Update_m96D8EC49BFAC67A6399CCD916F089CA88944C9F3 (void);
// 0x00000085 System.Void AudioController::_UpdateSystemTime()
extern void AudioController__UpdateSystemTime_m75EA3E855D8B506A4A38781A184112DF91A8D21F (void);
// 0x00000086 System.Void AudioController::Awake()
extern void AudioController_Awake_m9C4187D8D291F25B1E4CB38FF79B05E880F65782 (void);
// 0x00000087 System.Void AudioController::OnEnable()
extern void AudioController_OnEnable_m8C791747C9E414D616A557DCE48B7AD63828ECE0 (void);
// 0x00000088 System.Void AudioController::OnDisable()
extern void AudioController_OnDisable_mAB2D7A84B62BD5FB8DAC5515A6C0BB5365CB2654 (void);
// 0x00000089 System.Boolean AudioController::get_isSingletonObject()
extern void AudioController_get_isSingletonObject_m5354A3D1BB446AB255A14E05E05C305C9BE2C348 (void);
// 0x0000008A System.Void AudioController::OnDestroy()
extern void AudioController_OnDestroy_m807BE2EF8842B4C1C418B2A39F9A480E4711BDBA (void);
// 0x0000008B System.Void AudioController::AwakeSingleton()
extern void AudioController_AwakeSingleton_m31CC7DAA13683866880D1B4C83CDC08D5AF3B831 (void);
// 0x0000008C System.Void AudioController::_ValidateCategories()
extern void AudioController__ValidateCategories_m471CC1EF12524131EAA46FB192DFB34A47C6A6D0 (void);
// 0x0000008D System.Void AudioController::_InvalidateCategories()
extern void AudioController__InvalidateCategories_m95513017C4C25D8E1E3C4D61BD9A4B159417DF4E (void);
// 0x0000008E System.Void AudioController::InitializeAudioItems()
extern void AudioController_InitializeAudioItems_mA3584EAF35AC47EB92239EED9556DF08F70D944E (void);
// 0x0000008F System.Void AudioController::_InitializeAudioItems(AudioController)
extern void AudioController__InitializeAudioItems_m77C382C842E8E39E37BCF9D45A71E73E5CAE423E (void);
// 0x00000090 System.Void AudioController::_RegisterAdditionalAudioController(AudioController)
extern void AudioController__RegisterAdditionalAudioController_m2E3E6A2AAE20F8F815E8D4E8C788EE72569EBA14 (void);
// 0x00000091 System.Void AudioController::_SyncCategoryVolumes(AudioController,AudioController)
extern void AudioController__SyncCategoryVolumes_mFEF1D78C5E81B6E0C015AF6C3EEEC997D34E3F19 (void);
// 0x00000092 System.Void AudioController::_UnregisterAdditionalAudioController(AudioController)
extern void AudioController__UnregisterAdditionalAudioController_m9092623D5DF3410E0E5E2C5D617853E96E980974 (void);
// 0x00000093 System.Collections.Generic.List`1<ClockStone.AudioCategory> AudioController::_GetAllCategories(System.String)
extern void AudioController__GetAllCategories_m1EF772C7BC721F0E7EE8078A6022FF118A3452E5 (void);
// 0x00000094 ClockStone.AudioObject AudioController::PlayAudioSubItem(ClockStone.AudioSubItem,System.Single,UnityEngine.Vector3,UnityEngine.Transform,System.Single,System.Single,System.Boolean,ClockStone.AudioObject,System.Double,AudioChannelType,System.Single)
extern void AudioController_PlayAudioSubItem_m5A052936BD9CDEC341CE2FFC17B06AFD8C71AC10 (void);
// 0x00000095 AudioController AudioController::_GetAudioController(ClockStone.AudioSubItem)
extern void AudioController__GetAudioController_mF09635747A9D82F5557ECCFB58918B70B018D33F (void);
// 0x00000096 System.Void AudioController::_NotifyPlaylistTrackCompleteleyPlayed(ClockStone.AudioObject)
extern void AudioController__NotifyPlaylistTrackCompleteleyPlayed_mE299DF505D3F6EC3AE11291C5CE8FA0ADABE5F62 (void);
// 0x00000097 System.Void AudioController::_ValidateAudioObjectPrefab(UnityEngine.GameObject)
extern void AudioController__ValidateAudioObjectPrefab_mA98897503247F5534054C8445E5AB4D53D32DAA4 (void);
// 0x00000098 System.Void AudioController::OnAfterDeserialize()
extern void AudioController_OnAfterDeserialize_mCAB707ED504B020A4DF2ED2B86972A5E4439EE32 (void);
// 0x00000099 System.Void AudioController::OnBeforeSerialize()
extern void AudioController_OnBeforeSerialize_mCF32E10DAC00F1BC03DF96A0E0167B33988D4CE0 (void);
// 0x0000009A System.Void AudioController::_SetDefaultCurrentPlaylist()
extern void AudioController__SetDefaultCurrentPlaylist_m75BA2EBEDEAB1F97D8773ED5CD06B82011FE728C (void);
// 0x0000009B System.Void AudioController::.ctor()
extern void AudioController__ctor_m4707A1CF2E95213275460BDC4A6074ACBBC6E87E (void);
// 0x0000009C System.Void AudioController::.cctor()
extern void AudioController__cctor_mE2D45A4525CB11BF32141C2C6853AAC9AC679551 (void);
// 0x0000009D System.Void AudioController_CurrentInspectorSelection::.ctor()
extern void AudioController_CurrentInspectorSelection__ctor_m0D21B27029BA5D45C1BA68D6CE3CCCE56284E8AA (void);
// 0x0000009E T ArrayHelper::AddArrayElement(T[]&)
// 0x0000009F T ArrayHelper::AddArrayElement(T[]&,T)
// 0x000000A0 System.Void ArrayHelper::DeleteArrayElement(T[]&,System.Int32)
// 0x000000A1 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeIfExists(System.Object,System.String,System.Object[])
extern void MessengerThatIncludesInactiveElements_InvokeIfExists_mBD01E54ABE7D247127F96774E13E87B46F5C6AF9 (void);
// 0x000000A2 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeIfExists(System.Object,System.String)
extern void MessengerThatIncludesInactiveElements_InvokeIfExists_m35EEE08888B035FD536F0B47EFE8932381A1089A (void);
// 0x000000A3 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethod(UnityEngine.GameObject,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_InvokeMethod_m538E148F60B7753193A8FB9CFE4FC9CE82B7514E (void);
// 0x000000A4 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethod(UnityEngine.GameObject,System.String,System.Boolean)
extern void MessengerThatIncludesInactiveElements_InvokeMethod_m316C847913487369D808120B6E0083EFEC2E103A (void);
// 0x000000A5 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethod(UnityEngine.Component,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_InvokeMethod_mBB30CD88F60BFDF1081BF6D5F2BF0067306FE8A5 (void);
// 0x000000A6 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethod(UnityEngine.Component,System.String,System.Boolean)
extern void MessengerThatIncludesInactiveElements_InvokeMethod_mEFF30B7D3BA23590C48A3E8E60DEA05B7427D24F (void);
// 0x000000A7 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethodInChildren(UnityEngine.GameObject,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m4B1666B6F2E4815183BCFE5479B8BF926522CCBA (void);
// 0x000000A8 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethodInChildren(UnityEngine.GameObject,System.String,System.Boolean)
extern void MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m211A9231FDD6D440B8E6805B178293805D10BB74 (void);
// 0x000000A9 System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethodInChildren(UnityEngine.Component,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m0D710F984A550EA1BC23B0A69FCF7D3D0A5E8C98 (void);
// 0x000000AA System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::InvokeMethodInChildren(UnityEngine.Component,System.String,System.Boolean)
extern void MessengerThatIncludesInactiveElements_InvokeMethodInChildren_mB00BBF1DE4BF1DEBA602A6EECA63E136FFE61E05 (void);
// 0x000000AB System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::SendMessageUpwardsToAll(UnityEngine.GameObject,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_SendMessageUpwardsToAll_mC19DBE1962B0749DE1D968F7BD02ECCC60DEE5CB (void);
// 0x000000AC System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::SendMessageUpwardsToAll(UnityEngine.Component,System.String,System.Boolean,System.Object[])
extern void MessengerThatIncludesInactiveElements_SendMessageUpwardsToAll_m5254694F7D95304B8086953544C89BBDFCD577EC (void);
// 0x000000AD System.Void MessengerExtensions.MessengerThatIncludesInactiveElements::.cctor()
extern void MessengerThatIncludesInactiveElements__cctor_m9F48393AEE32910461FB91980778084F756D51F4 (void);
// 0x000000AE ClockStone.AudioSubItem[] ClockStone.AudioControllerHelper::_ChooseSubItems(ClockStone.AudioItem,ClockStone.AudioObject)
extern void AudioControllerHelper__ChooseSubItems_m08A389B0132A4281EFC1E95EF8AEA2B184B64B58 (void);
// 0x000000AF ClockStone.AudioSubItem ClockStone.AudioControllerHelper::_ChooseSingleSubItem(ClockStone.AudioItem,ClockStone.AudioPickSubItemMode,ClockStone.AudioObject)
extern void AudioControllerHelper__ChooseSingleSubItem_mF91C8E0BA02FA103F6534593E1D15F8E9122DB21 (void);
// 0x000000B0 ClockStone.AudioSubItem ClockStone.AudioControllerHelper::_ChooseSingleSubItem(ClockStone.AudioItem)
extern void AudioControllerHelper__ChooseSingleSubItem_m7A9AE766A27A20CF7CD3E5CC9160649491C96804 (void);
// 0x000000B1 ClockStone.AudioSubItem[] ClockStone.AudioControllerHelper::_ChooseSubItems(ClockStone.AudioItem,ClockStone.AudioPickSubItemMode,ClockStone.AudioObject)
extern void AudioControllerHelper__ChooseSubItems_m44F582836C1AA80EDE2D99C3A7E6903DE9E9CD7D (void);
// 0x000000B2 System.Int32 ClockStone.AudioControllerHelper::_ChooseRandomSubitem(ClockStone.AudioItem,System.Boolean,System.Int32,System.Boolean)
extern void AudioControllerHelper__ChooseRandomSubitem_m29E26B7FB3BBF372CF842CCA6637F4663F0F5087 (void);
// 0x000000B3 System.Boolean ClockStone.AudioControllerHelper::isOdd(System.Int32)
extern void AudioControllerHelper_isOdd_mEC1A7FD80C3E1BEA384CF1E9ACCA1710CEC4E232 (void);
// 0x000000B4 System.Double ClockStone.AudioFader::get_time()
extern void AudioFader_get_time_m699059A9841FF9E9E62CD3772C234974E556796A (void);
// 0x000000B5 System.Void ClockStone.AudioFader::set_time(System.Double)
extern void AudioFader_set_time_m99988CD4AA992FB991096ECC9843A2FB16252AD0 (void);
// 0x000000B6 System.Boolean ClockStone.AudioFader::get_isFadingOutComplete()
extern void AudioFader_get_isFadingOutComplete_m2EF4209002BBA0239226D7E180D3B1A41FC1AD5F (void);
// 0x000000B7 System.Boolean ClockStone.AudioFader::get_isFadingOut()
extern void AudioFader_get_isFadingOut_m0637F9F447EA58C3CF820F18C27150058BDCCD3D (void);
// 0x000000B8 System.Boolean ClockStone.AudioFader::get_isFadingOutOrScheduled()
extern void AudioFader_get_isFadingOutOrScheduled_m3178E66184742AB425E4E622D9D644024FC7DCF9 (void);
// 0x000000B9 System.Boolean ClockStone.AudioFader::get_isFadingIn()
extern void AudioFader_get_isFadingIn_m472451A43FB53B50B7B7DC982ADD6E60AB204BD0 (void);
// 0x000000BA System.Void ClockStone.AudioFader::Set0()
extern void AudioFader_Set0_m6360361DEB7A7EC1A8B67108865494297472E8A5 (void);
// 0x000000BB System.Void ClockStone.AudioFader::FadeIn(System.Single,System.Boolean)
extern void AudioFader_FadeIn_m808BAC5AC14F770F8AB6595DA90699FA92DD180C (void);
// 0x000000BC System.Void ClockStone.AudioFader::FadeIn(System.Single,System.Double,System.Boolean)
extern void AudioFader_FadeIn_m72161BBA10B01D475C1E38E96904AC6B7B9455A2 (void);
// 0x000000BD System.Void ClockStone.AudioFader::FadeOut(System.Single,System.Single)
extern void AudioFader_FadeOut_mE3C09C319302135EF7848D8B5FFCB60B39715FD0 (void);
// 0x000000BE System.Single ClockStone.AudioFader::Get()
extern void AudioFader_Get_m09EB22A5DDC2235A282C7F46E50D1CB7722B2CFA (void);
// 0x000000BF System.Single ClockStone.AudioFader::Get(System.Boolean&)
extern void AudioFader_Get_mB2BE5B156BDF16054DEEEE2B146EAC7AC67D16EE (void);
// 0x000000C0 System.Single ClockStone.AudioFader::_GetFadeOutValue()
extern void AudioFader__GetFadeOutValue_m23BC0E8A4DF42C8EA640D4F6D6E2447241D70AFB (void);
// 0x000000C1 System.Single ClockStone.AudioFader::_GetFadeInValue()
extern void AudioFader__GetFadeInValue_m6B80801F521CA0C31DA73A1EEBC514BD155CCF31 (void);
// 0x000000C2 System.Single ClockStone.AudioFader::_GetFadeValue(System.Single,System.Single)
extern void AudioFader__GetFadeValue_m1BE3C1AD7DF1EF0805C41483C854F8A80A58851C (void);
// 0x000000C3 System.Void ClockStone.AudioFader::.ctor()
extern void AudioFader__ctor_mFF2EE27126F60A3412FB4BE928D52ACCD0EF3531 (void);
// 0x000000C4 System.Single ClockStone.AudioCategory::get_Volume()
extern void AudioCategory_get_Volume_m510E2D4A0F47677FB01D24911E57989440B5218A (void);
// 0x000000C5 System.Void ClockStone.AudioCategory::set_Volume(System.Single)
extern void AudioCategory_set_Volume_m7FE0FBA212F03CCB180B2CD9B83A00B0E9777D6D (void);
// 0x000000C6 System.Single ClockStone.AudioCategory::get_VolumeTotal()
extern void AudioCategory_get_VolumeTotal_mE6562564CFAC2C57A194E752EBEBE0273CBA0D57 (void);
// 0x000000C7 System.Void ClockStone.AudioCategory::set_parentCategory(ClockStone.AudioCategory)
extern void AudioCategory_set_parentCategory_mA66D4CE43FE5D599DCEB65188B807E6D938419A5 (void);
// 0x000000C8 ClockStone.AudioCategory ClockStone.AudioCategory::get_parentCategory()
extern void AudioCategory_get_parentCategory_mA5C8A8B472DA464637D7D79852333EBE4D857BF0 (void);
// 0x000000C9 ClockStone.AudioFader ClockStone.AudioCategory::get_audioFader()
extern void AudioCategory_get_audioFader_mA21BBFBADD0F8DEB8D18164EDADF8076D043E440 (void);
// 0x000000CA AudioController ClockStone.AudioCategory::get_audioController()
extern void AudioCategory_get_audioController_m877D306C7B461CE160E5B7D9EC387779A77E2E14 (void);
// 0x000000CB System.Void ClockStone.AudioCategory::set_audioController(AudioController)
extern void AudioCategory_set_audioController_m41724F6F6CBEA516256E65EC581A18D9D61A6D95 (void);
// 0x000000CC System.Void ClockStone.AudioCategory::.ctor(AudioController)
extern void AudioCategory__ctor_mC290E34437C04DD36A8048F9AEE47E5A71AC316A (void);
// 0x000000CD UnityEngine.GameObject ClockStone.AudioCategory::GetAudioObjectPrefab()
extern void AudioCategory_GetAudioObjectPrefab_m672E92C3D772A43AFF6B8868DD9842D25C994641 (void);
// 0x000000CE UnityEngine.Audio.AudioMixerGroup ClockStone.AudioCategory::GetAudioMixerGroup()
extern void AudioCategory_GetAudioMixerGroup_mDCA56ABC5DE2F75A43A35929858A9E92CD4F2209 (void);
// 0x000000CF System.Void ClockStone.AudioCategory::_AnalyseAudioItems(System.Collections.Generic.Dictionary`2<System.String,ClockStone.AudioItem>)
extern void AudioCategory__AnalyseAudioItems_m584E9DB5796EF54AFC018941BC3A47C2D0015389 (void);
// 0x000000D0 System.Int32 ClockStone.AudioCategory::_GetIndexOf(ClockStone.AudioItem)
extern void AudioCategory__GetIndexOf_m1A2778A20F29C711D8820605D2C67FC623C2C35E (void);
// 0x000000D1 System.Void ClockStone.AudioCategory::_ApplyVolumeChange()
extern void AudioCategory__ApplyVolumeChange_m2A08B851ACC5F36C16F6D7B67CC12406CE2D4CA0 (void);
// 0x000000D2 System.Boolean ClockStone.AudioCategory::_IsCategoryParentOf(ClockStone.AudioCategory,ClockStone.AudioCategory)
extern void AudioCategory__IsCategoryParentOf_m44A9237AF051A69D1062C6A1AEE7ACAF12889352 (void);
// 0x000000D3 System.Void ClockStone.AudioCategory::UnloadAllAudioClips()
extern void AudioCategory_UnloadAllAudioClips_m1FE49636FC4847BCF50D007F2635A2C71C77BADE (void);
// 0x000000D4 System.Void ClockStone.AudioCategory::FadeIn(System.Single,System.Boolean)
extern void AudioCategory_FadeIn_m9DD20FE7FCB619D5AA9F639D6FB851549F390299 (void);
// 0x000000D5 System.Void ClockStone.AudioCategory::FadeOut(System.Single,System.Single)
extern void AudioCategory_FadeOut_m8C893182E0D3282A78CB52EA6DE9435185B8E8C2 (void);
// 0x000000D6 System.Void ClockStone.AudioCategory::_UpdateFadeTime()
extern void AudioCategory__UpdateFadeTime_m1EF8D36707DB097DC3931E235EC1A0FE76C7AC5E (void);
// 0x000000D7 System.Boolean ClockStone.AudioCategory::get_isFadingIn()
extern void AudioCategory_get_isFadingIn_mD93546C7382B6E96963BFF5CD7C31C3EAB3937FB (void);
// 0x000000D8 System.Boolean ClockStone.AudioCategory::get_isFadingOut()
extern void AudioCategory_get_isFadingOut_m7BDCA66FC9515612F4762917C1BA3C0E744D0832 (void);
// 0x000000D9 System.Boolean ClockStone.AudioCategory::get_isFadeOutComplete()
extern void AudioCategory_get_isFadeOutComplete_m960B7A2D90B082F2F48A8F2CC47A203CC33ADA24 (void);
// 0x000000DA System.Void ClockStone.AudioItem::.ctor()
extern void AudioItem__ctor_m53B4EA9AC820237462FC7ED15AC7FD3A31D4783E (void);
// 0x000000DB System.Void ClockStone.AudioItem::.ctor(ClockStone.AudioItem)
extern void AudioItem__ctor_m003AA826621C3BDF145C60CDDD9FF8795CA784B3 (void);
// 0x000000DC System.Void ClockStone.AudioItem::set_category(ClockStone.AudioCategory)
extern void AudioItem_set_category_mC727EC6722D1C8CB50CB635618189C656DF4C89C (void);
// 0x000000DD ClockStone.AudioCategory ClockStone.AudioItem::get_category()
extern void AudioItem_get_category_mDFB7E282B09E3B3E75244E2FDAC4EBED8DA6B2F0 (void);
// 0x000000DE System.Void ClockStone.AudioItem::Awake()
extern void AudioItem_Awake_mDA85D7085E0BBD33A742855D162827E90593A71D (void);
// 0x000000DF System.Void ClockStone.AudioItem::ResetSequence()
extern void AudioItem_ResetSequence_m2EC583AE93964000395A391E1D3BCEEA73F69CA8 (void);
// 0x000000E0 System.Void ClockStone.AudioItem::_Initialize(ClockStone.AudioCategory)
extern void AudioItem__Initialize_m5683A3DC8F4AAA8CDA3664B61FEBA416506D19D5 (void);
// 0x000000E1 System.Void ClockStone.AudioItem::_NormalizeSubItems()
extern void AudioItem__NormalizeSubItems_m61C64BE2457BD704A84E0E7566A27280E172F3E0 (void);
// 0x000000E2 System.Boolean ClockStone.AudioItem::_IsValidSubItem(ClockStone.AudioSubItem)
extern void AudioItem__IsValidSubItem_m6727D729A1212B026123B2D13E96B5A615E5464B (void);
// 0x000000E3 System.Void ClockStone.AudioItem::UnloadAudioClip()
extern void AudioItem_UnloadAudioClip_m6BFF649E48BA90D95DF824D2D1C60F2E429D7616 (void);
// 0x000000E4 System.Void ClockStone.AudioSubItem::.ctor()
extern void AudioSubItem__ctor_m5A243AE0823DE203820475C6EFCA9654970108D1 (void);
// 0x000000E5 System.Void ClockStone.AudioSubItem::.ctor(ClockStone.AudioSubItem,ClockStone.AudioItem)
extern void AudioSubItem__ctor_mEB4CCB2C415CC3B72EE7DAC09E3E39A31EAE9A84 (void);
// 0x000000E6 System.Single ClockStone.AudioSubItem::get__SummedProbability()
extern void AudioSubItem_get__SummedProbability_m218D747FAA7AAD8A3973570C411B37D9E7A7706F (void);
// 0x000000E7 System.Void ClockStone.AudioSubItem::set__SummedProbability(System.Single)
extern void AudioSubItem_set__SummedProbability_m102626FAA52C6E4D321622E5C9114F0A42790E16 (void);
// 0x000000E8 System.Void ClockStone.AudioSubItem::set_item(ClockStone.AudioItem)
extern void AudioSubItem_set_item_m2E30DF18D3BA7BB1776AFA92F4A66C757394E4EA (void);
// 0x000000E9 ClockStone.AudioItem ClockStone.AudioSubItem::get_item()
extern void AudioSubItem_get_item_m5461B0B361B262EA52774F61D5A5A7E7D9D43C05 (void);
// 0x000000EA System.String ClockStone.AudioSubItem::ToString()
extern void AudioSubItem_ToString_mF3CFD29B567C5EE5A632C6A971190796625D07BF (void);
// 0x000000EB System.Void ClockStone.Playlist::.ctor()
extern void Playlist__ctor_m6B40839B5C6EA4FD399238240DFF40B68DC3A982 (void);
// 0x000000EC System.Void ClockStone.Playlist::.ctor(System.String,System.String[])
extern void Playlist__ctor_mD13C4D2134A049A0AB12FD40835DDBC07DBDEBB3 (void);
// 0x000000ED System.String ClockStone.AudioObject::get_audioID()
extern void AudioObject_get_audioID_mFBC4A7E729F3B0CB77FC2411DD2774F6839CD592 (void);
// 0x000000EE System.Void ClockStone.AudioObject::set_audioID(System.String)
extern void AudioObject_set_audioID_m9EA3164B2A3F472520B11EF9812615C86D0251F2 (void);
// 0x000000EF ClockStone.AudioCategory ClockStone.AudioObject::get_category()
extern void AudioObject_get_category_m10BDAF4EF25E6A1EE421F275FF44B6E7BC0BECA2 (void);
// 0x000000F0 System.Void ClockStone.AudioObject::set_category(ClockStone.AudioCategory)
extern void AudioObject_set_category_m6FB880B500E5587C64161430865BADB98E0BC4F8 (void);
// 0x000000F1 ClockStone.AudioSubItem ClockStone.AudioObject::get_subItem()
extern void AudioObject_get_subItem_m45A6AECEA289218E6AA1C565E9FC8EB26B367C8A (void);
// 0x000000F2 System.Void ClockStone.AudioObject::set_subItem(ClockStone.AudioSubItem)
extern void AudioObject_set_subItem_mD6485FC017AF1C4B06086DB17F1CBD1C60CA4085 (void);
// 0x000000F3 AudioChannelType ClockStone.AudioObject::get_channel()
extern void AudioObject_get_channel_m214B4E811541BA90C353248847731FCB43E9F5F5 (void);
// 0x000000F4 System.Void ClockStone.AudioObject::set_channel(AudioChannelType)
extern void AudioObject_set_channel_mCE9D801261758EA7989A03806056DCA204184FA1 (void);
// 0x000000F5 ClockStone.AudioItem ClockStone.AudioObject::get_audioItem()
extern void AudioObject_get_audioItem_m25B4798545843CC3422DB66A26692C40AD480C7D (void);
// 0x000000F6 System.Void ClockStone.AudioObject::set_completelyPlayedDelegate(ClockStone.AudioObject/AudioEventDelegate)
extern void AudioObject_set_completelyPlayedDelegate_mA54E8983257AC9B3E7A139B8FC49894A17EBDD26 (void);
// 0x000000F7 ClockStone.AudioObject/AudioEventDelegate ClockStone.AudioObject::get_completelyPlayedDelegate()
extern void AudioObject_get_completelyPlayedDelegate_mE3806650C07E9F3B0CF901A0BFEC6634456DAA8E (void);
// 0x000000F8 System.Single ClockStone.AudioObject::get_volume()
extern void AudioObject_get_volume_mD5882AB345159F843725D75AF25606D75FF7122A (void);
// 0x000000F9 System.Void ClockStone.AudioObject::set_volume(System.Single)
extern void AudioObject_set_volume_m5CD3D2E1F4999FFAC4EBE3EA77999A8ACE83E71B (void);
// 0x000000FA System.Single ClockStone.AudioObject::get_volumeItem()
extern void AudioObject_get_volumeItem_m35940C69305EBC6000F06DD53D1692730D3FF07F (void);
// 0x000000FB System.Void ClockStone.AudioObject::set_volumeItem(System.Single)
extern void AudioObject_set_volumeItem_m09706B271378994B7CB247CB94E40E31C45A51B0 (void);
// 0x000000FC System.Single ClockStone.AudioObject::get_volumeTotal()
extern void AudioObject_get_volumeTotal_mB261EED7028FF2986AC6FCD2867D06E90DFC6160 (void);
// 0x000000FD System.Single ClockStone.AudioObject::get_volumeTotalWithoutFade()
extern void AudioObject_get_volumeTotalWithoutFade_mFE0130933F43DD21DF310F55A0D47B3B2BD13D88 (void);
// 0x000000FE System.Double ClockStone.AudioObject::get_playCalledAtTime()
extern void AudioObject_get_playCalledAtTime_mABB72A69B9A3576D835DA29FED78B52F3653D7F3 (void);
// 0x000000FF System.Double ClockStone.AudioObject::get_startedPlayingAtTime()
extern void AudioObject_get_startedPlayingAtTime_mCC056F1F38B7852D6C67D0932041AAE9C7962EF0 (void);
// 0x00000100 System.Single ClockStone.AudioObject::get_timeUntilEnd()
extern void AudioObject_get_timeUntilEnd_m9CC04AFA8C986B2C884B6729CE9E32B8A25ABBA6 (void);
// 0x00000101 System.Double ClockStone.AudioObject::get_scheduledPlayingAtDspTime()
extern void AudioObject_get_scheduledPlayingAtDspTime_m5A71E79F91DDB6A40F2C2E2DDBF958AC3784CB9C (void);
// 0x00000102 System.Void ClockStone.AudioObject::set_scheduledPlayingAtDspTime(System.Double)
extern void AudioObject_set_scheduledPlayingAtDspTime_m19C9322CBB818A1B6CEB5092105EE324DC45569C (void);
// 0x00000103 System.Single ClockStone.AudioObject::get_clipLength()
extern void AudioObject_get_clipLength_m464EA8E9CECEF04CECDB7BA8C47E0745DF9957F2 (void);
// 0x00000104 System.Single ClockStone.AudioObject::get_audioTime()
extern void AudioObject_get_audioTime_m5A6D4E5F320856CA5A32C5C1EB3B0AB13A4E54A6 (void);
// 0x00000105 System.Void ClockStone.AudioObject::set_audioTime(System.Single)
extern void AudioObject_set_audioTime_mD11E1A918442ACA14CD25AAC761256315895D382 (void);
// 0x00000106 System.Boolean ClockStone.AudioObject::get_isFadingOut()
extern void AudioObject_get_isFadingOut_m0EE34F33166E7B47CCD0466B9FDC94B51A12B05A (void);
// 0x00000107 System.Boolean ClockStone.AudioObject::get_isFadeOutComplete()
extern void AudioObject_get_isFadeOutComplete_m72995D0510B0D8EC24D1566C903C526D145F578E (void);
// 0x00000108 System.Boolean ClockStone.AudioObject::get_isFadingOutOrScheduled()
extern void AudioObject_get_isFadingOutOrScheduled_mC7ED0F40FB8542C440D2653D49050113619797B7 (void);
// 0x00000109 System.Boolean ClockStone.AudioObject::get_isFadingIn()
extern void AudioObject_get_isFadingIn_m978D5E9671955FACC7A9A7DAB1156C6FF0174310 (void);
// 0x0000010A System.Single ClockStone.AudioObject::get_pitch()
extern void AudioObject_get_pitch_m66A2EC1C50519C3C98578C8F5C473B92DBBFD7BE (void);
// 0x0000010B System.Void ClockStone.AudioObject::set_pitch(System.Single)
extern void AudioObject_set_pitch_m754A7894CD72B1440F26D56172DC4E44D8EAA839 (void);
// 0x0000010C System.Single ClockStone.AudioObject::get_pan()
extern void AudioObject_get_pan_mF94614A65E763B5517E6AE9C856B9023381288A9 (void);
// 0x0000010D System.Void ClockStone.AudioObject::set_pan(System.Single)
extern void AudioObject_set_pan_m1CE125BF5F4865947ED00EF5DCAEEB4F58056E60 (void);
// 0x0000010E System.Double ClockStone.AudioObject::get_audioObjectTime()
extern void AudioObject_get_audioObjectTime_mF3EB625AB53543CD2AE6CF787A9CD05D371DF23D (void);
// 0x0000010F System.Boolean ClockStone.AudioObject::get_stopAfterFadeOut()
extern void AudioObject_get_stopAfterFadeOut_m74F028C287C5DD0934523BEFF9F405E5FD32A854 (void);
// 0x00000110 System.Void ClockStone.AudioObject::set_stopAfterFadeOut(System.Boolean)
extern void AudioObject_set_stopAfterFadeOut_mB780C0D035532C99518DCA1DB6D7AACE190005B0 (void);
// 0x00000111 System.Void ClockStone.AudioObject::FadeIn(System.Single)
extern void AudioObject_FadeIn_m1E3A0C0CC1F554CFD276D5F141188F381D88C912 (void);
// 0x00000112 System.Void ClockStone.AudioObject::PlayScheduled(System.Double)
extern void AudioObject_PlayScheduled_m5CEE90BFD89D4856380BC32DD495B7A6A4C2D8C8 (void);
// 0x00000113 System.Void ClockStone.AudioObject::PlayAfter(System.String,System.Double,System.Single,System.Single)
extern void AudioObject_PlayAfter_m639B1539850A31AB79EC5FBD143BCC8BCF5DCC73 (void);
// 0x00000114 System.Void ClockStone.AudioObject::PlayNow(System.String,System.Single,System.Single,System.Single)
extern void AudioObject_PlayNow_m5D3356B046421790312F015A5527C700D985E6D0 (void);
// 0x00000115 System.Void ClockStone.AudioObject::Play(System.Single)
extern void AudioObject_Play_mA3CD358101CE9F3C7482883266D15BAF1BBCC7FA (void);
// 0x00000116 System.Void ClockStone.AudioObject::Stop()
extern void AudioObject_Stop_m05E92AEBFEED7FDD863CBEEB189B99CA7BFC6636 (void);
// 0x00000117 System.Void ClockStone.AudioObject::Stop(System.Single)
extern void AudioObject_Stop_m6BB671D8362BC2156ADDCEFAB4B7C4E1BA9DDA11 (void);
// 0x00000118 System.Void ClockStone.AudioObject::Stop(System.Single,System.Single)
extern void AudioObject_Stop_mEE44F331C9123517EE4FF57353417D11BCAA3123 (void);
// 0x00000119 System.Void ClockStone.AudioObject::FinishSequence()
extern void AudioObject_FinishSequence_m48CD494906842A309738F86D103958A0A1464B0E (void);
// 0x0000011A System.Collections.IEnumerator ClockStone.AudioObject::_WaitForSecondsThenStop(System.Single,System.Single)
extern void AudioObject__WaitForSecondsThenStop_m8ADAB0DE94E2F0AF56FEBDBEDC512253B93F19D0 (void);
// 0x0000011B System.Void ClockStone.AudioObject::FadeOut(System.Single)
extern void AudioObject_FadeOut_mF657F05390CBDB1026964B363149BD9ECFDAE15E (void);
// 0x0000011C System.Void ClockStone.AudioObject::FadeOut(System.Single,System.Single)
extern void AudioObject_FadeOut_m96292A89F534B0A920F4CA7DC26097C63208E726 (void);
// 0x0000011D System.Void ClockStone.AudioObject::Pause()
extern void AudioObject_Pause_m2E71E3CFBB6713F544539B966FF0759535D9D2FA (void);
// 0x0000011E System.Void ClockStone.AudioObject::Pause(System.Single)
extern void AudioObject_Pause_m60ED56AD83795BB69A0DA9B015A874415C7B8727 (void);
// 0x0000011F System.Void ClockStone.AudioObject::_PauseNow()
extern void AudioObject__PauseNow_mE056DC0B4B0C6A4852D6AFDA61D3326542BC4049 (void);
// 0x00000120 System.Void ClockStone.AudioObject::Unpause()
extern void AudioObject_Unpause_m37924F824AC43840A0FDC5ACAEA3E7A8A6893F54 (void);
// 0x00000121 System.Void ClockStone.AudioObject::Unpause(System.Single)
extern void AudioObject_Unpause_mA0F27F389514E0643B14140208FBF48AD84DEB08 (void);
// 0x00000122 System.Void ClockStone.AudioObject::_UnpauseNow()
extern void AudioObject__UnpauseNow_mA6F6C307CFFA8C3F0FDD94AC748E2CFEB61A8EF9 (void);
// 0x00000123 System.Collections.IEnumerator ClockStone.AudioObject::_WaitThenPause(System.Single,System.Int32)
extern void AudioObject__WaitThenPause_m2917D16A30ED1508B591713865BAABD7490AC041 (void);
// 0x00000124 System.Void ClockStone.AudioObject::_PauseAudioSources()
extern void AudioObject__PauseAudioSources_m1E26BCF5ABA372D4CD71A38563ED630DDB0257D7 (void);
// 0x00000125 System.Boolean ClockStone.AudioObject::IsPaused(System.Boolean)
extern void AudioObject_IsPaused_mD12935FC4707F89AD2C3247D84F83363D6672585 (void);
// 0x00000126 System.Boolean ClockStone.AudioObject::IsPlaying()
extern void AudioObject_IsPlaying_m0B9791708F69B24F952A25620D0BE94B623057DC (void);
// 0x00000127 System.Boolean ClockStone.AudioObject::IsPrimaryPlaying()
extern void AudioObject_IsPrimaryPlaying_mAD10E40EA66EA76543717AE4FB99F020DD6C042D (void);
// 0x00000128 System.Boolean ClockStone.AudioObject::IsSecondaryPlaying()
extern void AudioObject_IsSecondaryPlaying_m71812DAD40B61776F37031AFA0E1AF5A0B9DE236 (void);
// 0x00000129 UnityEngine.AudioSource ClockStone.AudioObject::get_primaryAudioSource()
extern void AudioObject_get_primaryAudioSource_m564625EA9319DB106D6A3750396F70B5881C7729 (void);
// 0x0000012A UnityEngine.AudioSource ClockStone.AudioObject::get_secondaryAudioSource()
extern void AudioObject_get_secondaryAudioSource_m6F50088F03A9F38D2A43D74104458080AE9C853A (void);
// 0x0000012B System.Void ClockStone.AudioObject::SwitchAudioSources()
extern void AudioObject_SwitchAudioSources_m10CD2CC122F98152732F378123029C51FC78FBD1 (void);
// 0x0000012C System.Void ClockStone.AudioObject::_SwitchValues(T&,T&)
// 0x0000012D System.Single ClockStone.AudioObject::get__volumeFromCategory()
extern void AudioObject_get__volumeFromCategory_m1725991C6C137E2227EA56E50731D6D60677D72F (void);
// 0x0000012E System.Single ClockStone.AudioObject::get__volumeWithCategory()
extern void AudioObject_get__volumeWithCategory_mCCFF1225F23AC1184CA1033FED08D6E59B6A4782 (void);
// 0x0000012F System.Single ClockStone.AudioObject::get__stopClipAtTime()
extern void AudioObject_get__stopClipAtTime_mBCD54A732DBFE50E636478E20A0CCE2857051DE0 (void);
// 0x00000130 System.Single ClockStone.AudioObject::get__startClipAtTime()
extern void AudioObject_get__startClipAtTime_mBB1416F98027BF56ECBA41958FB25363A8181DAC (void);
// 0x00000131 System.Void ClockStone.AudioObject::Awake()
extern void AudioObject_Awake_m490330743E605720C3877280AEB966DF25F8787B (void);
// 0x00000132 System.Void ClockStone.AudioObject::OnEnable()
extern void AudioObject_OnEnable_mD4FFE6C28310649786955C34860800FA83D8F870 (void);
// 0x00000133 System.Void ClockStone.AudioObject::OnDisable()
extern void AudioObject_OnDisable_m6406D78163C7A795834D9AC970286ABB51A15336 (void);
// 0x00000134 System.Void ClockStone.AudioObject::_CreateSecondAudioSource()
extern void AudioObject__CreateSecondAudioSource_m49F137313C2278123B7D04E3D8F59C570066FC6B (void);
// 0x00000135 System.Void ClockStone.AudioObject::_Set0()
extern void AudioObject__Set0_m4288010E694C41E42DB042BCEF5E3CC0AE0A3ADA (void);
// 0x00000136 System.Void ClockStone.AudioObject::_SetReferences0()
extern void AudioObject__SetReferences0_m849BE5264ECA98F43EE8C00480B6BB15312E0766 (void);
// 0x00000137 System.Void ClockStone.AudioObject::_PlayScheduled(System.Double)
extern void AudioObject__PlayScheduled_mF337C35BF233018481AB0F8F25A68F62F856104D (void);
// 0x00000138 System.Void ClockStone.AudioObject::_PlayDelayed(System.Single)
extern void AudioObject__PlayDelayed_m477DB5F0EBC503261C63F64D19037F488C6483B6 (void);
// 0x00000139 System.Void ClockStone.AudioObject::_OnPlay()
extern void AudioObject__OnPlay_mA74404657B8D2939E0292603BBE07C93D2F278B9 (void);
// 0x0000013A System.Void ClockStone.AudioObject::_Stop()
extern void AudioObject__Stop_mDF1AE2A5A7F72266B94AA419F243F2C758DB7DEB (void);
// 0x0000013B System.Void ClockStone.AudioObject::Update()
extern void AudioObject_Update_mA46A8360887367B8A67178AD3BFBE277F7C9FD1B (void);
// 0x0000013C System.Void ClockStone.AudioObject::_StartFadeOutIfNecessary()
extern void AudioObject__StartFadeOutIfNecessary_mF3670F52B5374045E174C32C75C9B3E8F4689E47 (void);
// 0x0000013D System.Boolean ClockStone.AudioObject::_IsAudioLoopSequenceMode()
extern void AudioObject__IsAudioLoopSequenceMode_mF7F606A1607AA7265D650B19DCD58F378B805836 (void);
// 0x0000013E System.Boolean ClockStone.AudioObject::_ScheduleNextInLoopSequence()
extern void AudioObject__ScheduleNextInLoopSequence_mC051E5A7C35D3E11F8B66FDE86F13CBF7B97137B (void);
// 0x0000013F System.Void ClockStone.AudioObject::_UpdateFadeVolume()
extern void AudioObject__UpdateFadeVolume_m1FB38B793417C6C5B708292EFD10E886BDE7A22B (void);
// 0x00000140 System.Single ClockStone.AudioObject::_EqualizePowerForCrossfading(System.Single)
extern void AudioObject__EqualizePowerForCrossfading_m1AE2A2C01C5DF0DFC974C8F5B66AED2CAD0F2144 (void);
// 0x00000141 System.Boolean ClockStone.AudioObject::get__shouldStopIfPrimaryFadedOut()
extern void AudioObject_get__shouldStopIfPrimaryFadedOut_mB9419D23CA2C725BDCCAF415FD2B2FE20CFFB190 (void);
// 0x00000142 System.Void ClockStone.AudioObject::OnApplicationPause(System.Boolean)
extern void AudioObject_OnApplicationPause_m43FEEFD6F02D11A48366719D0056EB0B4F4F62BA (void);
// 0x00000143 System.Void ClockStone.AudioObject::SetApplicationPaused(System.Boolean)
extern void AudioObject_SetApplicationPaused_mEE6AC1D3A7A671E24119DE71A62E82CED93A212A (void);
// 0x00000144 System.Void ClockStone.AudioObject::DestroyAudioObject()
extern void AudioObject_DestroyAudioObject_mE2D2BC83118A64F6520D5BF06AF76F7232DB90D3 (void);
// 0x00000145 System.Single ClockStone.AudioObject::TransformVolume(System.Single)
extern void AudioObject_TransformVolume_mB17969D18761D589E0EA39CEA1667A66419AC2A3 (void);
// 0x00000146 System.Single ClockStone.AudioObject::InverseTransformVolume(System.Single)
extern void AudioObject_InverseTransformVolume_mDBA1A47B85A8BE0F464EB0EA9A00BB8B39A818D0 (void);
// 0x00000147 System.Single ClockStone.AudioObject::TransformPitch(System.Single)
extern void AudioObject_TransformPitch_mD19D62731D7EF97FD9D98732A72D156CEE925804 (void);
// 0x00000148 System.Single ClockStone.AudioObject::InverseTransformPitch(System.Single)
extern void AudioObject_InverseTransformPitch_m58D69B1FA7D53C63B826E82D3DBFEE42D025BC78 (void);
// 0x00000149 System.Void ClockStone.AudioObject::_ApplyVolumeBoth()
extern void AudioObject__ApplyVolumeBoth_m54981F4B75E5A520DAEBB66CE7041CE90CB9B113 (void);
// 0x0000014A System.Void ClockStone.AudioObject::_ApplyVolumePrimary(System.Single)
extern void AudioObject__ApplyVolumePrimary_m7ED395302DB38EB78580B7F9E038C66AD3CE8105 (void);
// 0x0000014B System.Void ClockStone.AudioObject::_ApplyVolumeSecondary(System.Single)
extern void AudioObject__ApplyVolumeSecondary_m14CD474788BB158D9A33B1DCF5CBF27D86C0734C (void);
// 0x0000014C System.Void ClockStone.AudioObject::OnDestroy()
extern void AudioObject_OnDestroy_mBE259D6F2215D73C97EACA457D47D82D44631046 (void);
// 0x0000014D System.Void ClockStone.AudioObject::_RestoreOverrideAudioSourceSettings()
extern void AudioObject__RestoreOverrideAudioSourceSettings_m356B1A25509797DFE9714E51AA1D3478E6DE7D86 (void);
// 0x0000014E System.Boolean ClockStone.AudioObject::DoesBelongToCategory(System.String)
extern void AudioObject_DoesBelongToCategory_m2346104FC9506830BBB3FDA1A1D08E4C707451A7 (void);
// 0x0000014F System.Single ClockStone.AudioObject::_GetRandomLoopSequenceDelay(ClockStone.AudioItem)
extern void AudioObject__GetRandomLoopSequenceDelay_m23AFB2B78131DBCB7237EB8DE0D89FF0BF8AFD13 (void);
// 0x00000150 System.Void ClockStone.AudioObject::.ctor()
extern void AudioObject__ctor_m8A85B3BE9F77240BEC1A0FBC8EC2F051A2966730 (void);
// 0x00000151 System.Void ClockStone.AudioObject/AudioEventDelegate::.ctor(System.Object,System.IntPtr)
extern void AudioEventDelegate__ctor_mCFA5F0C7161978836E1723F84744CCA6E842FBBE (void);
// 0x00000152 System.Void ClockStone.AudioObject/AudioEventDelegate::Invoke(ClockStone.AudioObject)
extern void AudioEventDelegate_Invoke_m41886D3386CBDA6C9DDFD8AF46BDFFDAE26E929D (void);
// 0x00000153 System.IAsyncResult ClockStone.AudioObject/AudioEventDelegate::BeginInvoke(ClockStone.AudioObject,System.AsyncCallback,System.Object)
extern void AudioEventDelegate_BeginInvoke_mCEA1A7C2C9608726679A9B2470486477ECECD42A (void);
// 0x00000154 System.Void ClockStone.AudioObject/AudioEventDelegate::EndInvoke(System.IAsyncResult)
extern void AudioEventDelegate_EndInvoke_mB143BA6BFA8925442CF110F1C8318DFF58F3AC26 (void);
// 0x00000155 System.Void ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::.ctor(System.Int32)
extern void U3C_WaitForSecondsThenStopU3Ed__76__ctor_m5DDA60EBC326D1D3FFC858296CCF98B92FF225D5 (void);
// 0x00000156 System.Void ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::System.IDisposable.Dispose()
extern void U3C_WaitForSecondsThenStopU3Ed__76_System_IDisposable_Dispose_m3191FDCBB9064FFABA539AB7B8D1E150315D1AF8 (void);
// 0x00000157 System.Boolean ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::MoveNext()
extern void U3C_WaitForSecondsThenStopU3Ed__76_MoveNext_m186DEE9896F64DC79F5431DF69E7184B66A7170B (void);
// 0x00000158 System.Object ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m141F40A314ECCF21E980FBF5C6436206E5235FB7 (void);
// 0x00000159 System.Void ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::System.Collections.IEnumerator.Reset()
extern void U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_IEnumerator_Reset_mF72B523E7302A6CA5B499EF811A5EBDC0896175D (void);
// 0x0000015A System.Object ClockStone.AudioObject/<_WaitForSecondsThenStop>d__76::System.Collections.IEnumerator.get_Current()
extern void U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_IEnumerator_get_Current_m69DCFD72C63E8A4FA55F774BB70B3070546B9F0C (void);
// 0x0000015B System.Void ClockStone.AudioObject/<_WaitThenPause>d__86::.ctor(System.Int32)
extern void U3C_WaitThenPauseU3Ed__86__ctor_m1BDA461457E882576F04CE05AE8791044FDD4538 (void);
// 0x0000015C System.Void ClockStone.AudioObject/<_WaitThenPause>d__86::System.IDisposable.Dispose()
extern void U3C_WaitThenPauseU3Ed__86_System_IDisposable_Dispose_m46511038D58AEE9F4EBD59090A30B47F189ADAC8 (void);
// 0x0000015D System.Boolean ClockStone.AudioObject/<_WaitThenPause>d__86::MoveNext()
extern void U3C_WaitThenPauseU3Ed__86_MoveNext_m165C02EF6D004761CBE72373D5C2EF24A3097A98 (void);
// 0x0000015E System.Object ClockStone.AudioObject/<_WaitThenPause>d__86::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_WaitThenPauseU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8EAF2B62B42191EB3F29F00017B846814E85CA1 (void);
// 0x0000015F System.Void ClockStone.AudioObject/<_WaitThenPause>d__86::System.Collections.IEnumerator.Reset()
extern void U3C_WaitThenPauseU3Ed__86_System_Collections_IEnumerator_Reset_m932FE61892C90802C6A781726A614C20CD18A5E8 (void);
// 0x00000160 System.Object ClockStone.AudioObject/<_WaitThenPause>d__86::System.Collections.IEnumerator.get_Current()
extern void U3C_WaitThenPauseU3Ed__86_System_Collections_IEnumerator_get_Current_mF34D9ED7B8BBE87E947B980C2DB587D83D495784 (void);
// 0x00000161 System.Void ClockStone.PoolableExtensions::_SetActive(UnityEngine.GameObject,System.Boolean)
extern void PoolableExtensions__SetActive_m847F499D5C2D8A635CB92FE3B3F6B2D5BBBF2089 (void);
// 0x00000162 System.Boolean ClockStone.PoolableExtensions::_GetActive(UnityEngine.GameObject)
extern void PoolableExtensions__GetActive_m6ED3F5CE7259AC54A6ADAE60CE4739FF512AB76D (void);
// 0x00000163 System.Void ClockStone.PoolableObject::Awake()
extern void PoolableObject_Awake_m44149E9D3922BBFC21E8BD554B4AFF6602C8D3EF (void);
// 0x00000164 System.Void ClockStone.PoolableObject::OnDestroy()
extern void PoolableObject_OnDestroy_m0ED81C9FB8C4DBD4E026D4853F62B627BA1F377A (void);
// 0x00000165 System.Int32 ClockStone.PoolableObject::GetSerialNumber()
extern void PoolableObject_GetSerialNumber_m546018BEB42B14620C0699D8ACB831EE8D2777F6 (void);
// 0x00000166 System.Int32 ClockStone.PoolableObject::GetUsageCount()
extern void PoolableObject_GetUsageCount_m8F8EE9806CA4EE74AE2A60A11E38006CF32CF4F2 (void);
// 0x00000167 System.Int32 ClockStone.PoolableObject::DeactivateAllPoolableObjectsOfMyKind()
extern void PoolableObject_DeactivateAllPoolableObjectsOfMyKind_m7FB8BF1B07082590A268F21EE23B0B5E089894E8 (void);
// 0x00000168 System.Boolean ClockStone.PoolableObject::IsDeactivated()
extern void PoolableObject_IsDeactivated_mBCC08C6CD29CD2ACE1C2C2183FD0D43BC6A6275D (void);
// 0x00000169 System.Void ClockStone.PoolableObject::_PutIntoPool()
extern void PoolableObject__PutIntoPool_mF67B54ED9B1BA28AC04072648493C3C1BC5AC52B (void);
// 0x0000016A System.Void ClockStone.PoolableObject::TakeFromPool(UnityEngine.Transform,System.Boolean)
extern void PoolableObject_TakeFromPool_m6909B37433E7E6441895E0DB31595702F6BE16F8 (void);
// 0x0000016B System.Void ClockStone.PoolableObject::.ctor()
extern void PoolableObject__ctor_m5601824890188168CA58128F242D0E9B84AF993E (void);
// 0x0000016C System.Boolean ClockStone.ObjectPoolController::get_isDuringPreload()
extern void ObjectPoolController_get_isDuringPreload_mAB45F36DB7CE07912C90E2260A415D7C2C2F5259 (void);
// 0x0000016D System.Void ClockStone.ObjectPoolController::set_isDuringPreload(System.Boolean)
extern void ObjectPoolController_set_isDuringPreload_m179ACF20B6A9C1013B7B8D8CA87E7E7E2E0A0F4A (void);
// 0x0000016E UnityEngine.GameObject ClockStone.ObjectPoolController::Instantiate(UnityEngine.GameObject,UnityEngine.Transform)
extern void ObjectPoolController_Instantiate_mCCFC8F65563922BC6006E64CCF134497FF7640B5 (void);
// 0x0000016F UnityEngine.GameObject ClockStone.ObjectPoolController::Instantiate(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void ObjectPoolController_Instantiate_m8A9C4BBCDF7918B312B9FD4963D99F25AD2CD1B0 (void);
// 0x00000170 UnityEngine.GameObject ClockStone.ObjectPoolController::InstantiateWithoutPool(UnityEngine.GameObject,UnityEngine.Transform)
extern void ObjectPoolController_InstantiateWithoutPool_mE016A934F7E01FFCEFF7F2A7B83CDC3AFADAAB43 (void);
// 0x00000171 UnityEngine.GameObject ClockStone.ObjectPoolController::InstantiateWithoutPool(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void ObjectPoolController_InstantiateWithoutPool_mE340CA6B4F594FAFE428ECD795DFC6894200CA8C (void);
// 0x00000172 UnityEngine.GameObject ClockStone.ObjectPoolController::_InstantiateGameObject(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void ObjectPoolController__InstantiateGameObject_mB106642C3439E343D6AD3D13CD140E1686E7F499 (void);
// 0x00000173 System.Void ClockStone.ObjectPoolController::Destroy(UnityEngine.GameObject)
extern void ObjectPoolController_Destroy_mE579761AFDCA8A1CB45ED4D355DD5E80FCD128E3 (void);
// 0x00000174 System.Void ClockStone.ObjectPoolController::DestroyImmediate(UnityEngine.GameObject)
extern void ObjectPoolController_DestroyImmediate_mB44AEA643F3D5DBA8AF3B607A45A08B567E75D45 (void);
// 0x00000175 System.Void ClockStone.ObjectPoolController::Preload(UnityEngine.GameObject)
extern void ObjectPoolController_Preload_mDA376DA4A680AF4BF6CE8BBA45A0BE80E7D1B2D0 (void);
// 0x00000176 ClockStone.ObjectPoolController/ObjectPool ClockStone.ObjectPoolController::_GetPool(ClockStone.PoolableObject)
extern void ObjectPoolController__GetPool_mB45AD133527844543939CC7413734D0213F7730C (void);
// 0x00000177 System.Void ClockStone.ObjectPoolController::_DetachChildrenAndDestroy(UnityEngine.Transform,System.Boolean)
extern void ObjectPoolController__DetachChildrenAndDestroy_mC40DB44849951A8B8D9DE407140DF528867C74A7 (void);
// 0x00000178 System.Void ClockStone.ObjectPoolController::.cctor()
extern void ObjectPoolController__cctor_m782292556A296FD7DEE15327035D5F9DBE7D79D2 (void);
// 0x00000179 UnityEngine.Transform ClockStone.ObjectPoolController/ObjectPool::get_poolParent()
extern void ObjectPool_get_poolParent_mD8667A9BA2E76B2E3B491FF27E91D3AC9EC05A36 (void);
// 0x0000017A System.Void ClockStone.ObjectPoolController/ObjectPool::.ctor(UnityEngine.GameObject)
extern void ObjectPool__ctor_m827D8E689D9C5D4188521CA1FE31B1D84F897928 (void);
// 0x0000017B System.Void ClockStone.ObjectPoolController/ObjectPool::_ValidatePooledObjectDataContainer()
extern void ObjectPool__ValidatePooledObjectDataContainer_mF7B5D00D7B61788222BEFD64CFE7A198F06160F2 (void);
// 0x0000017C System.Void ClockStone.ObjectPoolController/ObjectPool::_ValidatePoolParentDummy()
extern void ObjectPool__ValidatePoolParentDummy_m02637E13866A0C606C142AE0B875D3C01249495A (void);
// 0x0000017D System.Void ClockStone.ObjectPoolController/ObjectPool::Remove(ClockStone.PoolableObject)
extern void ObjectPool_Remove_m2453AE200459FE7CD29A3818837B2309ED72CDBE (void);
// 0x0000017E System.Int32 ClockStone.ObjectPoolController/ObjectPool::GetObjectCount()
extern void ObjectPool_GetObjectCount_m7DE43D40FF05066E0D7EF1EBDDB39664B37D3204 (void);
// 0x0000017F UnityEngine.GameObject ClockStone.ObjectPoolController/ObjectPool::GetPooledInstance(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Boolean,UnityEngine.Transform)
extern void ObjectPool_GetPooledInstance_m0535CC67B77BA9BCFEAF561A719A6E79BC4D8379 (void);
// 0x00000180 ClockStone.PoolableObject ClockStone.ObjectPoolController/ObjectPool::PreloadInstance(System.Boolean)
extern void ObjectPool_PreloadInstance_m899A7277128D4EACF66738EC825580E9A6BBEBAF (void);
// 0x00000181 ClockStone.PoolableObject ClockStone.ObjectPoolController/ObjectPool::_NewPooledInstance(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Boolean,System.Boolean)
extern void ObjectPool__NewPooledInstance_m8371E8E63F03BADBB2E383C0D1699CE09F245A82 (void);
// 0x00000182 System.Int32 ClockStone.ObjectPoolController/ObjectPool::_SetAllAvailable()
extern void ObjectPool__SetAllAvailable_mF49B907C15D7586E947F9B03C15CADCF62AFB58D (void);
// 0x00000183 System.Void ClockStone.ObjectPoolController/ObjectPool::CallMethodOnObject(UnityEngine.GameObject,System.String,System.Boolean,System.Boolean,System.Boolean)
extern void ObjectPool_CallMethodOnObject_m31352F689F489E3FF4E48F8251BBA89290547009 (void);
// 0x00000184 System.Void ClockStone.PoolableReference`1::.ctor()
// 0x00000185 System.Void ClockStone.PoolableReference`1::.ctor(T)
// 0x00000186 System.Void ClockStone.PoolableReference`1::.ctor(ClockStone.PoolableReference`1<T>)
// 0x00000187 System.Void ClockStone.PoolableReference`1::Reset()
// 0x00000188 T ClockStone.PoolableReference`1::Get()
// 0x00000189 System.Void ClockStone.PoolableReference`1::Set(T)
// 0x0000018A System.Void ClockStone.PoolableReference`1::Set(T,System.Boolean)
// 0x0000018B System.Type ClockStone.IRegisteredComponent::GetRegisteredComponentBaseClassType()
// 0x0000018C System.Void ClockStone.RegisteredComponent::Awake()
extern void RegisteredComponent_Awake_mAA516B5C89747B659CCCDE1824881E2836E20B0F (void);
// 0x0000018D System.Void ClockStone.RegisteredComponent::OnDestroy()
extern void RegisteredComponent_OnDestroy_mFD66FB92672D6A0DE4E95A3771062155C16A43E2 (void);
// 0x0000018E System.Type ClockStone.RegisteredComponent::GetRegisteredComponentBaseClassType()
extern void RegisteredComponent_GetRegisteredComponentBaseClassType_m4888CBFF4EA46982DF38E8F15535219DBE4BCCE5 (void);
// 0x0000018F System.Void ClockStone.RegisteredComponent::.ctor()
extern void RegisteredComponent__ctor_mBEFF099BC32076D081C75A03724BE95FF1CD3D3E (void);
// 0x00000190 T[] ClockStone.RegisteredComponentController::GetAllOfType()
// 0x00000191 System.Object[] ClockStone.RegisteredComponentController::GetAllOfType(System.Type)
extern void RegisteredComponentController_GetAllOfType_m827B78DEB880BAA0E87ED44443CDE44B95568080 (void);
// 0x00000192 System.Int32 ClockStone.RegisteredComponentController::InstanceCountOfType()
// 0x00000193 ClockStone.RegisteredComponentController/InstanceContainer ClockStone.RegisteredComponentController::_GetInstanceContainer(System.Type)
extern void RegisteredComponentController__GetInstanceContainer_m4AE656B6C136FBF3CE54A69AA8AAA6BA0875EFAD (void);
// 0x00000194 System.Void ClockStone.RegisteredComponentController::_RegisterType(ClockStone.IRegisteredComponent,System.Type)
extern void RegisteredComponentController__RegisterType_mF38D17FC3438046ADBA4C4DA080BEBB5DDB9C5FD (void);
// 0x00000195 System.Void ClockStone.RegisteredComponentController::_Register(ClockStone.IRegisteredComponent)
extern void RegisteredComponentController__Register_m24751ECE64B7C7CAF4900008991DC5E7DF0F50E3 (void);
// 0x00000196 System.Void ClockStone.RegisteredComponentController::_UnregisterType(ClockStone.IRegisteredComponent,System.Type)
extern void RegisteredComponentController__UnregisterType_m0DA609F934E164E53D8C5338CF033E65DA0F454D (void);
// 0x00000197 System.Void ClockStone.RegisteredComponentController::_Unregister(ClockStone.IRegisteredComponent)
extern void RegisteredComponentController__Unregister_mB2C66F62730FDE21B28BC81B79B880ACC530E0FA (void);
// 0x00000198 System.Void ClockStone.RegisteredComponentController::.cctor()
extern void RegisteredComponentController__cctor_m88749DD8B506005BD2805970E94756F97BA30DA1 (void);
// 0x00000199 System.Void ClockStone.RegisteredComponentController/InstanceContainer::.ctor()
extern void InstanceContainer__ctor_m2C5A4BD2C07BD2466C94781477FF462315867B3B (void);
// 0x0000019A System.Double ClockStone.SystemTime::get_time()
extern void SystemTime_get_time_mB1B5E5B9244E38D7D6C33064583F0E557F5DEEF1 (void);
// 0x0000019B System.Double ClockStone.SystemTime::get_timeSinceLaunch()
extern void SystemTime_get_timeSinceLaunch_mA158534813497E7B9131AE511A30484CAA6D4948 (void);
// 0x0000019C System.Void ClockStone.SystemTime::.cctor()
extern void SystemTime__cctor_m3F778ED66B232FDD891BAE1BCC49C362A22686D6 (void);
// 0x0000019D T ClockStone.UnitySingleton`1::GetSingleton(System.Boolean,System.Boolean,System.Boolean)
// 0x0000019E System.Void ClockStone.UnitySingleton`1::.ctor()
// 0x0000019F System.Void ClockStone.UnitySingleton`1::_Awake(T)
// 0x000001A0 System.Void ClockStone.UnitySingleton`1::_Destroy()
// 0x000001A1 System.Void ClockStone.UnitySingleton`1::_AwakeSingleton(T)
// 0x000001A2 System.Void ClockStone.UnitySingleton`1::.cctor()
// 0x000001A3 System.Boolean ClockStone.ISingletonMonoBehaviour::get_isSingletonObject()
// 0x000001A4 T ClockStone.SingletonMonoBehaviour`1::get_Instance()
// 0x000001A5 T ClockStone.SingletonMonoBehaviour`1::DoesInstanceExist()
// 0x000001A6 System.Void ClockStone.SingletonMonoBehaviour`1::ActivateSingletonInstance()
// 0x000001A7 System.Void ClockStone.SingletonMonoBehaviour`1::SetSingletonAutoCreate(UnityEngine.GameObject)
// 0x000001A8 System.Void ClockStone.SingletonMonoBehaviour`1::Awake()
// 0x000001A9 System.Void ClockStone.SingletonMonoBehaviour`1::OnDestroy()
// 0x000001AA System.Boolean ClockStone.SingletonMonoBehaviour`1::get_isSingletonObject()
// 0x000001AB System.Void ClockStone.SingletonMonoBehaviour`1::.ctor()
static Il2CppMethodPointer s_methodPointers[427] = 
{
	AudioController_set_DisableAudio_m295462AF48B514A279B22F7A65A0D277206ACC7B,
	AudioController_get_DisableAudio_m597B19459A2F1F001DB3EECDDCC97721CD5D8896,
	AudioController_get_isAdditionalAudioController_mEB7B0AB2C7D9FB339CB03685B2922F6290DB8DB8,
	AudioController_set_isAdditionalAudioController_mA80630A3E3569DB85A7B7D4F193A34F72379D199,
	AudioController_get_Volume_mC5F1B0459B1EE3C6F6701C7F8F03530C0411CC89,
	AudioController_set_Volume_m1D7ED0C201F482C6472C6DC3BA1F1FB01A253619,
	AudioController_get_musicEnabled_m8BDE5B4C8A0978FCE880A5B296472CB80A0B8B30,
	AudioController_set_musicEnabled_m65386EAB8EF87FED1D170BF0DFB210B5C1B9939B,
	AudioController_get_ambienceSoundEnabled_m0823A620254DA9EB78ADE7FF74B6A6AD9D67FDC9,
	AudioController_set_ambienceSoundEnabled_mF50228ED3F4EAF97B3251D08856B981385C3CA4E,
	AudioController_get_soundMuted_m668DF129821C846D678564C93E19AC1A73F7B5E1,
	AudioController_set_soundMuted_m6041BF17A04B093927F7E0054B80FC2EB046B9BD,
	AudioController_get_musicCrossFadeTime_In_mB4950C27B87B8CEE715514EF71FE68E1A76629DE,
	AudioController_set_musicCrossFadeTime_In_m80917995680C2ECC0CEC6ACED75E19C94AE0963C,
	AudioController_get_musicCrossFadeTime_Out_m9940A28243D69E595F2B243EBD0293E18C4602A8,
	AudioController_set_musicCrossFadeTime_Out_mE45BB7C5BF27FBBAECE5F891C62C7BEBFF7DC502,
	AudioController_get_ambienceSoundCrossFadeTime_In_m748B2EE6E00FC8009FF84EB5376639E287885E94,
	AudioController_set_ambienceSoundCrossFadeTime_In_mFC6FB0595FDEE15C62441DA35B70E9FC22B80ED5,
	AudioController_get_ambienceSoundCrossFadeTime_Out_mB3DF6B8D722DA539EBB91882580BC5E6FF1AE5D3,
	AudioController_set_ambienceSoundCrossFadeTime_Out_mC8A46AA2413E7BC159BA13A36BEFF7F53E1589BB,
	AudioController_get_systemTime_m494F4E9476D0E721018B85D396745B45B4C0926D,
	AudioController_get_systemDeltaTime_mFA763378659DE5CFFC0C03A67F1B87750ED1C875,
	AudioController_set_musicParent_m6EDD0A0A7104E4468E0FAA96260FF231BCD7742C,
	AudioController_get_musicParent_m814A96325AE3A6611017552F27DAB200AB563256,
	AudioController_set_ambienceParent_m59EC177F59C61C7D7980E0D66C18BE74C44C4C4A,
	AudioController_get_ambienceParent_m2C30FD86590D662DFE3F1FF88970D16BD5FB65A8,
	AudioController_PlayMusic_m482EBCA3BDF1843F026B88EB07C07B473DEE6E34,
	AudioController_PlayMusic_m8024CA856B4E8D64F4EA685B6168B23DC103AA63,
	AudioController_PlayMusic_m0E426FF87EAA28F44943473A88D3471DAB4F51FC,
	AudioController_StopMusic_m2A2C7082F53E6F359533B4ADFAB6DC80EDF150D0,
	AudioController_StopMusic_m6034EF055E2385A49F2BD4261BD746A886276A74,
	AudioController_PauseMusic_mFC1C5F498122990BD5E8BC6F0357DF5576779B81,
	AudioController_IsMusicPaused_m37DA8D3596511ECB7ED8327739729634B2ECE741,
	AudioController_UnpauseMusic_m4129185AB4275627319452CDE83678B1DA28AF80,
	AudioController_PlayAmbienceSound_mF1A9705DC376A95631003986D617DC10DD64D7F7,
	AudioController_PlayAmbienceSound_mB9DBB0ECD24ABECCE3EAAE1AF31C89EFDE6FDB7E,
	AudioController_PlayAmbienceSound_m297559B4EB494EF8CC94EA90990223F33B4DF2D3,
	AudioController_StopAmbienceSound_m871EBF1B106DB083A2DC7414C91540C46D1FB49F,
	AudioController_StopAmbienceSound_m5E1CD4A36F947AD0944D783DF3431DB56173388B,
	AudioController_PauseAmbienceSound_mAD9BD0FB011E255206F394713141F0C17DCC4B8F,
	AudioController_IsAmbienceSoundPaused_m0B5EC151F775499153AB71A9BC3CDA2778EA8B2E,
	AudioController_UnpauseAmbienceSound_mE058348877FFA74441D37DD6F449F7D60F6CA635,
	AudioController_EnqueueMusic_m3098EA923E8ADEE146DB0F5667D74C7D2C437005,
	AudioController__GetCurrentPlaylist_mB77BE8AD3FAAB01FFE4A4E4E98C521E1AFE6762B,
	AudioController_GetPlaylistByName_m89C7AC165C3A7B5598CB26414C6AE197E9EE9BF7,
	AudioController_GetMusicPlaylist_mDEC93E65326944F36A09B52C5F37015A0E0DF286,
	AudioController_SetCurrentMusicPlaylist_mE9D141ED1922E7CE979BD17873A1E39ADAC43941,
	AudioController_PlayMusicPlaylist_m87CA056EBD775A831AE63648EFE1A5B44488367C,
	AudioController_PlayNextMusicOnPlaylist_m7F170CBF479F06FDEEA7921F5D766D2BAD9226C2,
	AudioController_PlayPreviousMusicOnPlaylist_m35AFAE185D7127288CE54D88D8BF9FF074482176,
	AudioController_IsPlaylistPlaying_m17E58AC6179961CE02D27167EB32FB1FD2F97F2A,
	AudioController_ClearPlaylists_m92555392BE58F316523C0BE570A079CA4499D49A,
	AudioController_AddPlaylist_m82BEA18EFCEEF73A7EE01CCA3F39800C36555E3D,
	AudioController_Play_m123199DEFD5FBB4B38E69D36C0DCB384E593641E,
	AudioController_Play_m1C28BA01681BA3F6E07E29A328D76BCA515044BA,
	AudioController_Play_mFEC54A6857032C457243D0A7F1F75C5F56892752,
	AudioController_Play_m2983C1AAF496D16B202DB1E9A300F20D8106F8AC,
	AudioController_Play_m237616BA746863A4843ABFCF56AC2B213CA4ADA4,
	AudioController_Play_m41F6CD5F387B1D5F5B84E4A47858D37B49C679C5,
	AudioController_PlayScheduled_mA8C31B12A2530892AFC7FD36C0619D6D87683ADB,
	AudioController_PlayAfter_mC002DD4EFE1067C0FE73D9F277A29CC125CA91DF,
	AudioController_Stop_m8D45A372C1B8F756B76861B732C9BF5777C0A67E,
	AudioController_Stop_m1D09E9179DC6D11DB6C19CEEE141870EAB1928EA,
	AudioController_StopAll_m2A6505718A6F3E6D7B241A524BA18D829F8516EA,
	AudioController_StopAll_m20BCF9359A00CBC63B8E00D2CFDDB93A79902ADC,
	AudioController_PauseAll_mBD74772389C3579B83A1D4D90D8A534FE9121B39,
	AudioController_UnpauseAll_m0207E757C9B874E96177A2A11FBC7F22DC51038D,
	AudioController_PauseCategory_mD371BE130C9C7461AB6CE4793144258E1FF0B3F6,
	AudioController_UnpauseCategory_mBBD59D9DC2EA41BCEC033331160A7BEE37750E4F,
	AudioController_StopCategory_mF187C966C3DD5BE6E69951717C2E63DDE04980F1,
	AudioController_IsPlaying_m52697A76ABB12E37C64F055DB2C03E2DFBD30138,
	AudioController_GetPlayingAudioObjects_m083C5E263EFDB1D14617D0BB4E2CEDB92BCEE8C7,
	AudioController_GetPlayingAudioObjectsInCategory_m4866203456FF37B473BA0CDC157EEB468F082C49,
	AudioController_GetPlayingAudioObjects_m11D79A5F8F90A49735F6BE2CD3C387AA6A38476A,
	AudioController_GetPlayingAudioObjectsCount_mE7AD0D99E5F5B13D898B3CE470764C81F2C44C60,
	AudioController_EnableMusic_m56B9FCF57FAB083D86BF6A3792FC58244F064816,
	AudioController_EnableAmbienceSound_m3EE7D636F6BF291BB2FB3851D153A1E5943C5A20,
	AudioController_MuteSound_mBEFC13772A7D5601E317E27AD175A01E0CB14A5C,
	AudioController_IsMusicEnabled_m32D131BE3DC5FF134C4AC111865DD3C8D11A5497,
	AudioController_IsAmbienceSoundEnabled_m6CF0C699F21BB238E6C077371AFF90403DD74D7F,
	AudioController_IsSoundMuted_m0ABD23FCCC1DE80620F824ED3CFF427F1A9B1FE8,
	AudioController_GetCurrentAudioListener_mE0617F3C93ECC5B545B008740B1BD0534328058E,
	AudioController_GetCurrentMusic_m79ACF97E79AA223EA5B83D4110B0D5A90AADA492,
	AudioController_GetCurrentAmbienceSound_m920DB6EF34C1B39CA5772445DF5C430593537F27,
	AudioController_GetCategory_mAFE4AFA000CE187E9AC655EE2FF064A060B14A97,
	AudioController_SetCategoryVolume_m881B91F2C5439C19DA908ECBBB7A44584145857F,
	AudioController_GetCategoryVolume_m763C4DCA2DECF03CD5CB8DE77491B93E3AA83F18,
	AudioController_FadeOutCategory_m63FB9CC73121D5F9516CB7547C7A2CC2E7896142,
	AudioController_FadeInCategory_m271FF67702CE8F90E4465051E45DF2E86FEA8850,
	AudioController_SetGlobalVolume_mF74923143285431C8E65E6126B770F9AE7F6F6A2,
	AudioController_GetGlobalVolume_m45AB41B17355297BB828F53FC82E77233F713EBD,
	AudioController_NewCategory_mF3414A42AEEB4CDC76DA76C22BA5283759E5B078,
	AudioController_RemoveCategory_mE54A12EA93F35F2AEDE8866F1FF4F0A646DE9773,
	AudioController_AddToCategory_m84197E4C3CE5991B301B09605BCED7A68CAE4764,
	AudioController_AddToCategory_m36BBE2E56B8E543543741423F445857217859397,
	AudioController_RemoveAudioItem_mDD50029C13DE8265B4120190BC91F340C6642CF0,
	AudioController_IsValidAudioID_m63ECE8BA90CEC294E016DDA06A670AFC91D26D16,
	AudioController_GetAudioItem_mE6C4344C17CD4F2B58DABFA0B33E9C906D5B1F79,
	AudioController_DetachAllAudios_m8EE8F26D5FD9CB58E9E2FB49D7A5C335187CA147,
	AudioController_GetAudioItemMaxDistance_mA3F56641F604FBB85EAB65CCB190D5408B46C35C,
	AudioController_UnloadAllAudioClips_mE7645242ECB7B53B0CFE900734B867A5F07AFD01,
	AudioController_set__currentMusic_mB8DF0925536F6BADF2679E67C8FBABA5BF1BFA6B,
	AudioController_get__currentMusic_mD95BC00BC979E0AC7D25672AC24D589782F6D739,
	AudioController_set__currentAmbienceSound_m21C624C8A8CE79EA6388829B23E9572AE1E27919,
	AudioController_get__currentAmbienceSound_m044706F14050DDF41A946D6BBC6EEA9C9CD877C4,
	AudioController__ApplyVolumeChange_m5F3A39CC1F16D858E99AD1CEB59F8DB759362B20,
	AudioController__GetAudioItem_m513C80870DFDA93ADEE36E5B128978CDA9CD3637,
	AudioController__PlayMusic_m50404041B9FF41BA6B826CE6B9A3DDE5086C0118,
	AudioController__PlayAmbienceSound_m3263BB7433BEC78B297F6A35237F893B6D1FAE11,
	AudioController__StopMusic_m7FE0EBB83A4ECE7885D8478639EA63D3D602FF0A,
	AudioController__PauseMusic_mB4AB1A62CCAC9B67A7F3A2729920BD9EB66724E3,
	AudioController__StopAmbienceSound_mCC471D17095B3FF18E1F5A44B84BF2C5D414FF15,
	AudioController__PauseAmbienceSound_mB7183135C5A01916178865FC0F48C294891509DD,
	AudioController__PlayMusic_mE4EA2492EAAA5499FD29E5985F92AF9312D99C1A,
	AudioController__PlayAmbienceSound_m7C8489D595CFAACE867A9FCB107FD4B3FA78C2FB,
	AudioController__EnqueueMusic_m7EF535DCCA9CBD73FA25BC12FB2857882D29859C,
	AudioController__PlayMusicPlaylist_mEA5069CA5CC87E5551621752BB11A56F2747D04B,
	AudioController__PlayMusicTrackWithID_mAC2C6FBBC4D3965105A9E5D57B2A75D2AA988670,
	AudioController__PlayNextMusicOnPlaylist_m90E1888D7CD66AA46785FB544C226B2BCE546270,
	AudioController__PlayPreviousMusicOnPlaylist_m2347CD04E54C0E58EA6C2DE9BDD24E5F69D6C204,
	AudioController__ResetLastPlayedList_m16138A0E8A544724F1C90993422E2082A68F5765,
	AudioController__GetNextMusicTrack_mCA218047D42CF9539961C1E4F3987CBA5E46AD3E,
	AudioController__GetPreviousMusicTrack_m349F522494425EEA2AD31EA3EE28A1DF3A091D03,
	AudioController__GetPreviousMusicTrackShuffled_m3AEFBC35AE6A82DBDDF9FF74765EAE64F00D74A6,
	AudioController__RemoveLastPlayedOnList_mE502C5E8FA0610FE6C1236E131FF4236E9F62DD9,
	AudioController__GetNextMusicTrackShuffled_mBCCF02A977878C0265EE2165CF68A5D33D612256,
	AudioController__GetNextMusicTrackInOrder_m4FC46D045122ADCA4AF0965A2099D81134773EAA,
	AudioController__GetPreviousMusicTrackInOrder_m18FB811DDD1E97C1F8D30BB2684F8DAFFFE82778,
	AudioController__PlayEx_mF576955AC35F19CAACB55AE246637C17E5DECA0B,
	AudioController_PlayAudioItem_mAAD24E42B8E0077252698974E18159670A1653B6,
	AudioController__GetCategory_mE2649060C97060F0EC8DC2A0FEF97AF63F527EBE,
	AudioController_Update_m96D8EC49BFAC67A6399CCD916F089CA88944C9F3,
	AudioController__UpdateSystemTime_m75EA3E855D8B506A4A38781A184112DF91A8D21F,
	AudioController_Awake_m9C4187D8D291F25B1E4CB38FF79B05E880F65782,
	AudioController_OnEnable_m8C791747C9E414D616A557DCE48B7AD63828ECE0,
	AudioController_OnDisable_mAB2D7A84B62BD5FB8DAC5515A6C0BB5365CB2654,
	AudioController_get_isSingletonObject_m5354A3D1BB446AB255A14E05E05C305C9BE2C348,
	AudioController_OnDestroy_m807BE2EF8842B4C1C418B2A39F9A480E4711BDBA,
	AudioController_AwakeSingleton_m31CC7DAA13683866880D1B4C83CDC08D5AF3B831,
	AudioController__ValidateCategories_m471CC1EF12524131EAA46FB192DFB34A47C6A6D0,
	AudioController__InvalidateCategories_m95513017C4C25D8E1E3C4D61BD9A4B159417DF4E,
	AudioController_InitializeAudioItems_mA3584EAF35AC47EB92239EED9556DF08F70D944E,
	AudioController__InitializeAudioItems_m77C382C842E8E39E37BCF9D45A71E73E5CAE423E,
	AudioController__RegisterAdditionalAudioController_m2E3E6A2AAE20F8F815E8D4E8C788EE72569EBA14,
	AudioController__SyncCategoryVolumes_mFEF1D78C5E81B6E0C015AF6C3EEEC997D34E3F19,
	AudioController__UnregisterAdditionalAudioController_m9092623D5DF3410E0E5E2C5D617853E96E980974,
	AudioController__GetAllCategories_m1EF772C7BC721F0E7EE8078A6022FF118A3452E5,
	AudioController_PlayAudioSubItem_m5A052936BD9CDEC341CE2FFC17B06AFD8C71AC10,
	AudioController__GetAudioController_mF09635747A9D82F5557ECCFB58918B70B018D33F,
	AudioController__NotifyPlaylistTrackCompleteleyPlayed_mE299DF505D3F6EC3AE11291C5CE8FA0ADABE5F62,
	AudioController__ValidateAudioObjectPrefab_mA98897503247F5534054C8445E5AB4D53D32DAA4,
	AudioController_OnAfterDeserialize_mCAB707ED504B020A4DF2ED2B86972A5E4439EE32,
	AudioController_OnBeforeSerialize_mCF32E10DAC00F1BC03DF96A0E0167B33988D4CE0,
	AudioController__SetDefaultCurrentPlaylist_m75BA2EBEDEAB1F97D8773ED5CD06B82011FE728C,
	AudioController__ctor_m4707A1CF2E95213275460BDC4A6074ACBBC6E87E,
	AudioController__cctor_mE2D45A4525CB11BF32141C2C6853AAC9AC679551,
	AudioController_CurrentInspectorSelection__ctor_m0D21B27029BA5D45C1BA68D6CE3CCCE56284E8AA,
	NULL,
	NULL,
	NULL,
	MessengerThatIncludesInactiveElements_InvokeIfExists_mBD01E54ABE7D247127F96774E13E87B46F5C6AF9,
	MessengerThatIncludesInactiveElements_InvokeIfExists_m35EEE08888B035FD536F0B47EFE8932381A1089A,
	MessengerThatIncludesInactiveElements_InvokeMethod_m538E148F60B7753193A8FB9CFE4FC9CE82B7514E,
	MessengerThatIncludesInactiveElements_InvokeMethod_m316C847913487369D808120B6E0083EFEC2E103A,
	MessengerThatIncludesInactiveElements_InvokeMethod_mBB30CD88F60BFDF1081BF6D5F2BF0067306FE8A5,
	MessengerThatIncludesInactiveElements_InvokeMethod_mEFF30B7D3BA23590C48A3E8E60DEA05B7427D24F,
	MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m4B1666B6F2E4815183BCFE5479B8BF926522CCBA,
	MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m211A9231FDD6D440B8E6805B178293805D10BB74,
	MessengerThatIncludesInactiveElements_InvokeMethodInChildren_m0D710F984A550EA1BC23B0A69FCF7D3D0A5E8C98,
	MessengerThatIncludesInactiveElements_InvokeMethodInChildren_mB00BBF1DE4BF1DEBA602A6EECA63E136FFE61E05,
	MessengerThatIncludesInactiveElements_SendMessageUpwardsToAll_mC19DBE1962B0749DE1D968F7BD02ECCC60DEE5CB,
	MessengerThatIncludesInactiveElements_SendMessageUpwardsToAll_m5254694F7D95304B8086953544C89BBDFCD577EC,
	MessengerThatIncludesInactiveElements__cctor_m9F48393AEE32910461FB91980778084F756D51F4,
	AudioControllerHelper__ChooseSubItems_m08A389B0132A4281EFC1E95EF8AEA2B184B64B58,
	AudioControllerHelper__ChooseSingleSubItem_mF91C8E0BA02FA103F6534593E1D15F8E9122DB21,
	AudioControllerHelper__ChooseSingleSubItem_m7A9AE766A27A20CF7CD3E5CC9160649491C96804,
	AudioControllerHelper__ChooseSubItems_m44F582836C1AA80EDE2D99C3A7E6903DE9E9CD7D,
	AudioControllerHelper__ChooseRandomSubitem_m29E26B7FB3BBF372CF842CCA6637F4663F0F5087,
	AudioControllerHelper_isOdd_mEC1A7FD80C3E1BEA384CF1E9ACCA1710CEC4E232,
	AudioFader_get_time_m699059A9841FF9E9E62CD3772C234974E556796A,
	AudioFader_set_time_m99988CD4AA992FB991096ECC9843A2FB16252AD0,
	AudioFader_get_isFadingOutComplete_m2EF4209002BBA0239226D7E180D3B1A41FC1AD5F,
	AudioFader_get_isFadingOut_m0637F9F447EA58C3CF820F18C27150058BDCCD3D,
	AudioFader_get_isFadingOutOrScheduled_m3178E66184742AB425E4E622D9D644024FC7DCF9,
	AudioFader_get_isFadingIn_m472451A43FB53B50B7B7DC982ADD6E60AB204BD0,
	AudioFader_Set0_m6360361DEB7A7EC1A8B67108865494297472E8A5,
	AudioFader_FadeIn_m808BAC5AC14F770F8AB6595DA90699FA92DD180C,
	AudioFader_FadeIn_m72161BBA10B01D475C1E38E96904AC6B7B9455A2,
	AudioFader_FadeOut_mE3C09C319302135EF7848D8B5FFCB60B39715FD0,
	AudioFader_Get_m09EB22A5DDC2235A282C7F46E50D1CB7722B2CFA,
	AudioFader_Get_mB2BE5B156BDF16054DEEEE2B146EAC7AC67D16EE,
	AudioFader__GetFadeOutValue_m23BC0E8A4DF42C8EA640D4F6D6E2447241D70AFB,
	AudioFader__GetFadeInValue_m6B80801F521CA0C31DA73A1EEBC514BD155CCF31,
	AudioFader__GetFadeValue_m1BE3C1AD7DF1EF0805C41483C854F8A80A58851C,
	AudioFader__ctor_mFF2EE27126F60A3412FB4BE928D52ACCD0EF3531,
	AudioCategory_get_Volume_m510E2D4A0F47677FB01D24911E57989440B5218A,
	AudioCategory_set_Volume_m7FE0FBA212F03CCB180B2CD9B83A00B0E9777D6D,
	AudioCategory_get_VolumeTotal_mE6562564CFAC2C57A194E752EBEBE0273CBA0D57,
	AudioCategory_set_parentCategory_mA66D4CE43FE5D599DCEB65188B807E6D938419A5,
	AudioCategory_get_parentCategory_mA5C8A8B472DA464637D7D79852333EBE4D857BF0,
	AudioCategory_get_audioFader_mA21BBFBADD0F8DEB8D18164EDADF8076D043E440,
	AudioCategory_get_audioController_m877D306C7B461CE160E5B7D9EC387779A77E2E14,
	AudioCategory_set_audioController_m41724F6F6CBEA516256E65EC581A18D9D61A6D95,
	AudioCategory__ctor_mC290E34437C04DD36A8048F9AEE47E5A71AC316A,
	AudioCategory_GetAudioObjectPrefab_m672E92C3D772A43AFF6B8868DD9842D25C994641,
	AudioCategory_GetAudioMixerGroup_mDCA56ABC5DE2F75A43A35929858A9E92CD4F2209,
	AudioCategory__AnalyseAudioItems_m584E9DB5796EF54AFC018941BC3A47C2D0015389,
	AudioCategory__GetIndexOf_m1A2778A20F29C711D8820605D2C67FC623C2C35E,
	AudioCategory__ApplyVolumeChange_m2A08B851ACC5F36C16F6D7B67CC12406CE2D4CA0,
	AudioCategory__IsCategoryParentOf_m44A9237AF051A69D1062C6A1AEE7ACAF12889352,
	AudioCategory_UnloadAllAudioClips_m1FE49636FC4847BCF50D007F2635A2C71C77BADE,
	AudioCategory_FadeIn_m9DD20FE7FCB619D5AA9F639D6FB851549F390299,
	AudioCategory_FadeOut_m8C893182E0D3282A78CB52EA6DE9435185B8E8C2,
	AudioCategory__UpdateFadeTime_m1EF8D36707DB097DC3931E235EC1A0FE76C7AC5E,
	AudioCategory_get_isFadingIn_mD93546C7382B6E96963BFF5CD7C31C3EAB3937FB,
	AudioCategory_get_isFadingOut_m7BDCA66FC9515612F4762917C1BA3C0E744D0832,
	AudioCategory_get_isFadeOutComplete_m960B7A2D90B082F2F48A8F2CC47A203CC33ADA24,
	AudioItem__ctor_m53B4EA9AC820237462FC7ED15AC7FD3A31D4783E,
	AudioItem__ctor_m003AA826621C3BDF145C60CDDD9FF8795CA784B3,
	AudioItem_set_category_mC727EC6722D1C8CB50CB635618189C656DF4C89C,
	AudioItem_get_category_mDFB7E282B09E3B3E75244E2FDAC4EBED8DA6B2F0,
	AudioItem_Awake_mDA85D7085E0BBD33A742855D162827E90593A71D,
	AudioItem_ResetSequence_m2EC583AE93964000395A391E1D3BCEEA73F69CA8,
	AudioItem__Initialize_m5683A3DC8F4AAA8CDA3664B61FEBA416506D19D5,
	AudioItem__NormalizeSubItems_m61C64BE2457BD704A84E0E7566A27280E172F3E0,
	AudioItem__IsValidSubItem_m6727D729A1212B026123B2D13E96B5A615E5464B,
	AudioItem_UnloadAudioClip_m6BFF649E48BA90D95DF824D2D1C60F2E429D7616,
	AudioSubItem__ctor_m5A243AE0823DE203820475C6EFCA9654970108D1,
	AudioSubItem__ctor_mEB4CCB2C415CC3B72EE7DAC09E3E39A31EAE9A84,
	AudioSubItem_get__SummedProbability_m218D747FAA7AAD8A3973570C411B37D9E7A7706F,
	AudioSubItem_set__SummedProbability_m102626FAA52C6E4D321622E5C9114F0A42790E16,
	AudioSubItem_set_item_m2E30DF18D3BA7BB1776AFA92F4A66C757394E4EA,
	AudioSubItem_get_item_m5461B0B361B262EA52774F61D5A5A7E7D9D43C05,
	AudioSubItem_ToString_mF3CFD29B567C5EE5A632C6A971190796625D07BF,
	Playlist__ctor_m6B40839B5C6EA4FD399238240DFF40B68DC3A982,
	Playlist__ctor_mD13C4D2134A049A0AB12FD40835DDBC07DBDEBB3,
	AudioObject_get_audioID_mFBC4A7E729F3B0CB77FC2411DD2774F6839CD592,
	AudioObject_set_audioID_m9EA3164B2A3F472520B11EF9812615C86D0251F2,
	AudioObject_get_category_m10BDAF4EF25E6A1EE421F275FF44B6E7BC0BECA2,
	AudioObject_set_category_m6FB880B500E5587C64161430865BADB98E0BC4F8,
	AudioObject_get_subItem_m45A6AECEA289218E6AA1C565E9FC8EB26B367C8A,
	AudioObject_set_subItem_mD6485FC017AF1C4B06086DB17F1CBD1C60CA4085,
	AudioObject_get_channel_m214B4E811541BA90C353248847731FCB43E9F5F5,
	AudioObject_set_channel_mCE9D801261758EA7989A03806056DCA204184FA1,
	AudioObject_get_audioItem_m25B4798545843CC3422DB66A26692C40AD480C7D,
	AudioObject_set_completelyPlayedDelegate_mA54E8983257AC9B3E7A139B8FC49894A17EBDD26,
	AudioObject_get_completelyPlayedDelegate_mE3806650C07E9F3B0CF901A0BFEC6634456DAA8E,
	AudioObject_get_volume_mD5882AB345159F843725D75AF25606D75FF7122A,
	AudioObject_set_volume_m5CD3D2E1F4999FFAC4EBE3EA77999A8ACE83E71B,
	AudioObject_get_volumeItem_m35940C69305EBC6000F06DD53D1692730D3FF07F,
	AudioObject_set_volumeItem_m09706B271378994B7CB247CB94E40E31C45A51B0,
	AudioObject_get_volumeTotal_mB261EED7028FF2986AC6FCD2867D06E90DFC6160,
	AudioObject_get_volumeTotalWithoutFade_mFE0130933F43DD21DF310F55A0D47B3B2BD13D88,
	AudioObject_get_playCalledAtTime_mABB72A69B9A3576D835DA29FED78B52F3653D7F3,
	AudioObject_get_startedPlayingAtTime_mCC056F1F38B7852D6C67D0932041AAE9C7962EF0,
	AudioObject_get_timeUntilEnd_m9CC04AFA8C986B2C884B6729CE9E32B8A25ABBA6,
	AudioObject_get_scheduledPlayingAtDspTime_m5A71E79F91DDB6A40F2C2E2DDBF958AC3784CB9C,
	AudioObject_set_scheduledPlayingAtDspTime_m19C9322CBB818A1B6CEB5092105EE324DC45569C,
	AudioObject_get_clipLength_m464EA8E9CECEF04CECDB7BA8C47E0745DF9957F2,
	AudioObject_get_audioTime_m5A6D4E5F320856CA5A32C5C1EB3B0AB13A4E54A6,
	AudioObject_set_audioTime_mD11E1A918442ACA14CD25AAC761256315895D382,
	AudioObject_get_isFadingOut_m0EE34F33166E7B47CCD0466B9FDC94B51A12B05A,
	AudioObject_get_isFadeOutComplete_m72995D0510B0D8EC24D1566C903C526D145F578E,
	AudioObject_get_isFadingOutOrScheduled_mC7ED0F40FB8542C440D2653D49050113619797B7,
	AudioObject_get_isFadingIn_m978D5E9671955FACC7A9A7DAB1156C6FF0174310,
	AudioObject_get_pitch_m66A2EC1C50519C3C98578C8F5C473B92DBBFD7BE,
	AudioObject_set_pitch_m754A7894CD72B1440F26D56172DC4E44D8EAA839,
	AudioObject_get_pan_mF94614A65E763B5517E6AE9C856B9023381288A9,
	AudioObject_set_pan_m1CE125BF5F4865947ED00EF5DCAEEB4F58056E60,
	AudioObject_get_audioObjectTime_mF3EB625AB53543CD2AE6CF787A9CD05D371DF23D,
	AudioObject_get_stopAfterFadeOut_m74F028C287C5DD0934523BEFF9F405E5FD32A854,
	AudioObject_set_stopAfterFadeOut_mB780C0D035532C99518DCA1DB6D7AACE190005B0,
	AudioObject_FadeIn_m1E3A0C0CC1F554CFD276D5F141188F381D88C912,
	AudioObject_PlayScheduled_m5CEE90BFD89D4856380BC32DD495B7A6A4C2D8C8,
	AudioObject_PlayAfter_m639B1539850A31AB79EC5FBD143BCC8BCF5DCC73,
	AudioObject_PlayNow_m5D3356B046421790312F015A5527C700D985E6D0,
	AudioObject_Play_mA3CD358101CE9F3C7482883266D15BAF1BBCC7FA,
	AudioObject_Stop_m05E92AEBFEED7FDD863CBEEB189B99CA7BFC6636,
	AudioObject_Stop_m6BB671D8362BC2156ADDCEFAB4B7C4E1BA9DDA11,
	AudioObject_Stop_mEE44F331C9123517EE4FF57353417D11BCAA3123,
	AudioObject_FinishSequence_m48CD494906842A309738F86D103958A0A1464B0E,
	AudioObject__WaitForSecondsThenStop_m8ADAB0DE94E2F0AF56FEBDBEDC512253B93F19D0,
	AudioObject_FadeOut_mF657F05390CBDB1026964B363149BD9ECFDAE15E,
	AudioObject_FadeOut_m96292A89F534B0A920F4CA7DC26097C63208E726,
	AudioObject_Pause_m2E71E3CFBB6713F544539B966FF0759535D9D2FA,
	AudioObject_Pause_m60ED56AD83795BB69A0DA9B015A874415C7B8727,
	AudioObject__PauseNow_mE056DC0B4B0C6A4852D6AFDA61D3326542BC4049,
	AudioObject_Unpause_m37924F824AC43840A0FDC5ACAEA3E7A8A6893F54,
	AudioObject_Unpause_mA0F27F389514E0643B14140208FBF48AD84DEB08,
	AudioObject__UnpauseNow_mA6F6C307CFFA8C3F0FDD94AC748E2CFEB61A8EF9,
	AudioObject__WaitThenPause_m2917D16A30ED1508B591713865BAABD7490AC041,
	AudioObject__PauseAudioSources_m1E26BCF5ABA372D4CD71A38563ED630DDB0257D7,
	AudioObject_IsPaused_mD12935FC4707F89AD2C3247D84F83363D6672585,
	AudioObject_IsPlaying_m0B9791708F69B24F952A25620D0BE94B623057DC,
	AudioObject_IsPrimaryPlaying_mAD10E40EA66EA76543717AE4FB99F020DD6C042D,
	AudioObject_IsSecondaryPlaying_m71812DAD40B61776F37031AFA0E1AF5A0B9DE236,
	AudioObject_get_primaryAudioSource_m564625EA9319DB106D6A3750396F70B5881C7729,
	AudioObject_get_secondaryAudioSource_m6F50088F03A9F38D2A43D74104458080AE9C853A,
	AudioObject_SwitchAudioSources_m10CD2CC122F98152732F378123029C51FC78FBD1,
	NULL,
	AudioObject_get__volumeFromCategory_m1725991C6C137E2227EA56E50731D6D60677D72F,
	AudioObject_get__volumeWithCategory_mCCFF1225F23AC1184CA1033FED08D6E59B6A4782,
	AudioObject_get__stopClipAtTime_mBCD54A732DBFE50E636478E20A0CCE2857051DE0,
	AudioObject_get__startClipAtTime_mBB1416F98027BF56ECBA41958FB25363A8181DAC,
	AudioObject_Awake_m490330743E605720C3877280AEB966DF25F8787B,
	AudioObject_OnEnable_mD4FFE6C28310649786955C34860800FA83D8F870,
	AudioObject_OnDisable_m6406D78163C7A795834D9AC970286ABB51A15336,
	AudioObject__CreateSecondAudioSource_m49F137313C2278123B7D04E3D8F59C570066FC6B,
	AudioObject__Set0_m4288010E694C41E42DB042BCEF5E3CC0AE0A3ADA,
	AudioObject__SetReferences0_m849BE5264ECA98F43EE8C00480B6BB15312E0766,
	AudioObject__PlayScheduled_mF337C35BF233018481AB0F8F25A68F62F856104D,
	AudioObject__PlayDelayed_m477DB5F0EBC503261C63F64D19037F488C6483B6,
	AudioObject__OnPlay_mA74404657B8D2939E0292603BBE07C93D2F278B9,
	AudioObject__Stop_mDF1AE2A5A7F72266B94AA419F243F2C758DB7DEB,
	AudioObject_Update_mA46A8360887367B8A67178AD3BFBE277F7C9FD1B,
	AudioObject__StartFadeOutIfNecessary_mF3670F52B5374045E174C32C75C9B3E8F4689E47,
	AudioObject__IsAudioLoopSequenceMode_mF7F606A1607AA7265D650B19DCD58F378B805836,
	AudioObject__ScheduleNextInLoopSequence_mC051E5A7C35D3E11F8B66FDE86F13CBF7B97137B,
	AudioObject__UpdateFadeVolume_m1FB38B793417C6C5B708292EFD10E886BDE7A22B,
	AudioObject__EqualizePowerForCrossfading_m1AE2A2C01C5DF0DFC974C8F5B66AED2CAD0F2144,
	AudioObject_get__shouldStopIfPrimaryFadedOut_mB9419D23CA2C725BDCCAF415FD2B2FE20CFFB190,
	AudioObject_OnApplicationPause_m43FEEFD6F02D11A48366719D0056EB0B4F4F62BA,
	AudioObject_SetApplicationPaused_mEE6AC1D3A7A671E24119DE71A62E82CED93A212A,
	AudioObject_DestroyAudioObject_mE2D2BC83118A64F6520D5BF06AF76F7232DB90D3,
	AudioObject_TransformVolume_mB17969D18761D589E0EA39CEA1667A66419AC2A3,
	AudioObject_InverseTransformVolume_mDBA1A47B85A8BE0F464EB0EA9A00BB8B39A818D0,
	AudioObject_TransformPitch_mD19D62731D7EF97FD9D98732A72D156CEE925804,
	AudioObject_InverseTransformPitch_m58D69B1FA7D53C63B826E82D3DBFEE42D025BC78,
	AudioObject__ApplyVolumeBoth_m54981F4B75E5A520DAEBB66CE7041CE90CB9B113,
	AudioObject__ApplyVolumePrimary_m7ED395302DB38EB78580B7F9E038C66AD3CE8105,
	AudioObject__ApplyVolumeSecondary_m14CD474788BB158D9A33B1DCF5CBF27D86C0734C,
	AudioObject_OnDestroy_mBE259D6F2215D73C97EACA457D47D82D44631046,
	AudioObject__RestoreOverrideAudioSourceSettings_m356B1A25509797DFE9714E51AA1D3478E6DE7D86,
	AudioObject_DoesBelongToCategory_m2346104FC9506830BBB3FDA1A1D08E4C707451A7,
	AudioObject__GetRandomLoopSequenceDelay_m23AFB2B78131DBCB7237EB8DE0D89FF0BF8AFD13,
	AudioObject__ctor_m8A85B3BE9F77240BEC1A0FBC8EC2F051A2966730,
	AudioEventDelegate__ctor_mCFA5F0C7161978836E1723F84744CCA6E842FBBE,
	AudioEventDelegate_Invoke_m41886D3386CBDA6C9DDFD8AF46BDFFDAE26E929D,
	AudioEventDelegate_BeginInvoke_mCEA1A7C2C9608726679A9B2470486477ECECD42A,
	AudioEventDelegate_EndInvoke_mB143BA6BFA8925442CF110F1C8318DFF58F3AC26,
	U3C_WaitForSecondsThenStopU3Ed__76__ctor_m5DDA60EBC326D1D3FFC858296CCF98B92FF225D5,
	U3C_WaitForSecondsThenStopU3Ed__76_System_IDisposable_Dispose_m3191FDCBB9064FFABA539AB7B8D1E150315D1AF8,
	U3C_WaitForSecondsThenStopU3Ed__76_MoveNext_m186DEE9896F64DC79F5431DF69E7184B66A7170B,
	U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m141F40A314ECCF21E980FBF5C6436206E5235FB7,
	U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_IEnumerator_Reset_mF72B523E7302A6CA5B499EF811A5EBDC0896175D,
	U3C_WaitForSecondsThenStopU3Ed__76_System_Collections_IEnumerator_get_Current_m69DCFD72C63E8A4FA55F774BB70B3070546B9F0C,
	U3C_WaitThenPauseU3Ed__86__ctor_m1BDA461457E882576F04CE05AE8791044FDD4538,
	U3C_WaitThenPauseU3Ed__86_System_IDisposable_Dispose_m46511038D58AEE9F4EBD59090A30B47F189ADAC8,
	U3C_WaitThenPauseU3Ed__86_MoveNext_m165C02EF6D004761CBE72373D5C2EF24A3097A98,
	U3C_WaitThenPauseU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8EAF2B62B42191EB3F29F00017B846814E85CA1,
	U3C_WaitThenPauseU3Ed__86_System_Collections_IEnumerator_Reset_m932FE61892C90802C6A781726A614C20CD18A5E8,
	U3C_WaitThenPauseU3Ed__86_System_Collections_IEnumerator_get_Current_mF34D9ED7B8BBE87E947B980C2DB587D83D495784,
	PoolableExtensions__SetActive_m847F499D5C2D8A635CB92FE3B3F6B2D5BBBF2089,
	PoolableExtensions__GetActive_m6ED3F5CE7259AC54A6ADAE60CE4739FF512AB76D,
	PoolableObject_Awake_m44149E9D3922BBFC21E8BD554B4AFF6602C8D3EF,
	PoolableObject_OnDestroy_m0ED81C9FB8C4DBD4E026D4853F62B627BA1F377A,
	PoolableObject_GetSerialNumber_m546018BEB42B14620C0699D8ACB831EE8D2777F6,
	PoolableObject_GetUsageCount_m8F8EE9806CA4EE74AE2A60A11E38006CF32CF4F2,
	PoolableObject_DeactivateAllPoolableObjectsOfMyKind_m7FB8BF1B07082590A268F21EE23B0B5E089894E8,
	PoolableObject_IsDeactivated_mBCC08C6CD29CD2ACE1C2C2183FD0D43BC6A6275D,
	PoolableObject__PutIntoPool_mF67B54ED9B1BA28AC04072648493C3C1BC5AC52B,
	PoolableObject_TakeFromPool_m6909B37433E7E6441895E0DB31595702F6BE16F8,
	PoolableObject__ctor_m5601824890188168CA58128F242D0E9B84AF993E,
	ObjectPoolController_get_isDuringPreload_mAB45F36DB7CE07912C90E2260A415D7C2C2F5259,
	ObjectPoolController_set_isDuringPreload_m179ACF20B6A9C1013B7B8D8CA87E7E7E2E0A0F4A,
	ObjectPoolController_Instantiate_mCCFC8F65563922BC6006E64CCF134497FF7640B5,
	ObjectPoolController_Instantiate_m8A9C4BBCDF7918B312B9FD4963D99F25AD2CD1B0,
	ObjectPoolController_InstantiateWithoutPool_mE016A934F7E01FFCEFF7F2A7B83CDC3AFADAAB43,
	ObjectPoolController_InstantiateWithoutPool_mE340CA6B4F594FAFE428ECD795DFC6894200CA8C,
	ObjectPoolController__InstantiateGameObject_mB106642C3439E343D6AD3D13CD140E1686E7F499,
	ObjectPoolController_Destroy_mE579761AFDCA8A1CB45ED4D355DD5E80FCD128E3,
	ObjectPoolController_DestroyImmediate_mB44AEA643F3D5DBA8AF3B607A45A08B567E75D45,
	ObjectPoolController_Preload_mDA376DA4A680AF4BF6CE8BBA45A0BE80E7D1B2D0,
	ObjectPoolController__GetPool_mB45AD133527844543939CC7413734D0213F7730C,
	ObjectPoolController__DetachChildrenAndDestroy_mC40DB44849951A8B8D9DE407140DF528867C74A7,
	ObjectPoolController__cctor_m782292556A296FD7DEE15327035D5F9DBE7D79D2,
	ObjectPool_get_poolParent_mD8667A9BA2E76B2E3B491FF27E91D3AC9EC05A36,
	ObjectPool__ctor_m827D8E689D9C5D4188521CA1FE31B1D84F897928,
	ObjectPool__ValidatePooledObjectDataContainer_mF7B5D00D7B61788222BEFD64CFE7A198F06160F2,
	ObjectPool__ValidatePoolParentDummy_m02637E13866A0C606C142AE0B875D3C01249495A,
	ObjectPool_Remove_m2453AE200459FE7CD29A3818837B2309ED72CDBE,
	ObjectPool_GetObjectCount_m7DE43D40FF05066E0D7EF1EBDDB39664B37D3204,
	ObjectPool_GetPooledInstance_m0535CC67B77BA9BCFEAF561A719A6E79BC4D8379,
	ObjectPool_PreloadInstance_m899A7277128D4EACF66738EC825580E9A6BBEBAF,
	ObjectPool__NewPooledInstance_m8371E8E63F03BADBB2E383C0D1699CE09F245A82,
	ObjectPool__SetAllAvailable_mF49B907C15D7586E947F9B03C15CADCF62AFB58D,
	ObjectPool_CallMethodOnObject_m31352F689F489E3FF4E48F8251BBA89290547009,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RegisteredComponent_Awake_mAA516B5C89747B659CCCDE1824881E2836E20B0F,
	RegisteredComponent_OnDestroy_mFD66FB92672D6A0DE4E95A3771062155C16A43E2,
	RegisteredComponent_GetRegisteredComponentBaseClassType_m4888CBFF4EA46982DF38E8F15535219DBE4BCCE5,
	RegisteredComponent__ctor_mBEFF099BC32076D081C75A03724BE95FF1CD3D3E,
	NULL,
	RegisteredComponentController_GetAllOfType_m827B78DEB880BAA0E87ED44443CDE44B95568080,
	NULL,
	RegisteredComponentController__GetInstanceContainer_m4AE656B6C136FBF3CE54A69AA8AAA6BA0875EFAD,
	RegisteredComponentController__RegisterType_mF38D17FC3438046ADBA4C4DA080BEBB5DDB9C5FD,
	RegisteredComponentController__Register_m24751ECE64B7C7CAF4900008991DC5E7DF0F50E3,
	RegisteredComponentController__UnregisterType_m0DA609F934E164E53D8C5338CF033E65DA0F454D,
	RegisteredComponentController__Unregister_mB2C66F62730FDE21B28BC81B79B880ACC530E0FA,
	RegisteredComponentController__cctor_m88749DD8B506005BD2805970E94756F97BA30DA1,
	InstanceContainer__ctor_m2C5A4BD2C07BD2466C94781477FF462315867B3B,
	SystemTime_get_time_mB1B5E5B9244E38D7D6C33064583F0E557F5DEEF1,
	SystemTime_get_timeSinceLaunch_mA158534813497E7B9131AE511A30484CAA6D4948,
	SystemTime__cctor_m3F778ED66B232FDD891BAE1BCC49C362A22686D6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[427] = 
{
	2585,
	3026,
	3026,
	2585,
	3029,
	2588,
	3026,
	2585,
	3026,
	2585,
	3026,
	2585,
	3029,
	2588,
	3029,
	2588,
	3029,
	2588,
	3029,
	2588,
	4778,
	4778,
	4745,
	4788,
	4745,
	4788,
	3655,
	3253,
	3413,
	4797,
	4632,
	4632,
	4797,
	4632,
	3655,
	3253,
	3413,
	4797,
	4632,
	4632,
	4797,
	4632,
	4524,
	3002,
	2029,
	4601,
	4629,
	4601,
	4788,
	4788,
	4797,
	4807,
	4393,
	4601,
	3655,
	4208,
	3413,
	3920,
	3253,
	3236,
	3392,
	4279,
	4629,
	4752,
	4807,
	4752,
	4752,
	4399,
	4399,
	4399,
	4629,
	4209,
	4209,
	4606,
	4144,
	4749,
	4749,
	4749,
	4797,
	4797,
	4797,
	4788,
	4788,
	4788,
	4601,
	4399,
	4647,
	4055,
	4054,
	4752,
	4800,
	4601,
	4745,
	4393,
	3907,
	4629,
	4629,
	4601,
	4745,
	4647,
	3068,
	4745,
	4788,
	4745,
	4788,
	3068,
	2029,
	526,
	526,
	2264,
	2264,
	2264,
	2264,
	194,
	194,
	1887,
	3002,
	812,
	2033,
	2033,
	3068,
	2985,
	2985,
	2985,
	3068,
	2985,
	2985,
	2985,
	19,
	21,
	2029,
	3068,
	4807,
	3068,
	3068,
	3068,
	3026,
	3068,
	3068,
	3068,
	3068,
	3068,
	2563,
	2563,
	1567,
	2563,
	4601,
	21,
	2029,
	2563,
	2563,
	3068,
	3068,
	3068,
	3068,
	4807,
	3068,
	-1,
	-1,
	-1,
	4047,
	4393,
	3778,
	4048,
	3778,
	4048,
	3778,
	4048,
	3778,
	4048,
	3778,
	3778,
	4807,
	4208,
	3900,
	4601,
	3900,
	3537,
	4626,
	2966,
	2528,
	3026,
	3026,
	3026,
	3026,
	3068,
	1587,
	1060,
	1588,
	3029,
	2346,
	3029,
	3029,
	1319,
	3068,
	3029,
	2588,
	3029,
	2563,
	3002,
	3002,
	3002,
	2563,
	2563,
	3002,
	3002,
	2563,
	1887,
	3068,
	1275,
	3068,
	1587,
	1588,
	3068,
	3026,
	3026,
	3026,
	3068,
	2563,
	2563,
	3002,
	3068,
	3068,
	2563,
	3068,
	4629,
	3068,
	3068,
	1567,
	3029,
	2588,
	2563,
	3002,
	3002,
	3068,
	1567,
	3002,
	2563,
	3002,
	2563,
	3002,
	2563,
	2985,
	2548,
	3002,
	2563,
	3002,
	3029,
	2588,
	3029,
	2588,
	3029,
	3029,
	2966,
	2966,
	3029,
	2966,
	2528,
	3029,
	3029,
	2588,
	3026,
	3026,
	3026,
	3026,
	3029,
	2588,
	3029,
	2588,
	2966,
	3026,
	2585,
	2588,
	2528,
	641,
	686,
	2588,
	3068,
	2588,
	1588,
	3068,
	1206,
	2588,
	1588,
	3068,
	2588,
	3068,
	3068,
	2588,
	3068,
	1203,
	3068,
	2260,
	3026,
	3026,
	3026,
	3002,
	3002,
	3068,
	-1,
	3029,
	3029,
	3029,
	3029,
	3068,
	3068,
	3068,
	3068,
	3068,
	3068,
	2528,
	2588,
	3068,
	3068,
	3068,
	3068,
	3026,
	3026,
	3068,
	2351,
	3026,
	2585,
	2585,
	3068,
	4649,
	4649,
	4649,
	4649,
	3068,
	2588,
	2588,
	3068,
	3068,
	2239,
	2349,
	3068,
	1566,
	2563,
	832,
	2563,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	2548,
	3068,
	3026,
	3002,
	3068,
	3002,
	4397,
	4629,
	3068,
	3068,
	2985,
	2985,
	2985,
	3026,
	3068,
	1569,
	3068,
	4797,
	4749,
	4208,
	3662,
	4208,
	3662,
	3662,
	4745,
	4745,
	4745,
	4601,
	4397,
	4807,
	3002,
	2563,
	3068,
	3068,
	2563,
	2985,
	475,
	2031,
	476,
	2985,
	377,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3002,
	3068,
	3068,
	3002,
	3068,
	-1,
	4601,
	-1,
	4601,
	4393,
	4745,
	4393,
	4745,
	4807,
	3068,
	4778,
	4778,
	4807,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3026,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000018, { 4, 3 } },
	{ 0x0200001E, { 11, 6 } },
	{ 0x02000020, { 17, 6 } },
	{ 0x0600009E, { 0, 2 } },
	{ 0x0600009F, { 2, 1 } },
	{ 0x060000A0, { 3, 1 } },
	{ 0x06000190, { 7, 3 } },
	{ 0x06000192, { 10, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[23] = 
{
	{ (Il2CppRGCTXDataType)3, 20009 },
	{ (Il2CppRGCTXDataType)3, 22498 },
	{ (Il2CppRGCTXDataType)2, 5113 },
	{ (Il2CppRGCTXDataType)2, 5111 },
	{ (Il2CppRGCTXDataType)3, 15391 },
	{ (Il2CppRGCTXDataType)3, 15392 },
	{ (Il2CppRGCTXDataType)2, 627 },
	{ (Il2CppRGCTXDataType)1, 305 },
	{ (Il2CppRGCTXDataType)2, 5147 },
	{ (Il2CppRGCTXDataType)2, 305 },
	{ (Il2CppRGCTXDataType)1, 304 },
	{ (Il2CppRGCTXDataType)2, 4601 },
	{ (Il2CppRGCTXDataType)2, 684 },
	{ (Il2CppRGCTXDataType)3, 23539 },
	{ (Il2CppRGCTXDataType)3, 23536 },
	{ (Il2CppRGCTXDataType)1, 684 },
	{ (Il2CppRGCTXDataType)3, 19294 },
	{ (Il2CppRGCTXDataType)3, 19291 },
	{ (Il2CppRGCTXDataType)2, 4600 },
	{ (Il2CppRGCTXDataType)3, 17687 },
	{ (Il2CppRGCTXDataType)2, 643 },
	{ (Il2CppRGCTXDataType)3, 19292 },
	{ (Il2CppRGCTXDataType)3, 19293 },
};
extern const CustomAttributesCacheGenerator g_AudioToolkit_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AudioToolkit_CodeGenModule;
const Il2CppCodeGenModule g_AudioToolkit_CodeGenModule = 
{
	"AudioToolkit.dll",
	427,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	23,
	s_rgctxValues,
	NULL,
	g_AudioToolkit_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
